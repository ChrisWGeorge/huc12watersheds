
# Installing and running MakeHAWQS
## Introduction
*MakeHAWQS* is a python script *makeHAWQS.py* and a Windows batch file *runMakeHAWQS.bat* to create vector and raster maps for HAWQS V2 projects.  This document explains how to install a suitable environment for all users and how individual users can use *MakeHAWQS*.
## Installation
Installation for all users as described here will need to be done by a user with admin privileges.
### Python
A Python 3 installation is required, and can most easily be achieved by downloading from [python.org](https://www.python.org/downloads/windows/) the *Windows installer (64-bit)* (64 bit is natural but not essential).  Choose the latest *Stable Release*.  Run the installer *as administrator*, selecting the option to install *for all users*, and the options to include *tcl/tk and IDLE* and to include *pip*.  We assume below the install will be to the directory C:/Program Files/Python310, but any generally accessible directory could be used.  If a different directory is chosen the final line of *runMakeHAWQS.bat* will need changing to provide the correct path to the Python executable *python.exe*.
### GDAL
The Python installation supplies most of what is necessary, but GDAL needs to be added.  This is easily done following the advice at [How to Install GDAL for Python with pip on Windows](https://opensourceoptions.com/blog/how-to-install-gdal-for-python-with-pip-on-windows/) by downloading a pre-built GDAL wheel file from [Christoph Gohlke�s website](https://www.lfd.uci.edu/~gohlke/pythonlibs/#gdal).  Choose the one matching the Python version, and for 64 bits, e.g *GDAL-3.3.3-cp310-cp310-win_amd64.whl* for Python 3.10.

To install GDAL, run a command shell as administrator, cd to the Python directory, e.g. C:/Program Files/Python310 (it will contain *python.exe*) and use the command 
`python -m pip install <path_to_whl_file>` and you should see something like

```
C:\Program Files\Python310>python -m pip install C:/Users/Chris/Downloads/GDAL-3.3.3-cp310-cp310-win_amd64.whl
Processing c:\users\chris\downloads\gdal-3.3.3-cp310-cp310-win_amd64.whl
Installing collected packages: GDAL
Successfully installed GDAL-3.3.3
```
### MakeHAWQS
Copy *makeHAWQS.py* and *runMakeHAWQS.bat* to somewhere generally accessible to users.  They must be placed in the same directory.

*makeHAWQS.py* will need editing to match the location of the files it needs to access.  Around line 27 can be found lines defining *MergedModelsDir* and *DataDir*, something like the following:

```
    MergedModelsDir = 'G:/HUCModels'
    DataDir = 'I:/Data'
```

The texts on the right of the equals signs need to be edited so that:
1.  An existing merged model is to be found as either *`<MergedModelsDir>/SWAT/Fields_CDL/HUCNN/MergedRR`* where NN is the scale (8, 	10, 12 or 14) and RR is the region (01, 02, ... or 18) or *`<MergedModelsDir>RR/SWAT/Fields_CDL/HUCNN/MergedRR`*.  The second form 	will be used if the directory *`<MergedModelsDir>RR`* exists.
2.  *DataDir/DEM* contains files *DEM_30m_RR.tif*
3.  *DataDir/landuse* contains directories *Region1*, *Region2*, ..., *Region18*, *Region1* containing 	*NCLD_NWI_Fields_CDL_30m_01.tif*, *Region2* containing *NCLD_NWI_Fields_CDL_30m_02.tif*, etc.
4.  *DataDir/soil_lkey* contains files *Soils_30m_RR.tif*.
5.  *DataDir/HAWQSProjectFile.qgs* exists.  This is a template QGIS project file used to optionally display the generated HAWQS maps.

## Usage

Ordinary users can use the tool to create HAWQS shapefiles and rasters provided they can create (if necessary) and write to the selected output directory.  They also need to be able to read files in *MergedModelsDir* and *DataDir*.

To create a HAWQS model from an existing merged model, start a command shell, if necessary cd to the directory containing *runMakeHAWQS.bat* and *makeHAWQS.py*, and use a command of the form

`runMakeHAWQS.bat NN fromToFile.csv outputDir projectName type`

where NN is the scale (8, 10, 12 or 14), fromToFile.csv is a csv file sumilar in structure to the one listed below, outputDir is a directory where the HAWQS output will be placed, and projectName is a name for the HAWQS project.  type is an optional parameter. If type is sqlite, then the project created will use sqlite project and reference databases.  If type is anything else, or is absent, then Access databases will be used.  outputDir and the appropriate subdirectories will be created if necessary.  The expected command shell output is 

```
Creating HAWQSNN type project projectName using fromToFile in outputDir
Watershed shapefile created
Channels shapefile created
Points shapefile created
DEM raster added
soil raster added
landuse raster added
Done
```

At the end (before Done is output) there will be an invitation to open QGIS on the HAWQS files.

### Example fromToFile

This example would be suitable for making a HAWQS12 model in region 01.

```
From_SWAT_ID,From_Subbasin,To_SWAT_ID,To_Subbasin
1,010100020101,5,010100020105
2,010100020102,5,010100020105
3,010100020103,5,010100020105
4,010100020104,5,010100020105
5,010100020105,9,010100020204
6,010100020201,9,010100020204
7,010100020202,9,010100020204
8,010100020203,9,010100020204
9,010100020204,19,010100020403
10,010100020301,12,010100020303
11,010100020302,12,010100020303
12,010100020303,13,010100020304
13,010100020304,17,010100020308
14,010100020305,17,010100020308
15,010100020306,17,010100020308
16,010100020307,17,010100020308
17,010100020308,20,010100020406
18,010100020402,19,010100020403
19,010100020403,20,010100020406
20,010100020406,0,0
```

