# -*- coding: utf-8 -*-
'''
Created on Dec 31, 2021

@author: Chris George
copyright: (C) 2021 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
'''
import sys
import os
import posixpath
import shutil
import csv
import sqlite3
#import tkinter as tk
import re
#from tkinter.messagebox import askyesno
#from tkinter.filedialog import askopenfilename
import subprocess
from osgeo import ogr  # type: ignore
from typing import Dict, List, Set, FrozenSet, Tuple, Optional, Union, Callable, Any, TYPE_CHECKING, KeysView  # @UnusedImport
from qgis.core import QgsApplication, QgsProcessingContext
from qgis.PyQt.QtWidgets import QFileDialog, QMessageBox
#from qgis.PyQt.QtCore import QThread
import processing
from processing.core.Processing import Processing  # type: ignore   # @UnusedImport
#from processing.algs.qgis.QgisAlgorithmProvider import QgisAlgorithmProvider

class Files:
    """Common file locations."""
    
    MergedModelsDir = 'K:/HUCModels'
    DataDir = 'F:/Data'
    demDir = DataDir + '/DEM'
    soilDir = DataDir + '/soil_lkey'
    landuseDir = DataDir + '/landuse'
    huc_reservoirs = posixpath.join(DataDir, '5072/huc_reservoirs.shp')
    lakes_base = posixpath.join(DataDir, '5072/NHDLakes')
    point_sources = posixpath.join(DataDir, '5072/Nut_PBS_Georef_1020.shp')
    projFileTemplate = posixpath.join(DataDir, 'HAWQSProjectFile.qgs')
    projDb = posixpath.join(DataDir, 'QSWATProjHUC.sqlite')  # only used if isSqlite
    SSURGODb = posixpath.join(DataDir, 'SSURGO_Soils_HUC.sqlite')  # only used if isSqlite
    refDb = posixpath.join(DataDir, 'QSWATRef2012.mdb')  # .sqlite used if isSqlite
    QGISBatFile = 'C:/Program Files/QGIS 3.22.12/bin/qgis-ltr.bat'

class Utils:
    """Utility function."""
    
    def __init__(self, scale: str, fromToFile: str, HAWQSModelsDir: str, projName: str, isSqlite: bool) -> None:   

        def makeUs() -> Dict[str, Set[str]]:
            """Create upstream table from csv file, collect all model HUCs, and set region."""
            with open(fromToFile, 'r', newline='') as csvFile:
                reader = csv.reader(csvFile)
                next(reader)
                for line in reader:
                    self.modelHUCs.add(line[1])
                    if line[3] != '0':
                        self.us.setdefault(line[3], set()).add(line[1])
                        self.modelHUCs.add(line[3])
                    else:
                        self.region = line[1][:2]
        
        self.scale = scale
        self.region = ''
        # upstream relation:
        self.us: Dict[str, Set[str]] = dict()
        self.modelHUCs: Set[str] = set()
        makeUs()
        # check if region used for input
        if os.path.isdir(Files.MergedModelsDir + self.region):
            Files.MergedModelsDir = Files.MergedModelsDir + self.region
        self.sourceDir = ''
        self.shapesDir = ''
        self.extent = (0.0,0.0,0.0,0.0)
        self.outModelDir = posixpath.join(HAWQSModelsDir, projName)
        self.projName = projName
        self.projFile = self.outModelDir + '.qgs'
        self.isSqlite = isSqlite
        self.projDb = posixpath.join(self.outModelDir, self.projName + '.sqlite') # only used if isSqlite
        self.reservoirsFile = ''
        self.extraOutletsFile = ''
        
    @staticmethod
    def makeDirs(direc: str) -> None:
        """Make directory dir unless it already exists."""
        if not os.path.exists(direc):
            os.makedirs(direc)
            
    def makeSubDirs(self, direc: str) -> None:
        """Create subdirectories of direc."""
        watershedDir = posixpath.join(direc, 'Watershed')
        Utils.makeDirs(watershedDir)
        self.sourceDir = posixpath.join(direc, 'Source')
        Utils.makeDirs(self.sourceDir)
        soilDir = posixpath.join(self.sourceDir, 'soil')
        Utils.makeDirs(soilDir)
        landuseDir = posixpath.join(self.sourceDir, 'crop')
        Utils.makeDirs(landuseDir)
        scenariosDir = posixpath.join(direc, 'Scenarios')
        Utils.makeDirs(scenariosDir)
        defaultDir = posixpath.join(scenariosDir, 'Default')
        Utils.makeDirs(defaultDir)              
        txtInOutDir = posixpath.join(defaultDir, 'TxtInOut')
        Utils.makeDirs(txtInOutDir)
        tablesInDir = posixpath.join(defaultDir, 'TablesIn')
        Utils.makeDirs(tablesInDir)
        tablesOutDir = posixpath.join(defaultDir, 'TablesOut')
        Utils.makeDirs(tablesOutDir)
        textDir = posixpath.join(watershedDir, 'Text')
        Utils.makeDirs(textDir)
        self.shapesDir = posixpath.join(watershedDir, 'Shapes')
        Utils.makeDirs(self.shapesDir)
    
    def copyShapes(self) -> None:
        """Create HAWQS shapefiles from merged watershed, channels and points shapefiles."""
        
        # no longer used
        #=======================================================================
        # def drainsTo(huc: str, nextHUC: str) -> bool:
        #     """Return True if huc drains to nextHUC via upstream relation us.
        #     Assumes nextHUC in self.upstreamHUCs"""
        #     
        #     if huc in self.upstreamHUCs:
        #         return True
        #     for usHUC in self.us.get(nextHUC, set()):
        #         self.upstreamHUCs.add(usHUC)
        #         if drainsTo(huc, usHUC):
        #             return True
        #     return False
        #=======================================================================
        
        inMergedDir = Files.MergedModelsDir + '/SWAT/Fields_CDL/HUC{0}/Merged{1}'.format(self.scale, self.region)
        if not os.path.isdir(inMergedDir):
            raise ValueError('Cannot find input directory {0}'.format(inMergedDir))
        Utils.makeDirs(self.outModelDir)
        self.makeSubDirs(self.outModelDir)
        inWshedFile = posixpath.join(inMergedDir, 'Watershed/Shapes/demwshed.shp')
        if not os.path.isfile(inWshedFile):
            raise ValueError('Cannot find file {0}'.format(inWshedFile))
        inChannelsFile = posixpath.join(inMergedDir, 'Watershed/Shapes/channels.shp')
        if not os.path.isfile(inChannelsFile):
            raise ValueError('Cannot find file {0}'.format(inChannelsFile))
        inPointsFile = posixpath.join(inMergedDir, 'Watershed/Shapes/points.shp')
        if not os.path.isfile(inPointsFile):
            raise ValueError('Cannot find file {0}'.format(inPointsFile))
        inReservoirsFile = posixpath.join(Files.lakes_base + '{0}.sqlite'.format(self.region))
        if not os.path.isfile(inReservoirsFile):
            raise ValueError('Cannot find file {0}'.format(inReservoirsFile))
        
        # Get the input Layers
        inDriver = ogr.GetDriverByName("ESRI Shapefile")
        sqliteDriver = ogr.GetDriverByName('SQLite')
        inWshedSource = inDriver.Open(inWshedFile, 0)
        inWshedLayer = inWshedSource.GetLayer()
        inChannelsSource = inDriver.Open(inChannelsFile, 0)
        inChannelsLayer = inChannelsSource.GetLayer()
        inPointsSource = inDriver.Open(inPointsFile, 0)
        inPointsLayer = inPointsSource.GetLayer()
        inReservoirsSource = sqliteDriver.Open(inReservoirsFile, 0)
        inReservoirsLayer = inReservoirsSource.GetLayer()
    
        # Create the output Layers
        outDriver = ogr.GetDriverByName("ESRI Shapefile")
    
        # Remove output shapefiles if they already exist
        outWshedFile = posixpath.join(self.shapesDir, 'demwshed.shp')
        if os.path.exists(outWshedFile):
            outDriver.DeleteDataSource(outWshedFile)
        outChannelsFile = posixpath.join(self.shapesDir, 'channels.shp')
        if os.path.exists(outChannelsFile):
            outDriver.DeleteDataSource(outChannelsFile)
        outPointsFile = posixpath.join(self.shapesDir, 'points.shp')
        if os.path.exists(outPointsFile):
            outDriver.DeleteDataSource(outPointsFile)
        resOutletsFile = posixpath.join(self.shapesDir, 'res_outlets.shp')
        if os.path.exists(resOutletsFile):
            outDriver.DeleteDataSource(resOutletsFile)
    
        # Create the output shapefiles
        outWshedSource = outDriver.CreateDataSource(outWshedFile)
        if outWshedSource is None:
            raise ValueError('Cannot create data source for {0}.  Do you have it open?'.format(outWshedFile))
        outChannelsSource = outDriver.CreateDataSource(outChannelsFile)
        if outChannelsSource is None:
            raise ValueError('Cannot create data source for {0}.  Do you have it open?'.format(outChannelsFile))
        outPointsSource = outDriver.CreateDataSource(outPointsFile)
        if outPointsSource is None:
            raise ValueError('Cannot create data source for {0}.  Do you have it open?'.format(outPointsFile))
        outWshedLayer = outWshedSource.CreateLayer('wshed', geom_type=ogr.wkbMultiPolygon )
        outChannelsLayer = outChannelsSource.CreateLayer('channels', geom_type=ogr.wkbMultiLineString )
        outPointsLayer = outPointsSource.CreateLayer('points', geom_type=ogr.wkbPoint )
        # copy .prj files
        inWshedPrj = posixpath.join(inMergedDir, 'Watershed/Shapes/demwshed.prj')
        if os.path.isfile(inWshedPrj):
            shutil.copy(inWshedPrj, self.shapesDir)
        inChannelsPrj = posixpath.join(inMergedDir, 'Watershed/Shapes/channels.prj')
        if os.path.isfile(inChannelsPrj):
            shutil.copy(inChannelsPrj, self.shapesDir)
        inPointsPrj = posixpath.join(inMergedDir, 'Watershed/Shapes/points.prj')
        if os.path.isfile(inPointsPrj):
            shutil.copy(inPointsPrj, self.shapesDir)
    
        # Add input Layer Fields to the output Layers
        inWshedLayerDefn = inWshedLayer.GetLayerDefn()
        for i in range(0, inWshedLayerDefn.GetFieldCount()):
            fieldDefn = inWshedLayerDefn.GetFieldDefn(i)
            outWshedLayer.CreateField(fieldDefn)
        inChannelsLayerDefn = inChannelsLayer.GetLayerDefn()
        for i in range(0, inChannelsLayerDefn.GetFieldCount()):
            fieldDefn = inChannelsLayerDefn.GetFieldDefn(i)
            outChannelsLayer.CreateField(fieldDefn)
        inPointsLayerDefn = inPointsLayer.GetLayerDefn()
        for i in range(0, inPointsLayerDefn.GetFieldCount()):
            fieldDefn = inPointsLayerDefn.GetFieldDefn(i)
            outPointsLayer.CreateField(fieldDefn)
    
        # Get the output Layers' Feature definitions
        outWshedLayerDefn = outWshedLayer.GetLayerDefn()
        outChannelsLayerDefn = outChannelsLayer.GetLayerDefn()
        outPointsLayerDefn = outPointsLayer.GetLayerDefn()
        
        # us now defined from input fromToFile
        #=======================================================================
        # # read channels to create upstream relation
        # # in case called twice
        # self.us = dict()
        # hucStr = 'HUC{0}'.format(self.scale)
        # hucRStr = 'HUC{0}R'.format(self.scale)
        # for channel in inChannelsLayer:
        #     huc = channel.GetField(hucStr)
        #     hucR = channel.GetField(hucRStr)
        #     if hucR is not None:
        #         self.us.setdefault(hucR, set()).add(huc)
        # # prepare to read again
        # inChannelsLayer.ResetReading()
        #=======================================================================
        
        # get maximum point id from channels
        maxPointId = 0
        for channel in inChannelsLayer:
            dsNodeId = int(channel.GetField('DSNODEID'))
            maxPointId = max(maxPointId, dsNodeId)
        # prepare to read again
        inChannelsLayer.ResetReading()
        
        hucStr = 'HUC{0}'.format(self.scale)
        hucRStr = 'HUC{0}R'.format(self.scale)
        # Add features to the output layers
        for inFeature in inWshedLayer:
            huc = inFeature.GetField(hucStr)
            #if not drainsTo(huc, self.outletHUC):
            if huc not in self.modelHUCs:
                continue
            # Create output Feature
            outFeature = ogr.Feature(outWshedLayerDefn)
    
            # Add field values from input Layer
            for i in range(0, outWshedLayerDefn.GetFieldCount()):
                outFeature.SetField(outWshedLayerDefn.GetFieldDefn(i).GetNameRef(),
                    inFeature.GetField(i))
    
            # Set geometry
            geom = inFeature.GetGeometryRef()
            outFeature.SetGeometry(geom.Clone())
            # Add new feature to output Layer
            outWshedLayer.CreateFeature(outFeature)
            outFeature = None
        print('Watershed shapefile created')
        self.extent = outWshedLayer.GetExtent()
        pointsWanted: Set[int] = set()
        extraOutlets: dict[str, int] = dict()
        for inFeature in inChannelsLayer:
            huc = inFeature.GetField(hucStr)
            hucR = inFeature.GetField(hucRStr)
            dsNode = int(inFeature.GetField('DSNODEID'))
            #if not drainsTo(huc, self.outletHUC):
            if huc not in self.modelHUCs:
                continue
            if dsNode > 0:
                pointsWanted.add(dsNode)
            # Create output Feature
            outFeature = ogr.Feature(outChannelsLayerDefn)
    
            # Add field values from input Layer
            for i in range(0, outChannelsLayerDefn.GetFieldCount()):
                outFeature.SetField(outChannelsLayerDefn.GetFieldDefn(i).GetNameRef(),
                    inFeature.GetField(i))
            # if hucR is not in the model and dsNode is -1, need to make it an outlet
            if hucR not in self.modelHUCs and dsNode == -1:
                maxPointId += 1
                outFeature.SetField('DSLINKNO', -1)
                outFeature.SetField('DSNODEID', maxPointId)
                outFeature.SetField('SubbasinR', 0)
                outFeature.SetField(hucRStr, '')
                extraOutlets[huc] = maxPointId
            # Set geometry
            geom = inFeature.GetGeometryRef()
            outFeature.SetGeometry(geom.Clone())
            # Add new feature to output Layer
            outChannelsLayer.CreateFeature(outFeature)
            outFeature = None
        print('Channels shapefile created')
        for inFeature in inPointsLayer:
            # ptId = int(inFeature.GetField('ID'))
            # inlet = int(inFeature.GetField('INLET'))
            water = int(inFeature.GetField('RES'))
            huc = inFeature.GetField(hucStr)
            if water != 0 or huc == '':  # TODO: include HUC14_12 in water points so can use them
                continue
            # check HUC14_12 is within model
            #if not drainsTo(huc, self.outletHUC):
            if huc not in self.modelHUCs:
                continue
            # Create output Feature
            outFeature = ogr.Feature(outPointsLayerDefn)
    
            # Add field values from input Layer
            for i in range(0, outPointsLayerDefn.GetFieldCount()):
                outFeature.SetField(outPointsLayerDefn.GetFieldDefn(i).GetNameRef(),
                    inFeature.GetField(i))
    
            # Set geometry
            geom = inFeature.GetGeometryRef()
            outFeature.SetGeometry(geom.Clone())
            # Add new feature to output Layer
            outPointsLayer.CreateFeature(outFeature)
            outFeature = None
        # add extra outlets
        # use MonitoringPoint table to get geometry of outlet points
        db = inMergedDir + '/HUC{0}_{1}.sqlite'.format(self.scale, self.region)
        sql = "SELECT Xpr, Ypr, Type FROM MonitoringPoint WHERE Type='L' AND HUC14=?"
        #print('Database is {0}'.format(db))
        with sqlite3.connect(db) as conn:
            for huc, pointId in extraOutlets.items():
                row = conn.execute(sql, (huc,)).fetchone()
                if row is None:
                    raise ValueError('Cannot find outlet for {0} in MonitoringPoint table in {1}'.format(huc, db))
                wkt = 'POINT({0} {1})'.format(row[0], row[1])
                pt = ogr.CreateGeometryFromWkt(wkt)
                outFeature = ogr.Feature(outPointsLayerDefn)
                outFeature.SetField('ID', pointId)
                outFeature.SetField('INLET', 0)
                outFeature.SetField('RES', 0)
                outFeature.SetField('PTSOURCE', 0)
                outFeature.SetField('Name', '')
                outFeature.SetField(hucStr, huc)
                outFeature.SetGeometry(pt)
                outPointsLayer.CreateFeature(outFeature)
                outFeature = None
        print('Points shapefile created')
        
        # Save and close DataSources (which writes shapefiles)
        inWshedSource = None
        inChannelsSource = None
        inPointsSource = None
        outWshedSource = None
        outChannelsSource = None
        outPointsSource = None
        
        # copy WaterBodies.sqlite if is sqlite
        if self.isSqlite:
            shutil.copy(inMergedDir + '/WaterBodies.sqlite', self.outModelDir)
    
        
        # add reservoir shapes
        # first extract reservoir outlet points from huc_reservoirs by spatial join with watershed shapefile
        Processing.initialize()
        #if 'qgis' not in [p.id() for p in QgsApplication.processingRegistry().providers()]:
        #    QgsApplication.processingRegistry().addProvider(QgisAlgorithmProvider())
        context = QgsProcessingContext()
        #thread = QThread()
        #context.pushToThread(thread)
        #print('Spatial join on {0} and {1}'.format(Files.huc_reservoirs, outWshedFile))
        result = processing.run("qgis:joinattributesbylocation", 
                   {'DISCARD_NONMATCHING' : True,
                    'INPUT': Files.huc_reservoirs, 
                    'JOIN': outWshedFile,
                    'JOIN_FIELDS': ['Subbasin'], 
                    'METHOD' : 0, 
                    'OUTPUT' : resOutletsFile, 
                    'PREDICATE' : [5], 
                    'PREFIX' : '' }, context=context)
        #print('Processing returned {!s}'.format(result))
        lakeIds = set()
        reservoirCcount = result['JOINED_COUNT']
        extraOutletsFile = posixpath.join(self.shapesDir, 'extrapoints.shp')
        maxReservoirOutletId = 0
        if reservoirCcount > 0:
            if os.path.exists(extraOutletsFile):
                outDriver.DeleteDataSource(extraOutletsFile)
            shutil.copy(inPointsPrj, posixpath.join(self.shapesDir, 'extrapoints.prj'))
            extraOutletsSource = outDriver.CreateDataSource(extraOutletsFile)
            extraOutletsLayer = extraOutletsSource.CreateLayer('extrapoints', geom_type=ogr.wkbPoint)
            for i in range(0, inPointsLayerDefn.GetFieldCount()):
                fieldDefn = inPointsLayerDefn.GetFieldDefn(i)
                extraOutletsLayer.CreateField(fieldDefn)
            resOutletsSource = inDriver.Open(resOutletsFile)
            resOutletsLayer = resOutletsSource.GetLayer()
            extraOutletsLayerDefn = extraOutletsLayer.GetLayerDefn()
            for resOutletFeature in resOutletsLayer:
                try:
                    val = int(resOutletFeature.GetField('l_OBJECTID'))
                    lakeIds.add(val)
                    if val > maxReservoirOutletId:
                        maxReservoirOutletId = val
                    outFeature = ogr.Feature(extraOutletsLayerDefn)
                    outFeature.SetField('ID', val)
                    outFeature.SetField('INLET', 0)
                    outFeature.SetField('RES', 1)
                    outFeature.SetField('PTSOURCE', 0)
                    outFeature.SetField('Name', '')
                    outFeature.SetField(hucStr, '')
                    geom = resOutletFeature.GetGeometryRef()
                    outFeature.SetGeometry(geom.Clone())
                    extraOutletsLayer.CreateFeature(outFeature)
                    outFeature = None
                except:
                    continue
            resOutletsSource = None 
            # now copy lakes to reservoirs.shp
            outReservoirsFile = posixpath.join(self.shapesDir, 'reservoirs.shp')
            if os.path.exists(outReservoirsFile):
                outDriver.DeleteDataSource(outReservoirsFile)
            shutil.copy(inPointsPrj, posixpath.join(self.shapesDir, 'reservoirs.prj'))
            outReservoirsSource = outDriver.CreateDataSource(outReservoirsFile)
            if outReservoirsSource is None:
                raise ValueError('Cannot create data source for {0}.  Do you have it open?'.format(outReservoirsFile))
            outReservoirsLayer = outReservoirsSource.CreateLayer('reservoirs', geom_type=ogr.wkbMultiPolygon )
            inReservoirsLayerDefn = inReservoirsLayer.GetLayerDefn()
            for i in range(0, inReservoirsLayerDefn.GetFieldCount()):
                fieldDefn = inReservoirsLayerDefn.GetFieldDefn(i)
                outReservoirsLayer.CreateField(fieldDefn)
            outReservoirsLayerDefn = outReservoirsLayer.GetLayerDefn()
            for lake in inReservoirsLayer:
                try:
                    lid = int(lake.GetField('objectid'))
                    if lid in lakeIds:
                        # Create output Feature
                        outFeature = ogr.Feature(outReservoirsLayerDefn)
                
                        # Add field values from input Layer
                        for i in range(0, outReservoirsLayerDefn.GetFieldCount()):
                            outFeature.SetField(outReservoirsLayerDefn.GetFieldDefn(i).GetNameRef(),
                                lake.GetField(i))
                
                        # Set geometry
                        geom = lake.GetGeometryRef()
                        outFeature.SetGeometry(geom.Clone())
                        # Add new feature to output Layer
                        outReservoirsLayer.CreateFeature(outFeature)
                        outFeature = None
                except:
                    continue
            print('Reservoirs shapefile {0} created'.format(outReservoirsFile))
            self.reservoirsFile = outReservoirsFile
            outReservoirsSource = None
            extraOutletsSource = None  
            
        # add point sources
        extraPointSourcesFile = posixpath.join(self.shapesDir, 'pointsources.shp')
        result = processing.run("qgis:joinattributesbylocation", 
                       { 'DISCARD_NONMATCHING' : True, 
                        'INPUT' : Files.point_sources, 
                        'JOIN' : outWshedFile, 
                        'JOIN_FIELDS' : ['Subbasin'], 
                        'METHOD' : 0, 
                        'OUTPUT' : extraPointSourcesFile, 
                        'PREDICATE' : [5], 
                        'PREFIX' : '' }, context=context)
        #print('Join returned {0}'.format(result))
        if result['JOINED_COUNT'] > 0:
            if not os.path.isfile(extraOutletsFile):
                shutil.copy(inPointsPrj, posixpath.join(self.shapesDir, 'extrapoints.prj'))
                extraOutletsSource = outDriver.CreateDataSource(extraOutletsFile)
                extraOutletsLayer = extraOutletsSource.CreateLayer('extrapoints', geom_type=ogr.wkbPoint)
                for i in range(0, inPointsLayerDefn.GetFieldCount()):
                    fieldDefn = inPointsLayerDefn.GetFieldDefn(i)
                    extraOutletsLayer.CreateField(fieldDefn)
            else:
                extraOutletsSource = outDriver.Open(extraOutletsFile, 1)
                extraOutletsLayer = extraOutletsSource.GetLayer()
            extraOutletsLayerDefn = extraOutletsLayer.GetLayerDefn()
            extraPointSourcesLayer = inDriver.Open(extraPointSourcesFile, 0)
            lastId = maxReservoirOutletId
            for ptSource in extraPointSourcesLayer:
                outFeature = ogr.Feature(extraOutletsLayerDefn)
                lastId += 1
                outFeature.SetField('ID', lastId)
                outFeature.SetField('INLET', 1)
                outFeature.SetField('RES', 0)
                outFeature.SetField('PTSOURCE', 1)
                outFeature.SetField('Name', ptSource.GetField('Outfall_ID'))
                outFeature.SetField(hucStr, ptSource.GetField('HUC14'))
                geom = ptSource.GetGeometryRef()
                outFeature.SetGeometry(geom.Clone())
                extraOutletsLayer.CreateFeature(outFeature)
                outFeature = None
        if os.path.isfile(extraOutletsFile):
            print('Point sources shapefile {0} created'.format(extraOutletsFile))
            self.extraOutletsFile = extraOutletsFile
            extraOutletsSource = None        
            
    def addRaster(self, base: str):
        """Clip raster map and add to model.  Base is DEM, soil or landuse."""
        if base == 'DEM':
            raster = posixpath.join(Files.demDir, 'DEM_30m_{0}.tif'.format(self.region))
            outRaster = self.outModelDir + '/Source/dem.tif'
        elif base == 'soil':
            raster = posixpath.join(Files.soilDir, 'Soils_30m_{0}.tif'.format(self.region))
            outRaster = self.outModelDir + '/Source/soil/soil.tif'
        else:
            fileName = 'NLCD_NWI_Fields_CDL_30m_{0}.tif'.format(self.region)
            reg = self.region[1:] if self.region.startswith('0') else self.region
            raster = posixpath.join(posixpath.join(Files.landuseDir, 'Region{0}'.format(reg)), fileName)
            outRaster = self.outModelDir + '/Source/crop/landuse.tif'
        if os.path.isfile(outRaster):
            os.remove(outRaster)
        # allow 3 dem pixels (~90m) all round
        xMin = self.extent[0] - 100
        xMax = self.extent[1] + 100
        yMin = self.extent[2] - 100
        yMax = self.extent[3] + 100
        command = 'gdal_translate -q -of GTiff -projwin {0} {1} {2} {3} "{4}" "{5}"' \
                        .format(xMin, yMax, xMax, yMin, raster, outRaster)
        os.system(command)
        if not os.path.isfile(outRaster):
            raise ValueError('Failed to create {0}'.format(outRaster))
        else:
            print('{0} raster added'.format(base))
            
    def makeHillshade(self):
        """Create hillshade file."""
        demFile = posixpath.join(Files.demDir, 'DEM_30m_{0}.tif'.format(self.region))
        hillshadeFile = os.path.split(demFile)[0] + '/hillshade.tif'
        command = 'gdaldem.exe hillshade -compute_edges -z 5 "{0}" "{1}"'.format(demFile, hillshadeFile)
        subprocess.call(command, shell=True)
        
    def makeProjectFile(self):
        """Create QGIS project file from template.
        Also copy refDb, plus SSURGO db and project database if sqlite."""
        if self.isSqlite:
            refDb = re.sub('.mdb$', '.sqlite', Files.refDb)
            shutil.copyfile(Files.projDb, self.projDb)
            shutil.copy(Files.SSURGODb, self.outModelDir)
        else:
            refDb = Files.refDb
        with open(Files.projFileTemplate, 'r') as inFile:
            with open(self.projFile, 'w', newline='') as outFile:
                for line in inFile:
                    line = line.replace('X-project-name-X', self.projName)
                    line = line.replace('<isHAWQS type="bool">false</isHAWQS>', '<isHAWQS type="bool">true</isHAWQS>')
                    line = line.replace('landuse_fields_CDL_NN', 'landuse_fields_CDL_{0}'.format(self.region))
                    if self.isSqlite:
                        line = line.replace('<useSQLite type="bool">false</useSQLite>', '<useSQLite type="bool">true</useSQLite>')
                        line = line.replace('<useSSURGO type="int">0</useSSURGO>', '<useSSURGO type="int">1</useSSURGO>')
                    outFile.write(line)
        shutil.copy(refDb, self.outModelDir)
    
    def startQGIS(self):  
        """Allow user to select QGIS bat file and start QGIS with project file."""
    
        #root = tk.Tk()
        #root.withdraw() 
        app = QgsApplication([], True)
        response = Utils.question('QGIS', 'Open QGIS on the HAWQS files?')
        if response == QMessageBox.Yes:
            if posixpath.isfile(Files.QGISBatFile):
                batFile = Files.QGISBatFile
            else:
                msg = 'Please select QGIS start file, probably something like {0}'.format(Files.QGISBatFile)
                filetypes = "Bat files (*.bat);; All files (*,*)"
                batFile = QFileDialog.getOpenFileName(None, msg, 'C:/', filetypes)
                if batFile is None:
                    return
            command = '"{0}" --project "{1}"'.format(batFile, self.projFile)
            if self.reservoirsFile != "":
                command += ' "{0}"'.format(self.reservoirsFile)
            if self.extraOutletsFile != "":
                command += ' "{0}"'.format(self.extraOutletsFile)
            #print(command)
            subprocess.call(command, shell=True) 
    
    @staticmethod
    def question(title, msg):
        """Ask msg as a question, returning Yes or No."""
        questionBox = QMessageBox()
        questionBox.setWindowTitle(title)
        questionBox.setIcon(QMessageBox.Question)
        questionBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        questionBox.setText(msg)
        result = questionBox.exec_()
        return result 
                

if __name__ == '__main__':
    try:
        if len(sys.argv) == 1:  # for debugging
            sys.argv.append('12')
            sys.argv.append('G:/Users/Public/HUC12Watersheds/subbasins-mapping.csv')
            sys.argv.append('G:/HAWQSModels/SWAT/Fields_CDL/HUC12/Models01')
            sys.argv.append('HAWQSProject')
            #sys.argv.append('sqlite')
            sys.argv.append('')
        scale = sys.argv[1]
        if scale == '' or int(scale) not in {8, 10, 12, 14}:
            raise ValueError('Scale {0} is not 8, 10, 12 or 14'.format(scale))
        fromToFile = sys.argv[2]
        outputDir = sys.argv[3]
        projName = sys.argv[4]
        projType = sys.argv[5].strip()
        isSqlite = projType == 'sqlite'
        print('Creating HAWQS{0} {4} project {3} using fromToFile {1} in {2}'.format(scale, fromToFile, outputDir, projName, projType))
        u = Utils(scale, fromToFile, outputDir, projName, isSqlite)
        u.copyShapes()
        u.addRaster('DEM')
        # too slow
        # u.makeHillshade()
        u.addRaster('soil')
        u.addRaster('landuse')
        u.makeProjectFile()
        u.startQGIS()
        print('Done')
    except Exception as e:
        print('MakeHAWQS encountered an error:')
        print(e)
        
        