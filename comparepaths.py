# -*- coding: utf-8 -*-
'''
Created on 5 October 2021

@author: Chris George
copyright: (C) 2021 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

comparepaths.py 
From two files produced by connections.py list nodes whose paths differ
by a threshold amount (default 10).  Intended to support comparisons between path length files of a region, 
in particular HAWQS V1 versus HAWQS V2
'''

import csv
import sys 

def comparePaths(file1, file2, threshold, outFile=None):
    """Report on differences in path lengths."""
    if outFile is None:
        outStream = sys.stdout
    else:
        outStream = open(outFile, 'w')
    with open(file1, 'r', newline='') as f1, open(file2, 'r', newline='') as f2:
        read1 = csv.reader(f1)
        read2 = csv.reader(f2)
        done = []
        paths1 = dict()
        for node1, l1 in read1:
            paths1[node1] = int(l1)
            done.append(node1)
        for node2, l2 in read2:
            l1 = paths1.get(node2, -1)
            l2 = int(l2)
            if l1 < 0:
                outStream.write('{0} not in {1}\n'.format(node2, file1))
            else:
                done.remove(node2)
                if abs(l1 - l2) > threshold:
                    outStream.write('Path for {0} changed from {1} to {2}\n'.format(node2, l1, l2))
        for node in done:
            outStream.write('{0} not in {1}\n'.format(node, file2))
    if outFile is not None:
        outStream.close()
            
if __name__ == '__main__':
    if len(sys.argv) > 3:
        threshold = int(sys.argv[3])
    else:
        threshold = 10
    comparePaths(sys.argv[1], sys.argv[2], threshold)
