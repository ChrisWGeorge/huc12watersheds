# HUC12Watersheds

The project is Python 3 code for generating SWAT and SWAT+ models for HUC14, HUC12 and HUC10 watersheds in the USA.

## Running

Currently the project is in a rudimentary state, and is run by calling the *__main__* function in *HUC12Models.py*.  Prior to this there are a number of variables to set, all found in the *__main__* function code:

* *selection* is a string that determines what models will be created.  E.g a 10-digit string (provided it is an huc10 reference) will create a single huc12 project, or all the huc14 projects with that string as a prefix.  For creating complete sets of models a two digit selection string, like *01* is intended to be used, which will create all the models for the 01 region, etc.

* *minHRUha* is set to an integer value, the minimum number of hectares in the model HRUs.  When models are run that value is passed to QSWAT3 as the HRU area threshold.

* *withShapes* is set to True if shapefiles (channels, streams and watersheds) are to be created.  Intended use is to allow shapefiles to be created and checking for errors without creating and running projects, by setting *withShapes* True and *withProject* False, and later running the projects without recreating all the shapefiles by setting *withShapes* False and *withProject* and *runProject* True.

* *withProject* is set to True if the project file, project databases, DEM, landuse and soil maps are to be created.  Setting it False speeds processing if the objective is just to create and check the channel and subbasin maps.  If *withProject* is True then projects previously created with the same *selection* are entirely deleted.  If it is False then nothing is deleted and existing shapefiles may be overwritten.

* *runProject* is set to True if the SWAT or SWAT+ projects are to be run: only effective if *withProject* is also True.  The projects will be run up to and including HRU creation.

* *isPlus* is set to True for SWAT+, False for SWAT.

* *is14* is set to True for HUC14 models to be generated.  These have HUC12 watersheds as basins, (merged) catchments as subbasins.

* *is12* is set to True for HUC12 models to be generated.  These have HUC10 watersheds as basins, HUC12 watersheds as subbasins.

* *is10* is set to True for HUC10 models to be generated.  These have HUC8 watersheds as basins, HUC10 watersheds as subbasins.

* *is8* is set to True for HUC8 models to be generated.  These have HUC6 watersheds as basins, HUC8 watersheds as subbasins.

* *isCDL* is set to True for the *Fields_CDL*  landuse data to be used, or False for the *Fields* landuse data to be used.  Only effective if *withProject* is True.

* *runSequence* is set to True if a sequence of models of increasing scale is to be built.  E.g. if *is14* is True and *runSequence* is True then HUC14, HUC12, HUC10 and HUC8 models will be built in that order.  If *is14* is True and *runSequence* is False then only HUC14 will be built.

* *makeOriginals* (HUC14 only) is set to True if copies of the original catchment maps are to be created.  Since these will contain the original comids as polygon ids respectively, which can appear in error messages, these maps can be useful in debugging.  They have the same directory and names as the final maps, but with an extra 0 at the end of the name, e.g. *demwshed0.shp*.

The output projects are placed in folders named first *Models*, then in a folder *SWAT* or *SWATPlus*, then in a folder *Fields_CDL* or *Fields*, then in *HUCnn*, where *nn* is 8, 10, 12 or 14, and then in a folder named by the selection value.  So, for example, collections of projects created by selections *01*, *0105* and *01050001* will all be distinct, and rerunning one of them will not affect the others.  The project folders, and project names, take the form *hucN* where *N* is a 12, 10, 8 or 6 digit number for HUC14, HUC12, HUC10 and HUC8 projects respectively.  Maps *combinedNet.shp* and *combinedWshed.shp* are also created in the collection of projects folder, containing effectively a collection of all the channels and all the subbasins in the projects.  This is aimed at providing a means of visual inspection to look for anomalies.  For HUC14 a shapefile *combinedChannels0.shp* is created, containing all the original flowlines (with minor ones removed for *SWAT* models) plus a spatialite copy of this *combinedChannels0.sqlite* plus, if *makeOriginals* is True, a map *combinedWshed0.shp* collecting all the original catchments.

Generating a collection of HUC12 projects depends on the existence of an equivalent or larger HUC14 collection.  So, for example, HUC12 selection '0101' could be run after running HUC14 selection '01' or HUC14 selection '0101'.  HUC10 requires HUC12 in the same way, and HUC8 requires HUC10.

Note that the locations of the data used and of the output are all hard coded in *HUC12Models.py*, and will need editing before use.

There is a simple batch file *runshed.bat* that will apply Python 3 to *HUC12Models.py*, which assumes the user has 64-bit QGIS 3.10 installed.  The command *runshed.bat 01*, for example, will run with selection *01*.  It will run with whatever parameter settings *HUC12Models.py* has, just setting the *selection* to *01*.

More information is in *Creating HUC12 QSWATPlus models*.

The models are run by QSWAT3, but a number of adaptations have been made in order for this to be possible.  All these adaptations are triggered by a boolean flag *isHUC* which appears a number of time in the QSWAT3 code. Most notable amongst the adaptations are

* the project database is SQLite, not Microsoft Access.  The fields of the tables generated are the same as the Access tables.  Such projects cannot be run with the SWAT Editor, of course, but are processed to generate SWAT input files using a special batch program.

* collections of projects share a common Access reference database placed in the folder containing all the projects, to avoid unnecessary duplication.  

The HUC12Watersheds project is set up as an Eclipse PyDev project and can be run from Eclipse.

## Current Status

* SWAT HUC14, 12, 10 and 8 projects are running.  

* SWAT+ HUC14 needs more work, and HUC12/10/8 have not started.

## Project Database

A project data base *HUC12Watershed.sqlite* provides a number of tables used in generating HUC14 data from NHDV2 data.  These tables are built using a number of python functions:

* *importCatchmentTable* imports *combinedCrosswalk.dbf*
* *importFromToTable* imports *fromtoComids.dbf*
* *importAreasTable* imports *catchmentAreas.dbf*
* *importDsTable* imports *huc12ds.dbf*
* *createFromToMajorTable*  imports *Flowline* table from *Flowline.sqlite*  (not currently used)

More information on the imported source files is in *Creating HUC12 QSWATPlus models*.

These functions hould only need to be run once, and so their invocations at the start of *main* are commented out.

### HUC patches

There are two patch files *catchments_patch.csv* and *fromto_patch.csv* that contain corrections to the *catchments* and *fromto* tables.  Functions *patchCatchmentsTable* and *patchFromToTable* need to be run if these are changed.  It is considered better to run patch files as needed rather than changing the original NHD data, as it makes the changes both transparent and reversible.

## Files required

Once the project database *HUC12Watershed.sqlite* has been established as noted above, the only other inputs needed are spatialite shapefiles *HUC12.sqlite*, *Catchment.sqlite* and *Flowline.sqlite*.  Their generation from NHD data is described in *Creating HUC12 QSWATPlus models*.

## Outputs

As well as the QSWAT projects with their assocated maps and project files, each run generates in the project collection folder

* *problems.txt* containing information about problems encountered

* *oi.csv* showing the relation between the output of one project with the input to another.  Each line takes the form
*ON, OId, IN, IId* where *ON* is the HUC12/10/8 code for a project, *OId* the number of its outlet point, *IN* is the HUC12/10/8 code for a project, and *IId* is the number of an inlet point for that project.

* *fromto.csv* is a reduction of *oi.csv*, ignoring the output and inlet points, and just showing the outlet-inlet relation between projects with lines of the form *ON, IN*.


