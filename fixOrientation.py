# -*- coding: utf-8 -*-
'''
Created on Nov 29, 2019

@author: Chris George
copyright: (C) 2019 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
'''

# This code fixes HUC project databases that were wrong when generated with QSWAT3 versions prior to 1.1.7
# which had the source and outlet points of streams swapped.

from qgis._core import QgsVectorLayer, QgsPointXY, QgsCoordinateTransform, QgsGeometry, QgsCoordinateReferenceSystem, QgsProject, QgsApplication
import glob
import os
import sqlite3
import sys
import traceback

app = QgsApplication([], False)
QgsApplication.setPrefixPath('C:/Program Files/QGIS 3.10/apps/qgis-ltr', True)
QgsApplication.initQgis()

#TopDir = 'E:/Models/SWAT/Fields_CDL'
TopDir = 'D:/Srini/Chris/Models{0}/SWAT/Fields_CDL'

class SubData:
    
    def __init__(self, outletX, outletY, sourceX, sourceY):
        self.slope = 0
        self.minEl = 0
        self.maxEl = 0
        self.sourceCol = 0
        self.sourceRow = 0
        self.outletCol = 0
        self.outletRow = 0
        self.outletX = outletX
        self.outletY = outletY
        self.sourceX = sourceX
        self.sourceY = sourceY

class FixDbs:
    
    def __init__(self, scale, selection):
        self.subData = dict()
        self.polyToSub = dict()
        self.subToPoly = dict()
        self.collectionDir = TopDir.format(selection[:2]) + '/HUC' + '{0}/'.format(scale) + selection
        self.proj = ''
        self.conn = None
        self.crsLatLong = QgsCoordinateReferenceSystem.fromEpsgId(4269)
        self.crsProject = QgsCoordinateReferenceSystem.fromEpsgId(5072)
        
    def run(self):
        if os.path.isdir(self.collectionDir):
            print('Running {0}'.format(self.collectionDir))
            pattern = self.collectionDir + '/huc*.qgs'
            for f in glob.iglob(pattern):
                d = os.path.splitext(f)[0]
                self.proj = os.path.split(d)[1]
                db = d + '/{0}.sqlite'.format(self.proj)
                if not os.path.isfile(db):
                    continue
                self.channelsFile = d + '/Watershed/Shapes/channels.shp'
                self.wshedFile = d + '/Watershed/Shapes/demwshed.shp'
                self.conn = sqlite3.connect(db)  # @UndefinedVariable
                try:
                    self.fixDb()
                    self.conn.commit()
                except Exception:
                    print('Error in fixing {0}: {1}'.format(self.proj, traceback.format_exc()))
            
    def fixDb(self):
        self.readWshed()
        self.readChannels()
        if not self.fixMonitoringPoint():
            print('Skipping {0}'.format(self.proj))
            return
        self.fixReach()
        self.fixWatershed()
        self.fixBasinsData1()
        
    def readWshed(self):
        wshedLayer = QgsVectorLayer(self.wshedFile, 'wshed', 'ogr')
        for polygon in wshedLayer.getFeatures():
            poly = int(polygon[polygon.fieldNameIndex('PolygonId')])
            sub = int(polygon[polygon.fieldNameIndex('Subbasin')])
            self.polyToSub[poly] = sub
            self.subToPoly[sub] = poly
        
    def fixReach(self):
        sql = 'SELECT Subbasin, Slo2, MinEl, MaxEl FROM Reach'
        for row in self.conn.execute(sql):
            data = self.subData[int(row[0])]
            data.slope = 0 - float(row[1])
            data.maxEl = float(row[2])
            data.minEl = float(row[3])
        sql2 = 'UPDATE Reach SET Slo2 = ?, MinEl = ?, MaxEl = ? WHERE Subbasin = ?'
        for subbasin, data in self.subData.items():
            self.conn.execute(sql2, (data.slope, data.minEl, data.maxEl, subbasin))
            
    def fixWatershed(self):
        sql = 'UPDATE Watershed SET ElevMin = ? WHERE Subbasin = ?'
        for sub, data in self.subData.items():
            self.conn.execute(sql, (data.minEl, sub))
            
    def readChannels(self):
        channelsLayer = QgsVectorLayer(self.channelsFile, 'channels', 'ogr')
        for channel in channelsLayer.getFeatures():
            wsno = int(channel[channel.fieldNameIndex('WSNO')])
            if 0 < wsno < 10000:   # point ids start at 10000; 0 used for added inlet
                sub = self.polyToSub[wsno]
                outletX = float(channel[channel.fieldNameIndex('OutletX')])
                outletY = float(channel[channel.fieldNameIndex('OutletY')])
                sourceX = float(channel[channel.fieldNameIndex('SourceX')])
                sourceY = float(channel[channel.fieldNameIndex('SourceY')])
                data = SubData(outletX, outletY, sourceX, sourceY)
                self.subData[sub] = data
            
    def fixMonitoringPoint(self):
    
        def pointToLatLong(point: QgsPointXY) -> QgsPointXY: 
            """Convert a QgsPointXY to latlong coordinates and return it."""
            crsTransform = QgsCoordinateTransform(self.crsProject, self.crsLatLong, QgsProject.instance())
            geom = QgsGeometry().fromPointXY(point)
            geom.transform(crsTransform)
            return geom.asPoint()
        
        def close(a, b):
            return abs(a - b) < 1.0
        
        sql = 'SELECT GRID_CODE, Xpr, Ypr FROM MonitoringPoint'
        OK = False
        for row in self.conn.execute(sql):
            data = self.subData.get(int(row[0]), None)
            if data is None:
                continue
            Xpr = float(row[1])
            Ypr = float(row[2])
            # if Xpr, Ypr is the source and not the outlet, database needs updating
            if close(Xpr, data.sourceX) and close(Ypr, data.sourceY) and not close(Xpr, data.outletX) and not close(Ypr, data.outletY):
                OK = True
                break
        if not OK:
            return False 
        sql2 = 'UPDATE MonitoringPoint SET Xpr=?, Ypr=?, Lat=?, Long_=? WHERE GRID_CODE=?'
        for sub, data in self.subData.items():
            pt = QgsPointXY(data.outletX, data.outletY)
            pt2 = pointToLatLong(pt)
            self.conn.execute(sql2, (data.outletX, data.outletY, pt2.y(), pt2.x(), sub))
        return True
            
    def fixBasinsData1(self):
        sql = 'SELECT basin, outletCol, outletRow, startCol, startRow FROM BASINSDATA1'
        for row in self.conn.execute(sql):
            sub = self.polyToSub[int(row[0])]
            data = self.subData[sub]
            data.sourceCol = int(row[1])
            data.sourceRow = int(row[2])
            data.outletCol = int(row[3])
            data.outletRow = int(row[4])
        sql2 = 'UPDATE BASINSDATA1 SET outletCol=?, outletRow=?, startCol=?, startRow=?, outletElevation=?, startToOutletDrop=? WHERE basin=?'
        for sub, data in self.subData.items():
            drop = max(0, data.maxEl - data.minEl)
            self.conn.execute(sql2, (data.outletCol, data.outletRow, data.sourceCol, data.sourceRow, data.minEl, drop, self.subToPoly[sub]))

if __name__ == '__main__':
    selection = sys.argv[1]
    for scale in [14, 12, 10, 8]:
        fix = FixDbs(scale, selection)
        fix.run()