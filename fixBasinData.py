# -*- coding: utf-8 -*-
'''
Created on April 16 2021

@author: Chris George
copyright: (C) 2021 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
'''

## this code fixes HUC projects generated with code prior to 1.2, including water data in BASINSDATAHUC1, which replaces BASINSDATA1
## and so allowing projects to be rerun after water data in WaterBodies.sqlite is changed without rerunning delineation ar land and soil overlay

import sys
import os
import sqlite3
import glob
import csv
from osgeo import ogr
ogr.UseExceptions()


class Files:
    """Common file locations."""
    
    ModelsDir = 'C:/HUCModels'
    
    HUC8ProjectParent = '/HUC8'
    HUC10ProjectParent ='/HUC10'
    HUC12ProjectParent ='/HUC12'
    HUC14ProjectParent = '/HUC14'
    
def readWaterStatsFile(waterStatsFile):
    if not os.path.isfile(waterStatsFile):
        print('Waterbody statistics file {0} not found: channel water will be set to zero'.format(waterStatsFile))
        return None
    waterStats = dict()
    with open(waterStatsFile, 'r') as f:
        reader = csv.reader(f)
        next(reader)  # skip header
        for row in reader:
            # row[0] is a string containing a string: need to strip quotes
            waterStats[row[0][1:-1]] = (float(row[5]), float(row[6]), float(row[8]), float(row[9]))
    return waterStats
    
def basinToHUC(channelsFile, subScale):
    if not os.path.isfile(channelsFile):
        print('Channels shapefile {0} not found: : channel water will be set to zero'.format(channelsFile))
        return None
    driverName = "ESRI Shapefile"
    drv = ogr.GetDriverByName( driverName )
    channelsSource = drv.Open(channelsFile, 0) # 0 means read-only. 1 means writeable.
    channelsLayer = channelsSource.GetLayer()
    hucSubName = 'HUC{0}'.format(subScale)
    basinHUC = dict()
    for feature in channelsLayer:
        basinHUC[int(feature.GetField('WSNO'))] = feature.GetField(hucSubName)
    return basinHUC
    
def fixBasinData(projDb, basinHUC, waterStats):
    sqlIn = 'SELECT * FROM BASINSDATA1'
    sqlCreate = 'CREATE TABLE IF NOT EXISTS BASINSDATAHUC1 ' + _BASINSDATA1TABLEHUC
    sqlClear = 'DELETE FROM BASINSDATAHUC1'
    sqlOut = 'INSERT INTO BASINSDATAHUC1 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    with sqlite3.connect(projDb) as conn:
        row = conn.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='BASINSDATA1'").fetchone()
        if row[0] == 0:
            print('No table BASINSDATA1 in {0}: skipping this project.'.format(projDb))
            return
        conn.execute(sqlCreate)
        conn.execute(sqlClear)
        for row in conn.execute(sqlIn):
            basin = int(row[0])
            WATRInStreamAreaHa = 0
            streamAreaHa = 0
            wetlandAreaHa = 0
            playaAreaHa = 0
            if basinHUC is not None:
                huc = basinHUC.get(basin, None)
                if huc is not None and waterStats is not None:
                    data = waterStats.get(huc, None)
                    if data is not None:
                        WATRInStreamAreaHa,  streamAreaHa, wetlandAreaHa, playaAreaHa = data
            conn.execute(sqlOut, (row[0], row[1], row[2], row[3], row[4], row[5], playaAreaHa * 1E4, row[4], row[5], playaAreaHa * 1E4, wetlandAreaHa * 1E4, \
                                 row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], \
                                 row[18], row[19], row[20], row[21], row[22], streamAreaHa * 1E4, WATRInStreamAreaHa * 1E4))
        conn.commit()
        
def runScale(SWATDir, scale, selection):
    parent = Files.ModelsDir + SWATDir + '/HUC{0}/'.format(scale) + selection
    if os.path.isdir(parent):
        print('Fixing basin data in {0}'.format(parent))
        waterStatsFile = os.path.join(parent, 'waterStats{0}_HUC{1}.csv'.format(selection[:2], scale))
        waterStats = readWaterStatsFile(waterStatsFile)
        pattern = parent + '/huc*'
        for direc in glob.iglob(pattern):
            if not os.path.isdir(direc):
                continue
            huc = os.path.split(direc)[1][3:] 
            projDb = direc + '/huc{0}.sqlite'.format(huc)
            if not os.path.isfile(projDb):
                print('Cannot find project database {0}.  Skipping this project.'.format(projDb))
                continue
            channelsFile = direc + '/Watershed/Shapes/channels.shp'.format(huc)
            basinHUC = basinToHUC(channelsFile, len(huc) + 2)
            fixBasinData(projDb, basinHUC, waterStats)
    else:
        print('Cannot find directory {0}'.format(parent))

_BASINSDATA1TABLEHUC = \
"""
(basin INTEGER, 
cellCount INTEGER, 
area REAL, 
drainArea REAL, 
pondArea REAL, 
reservoirArea REAL, 
playaArea REAL,
pondAreaOriginally REAL,
reservoirAreaOriginally REAL,
playaAreaOriginally REAL,
wetlandArea REAL,
totalElevation REAL, 
totalSlope REAL, 
outletCol INTEGER, 
outletRow INTEGER, 
outletElevation REAL, 
startCol INTEGER, 
startRow INTEGER, 
startToOutletDistance REAL, 
startToOutletDrop REAL, 
farCol INTEGER, 
farRow INTEGER, 
farthest INTEGER, 
farElevation REAL, 
farDistance REAL, 
maxElevation REAL, 
cropSoilSlopeArea REAL, 
hru INTEGER,
streamArea REAL,
WATRInStreamArea REAL)
"""
    
if __name__ == '__main__': 
    # pattern for choosing region (2 to 12 digits)
    selection = ''
    if len(sys.argv) > 1:
        selection = sys.argv[1]
    if selection == '':
        selection = '140100010404'
    print('Selection is {0}'.format(selection))
    # if True use Fields_CDL landuse maps and lookup table, else Fields
    isCDL = True
    # set true for SWAT+, false for SWAT
    isPlus = False
    # set true for HUC8 projects.
    is8 = True
    # set true for HUC10 projects.
    is10 = True
    # set true for HUC12 projects
    is12 = True
    # set true for HUC14 projects
    is14 = True
    extDir = '/Fields_CDL' if isCDL else '/Fields'
    SWATDir = '/SWATPlus' + extDir if isPlus else '/SWAT' + extDir
    if is14:
        runScale(SWATDir, 14, selection)
    if is12:
        runScale(SWATDir, 12, selection)
    if is10:
        runScale(SWATDir, 10, selection)
    if is8:
        runScale(SWATDir, 8, selection)
    print('Fix basin data finished')
    