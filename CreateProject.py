# -*- coding: utf-8 -*-
'''
Created on Nov 29, 2019

@author: Chris George
copyright: (C) 2019 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
'''

import os
import posixpath
import shutil
import traceback
# import glob
# import time


class CreateProject():
    
    def __init__(self, huc12: str, parent: str, dbDir: str, isPlus: bool, withProject: bool, keepDirs: bool) -> None:
        ## QSWAT or QSWAT+ project
        self.isPlus = isPlus
        ## project name
        self.projName = 'huc' + huc12
        ## parent directory
        self.projParent = parent
        ## project directory
        self.projDir = ''
        ## SWAT databases directory
        self.SWATDir = dbDir + '/SWAT'
        ## SWATPlus databases directory
        self.SWATPlusDir = dbDir + '/SWATPlus'
        # Create project directory and file.
        self.projDir = posixpath.join(self.projParent, self.projName)
        if withProject and os.path.exists(self.projDir) and not keepDirs:
            try:
                shutil.rmtree(self.projDir, ignore_errors=True)
                # time.sleep(2)  # give deletion time to complete
            except Exception:
                CreateProject.exceptionError('Problems encountered removing {0}: {1}.  Trying to continue regardless.'.format(self.projDir, traceback.format_exc()))
        # projFile = posixpath.join(self.projDir, self.projName + '.qgs')
        try: 
            CreateProject.makeDirs(self.projDir)
        except Exception:
            CreateProject.exceptionError('Failed to create project directory {0}'.format(self.projDir))
            return
        try:
            # print('Creating directories ...')
            self.createSubDirectories()
        except Exception:
            CreateProject.exceptionError('Problems creating subdirectories')
            return
        if withProject:
            try:
                # print('Copying databases ...')
                self.copyDbs()
            except Exception:
                CreateProject.exceptionError('Problems creating databases or project file: {0}')
                return 
 
    def createSubDirectories(self):
        """Create subdirectories under QSWAT+ project's directory."""
        watershedDir = posixpath.join(self.projDir, 'Watershed')
        CreateProject.makeDirs(watershedDir)
        if self.isPlus:
            rastersDir = posixpath.join(watershedDir, 'Rasters')
            CreateProject.makeDirs(rastersDir)
            demDir = posixpath.join(rastersDir, 'DEM')
            CreateProject.makeDirs(demDir)
            soilDir = posixpath.join(rastersDir, 'Soil')
            CreateProject.makeDirs(soilDir)
            landuseDir = posixpath.join(rastersDir, 'Landuse')
            CreateProject.makeDirs(landuseDir)
            landscapeDir = posixpath.join(rastersDir, 'Landscape')
            CreateProject.makeDirs(landscapeDir)    
            floodDir = posixpath.join(landscapeDir, 'Flood')
            CreateProject.makeDirs(floodDir)
        else:
            sourceDir = posixpath.join(self.projDir, 'Source')
            CreateProject.makeDirs(sourceDir)
            soilDir = posixpath.join(sourceDir, 'soil')
            CreateProject.makeDirs(soilDir)
            landuseDir = posixpath.join(sourceDir, 'crop')
            CreateProject.makeDirs(landuseDir)
        scenariosDir = posixpath.join(self.projDir, 'Scenarios')
        CreateProject.makeDirs(scenariosDir)
        defaultDir = posixpath.join(scenariosDir, 'Default')
        CreateProject.makeDirs(defaultDir)              
        txtInOutDir = posixpath.join(defaultDir, 'TxtInOut')
        CreateProject.makeDirs(txtInOutDir)
        if self.isPlus:
            resultsDir = posixpath.join(defaultDir, 'Results')
            CreateProject.makeDirs(resultsDir)
            plotsDir = posixpath.join(resultsDir, 'Plots')
            CreateProject.makeDirs(plotsDir)
            animationDir = posixpath.join(resultsDir, 'Animation')
            CreateProject.makeDirs(animationDir)
            pngDir = posixpath.join(animationDir, 'Png')
            CreateProject.makeDirs(pngDir)
        else:
            tablesInDir = posixpath.join(defaultDir, 'TablesIn')
            CreateProject.makeDirs(tablesInDir)
            tablesOutDir = posixpath.join(defaultDir, 'TablesOut')
            CreateProject.makeDirs(tablesOutDir)
        textDir = posixpath.join(watershedDir, 'Text')
        CreateProject.makeDirs(textDir)
        shapesDir = posixpath.join(watershedDir, 'Shapes')
        CreateProject.makeDirs(shapesDir)
            
    def copyDbs(self):
        """Set up project and reference databases."""
        if self.isPlus:
            projDbTemplate = posixpath.join(self.SWATPlusDir, 'QSWATPlusProjHUC.sqlite')
            refDbName = 'swatplus_datasets.sqlite'
            refDbTemplate = posixpath.join(self.SWATPlusDir, refDbName)
            # use same SSURGOD database as QSWAT
            SSURGODbName = 'SSURGO_Soils_HUC.sqlite'
            SSURGODbTemplate = posixpath.join(self.SWATDir, SSURGODbName)
            projFileTemplate = posixpath.join(self.SWATPlusDir, 'example.qgs')
            shutil.copy(projDbTemplate, posixpath.join(self.projDir, self.projName + '.sqlite'))
            # reference database is shared
            refDb = posixpath.join(self.projParent, refDbName)
            if not os.path.isfile(refDb):
                shutil.copyfile(refDbTemplate, refDb)
            # SSURGO database is shared
            SSURGODb = posixpath.join(self.projParent, SSURGODbName)
            if not os.path.isfile(SSURGODb):
                shutil.copyfile(SSURGODbTemplate, SSURGODb)
            shutil.copy(projFileTemplate, posixpath.join(self.projDir, self.projName + '.qgs'))
        else:
            projDbTemplate = posixpath.join(self.SWATDir, 'QSWATProjHUC.sqlite')
            refDbName = 'QSWATRef2012.sqlite'
            refDbTemplate = posixpath.join(self.SWATDir, refDbName)
            SSURGODbName = 'SSURGO_Soils_HUC.sqlite'
            SSURGODbTemplate = posixpath.join(self.SWATDir, SSURGODbName)
            projFileTemplate = posixpath.join(self.SWATDir, 'example.qgs')
            shutil.copy(projDbTemplate, posixpath.join(self.projDir, self.projName + '.sqlite'))
            # reference database is shared
            refDb = posixpath.join(self.projParent, refDbName)
            if not os.path.isfile(refDb):
                shutil.copyfile(refDbTemplate, refDb)
            # SSURGO database is shared
            SSURGODb = posixpath.join(self.projParent, SSURGODbName)
            if not os.path.isfile(SSURGODb):
                shutil.copyfile(SSURGODbTemplate, SSURGODb)
            shutil.copy(projFileTemplate, self.projDir + '.qgs')
        
    @staticmethod
    def makeDirs(direc: str) -> None:
        """Make directory dir unless it already exists."""
        if not os.path.exists(direc):
            os.makedirs(direc)
           
    @staticmethod
    def error(msg: str) -> None:
        """Report msg as an error."""
        from PyQt5.QtWidgets import QMessageBox
        msgbox = QMessageBox()
        msgbox.setWindowTitle('HUC12')
        msgbox.setIcon(QMessageBox.Critical)
        msgbox.setText(msg)
        msgbox.exec_()
        return
   
#     @staticmethod
#     def information(msg: str) -> None:
#         """Report msg."""
#         from PyQt5.QtWidgets import QMessageBox
#         msgbox = QMessageBox()
#         msgbox.setWindowTitle('HUC12')
#         msgbox.setIcon(QMessageBox.Information)
#         msgbox.setText(msg)
#         msgbox.exec_()
#         return
    
    @staticmethod
    def exceptionError(msg: str) -> None:
        """Report exception."""
        import traceback
        CreateProject.error('{0}: {1}'.format(msg, traceback.format_exc()))
        return
    
