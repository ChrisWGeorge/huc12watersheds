# -*- coding: utf-8 -*-
'''
Created on 5 October 2021

@author: Chris George
copyright: (C) 2021 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

connections.py 
From an HUC fromto file calculate the lengths of connections paths from all (or leaf) nodes.
Intended to allow comparisons between fromto files of a region, in particular
HAWQS V1 versus HAWQS V2
'''

import csv
import os
import sys 


def calcConnections(fromto, selection):
    """Calculate connections."""
    f, ext = os.path.splitext(fromto)
    outFile = f + '_' + selection + 'len' + ext
    connLengths = dict()
    us = dict()
    exits = []
    with open(fromto, newline='') as fromtoFile:
        reader = csv.reader(fromtoFile)
        _ = next(reader)  # skip header
        for node, dsNode in reader:
            dsNode = dsNode.strip()
            if node.startswith(selection) and (dsNode == '' or dsNode =='0' or dsNode.startswith(selection)):
                connLengths[node] = 0
                if dsNode == '' or dsNode == '0':
                    exits.append(node)
                else:
                    _ = us.setdefault(dsNode, set())
                    us[dsNode].add(node)
    queue = exits
    while len(queue)  > 0:
        node = queue.pop(0)
        l = connLengths[node]
        for upNode in us.get(node, set()):
            connLengths[upNode] = max(connLengths[upNode], l+1)
            queue.append(upNode)
    results = []
    for node, length in connLengths.items():
        if True:   # len(us.get(node, [])) == 0: # use second option for leafs only
            results.append((node, length))
    results.sort(reverse=True, key = lambda x: x[1])
    with open(outFile, 'w', newline='') as resultFile:
        for node, length in results:
            resultFile.write('{0}, {1}\n'.format(node, length))
            
if __name__ == '__main__':
    if len(sys.argv) > 1:
        fromto = sys.argv[1]
        if len(sys.argv) > 2:
            selection = sys.argv[2]
        else: selection = ''
        calcConnections(fromto, selection)