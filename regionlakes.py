'''
Created on Oct 30, 2021

@author: Chris
'''

"""from NHDLakes.sqlite and lakes table construct lakes spatialite database for each region"""

from osgeo import ogr, osr
import sqlite3
import os

ogr.UseExceptions()
dataDir = 'I:/Data/5072/'
huc12Db = 'I:/Data/HUC12Watershed.sqlite'
lakesFile = dataDir + 'NHDLakes.sqlite'
driver = ogr.GetDriverByName('SQLite')
sql1 = 'SELECT objectid, areaha, comid, huc12, gnis_name FROM lakes WHERE huc12 LIKE ?'
sql2 = 'SELECT AsText(geometry) FROM nhdlakes WHERE objectid=?'
with sqlite3.connect(huc12Db) as conn1, sqlite3.connect(lakesFile) as conn2:
    conn2.enable_load_extension(True)
    conn2.execute("SELECT load_extension('mod_spatialite')")
    for region in range(1, 19):
        regionStr = f'{region:02}'
        likeStr = regionStr + '%'
        regionLakesFile = dataDir + 'NHDLakes{0}.sqlite'.format(regionStr)
        if os.path.isfile(regionLakesFile):
            os.remove(regionLakesFile)
        data_source = driver.CreateDataSource(regionLakesFile, options=['SPATIALITE=YES'])
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(5072)
        layer = data_source.CreateLayer("lakes", srs, geom_type=ogr.wkbMultiPolygon)
        idField = ogr.FieldDefn('objectid', ogr.OFTInteger)
        areaField = ogr.FieldDefn('areaha', ogr.OFTReal)
        comidField = ogr.FieldDefn('comid', ogr.OFTInteger)
        huc12Field = ogr.FieldDefn('huc12', ogr.OFTString)
        nameField = ogr.FieldDefn('gnis_name', ogr.OFTString)
        layer.CreateField(idField)
        layer.CreateField(areaField)
        layer.CreateField(comidField)
        layer.CreateField(huc12Field)
        layer.CreateField(nameField)
        for row1 in conn1.execute(sql1, (likeStr,)):
            objectid = int(row1[0])
            row2 = conn2.execute(sql2, (objectid,)).fetchone()
            feature = ogr.Feature(layer.GetLayerDefn())
            feature.SetField('objectid', objectid)
            feature.SetField('areaha', float(row1[1]))
            feature.SetField('comid', int(row1[2]))
            feature.SetField('huc12', row1[3]) 
            feature.SetField('gnis_name', row1[4])               
            feature.SetGeometry(ogr.CreateGeometryFromWkt(row2[0]))
            layer.CreateFeature(feature)
        data_source = None
            