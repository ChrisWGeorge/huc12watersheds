# -*- coding: utf-8 -*-
'''
Created on Sep 6, 2020

@author: Chris George
copyright: (C) 2020 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
'''

from dbfread import DBF 
import sys 
import csv 
import os

if __name__ == '__main__': 
    dbf = sys.argv[1]
    if not os.path.isfile(dbf):
        print('Cannot find dbf file {0}'.format(dbf))
        exit()
    csvFile = os.path.splitext(dbf)[0] + '.csv'
    
    table = DBF(dbf)
    with open(csvFile, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(table.field_names)
        for record in table:
            writer.writerow(list(record.values()))