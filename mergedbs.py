# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSWAT
                                 A QGIS plugin
 Merge HUC14 or HUC12 projects
                              -------------------
        begin                : 2014-07-18
        copyright            : (C) 2014 by Chris George
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# from qgis.core import QgsVectorLayer, QgsExpression, QgsFeatureRequest
from qgis.core import *  # @UnusedWildImport

import os
import shutil
import glob
import csv
import sys
from numpy import zeros
from osgeo import gdal, ogr, osr
import sqlite3

from connections import calcConnections
from comparepaths import comparePaths

class MergeDbs:
    
    """Merge databases to combine HUC14/12/10/8 projects."""
    
    # where to find HUC12Watershed.sqlite
    dataDir = 'I:/Data/'
    # where to find HAWQS V1 path length files, e.g. hawqs_v1_from-to_huc12_01len.csv
    HAWQSDir = 'I:/Models/SWAT/Fields_CDL/'
    # threshold difference in path lengths: only differences greater than this are reported
    pathDiffThreshold = 10 
    # run merge
    runMerge = True 
    # generate path lengths and report on differences
    runPaths = False
    # huc12 ds table to use: huc12ds for NHD original, huc12HAWQSV1ds for HAWQS version 1
    HUC12dsTable = 'huc12ds'
    #HUC12dsTable = 'huc12HAWQSV1ds'
    basePointId = 100000  # must be same as Common.basePointId in HUC12Models.py
    
    def __init__(self, projDir, sourceDir, selection, isPlus):
        """Initialize"""
        ## merged project directory
        self.projDir = projDir
        if not os.path.isdir(self.projDir):
            os.makedirs(self.projDir)
        ## log file for messages about connections
        logName = 'mergelogHAWQSV1.txt' if 'HAWQSV1' in MergeDbs.HUC12dsTable else 'mergelogNHD.txt'
        self.logFile = os.path.join(projDir, logName)
        with open(self.logFile, 'w') as log:
            log.write('Merging with table {0}\n'.format(MergeDbs.HUC12dsTable))
        ## source directory
        self.sourceDir = sourceDir
        ## selection
        self.selection = selection
        ## list of project names
        self.projNames = []
        ## huc12_10_8_6 -> dsnode -> (huc12_10_8_6, dsnode)
        self.oiBaseMap = dict()
        ## huc12_10_8_6 -> subbasin -> (huc12_10_8_6, subbasin)
        self.oiMap = dict()
        ## huc12_10_8 -> huc12_10_8 from fromto.csv at 14/12/10 upper scale
        # left empty when merging HUC14
        self.fromtoMap = dict()
        ## map of HUC12_10 to outlet pointid to HUC14_12
        self.outlets = dict()
        ## added inlets (for projects below 95% soil coverage)
        self.addedInlets = set()
        ## huc12_10 -> SWATId -> subNum, where SWATId is relative subbasin number and subNum is global subbasin number
        # result is set to -1 if the subbasin does not exist (because of empty project at level above)
        self.subMap = dict()
        ## another version of subMap, HUC14_12 -> (subbasin, subNum)
        self.subMap14 = dict()
        ## inverse submap: global subbasin number to HUC14_12 string
        self.HUC14_12Map = dict()
        ## huc12_10 -> old Subbasin -> new Subbasin (or -1 if removed)
        self.subbasinChanges = dict()
        ## next subbasin number
        self.nextSubNum = 1
        ## numpy array mapping subNums to drain area
        self.drainAreas = None  
        ## subNum -> downstream subNum or -1 for outlet
        self.dsSubMap = dict()
        ## subNum -> area
        self.areas = dict()
        ## set of used landuses
        self.landuses = set()
        ## flag for SWAT or SWAT+
        self.isPlus = isPlus
        gdal.UseExceptions()
        ogr.UseExceptions()
        self.createSubDirectories() 
        # projections
        self.srs = osr.SpatialReference()
        self.srs.ImportFromEPSG(5072)
        latlonSrs = osr.SpatialReference()
        latlonSrs.ImportFromEPSG(4326)
        self.toLatlon = osr.CoordinateTransformation(self.srs, latlonSrs)

    def createSubDirectories(self):
        """Create subdirectories under QSWAT project's directory."""
        
        def makeDirs(direc):
            """Make directory dir unless it already exists."""
            if not os.path.isdir(direc):
                os.makedirs(direc)
        
        makeDirs(self.projDir)
        watershedDir = os.path.join(self.projDir, 'Watershed')
        makeDirs(watershedDir)
        if self.isPlus:
            rastersDir = os.path.join(watershedDir, 'Rasters')
            makeDirs(rastersDir)
            demDir = os.path.join(rastersDir, 'DEM')
            makeDirs(demDir)
            soilDir = os.path.join(rastersDir, 'Soil')
            makeDirs(soilDir)
            landuseDir = os.path.join(rastersDir, 'Landuse')
            makeDirs(landuseDir)
            landscapeDir = os.path.join(rastersDir, 'Landscape')
            makeDirs(landscapeDir)    
            floodDir = os.path.join(landscapeDir, 'Flood')
            makeDirs(floodDir)
        else:
            sourceDir = os.path.join(self.projDir, 'Source')
            makeDirs(sourceDir)
            soilDir = os.path.join(sourceDir, 'soil')
            makeDirs(soilDir)
            landuseDir = os.path.join(sourceDir, 'crop')
            makeDirs(landuseDir)
        scenariosDir = os.path.join(self.projDir, 'Scenarios')
        makeDirs(scenariosDir)
        defaultDir = os.path.join(scenariosDir, 'Default')
        makeDirs(defaultDir)              
        txtInOutDir = os.path.join(defaultDir, 'TxtInOut')
        makeDirs(txtInOutDir)
        if self.isPlus:
            resultsDir = os.path.join(defaultDir, 'Results')
            makeDirs(resultsDir)
            plotsDir = os.path.join(resultsDir, 'Plots')
            makeDirs(plotsDir)
            animationDir = os.path.join(resultsDir, 'Animation')
            makeDirs(animationDir)
            pngDir = os.path.join(animationDir, 'Png')
            makeDirs(pngDir)
        else:
            tablesInDir = os.path.join(defaultDir, 'TablesIn')
            makeDirs(tablesInDir)
            tablesOutDir = os.path.join(defaultDir, 'TablesOut')
            makeDirs(tablesOutDir)
        textDir = os.path.join(watershedDir, 'Text')
        makeDirs(textDir)
        shapesDir = os.path.join(watershedDir, 'Shapes')
        makeDirs(shapesDir)

    def makeProjNames(self):
        """Make project names."""
        self.projNames = []
        pattern = self.sourceDir + '/huc' + self.selection + '*.qgs'
        for d in glob.iglob(pattern):
            # remove '.qgs'
            d = d[:-4]
            # print(d)
            projName = os.path.split(d)[1]
            self.projNames.append(projName)
            # print('{0}'.format(projNames))
        if len(self.projNames) == 0:
            print('No projects found.  Have you run them?')
            
    def patchFromto(self, upFromto, fromtoFile, scale, selection):
        """Copy upFromto to fromtoFile, filling in missing targets using huc{scale-2}ds table from HUC12Watershed.sqlite,
        and skipping non-existent HUC{scale-2}s."""
        
        def skipNonHUCs(upParent, ds, sql, cursor):
            """If ds does not exist, try to improve on it using huc{scale-2}ds table.  Assumes ds is not empty string,
            and that huc{scale-2}ds table is not circular."""
            if not ds.startswith(selection):
                return ds  # link to a different region        
            upProject = os.path.join(upParent, 'huc' + ds + '.qgs')
            if not os.path.isfile(upProject):
                row = cursor.execute(sql, (ds,)).fetchone()
                if row is None:
                    return ds
                ds1 = row[0]
                if ds1 == '' or not ds1.isdigit():  # check for OCEAN, CANADA, UNKNOWN etc
                    return ''
                return skipNonHUCs(upParent, ds1, sql, cursor)
            else:
                return ds 
        
        upParent = os.path.split(upFromto)[0]
        table = MergeDbs.HUC12dsTable.replace('12', str(scale))
        sql = 'SELECT ds from {0} WHERE huc{1}=?'.format(table, scale)
        with open(upFromto) as f1, open(fromtoFile, 'w', newline='') as f2, sqlite3.connect(MergeDbs.dataDir + 'HUC12Watershed.sqlite') as conn:
            readr = csv.reader(f1)
            writr = csv.writer(f2)
            cursor = conn.cursor()
            header = next(readr)
            writr.writerow(header)
            for row in readr:
                if row[1].strip() == '':
                    target = cursor.execute(sql, (row[0],)).fetchone()
                    if target is None or not target[0].isdigit():  # do not exclude targets from other regions; not checking against selection
                        ds = ''
                    else:
                        ds = target[0]
                else:
                    ds = row[1]
                if ds == '':
                    ds1 = ''
                else:
                    # check for non-existent target
                    ds1 = skipNonHUCs(upParent, ds, sql, cursor)
                if ds1 == '':
                    self.log('Nothing downstream found for {0}'.format(row[0]))
                elif ds1 != ds:
                    self.log('Downstream from {0} changed from {1} to {2}'.format(row[0], ds, ds1))
                writr.writerow([row[0], ds1])

    def makeOIMap(self, fromtoFile):
        """Make oiBaseMap: huc12_10_8_6 -> dsnode -> (huc12_10_8_6, dsnode) and
        oiMap: huc12_10_8_6 -> subbasin -> (huc12_10_8_6, subbasin) for both huc12_10_8_6s that are within selected region.
        Only accept entries for oiMap that are consistent with fromto.csv, if available, 
        since that ensures the downstream relation between subbasins is many-one"""
        self.fromtoMap = dict()
        # make a fromto table
        if fromtoFile is not None and os.path.isfile(fromtoFile):
            haveFromto = True
            with open(fromtoFile) as f:
                readr = csv.reader(f)
                next(readr)  # skip header
                for row in readr:
                    self.fromtoMap[row[0]] = row[1]
        else:
            haveFromto = False        
        oiFile = self.sourceDir + '/oi.csv'
        self.oiBaseMap = dict()
        self.oiMap = dict()
        with open(oiFile) as csvFile:
            reader = csv.reader(csvFile)
            next(reader)  # skip header
            for row in reader:
                outHUC = row[0]
                outLink = row[1]
                inHUC = row[2]
                inLink = row[3]
                if outHUC.startswith(self.selection) and inHUC.startswith(self.selection):
                    outProjName = 'huc' + outHUC
                    outDir = os.path.join(self.sourceDir, os.path.join(outProjName))
                    outQgs = outDir + '.qgs'
                    if not os.path.isdir(outDir) or not os.path.isfile(outQgs) or outProjName not in self.projNames:
                        # probably an empty project
                        continue
                    inProjName = 'huc' + inHUC
                    inDir = os.path.join(self.sourceDir, os.path.join(inProjName))
                    inQgs = inDir + '.qgs'
                    if not os.path.isdir(inDir) or not os.path.isfile(inQgs) or inProjName not in self.projNames:
                        continue
                    self.oiBaseMap[outHUC] = {int(outLink): (inHUC, int(inLink))}
                    outStreamsFile = outDir + '/Watershed/Shapes/channels.shp'
                    if not os.path.isfile(outStreamsFile):
                        print('Cannot find {0}'.format(outStreamsFile))
                        continue
                    # using QgsVectorLayer does not work here, 
                    # probably because did not make QgsApplication, so we use ogr directly
                    driverName = "ESRI Shapefile"
                    drv = ogr.GetDriverByName( driverName )
                    outSource = drv.Open(outStreamsFile, 0) # 0 means read-only. 1 means writeable.
                    outLayer = outSource.GetLayer()
                    outLayer.SetAttributeFilter('DSNODEID = {0}'.format(outLink))
                    removed = False
                    outSubbasins = []
                    for outStream in outLayer:
                        outSubbasin0 = int(outStream.GetField('LINKNO'))
                        outSubbasin = self.subbasinChanges[outHUC].get(outSubbasin0, outSubbasin0)
                        if outSubbasin == -1:
                            # removed subbasin
                            removed = True
                        else:
                            outSubbasins.append(outSubbasin)
                    if len(outSubbasins) == 0:
                        if removed:
                            continue
                        print('Cannot find stream with DSNODEID {0} in {1}'.format(outLink, outStreamsFile))
                        return False
                    outletMap = self.oiMap.setdefault(outHUC, dict())
                    inStreamsFile = inDir + '/Watershed/Shapes/channels.shp'
                    inSource = drv.Open(inStreamsFile, 0)
                    inLayer = inSource.GetLayer()
                    inLayer.SetAttributeFilter('LINKNO = {0}'.format(inLink))
                    inSubbasin = -2
                    for inStream in inLayer:
                        inSubbasin0 = int(inStream.GetField('DSLINKNO'))
                        inSubbasin = self.subbasinChanges[inHUC].get(inSubbasin0, inSubbasin0)
                        break
                    if inSubbasin == -1:
                        # removed subbasin
                        continue
                    if inSubbasin == -2:
                        print('Cannot find stream with LINKNO {0} in {1}'.format(inLink, inStreamsFile))
                        return False
                    try:
                        inSub = self.subMap[inHUC][inSubbasin]
                        if inSub < 0:
                            print('Inlet for {0} to subbasin {1} connected to {2} from subbasin in {3} does not exist'.format(inHUC, inSubbasin, outHUC, outSubbasins))
                    except:
                        print('Inlet for {0} to subbasin {1} connected to {2} from subbasin in {3} crashes'.format(inHUC, inSubbasin, outHUC, outSubbasins))
                        inSub = -1
                    for outSubbasin in outSubbasins:
                        try:
                            outSub = self.subMap[outHUC][outSubbasin]
                        except:
                            print('Outlet for {0} subbasin {1} crashes'.format(outHUC, outSubbasin))
                            outSub = -1
                        if inSub > 0 and outSub > 0:  # otherwise subbasin is empty
                            outHUC14_12_10 = self.HUC14_12Map[outSub]
                            inHUC14_12_10 = self.HUC14_12Map[inSub]
                            if haveFromto:  
                                toHUC14_12_10 = self.fromtoMap.get(outHUC14_12_10, None)  
                                if toHUC14_12_10 is None:
                                    print('Cannot find {0} in {1}'.format(outHUC14_12_10, fromtoFile))
                                    continue
                                if toHUC14_12_10 == '':
                                    # outlet: not needed in oiMap
                                    continue
                                elif inHUC14_12_10 != toHUC14_12_10:
                                    # follow fromtoMap
                                    inHUC = toHUC14_12_10[:-2]
                                    inSubbasin, _ = self.subMap14.get(toHUC14_12_10, (0, 0))
                                    if inSubbasin == 0:
                                        self.log('Failed to find subbasin for {0}'.format(toHUC14_12_10))
                                    else:
                                        self.log('Correcting target for subbasin {0} of {1} to {2}'.format(outSubbasin, outHUC, toHUC14_12_10))
                                    # omit from ioMap: inconsistent with fromto
                                    continue
                            outletMap[outSubbasin] = (inHUC, inSubbasin)
        #print('oiBaseMap: {0}'.format(self.oiBaseMap))
        #print('oiMap: {0}'.format(self.oiMap))
        return True
                
    def makeWatershedTable(self, mainDb, mainShapesDir, HUCScale, upCollectionDir):
        """Populate subMap, areas and Watershed table, and make wshed shapefile."""
        self.subMap = dict()
        self.subMap14 = dict()
        self.HUC14_12Map = dict()
        self.subbasinChanges = dict()
        self.nextSubNum = 1
        self.areas = dict()
        with sqlite3.connect(mainDb) as conn:  # @UndefinedVariable
            # print('{0}'.format(conn))
            cursor = conn.cursor()
            sql0 = 'DROP TABLE IF EXISTS Watershed'
            sql0a = 'DROP TABLE IF EXISTS RemovedHUCs'
            cursor.execute(sql0)
            cursor.execute(sql0a)
            sql1 = MergeDbs._WATERSHEDCREATESQL
            sql1a = MergeDbs._REMOVEDHUCSCREATESQL
            cursor.execute(sql1)
            cursor.execute(sql1a)
            # print('ProjNames: {0}'.format(self.projNames))
            sql2 = 'SELECT * FROM Watershed'
            sql3 = 'INSERT INTO Watershed VALUES(?,0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
            sql4 = "SELECT name FROM sqlite_master WHERE type='table' AND name='Watershed'"
            sql5 = "SELECT name FROM sqlite_master WHERE type='table' AND name='SubbasinChanges'"
            sql6 = 'SELECT * FROM SubbasinChanges'
            sql7 = 'INSERT INTO RemovedHUCs VALUES(?)'
            nonProjects = set()
            # using QgsVectorLayer does not work here, 
            # probably because did not make QgsApplication, so we use ogr directly
            driverName = "ESRI Shapefile"
            mainDrv = ogr.GetDriverByName( driverName )
            mainWshedFile = mainShapesDir + '/demwshed.shp'
            if os.path.exists(mainWshedFile):
                mainDrv.DeleteDataSource(mainWshedFile)
            mainWshedSource = mainDrv.CreateDataSource(mainWshedFile)
            mainLayer = mainWshedSource.CreateLayer('demwshed', self.srs, ogr.wkbPolygon)
            polyField = ogr.FieldDefn('PolygonId', ogr.OFTInteger)
            mainLayer.CreateField(polyField)
            subField = ogr.FieldDefn('Subbasin', ogr.OFTInteger)
            mainLayer.CreateField(subField)
            hucSubName = 'HUC{0}'.format(HUCScale)
            hucField = ogr.FieldDefn(hucSubName, ogr.OFTString)
            hucField.SetWidth(24)
            mainLayer.CreateField(hucField)
            areaField = ogr.FieldDefn('Area', ogr.OFTReal)
            mainLayer.CreateField(areaField)
            mainFeatureDefn = mainLayer.GetLayerDefn()
            for projName in self.projNames:
                huc12_10 = projName[3:]
                subs = self.subMap.setdefault(huc12_10, dict())
                subChanges = self.subbasinChanges.setdefault(huc12_10, dict())
                projDir = os.path.join(self.sourceDir, projName)
                db = os.path.join(projDir, projName + '.sqlite')
                with sqlite3.connect(db) as projConn:  # @UndefinedVariable
                    projCursor = projConn.cursor()
                    # check Watershed table exists
                    watershedCheck = projCursor.execute(sql4).fetchone()
                    if watershedCheck is None:
                        self.log('No Watershed table in {0}: skipping this project'.format(projName))
                        nonProjects.add(projName)
                        continue
                    # check for changed subbasin numbers because of removed subbasins outside soil map
                    # these changes are reflected in Watershed and Reach tables but not in shapefiles
                    # so subbasin numbers read from shapefiles may need changing
                    subbasinChangesCheck = projCursor.execute(sql5).fetchone()
                    if subbasinChangesCheck is not None:
                        for row in projCursor.execute(sql6):
                            subChanges[int(row[0])] = int(row[1])
                        #print(projName + ' has SubbasinChanges: {0}'.format(subChanges))
                    # make a map of subbasin number to HUC code of the subbasin
                    # using demwshed.shp file of the project
                    wshedFile = os.path.join(projDir, 'Watershed/Shapes/demwshed.shp')
                    drv = ogr.GetDriverByName( driverName )
                    wshedSource = drv.Open(wshedFile, 0) # 0 means read-only. 1 means writeable.
                    wshedLayer = wshedSource.GetLayer()
                    hucSubs = dict()
                    for feature in wshedLayer:
                        subbasin0 = int(feature.GetField('Subbasin'))
                        subbasin1 = subChanges.get(subbasin0, subbasin0)
                        if subbasin1 == -1:  # subbasin was removed
                            cursor.execute(sql7, (feature.GetField(hucSubName),))
                        else:  
                            hucSubs[subbasin1] = (feature.GetField(hucSubName), feature.GetGeometryRef().Clone())
                    #if len(subChanges) > 0:
                    #     print('hucSubs keys: {0}'.format(hucSubs.keys()))
                    for row in projCursor.execute(sql2):
                        subbasin = int(row[3])
                        huc14_12, geom = hucSubs[subbasin]
                        # subbasin exists if project at previous level exists: else presumably an empty project
                        if upCollectionDir is None:
                            # current scale is 14
                            subExists = True
                        else:
                            huc14_12ProjectFile = upCollectionDir + '/huc{0}.qgs'.format(huc14_12)
                            subExists = os.path.isfile(huc14_12ProjectFile)
                            # if subExists:
                            #     print('Have watershed subbasin {0} for HUC14 {1}, subbasin {2} of {3}'.format(self.nextSubNum, huc14_12, subbasin, huc12_10))
                            # else:
                            #     print('No watershed subbasin for HUC14 {0}, subbasin {1} of {1}'.format(huc14_12, subbasin, huc12_10))
                        if subExists:
                            subs[subbasin] = self.nextSubNum
                            self.areas[self.nextSubNum] = float(row[4])
                            self.HUC14_12Map[self.nextSubNum] = huc14_12
                            self.subMap14[huc14_12] = (subbasin, self.nextSubNum)
                            hydroid = 300000 + self.nextSubNum
                            outletid = 100000 + self.nextSubNum
                            cursor.execute(sql3, (self.nextSubNum, self.nextSubNum, self.nextSubNum, huc14_12, 
                                           row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], 
                                           row[12], row[13], row[14], row[15], row[16], row[17], row[18], row[19], row[20],
                                           hydroid, outletid))
                            mainFeature = ogr.Feature(mainFeatureDefn)
                            mainFeature.SetGeometry(geom)
                            mainFeature.SetField('PolygonId', self.nextSubNum)
                            mainFeature.SetField('Subbasin', self.nextSubNum)
                            mainFeature.SetField(hucSubName, huc14_12)
                            mainFeature.SetField('Area', float(row[4]))
                            mainLayer.CreateFeature(mainFeature)
                            self.nextSubNum += 1
                        else:
                            subs[subbasin] = -1
                    wshedSource.Destroy()
            conn.commit()
            mainWshedSource.Destroy()
            for nonProject in nonProjects:
                self.projNames.remove(nonProject)    
                        
    def makeDownstreamMap(self, scale):
        """Make downstream map from source Reach tables and oiMap."""
        
        def downstreamSubbasin(huc12, subbasin):
            """Try to find a downstream sub for unconnected HUC12 subbasin>"""
        
            def nearestSubbasin(huc12, point, pointSub, exitSub, drv, channelsLayer):
                """Return subbasin number with centroid nearest to point, 
                excluding point's subbasin pointSub if it is positive (meaning it is in huc12),
                from which exitSub is reachable.  drv is ESRI Shapefile driver."""
                
                def reachable(target, source, ds):
                    """Return True if target is reachable from source in downstream relation ds.
                    ds is assumed not to be circular"""
                    if target == source:
                        return True
                    nxt = ds.get(source, -1)
                    if nxt < 0:
                        return False
                    return reachable(target, nxt, ds)
                
                def nearest(point, centroids):
                    """Return subbasin of nearest centroid to point."""
                    minMeasure = float('inf')
                    subbasin = -1
                    for nxt, centroid in centroids.items():
                        x = centroid.x() - point.x()
                        y = centroid.y() - point.y()
                        measure = x * x + y * y 
                        if measure < minMeasure:
                            minMeasure = measure
                            subbasin = nxt
                    return subbasin
                
                # note that exitSub may have been changed by subbasin changes, but values read from
                # shapefiles need to be checked for changes
                subChanges = self.subbasinChanges.get(huc12, dict())
                wshedFile = os.path.join(self.sourceDir, 'huc{0}/Watershed/Shapes/demwshed.shp'.format(huc12))
                wshedSource = drv.Open(wshedFile, 0)
                wshedLayer = wshedSource.GetLayer()
                # collect centroids
                centroids = dict()
                for basin in wshedLayer:
                    subbasin0 = int(basin.GetField('Subbasin'))
                    subbasin = subChanges.get(subbasin0, subbasin0)
                    if subbasin > 0:
                        centroid = basin.GetGeometryRef().Centroid()
                        centroids[subbasin] = QgsPointXY(float(centroid.GetX()), float(centroid.GetY()))
                # make downstream relation
                ds = dict()
                for channel in channelsLayer:
                    link0 = int(channel.GetField('LINKNO'))
                    dsLink0 = int(channel.GetField('DSLINKNO'))
                    if link0 < MergeDbs.basePointId and dsLink0 > 0:
                        link = subChanges.get(link0, link0)
                        dsLink = subChanges.get(dsLink0, dsLink0)
                        if link > 0 and dsLink > 0:
                            ds[link] = dsLink
                # delete point's subbasin from centroids if positive
                if pointSub > 0 and pointSub in centroids:
                    del centroids[pointSub]
                # prune centroids to those from which exitSub is reachable: assumes ds is not circular
                centroids2 = dict()
                for subbasin, centroid in centroids.items():
                    if reachable(exitSub, subbasin, ds):
                        centroids2[subbasin] = centroid
                return nearest(point, centroids2)
            
            # first check the HUC12 model has a downstream model
            sql12 = 'SELECT ds FROM {0} WHERE huc12=?'.format(MergeDbs.HUC12dsTable)
            db = MergeDbs.dataDir + 'HUC12Watershed.sqlite'
            with sqlite3.connect(db) as conn:
                target12 = conn.execute(sql12, (huc12,)).fetchone()
                if target12 is None or not target12[0].isdigit():
                    return 0  # no downstream HUC12
                #print('Downstream HUC12 is {0}'.format(target12[0]))
                # find the outlet subbasin:
                outSub = -1
                #print('Searching for outlet from {0}'.format(huc12))
                for outSubbasin, (inHUC, _) in self.oiMap.get(huc12, dict()).items():
                    outSub = outSubbasin
                    # find the appropriate exit if there is a choice
                    if inHUC == target12[0]:
                        break
                #print('Outlet is {0}'.format(outSub))
                # find outlet point for subbasin's channel
                channelsFile = os.path.join(self.sourceDir, 'huc{0}/Watershed/Shapes/channels.shp'.format(huc12))
                driverName = "ESRI Shapefile"
                drv = ogr.GetDriverByName( driverName )
                source = drv.Open(channelsFile, 0) # 0 means read-only. 1 means writeable.
                layer = source.GetLayer()
                point = None
                for channel in layer:
                    if int(channel.GetField('WSNO')) == subbasin:
                        point = QgsPointXY(float(channel.GetField('OutletX')), float(channel.GetField('OutletY')))
                        break
                if point is None:
                    print('Failed to find channel with WSNO {0} in channels file {1}'.format(subbasin, channelsFile))
                    return self.subMap[huc12][outSub] if outSub > 0 else 0
                if outSub < 0:  # failed to find exit in oiMap from current huc12
                    # if huc10 not in huc10ds then no point in choosing another subbasin
                    # and danger of following coastline
                    sql10 = 'SELECT ds FROM huc10ds WHERE huc10=?'
                    target10 = conn.execute(sql10, (huc12[:-2],)).fetchone()
                    if target10 is None:  # non-HUC10s like OCEAN not included as ds values in huc10ds, so no need to check isdigit
                        return 0
                    # find nearest subbasin in target
                    target = target12[0]
                    #print('Target is {0}'.format(target))
                    targetOutSub = -1
                    # find an outlet subbasin
                    for targetOutSubbasin in self.oiMap.get(target, dict()):
                        targetOutSub = targetOutSubbasin
                        break
                    if targetOutSub < 0:
                        # target has no outlets in oiMap
                        # possible outlet to another region: take an arbitrary sub
                        for subR in self.subMap.get(target, dict()).values():
                            return subR
                        return 0
                    #print('Target out subbasin is {0}'.format(targetOutSub))
                    nearestSub = nearestSubbasin(target, point, -1, targetOutSub, drv, layer)
                    #print('Nearest subbasin is {0}'.format(nearestSub))
                    if nearestSub < 0:  # should not happen - safety
                        return self.subMap[target][targetOutSub]
                    #print('subMap for {0}: {1}'.format(target, self.subMap[target]))
                    ##print('oiMap for {0}: {1}'.format(target, self.oiMap[target]))
                    return self.subMap[target][nearestSub]
                # find nearest subbasin that drains to outSub:
                nearestSub = nearestSubbasin(huc12, point, subbasin, outSub, drv, layer)
                if nearestSub < 0:  # should not happen - safety
                    return self.subMap[huc12][outSub]
                return self.subMap[huc12][nearestSub]
                    
        self.dsSubMap = dict()
        sql = 'SELECT Subbasin, SubbasinR FROM Reach'
        for projName in self.projNames:
            huc12_10 = projName[3:]
            db = os.path.join(self.sourceDir, os.path.join(projName, projName + '.sqlite'))
            with sqlite3.connect(db) as projConn:  # @UndefinedVariable
                projCursor = projConn.cursor()
                for row in projCursor.execute(sql):
                    subbasin = int(row[0])
                    sub = self.subMap[huc12_10][subbasin]
                    if sub > 0:
                        subbasinR = int(row[1])
                        if subbasinR > 0:
                            subR = self.subMap[huc12_10][subbasinR]
                            if subR < 0:
                                subR = 0
                        elif len(self.fromtoMap) > 0:
                            huc14_12 = self.HUC14_12Map[sub]
                            huc14_12R = self.fromtoMap.get(huc14_12, '')
                            subR = self.subMap14.get(huc14_12R, (0, 0))[1]
                        else:        
                            try:
                                #print('Seeking inlet to match subbasin {0} of {1}'.format(subbasin, huc12_10))
                                #print('oiMap for {0}: {1}'.format(huc12_10, self.oiMap[huc12_10]))
                                inHUC, inSubbasin = self.oiMap[huc12_10][subbasin]
                                subR = self.subMap[inHUC][inSubbasin]
                                if subR < 0:
                                    print('Negative sub for subbasin {0} of {1}'.format(subbasin, huc12_10))
                                    subR = 0
                                elif subR == 0:
                                    print('Zero sub for subbasin {0} of {1}'.format(subbasin, huc12_10)) 
                            except:
                                if scale == 14:
                                    #print('Looking for downstream link for subbasin {0} of {1}'.format(subbasin, huc12_10))
                                    subR = downstreamSubbasin(huc12_10, subbasin)
                                    #HUC14R = self.HUC14_12Map.get(subR, '')
                                    #print('Looking for downstream link for subbasin {0} of {1}: found {2}, which is HUC14 {3}'.format(subbasin, huc12_10, subR, HUC14R))
                                else:
                                    subR = 0  # watershed outlet.  oiMap only records outlet-inlet pairs within selection region
                                if subR == 0:
                                    self.log('Failed to find inHUC and inSubbasin for subbasin {0} of {1}'.format(subbasin, huc12_10))
                        self.dsSubMap[sub] = subR
                        
    def makeDrainage(self):
        """Create drainage map from dsSubMap and areas map."""
        # start with drainAreas containing each sub's area
        self.drainAreas = zeros((self.nextSubNum), dtype=float)
        for sub in range(1, self.nextSubNum):
            self.drainAreas[sub] = self.areas[sub]
        # number of incoming links for each subbasin
        incount = zeros((self.nextSubNum), dtype=int)
        for dsSub in self.dsSubMap.values():
            if dsSub > 0:
                incount[dsSub] += 1
        # queue contains all subs whose drainage areas have been calculated 
        # i.e. will not increase and can be propagated
        queue = [sub for sub in range(1, self.nextSubNum) if incount[sub] == 0]
        while queue:
            sub = queue.pop(0)
            dsSub = self.dsSubMap.get(sub, 0)
            if dsSub > 0:
                self.drainAreas[dsSub] += self.drainAreas[sub]
                incount[dsSub] -= 1
                if incount[dsSub] == 0:
                    queue.append(dsSub)
        # incount values should now all be zero
        remainder = [sub for sub in range(1, self.nextSubNum) if incount[sub] > 0]
        if remainder:
            print('Drainage areas incomplete.  There is a circularity in subbasins {0!s}'.format(remainder))
            
    def makeReachTable(self, mainDb, mainShapesDir, HUCScale, fromtoFile): 
        """Create Reach table from dsSubMap and Drainage areas map, cnd write channels shapefile.
        Also collect outlets as map from point id to HUC14_12
        Also collect added inlets as (huc14_12_10_8, subbasin, x, y) set"""
        
        def correctFromto(fromtoFile, fromtoCorrections):
            fromtoFile2 = fromtoFile.replace('.csv', '2.csv')
            with open(fromtoFile, 'r', newline='') as f, open(fromtoFile2, 'w', newline='') as f2:
                reader = csv.reader(f)
                writer = csv.writer(f2)
                writer.writerow(next(reader))  # header
                for row in reader:
                    huc = row[0]
                    correction = fromtoCorrections.get(huc, None)
                    if correction is None:
                        writer.writerow(row)
                    else:
                        f2.write('{0},{1}\n'.format(huc, correction))
            os.remove(fromtoFile)
            os.rename(fromtoFile2, fromtoFile)
            
        haveFromto = os.path.isfile(fromtoFile)
        if not haveFromto:
            fromtoWriter = open(fromtoFile, 'w')
            fromtoWriter.write('OutletHUC,InletHUC\n')
        fromtoName, ext = os.path.splitext(fromtoFile)
        # actual fromto relation used in Reach table
        # preferred to fromto as no ambiguity: only one downstream for each node
        # fromto is more like an enabling table
        fromtoActFile = fromtoName + '_act' + ext
        fromtoActWriter = open(fromtoActFile, 'w')
        fromtoActWriter.write('OutletHUC,InletHUC\n')
        with sqlite3.connect(mainDb) as conn:  # @UndefinedVariable
            driverName = "ESRI Shapefile"
            mainDrv = ogr.GetDriverByName( driverName )
            mainChannelsFile = mainShapesDir + '/channels.shp'
            if os.path.exists(mainChannelsFile):
                mainDrv.DeleteDataSource(mainChannelsFile)
            mainChannelsSource = mainDrv.CreateDataSource(mainChannelsFile)
            mainLayer = mainChannelsSource.CreateLayer('channels', self.srs, ogr.wkbLineString)
            linkField = ogr.FieldDefn('LINKNO', ogr.OFTInteger)
            mainLayer.CreateField(linkField)
            dsLinkField = ogr.FieldDefn('DSLINKNO', ogr.OFTInteger)
            mainLayer.CreateField(dsLinkField)
            dsNodeField = ogr.FieldDefn('DSNODEID', ogr.OFTInteger)
            mainLayer.CreateField(dsNodeField)
            wsnoField = ogr.FieldDefn('WSNO', ogr.OFTInteger)
            mainLayer.CreateField(wsnoField)
            nameField = ogr.FieldDefn('Name', ogr.OFTString)
            nameField.SetWidth(254)
            mainLayer.CreateField(nameField)
            subField = ogr.FieldDefn('Subbasin', ogr.OFTInteger)
            mainLayer.CreateField(subField)
            hucSubName = 'HUC{0}'.format(HUCScale)
            hucField = ogr.FieldDefn(hucSubName, ogr.OFTString)
            hucField.SetWidth(24)
            mainLayer.CreateField(hucField)
            subRField = ogr.FieldDefn('SubbasinR', ogr.OFTInteger)
            mainLayer.CreateField(subRField)
            hucSubRName = 'HUC{0}R'.format(HUCScale)
            hucRField = ogr.FieldDefn(hucSubRName, ogr.OFTString)
            hucRField.SetWidth(24)
            mainLayer.CreateField(hucRField)
            totDAField = ogr.FieldDefn('TotDASqKm', ogr.OFTReal)
            mainLayer.CreateField(totDAField)
            lengthField = ogr.FieldDefn('Length', ogr.OFTReal)
            mainLayer.CreateField(lengthField)
            sourceXField = ogr.FieldDefn('SourceX', ogr.OFTReal)
            mainLayer.CreateField(sourceXField)
            sourceYField = ogr.FieldDefn('SourceY', ogr.OFTReal)
            mainLayer.CreateField(sourceYField)
            outletXField = ogr.FieldDefn('OutletX', ogr.OFTReal)
            mainLayer.CreateField(outletXField)
            outletYField = ogr.FieldDefn('OutletY', ogr.OFTReal)
            mainLayer.CreateField(outletYField)
            mainFeatureDefn = mainLayer.GetLayerDefn()
            cursor = conn.cursor()
            sql0 = 'DROP TABLE IF EXISTS Reach'
            cursor.execute(sql0)
            sql1 = MergeDbs._REACHCREATESQL
            cursor.execute(sql1)
            sql2 = 'SELECT * FROM Reach'
            sql3 = 'INSERT INTO Reach VALUES(?,0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
            # When there are multiple outlets from a model the oi.csv file is ambiguous, and fromto includes only one possibility.
            # If QSWAT has made a different choice the project's Reach table is inconsistent with the fromto file.
            # If this inconsistency occurs, the fromto file is corrected provided the alternative downstream HUC is in oiBaseMap
            # Corrections is a table HUC -> downstream HUC
            #fromtoCorrections = dict()
            for projName in self.projNames:
                #print('Project: {0}'.format(projName))
                huc12_10 = projName[3:]
                outlets = self.outlets.setdefault(huc12_10, dict())
                db = os.path.join(self.sourceDir, os.path.join(projName, projName + '.sqlite'))
                with sqlite3.connect(db) as projConn:  # @UndefinedVariable
                    projCursor = projConn.cursor()
                    for row in projCursor.execute(sql2):
                        subbasin = int(row[6])
                        sub = self.subMap[huc12_10][subbasin]
                        if sub < 0:
                            continue  # empty project at level above
                        huc14_12 = self.HUC14_12Map[sub]
                        dsSub = self.dsSubMap[sub]
                        huc14_12R = '' if dsSub == 0 else self.HUC14_12Map[dsSub]
                        if haveFromto:
                            toHUC14_12 = self.fromtoMap.get(huc14_12, '')
                            if toHUC14_12 != huc14_12R:
                                # prefer fromto unless it is empty
                                if toHUC14_12 != '':
                                    choice = toHUC14_12
                                else:
                                    choice = huc14_12R
                                self.log('WARNING: Downstream from {0} in Reach table is {1}, while fromto file gives {2}.  Using {3}'.format(huc14_12, huc14_12R, toHUC14_12, choice))
                                huc14_12R = choice
                                dsSub = self.subMap14.get(choice, (0, 0))[1]
                                self.dsSubMap[sub] = dsSub
                        if dsSub == 0:
                            huc14_12R = ''
                        drainAreaHa = float(self.drainAreas[sub])  # convert numpy float
                        drainAreaKm = drainAreaHa / 100
                        wid2 = float(1.29 * drainAreaKm ** 0.6)
                        dep2 = float(0.13 * drainAreaKm ** 0.4)
                        cursor.execute(sql3, (sub, sub, sub, sub, dsSub, sub, huc14_12, dsSub, huc14_12R, drainAreaHa, row[9],
                                           row[10], wid2, dep2, row[13], row[14], row[15], 200000 + sub, 100000 + sub))
                        # write to fromtoFile
                        if not haveFromto:
                            fromtoWriter.write('{0},{1}\n'.format(huc14_12, huc14_12R))
                        fromtoActWriter.write('{0},{1}\n'.format(huc14_12, huc14_12R))
                #if len(fromtoCorrections) > 0:
                #    correctFromto(fromtoFile, fromtoCorrections)
                projDir = os.path.join(self.sourceDir, projName)
                channelsFile = os.path.join(projDir, 'Watershed/Shapes/channels.shp')
                drv = ogr.GetDriverByName( driverName )
                channelsSource = drv.Open(channelsFile, 0) # 0 means read-only. 1 means writeable.
                channelsLayer = channelsSource.GetLayer()
                for feature in channelsLayer:
                    link0 = int(feature.GetField('LINKNO'))
                    link = self.subbasinChanges[huc12_10].get(link0, link0)
                    if link < 0:  # removed subbasin
                        continue
                    dsNode0 = int(feature.GetField('DSNODEID'))
                    dsNode = self.subbasinChanges[huc12_10].get(dsNode0, dsNode0)
                    if link == dsNode:  # zero length channel for inlet
                        if link == MergeDbs.basePointId:  # used for added inlets
                            dsLink0 = int(feature.GetField('DSLINKNO'))
                            dsLink = self.subbasinChanges[huc12_10].get(dsLink0, dsLink0)
                            dsSub = self.subMap[huc12_10].get(dsLink, -1)
                            if dsSub > 0:
                                huc14_12 = self.HUC14_12Map[dsSub]
                                x = float(feature.GetField('SourceX'))
                                y = float(feature.GetField('SourceY'))
                                self.addedInlets.add((huc14_12, dsLink, x, y))
                        continue
                    name = feature.GetField('NAME')
                    sub = self.subMap[huc12_10][link]
                    if sub > 0:
                        huc14_12 = self.HUC14_12Map[sub]
                        dsSub = self.dsSubMap.get(sub, 0)
                        huc14_12R = '' if dsSub == 0 else self.HUC14_12Map[dsSub]
                        geom = feature.GetGeometryRef().Clone()
                        mainFeature = ogr.Feature(mainFeatureDefn)
                        mainFeature.SetGeometry(geom)
                        mainFeature.SetField('LINKNO', sub)
                        mainFeature.SetField('DSLINKNO', -1 if dsSub == 0 else dsSub)
                        mainFeature.SetField('DSNODEID', -1 if dsSub > 0 else dsNode)
                        mainFeature.SetField('WSNO', sub)
                        mainFeature.SetField('Name', name)
                        mainFeature.SetField('Subbasin', sub)
                        mainFeature.SetField(hucSubName, huc14_12)
                        mainFeature.SetField('SubbasinR', dsSub)
                        mainFeature.SetField(hucSubRName, huc14_12R)
                        mainFeature.SetField('TotDASqKm', float(feature.GetField('TotDASqKm')))
                        mainFeature.SetField('Length', float(feature.GetField('Length')))
                        mainFeature.SetField('SourceX', float(feature.GetField('SourceX')))
                        mainFeature.SetField('SourceY', float(feature.GetField('SourceY')))
                        mainFeature.SetField('OutletX', float(feature.GetField('OutletX')))
                        mainFeature.SetField('OutletY', float(feature.GetField('OutletY')))
                        mainLayer.CreateFeature(mainFeature)
                        if dsSub == 0:
                            outlets[dsNode] = huc14_12
                channelsSource.Destroy()
            mainChannelsSource.Destroy()
            conn.commit()
            if not haveFromto:
                fromtoWriter.close()
            fromtoActWriter.close()
                        
    def makeMonitoringPoint(self, mainDb, mainShapesDir, HUCScale):
        """Create MonitoringPoint table and points shapefile."""
        with sqlite3.connect(mainDb) as conn:  # @UndefinedVariable
            driverName = "ESRI Shapefile"
            mainDrv = ogr.GetDriverByName( driverName )
            mainPointsFile = mainShapesDir + '/points.shp'
            if os.path.exists(mainPointsFile):
                mainDrv.DeleteDataSource(mainPointsFile)
            mainPointsSource = mainDrv.CreateDataSource(mainPointsFile)
            mainLayer = mainPointsSource.CreateLayer('points', self.srs, ogr.wkbPoint)
            idField = ogr.FieldDefn('ID', ogr.OFTInteger)
            mainLayer.CreateField(idField)
            inletField = ogr.FieldDefn('INLET', ogr.OFTInteger)
            mainLayer.CreateField(inletField)
            resField = ogr.FieldDefn('RES', ogr.OFTInteger)
            mainLayer.CreateField(resField)
            ptsrcField = ogr.FieldDefn('PTSOURCE', ogr.OFTInteger)
            mainLayer.CreateField(ptsrcField)
            nameField = ogr.FieldDefn('Name', ogr.OFTString)
            nameField.SetWidth(254)
            mainLayer.CreateField(nameField)
            # TODO: need to find way of getting HUC14_12 number for water body points 
            HUCFieldName = 'HUC{0}'.format(HUCScale)
            HUCField = ogr.FieldDefn(HUCFieldName, ogr.OFTString)
            HUCField.SetWidth(int(HUCScale))
            mainLayer.CreateField(HUCField)
            mainFeatureDefn = mainLayer.GetLayerDefn()
            cursor = conn.cursor()
            sql0 = 'DROP TABLE IF EXISTS MonitoringPoint'
            sql0a = 'DROP TABLE IF EXISTS submapping'
            cursor.execute(sql0)
            cursor.execute(sql0a)
            sql1 = MergeDbs._MONITORINGPOINTCREATESQL
            sql1a = MergeDbs._SUBMAPPINGCREATESQL
            cursor.execute(sql1)
            cursor.execute(sql1a)
            sql2 = 'SELECT * FROM MonitoringPoint'
            sql3 = 'INSERT INTO MonitoringPoint VALUES(?,0,?,?,?,?,?,?,?,?,?,?,?,?,?)'
            sql3a = 'INSERT INTO submapping VALUES(?,?,?)'
            nextObjectId = 1
            maxPid = 0
            for projName in self.projNames:
                huc12_10 = projName[3:]
                #print('HUC12: {0}'.format(huc12_10))
                db = os.path.join(self.sourceDir, os.path.join(projName, projName + '.sqlite'))
                with sqlite3.connect(db) as projConn:  # @UndefinedVariable
                    projCursor = projConn.cursor()
                    for row in projCursor.execute(sql2):
                        if row[10] in {'L', 'T'}:  # outlet
                            subbasin = int(row[11])
                            sub = self.subMap[huc12_10][subbasin]
                            if sub > 0:
                                huc14_12 = self.HUC14_12Map[sub]
                                dsSub = self.dsSubMap.get(sub, 0)
                                typ = 'T' if dsSub == 0 else 'L'
                                cursor.execute(sql3, (nextObjectId, row[2], sub, row[4], row[5], row[6], row[7], row[8],
                                               row[9], typ, sub, huc14_12, 400000 + nextObjectId, 100000 + nextObjectId))
                                cursor.execute(sql3a, (sub, self.selection + str(sub), 1 if dsSub == 0 else 0))
                                nextObjectId += 1
                        #======================================================= added below
                        # elif row[10] == 'W':
                        #     # external inlets will not be in oiMap; need to add these to monitoring points
                        #     subbasin = int(row[11])
                        #     found = False
                        #     for outlets in self.oiMap.values():
                        #         for inlet in outlets.values():
                        #             if inlet == (huc12_10, subbasin):
                        #                 found = True
                        #                 break
                        #         if found:
                        #             break
                        #     if not found:
                        #         sub = self.subMap[huc12_10][subbasin]
                        #         if sub > 0:
                        #             huc14_12 = self.HUC14_12Map[sub]
                        #             cursor.execute(sql3, (nextObjectId, row[2], sub, row[4], row[5], row[6], row[7], row[8],
                        #                            row[9], 'W', sub, huc14_12, 400000 + nextObjectId, 100000 + nextObjectId))
                        #             nextObjectId += 1
                        #=======================================================
                projDir = os.path.join(self.sourceDir, projName)
                pointsFile = os.path.join(projDir, 'Watershed/Shapes/points.shp')
                drv = ogr.GetDriverByName( driverName )
                pointsSource = drv.Open(pointsFile, 0) # 0 means read-only. 1 means writeable.
                pointsLayer = pointsSource.GetLayer()
                for feature in pointsLayer:
                    pid = int(feature.GetField('ID'))
                    maxPid = max(maxPid, pid)
                    inlet = int(feature.GetField('INLET'))
                    res = int(feature.GetField('RES'))
                    ptsrc = int(feature.GetField('PTSOURCE'))
                    huc14_12 = ''
                    # ignore outlets and inlets in oiBasemap, since internal to merged model
                    if inlet == 0:
                        if res == 0:
                            if pid in self.oiBaseMap.get(huc12_10, dict()):
                                continue
                            # should be in outlets
                            huc14_12 = self.outlets.get(huc12_10, dict()).get(pid, '')
                    elif ptsrc == 0:
                        if pid == MergeDbs.basePointId:
                            continue  # added inlets dealt with later
                        found = False
                        for outlets in self.oiBaseMap.values():
                            for inletrec in outlets.values():
                                if inletrec == (huc12_10, pid):
                                    found = True
                                    break
                            if found:
                                break 
                        if found:
                            continue   
                    geom = feature.GetGeometryRef().Clone()
                    mainFeature = ogr.Feature(mainFeatureDefn)
                    mainFeature.SetGeometry(geom)
                    mainFeature.SetField('ID', pid)
                    mainFeature.SetField('INLET', inlet)
                    mainFeature.SetField('RES', res)
                    mainFeature.SetField('PTSOURCE', ptsrc)
                    mainFeature.SetField('Name', feature.GetField('Name'))
                    mainFeature.SetField(HUCFieldName, huc14_12)
                    mainLayer.CreateFeature(mainFeature)
            # add added inlets to MonitoringPoint table
            #print('Added inlets: {0}'.format(self.addedInlets))
            for (huc14_12, subbasin, x, y) in self.addedInlets:
                huc12_10 = huc14_12[:-2]
                sub = self.subMap[huc12_10][subbasin]
                if sub > 0:
                    wkt = "POINT ({0} {1})".format(x, y)
                    pt = ogr.CreateGeometryFromWkt(wkt)
                    pt.Transform(self.toLatlon)
                    cursor.execute(sql3, (nextObjectId, maxPid + 1, sub, x, y, pt.GetX(), pt.GetY(), 0,
                                               '', 'W', sub, huc14_12, 400000 + nextObjectId, 100000 + nextObjectId))
                    nextObjectId += 1
                    mainFeature = ogr.Feature(mainFeatureDefn)
                    # get point in projected coordinates
                    pt = ogr.CreateGeometryFromWkt(wkt)
                    mainFeature.SetGeometry(pt)
                    mainFeature.SetField('ID', maxPid + 1)
                    mainFeature.SetField('INLET', 1)
                    mainFeature.SetField('RES', 0)
                    mainFeature.SetField('PTSOURCE', 0)
                    mainFeature.SetField('Name', '')
                    mainFeature.SetField(HUCFieldName, huc14_12)
                    mainLayer.CreateFeature(mainFeature)
                    maxPid += 1
            mainPointsSource.Destroy()            
            conn.commit()
                                
    def makeWaterBodiesTables(self, mainDb):
        """Make pnd and res tables>"""
        with sqlite3.connect(mainDb) as conn:  # @UndefinedVariable
            cursor = conn.cursor()
            sql0 = 'DROP TABLE IF EXISTS pnd'
            cursor.execute(sql0)
            sql0 = 'DROP TABLE IF EXISTS res'
            cursor.execute(sql0)
            sql0 = 'DROP TABLE IF EXISTS lake'
            cursor.execute(sql0)
            sql0 = 'DROP TABLE IF EXISTS playa'
            cursor.execute(sql0)
            sql1 = MergeDbs._PNDCREATESQL
            cursor.execute(sql1)
            sql1 = MergeDbs._RESCREATESQL
            cursor.execute(sql1)
            sql1 = MergeDbs._LAKECREATESQL
            cursor.execute(sql1)
            sql1 = MergeDbs._PLAYACREATESQL
            cursor.execute(sql1)
            sql2a = 'SELECT * FROM pnd'
            sql2b = 'SELECT * FROM res'
            sql2c = 'SELECT * FROM lake'
            sql2d = 'SELECT * FROM playa'
            sql3a = 'INSERT INTO pnd VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
            sql3b = 'INSERT INTO res VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
            sql3c = 'INSERT INTO lake VALUES(?,?,?,?)'
            sql3d = 'INSERT INTO playa VALUES(?,?,?,?)'
            for projName in self.projNames:
                huc12_10 = projName[3:]
                db = os.path.join(self.sourceDir, os.path.join(projName, projName + '.sqlite'))
                with sqlite3.connect(db) as projConn:  # @UndefinedVariable
                    projCursor = projConn.cursor()
                    oid = 0
                    for row in projCursor.execute(sql2a):
                        subbasin = int(row[1])
                        sub = self.subMap[huc12_10].get(subbasin, -1)
                        if sub > 0:
                            rowl = list(row)
                            oid += 1
                            rowl[0] = oid
                            rowl[1] = sub
                            huc14_12 = self.HUC14_12Map[sub]
                            rowl.append(huc14_12)
                            cursor.execute(sql3a, tuple(rowl))
                        else:
                            print('Cannot find subbasin {0} in {1} for pond'.format(subbasin, huc12_10))
                    oid = 0
                    for row in projCursor.execute(sql2b):
                        subbasin = int(row[1])
                        sub = self.subMap[huc12_10].get(subbasin, -1)
                        if sub > 0:
                            rowl = list(row)
                            oid += 1
                            rowl[0] = oid
                            rowl[1] = sub
                            huc14_12 = self.HUC14_12Map[sub]
                            rowl.append(huc14_12)
                            cursor.execute(sql3b, tuple(rowl))
                        else:
                            print('Cannot find subbasin {0} in {1} for reservoir'.format(subbasin, huc12_10))
                    for row in projCursor.execute(sql2c):
                        subbasin = int(row[1])
                        sub = self.subMap[huc12_10].get(subbasin, -1)
                        if sub > 0:
                            rowl = list(row)
                            oid += 1
                            rowl[0] = oid
                            rowl[1] = sub
                            huc14_12 = self.HUC14_12Map[sub]
                            rowl.append(huc14_12)
                            cursor.execute(sql3c, tuple(rowl))
                        else:
                            print('Cannot find subbasin {0} in {1} for lake'.format(subbasin, huc12_10))
                    oid = 0
                    for row in projCursor.execute(sql2d):
                        subbasin = int(row[1])
                        sub = self.subMap[huc12_10].get(subbasin, -1)
                        if sub > 0:
                            rowl = list(row)
                            oid += 1
                            rowl[0] = oid
                            rowl[1] = sub
                            huc14_12 = self.HUC14_12Map[sub]
                            rowl.append(huc14_12)
                            cursor.execute(sql3d, tuple(rowl))
                        else:
                            print('Cannot find subbasin {0} in {1} for playa'.format(subbasin, huc12_10))
            conn.commit()
                        
                            
    def makeHRUsTable(self, mainDb):
        """Make hrus and uncomb tables"""
        self.landuses = set()
        with sqlite3.connect(mainDb) as conn:  # @UndefinedVariable
            cursor = conn.cursor()
            sql0 = 'DROP TABLE IF EXISTS hrus'
            sql0a = 'DROP TABLE IF EXISTS uncomb'
            cursor.execute(sql0)
            cursor.execute(sql0a)
            sql1 = MergeDbs._HRUSCREATESQL
            cursor.execute(sql1)
            sql1a = MergeDbs._UNCOMBCREATESQL
            cursor.execute(sql1a)
            sql2 = 'SELECT * FROM hrus'
            sql2a = 'SELECT LU_NUM, SLOPE_NUM FROM uncomb WHERE OID=?'
            sql3 = 'INSERT INTO hrus VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
            sql3a = 'INSERT INTO uncomb VALUES(?,?,?,?,?,?,?,?,?,?,?,?)'
            nextHRUId = 1
            for projName in self.projNames:
                huc12_10 = projName[3:]
                db = os.path.join(self.sourceDir, os.path.join(projName, projName + '.sqlite'))
                with sqlite3.connect(db) as projConn:  # @UndefinedVariable
                    projCursor = projConn.cursor()
                    for row in projCursor.execute(sql2):
                        subbasin = int(row[1])
                        sub = self.subMap[huc12_10][subbasin]
                        if sub > 0:
                            huc14_12 = self.HUC14_12Map[sub]
                            HRU_GIS = '{0:0>5d}'.format(sub) + row[12][5:]
                            cursor.execute(sql3, (nextHRUId, sub, huc14_12, row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], 
                                            row[10], nextHRUId, HRU_GIS))
                            # don't use projCursor here or it resets it: using the coonection to execuate creates a new cursor
                            uncombRow = projConn.execute(sql2a, (row[0],)).fetchone()
                            cursor.execute(sql3a, (nextHRUId, sub, huc14_12, uncombRow[0], row[3], row[5], row[5], uncombRow[1], row[7], 
                                           row[9], row[8], row[10]))
                            self.landuses.add(int(uncombRow[0]))
                            nextHRUId += 1
            conn.commit()
            
    def makeElevationBand(self, mainDb):
        """Make ElevationBand table."""
        with sqlite3.connect(mainDb) as conn:  # @UndefinedVariable
            cursor = conn.cursor()
            sql0 = 'DROP TABLE IF EXISTS ElevationBand'
            cursor.execute(sql0)
            sql1 = MergeDbs._ELEVATIONBANDCREATESQL
            cursor.execute(sql1)
            sql2 = 'SELECT * FROM ElevationBand'
            sql3 = 'INSERT INTO ElevationBand VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
            nextOid = 1
            for projName in self.projNames:
                huc12_10 = projName[3:]
                db = os.path.join(self.sourceDir, os.path.join(projName, projName + '.sqlite'))
                with sqlite3.connect(db) as projConn:  # @UndefinedVariable
                    projCursor = projConn.cursor()
                    for row in projCursor.execute(sql2):
                        subbasin = int(row[1])
                        sub = self.subMap[huc12_10][subbasin]
                        if sub > 0:
                            rowl = list(row)
                            rowl[0] = nextOid
                            rowl[1] = sub
                            cursor.execute(sql3, tuple(rowl))
                            nextOid += 1
            conn.commit()
        
                        
    def makeMasterProgress(self, mainDb, mergeDir):
        """Make master progress recod."""
        with sqlite3.connect(mainDb) as conn:  # @UndefinedVariable
            cursor = conn.cursor()
            sql0 = 'DROP TABLE IF EXISTS MasterProgress'
            cursor.execute(sql0)
            sql1 = MergeDbs._MASTERPROGRESSCREATESQL
            cursor.execute(sql1)
            sql2 = 'INSERT INTO MasterProgress VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);'
            workdir, gdb = os.path.split(mainDb)
            gdb = gdb[:-7]
            swatgdb = mergeDir + '/QSWATRef2012.sqlite'
            SWATEDITORVERSION = '2012.10.19'
            cursor.execute(sql2, (workdir, gdb, '', swatgdb, '', '', 'ssurgo', len(self.landuses),
                           1, 1, 0, 0, 1, 0, '', SWATEDITORVERSION, '', 0))
            conn.commit()
            
    def log(self, msg):
        with open(self.logFile, 'a') as log:
            log.write(msg + '\n')
            
    _WATERSHEDCREATESQL = \
    """
    CREATE TABLE Watershed (
        OBJECTID INTEGER,
        Shape    BLOB,
        GRIDCODE INTEGER,
        Subbasin INTEGER,
        HUC14    TEXT,
        Area     REAL,
        Slo1     REAL,
        Len1     REAL,
        Sll      REAL,
        Csl      REAL,
        Wid1     REAL,
        Dep1     REAL,
        Lat      REAL,
        Long_    REAL,
        Elev     REAL,
        ElevMin  REAL,
        ElevMax  REAL,
        Bname    TEXT,
        Shape_Length  REAL,
        Shape_Area    REAL,
        Defined_Area  REAL,
        HRU_Area REAL,
        HydroID  INTEGER,
        OutletID INTEGER
    );
    """
    
    _REMOVEDHUCSCREATESQL = \
    """
    CREATE TABLE RemovedHUCs (
        HUC   TEXT
    );
    """
        
    _REACHCREATESQL = \
    """
    CREATE TABLE Reach (
        OBJECTID INTEGER,
        Shape    BLOB,
        ARCID    INTEGER,
        GRID_CODE INTEGER,
        FROM_NODE INTEGER,
        TO_NODE  INTEGER,
        Subbasin INTEGER,
        HUC14    TEXT,
        SubbasinR INTEGER,
        HUC14R   TEXT,
        AreaC    REAL,
        Len2     REAL,
        Slo2     REAL,
        Wid2     REAL,
        Dep2     REAL,
        MinEl    REAL,
        MaxEl    REAL,
        Shape_Length  REAL,
        HydroID  INTEGER,
        OutletID INTEGER
    );
    """
    
    _HRUSCREATESQL= \
    """
    CREATE TABLE hrus (
        OID      INTEGER,
        SUBBASIN INTEGER,
        HUC14    TEXT,
        ARSUB    REAL,
        LANDUSE  TEXT,
        ARLU     REAL,
        SOIL     TEXT,
        ARSO     REAL,
        SLP      TEXT,
        ARSLP    REAL,
        SLOPE    REAL,
        UNIQUECOMB TEXT,
        HRU_ID   INTEGER,
        HRU_GIS  TEXT
    );
    """
    
    _UNCOMBCREATESQL= \
    """
    CREATE TABLE uncomb (
        OID        INTEGER,
        SUBBASIN   INTEGER,
        HUC14      TEXT,
        LU_NUM     INTEGER,
        LU_CODE    TEXT,
        SOIL_NUM   INTEGER,
        SOIL_CODE  TEXT,
        SLOPE_NUM  INTEGER,
        SLOPE_CODE TEXT,
        MEAN_SLOPE REAL,
        AREA       REAL,
        UNCOMB     TEXT
    );
    """
    
    _RESCREATESQL= \
    """
    CREATE TABLE res (
    OID      INTEGER,
    SUBBASIN INTEGER,
    MORES    INTEGER DEFAULT (1),
    IYRES    INTEGER,
    RES_ESA  REAL,
    RES_EVOL REAL,
    RES_PSA  REAL,
    RES_PVOL REAL,
    RES_VOL  REAL,
    RES_SED  REAL DEFAULT (4000),
    RES_NSED REAL DEFAULT (4000),
    RES_K    REAL DEFAULT (0),
    IRESCO   INTEGER DEFAULT (2),
    OFLOWMX1 REAL DEFAULT (0),
    OFLOWMX2 REAL DEFAULT (0),
    OFLOWMX3 REAL DEFAULT (0),
    OFLOWMX4 REAL DEFAULT (0),
    OFLOWMX5 REAL DEFAULT (0),
    OFLOWMX6 REAL DEFAULT (0),
    OFLOWMX7 REAL DEFAULT (0),
    OFLOWMX8 REAL DEFAULT (0),
    OFLOWMX9 REAL DEFAULT (0),
    OFLOWMX10 REAL DEFAULT (0),
    OFLOWMX11 REAL DEFAULT (0),
    OFLOWMX12 REAL DEFAULT (0),
    OFLOWMN1 REAL DEFAULT (0),
    OFLOWMN2 REAL DEFAULT (0),
    OFLOWMN3 REAL DEFAULT (0),
    OFLOWMN4 REAL DEFAULT (0),
    OFLOWMN5 REAL DEFAULT (0),
    OFLOWMN6 REAL DEFAULT (0),
    OFLOWMN7 REAL DEFAULT (0),
    OFLOWMN8 REAL DEFAULT (0),
    OFLOWMN9 REAL DEFAULT (0),
    OFLOWMN10 REAL DEFAULT (0),
    OFLOWMN11 REAL DEFAULT (0),
    OFLOWMN12 REAL DEFAULT (0),
    RES_RR   REAL DEFAULT (0),
    RESMONO  TEXT DEFAULT('XXXX'),
    IFLOOD1R INTEGER DEFAULT (10),
    IFLOOD2R INTEGER DEFAULT (4),
    NDTARGR  INTEGER DEFAULT (10),
    STARG1   REAL DEFAULT (0),
    STARG2   REAL DEFAULT (0),
    STARG3   REAL DEFAULT (0),
    STARG4   REAL DEFAULT (0),
    STARG5   REAL DEFAULT (0),
    STARG6   REAL DEFAULT (0),
    STARG7   REAL DEFAULT (0),
    STARG8   REAL DEFAULT (0),
    STARG9   REAL DEFAULT (0),
    STARG10  REAL DEFAULT (0),
    STARG11  REAL DEFAULT (0),
    STARG12  REAL DEFAULT (0),
    RESDAYO  TEXT DEFAULT('XXXX'),
    WURESN1  REAL DEFAULT (0),
    WURESN2  REAL DEFAULT (0),
    WURESN3  REAL DEFAULT (0),
    WURESN4  REAL DEFAULT (0),
    WURESN5  REAL DEFAULT (0),
    WURESN6  REAL DEFAULT (0),
    WURESN7  REAL DEFAULT (0),
    WURESN8  REAL DEFAULT (0),
    WURESN9  REAL DEFAULT (0),
    WURESN10 REAL DEFAULT (0),
    WURESN11 REAL DEFAULT (0),
    WURESN12 REAL DEFAULT (0),
    WURTNF   REAL DEFAULT (0),
    IRES1    REAL DEFAULT (1),
    IRES2    REAL DEFAULT (1),
    PSETLR1  REAL DEFAULT (10),
    PSETLR2  REAL DEFAULT (10),
    NSETLR1  REAL DEFAULT (5.5),
    MSETLR2  REAL DEFAULT (5.5),
    CHLAR    REAL DEFAULT (1),
    SECCIR   REAL DEFAULT (1),
    RES_ORGP REAL DEFAULT (0),
    RES_SOLP REAL DEFAULT (0),
    RES_ORGN REAL DEFAULT (0),
    RES_NO3  REAL DEFAULT (0),
    RES_NH3  REAL DEFAULT (0),
    RES_NO2  REAL DEFAULT (0),
    LKPST_CONC REAL DEFAULT(0),
    LKPST_REA  REAL DEFAULT(0.007),
    LKPST_VOL  REAL DEFAULT(0.01),
    LKPST_KOC  REAL DEFAULT(0),
    LKPST_STL  REAL DEFAULT(1),
    LKPST_RSP  REAL DEFAULT(0.002),
    LKPST_MIX  REAL DEFAULT(0.001),
    LKSPSTCONC REAL DEFAULT(0),
    LKSPST_REA REAL DEFAULT(0.05),
    LKSPST_BRY REAL DEFAULT(0.002),
    LKSPST_ACT REAL DEFAULT(0.03),
    RES_D50    REAL DEFAULT(10),
    RESID_FIG  REAL DEFAULT (1),
    EVRSV      REAL DEFAULT (0.6),
    OFLOWMN_FPS REAL DEFAULT(0),
    STARG_FPS  REAL DEFAULT(1),
    RES_SUB    INTEGER,
    HUC14      TEXT
    );
    """
    
    _PNDCREATESQL= \
    """
    CREATE TABLE pnd (
    OID      INTEGER,
    SUBBASIN INTEGER,
    PND_FR   REAL DEFAULT (0),
    PND_PSA  REAL DEFAULT (0),
    PND_PVOL REAL DEFAULT (0),
    PND_ESA  REAL DEFAULT (0),
    PND_EVOL REAL DEFAULT (0),
    PND_VOL  REAL DEFAULT (0),
    PND_SED  REAL DEFAULT (0),
    PND_NSED REAL DEFAULT (0),
    PND_K    REAL DEFAULT (0),
    IFLOD1   INTEGER DEFAULT (10),
    IFLOD2   INTEGER DEFAULT (4),
    NDTARG   INTEGER DEFAULT (10),
    PSETLP1  REAL DEFAULT (0),
    PSETLP2  REAL DEFAULT (0),
    NSETLP1  REAL DEFAULT (0),
    NSETLP2  REAL DEFAULT (0),
    CHLAP    REAL DEFAULT (0),
    SECCIP   REAL DEFAULT (0),
    PND_NO3  REAL DEFAULT (0),
    PND_SOLP REAL DEFAULT (0),
    PND_ORGN REAL DEFAULT (0),
    PND_ORGP REAL DEFAULT (0),
    IPND1    INTEGER DEFAULT(0),
    IPND2    INTEGER DEFAULT(0),
    WET_FR   REAL DEFAULT (0),
    WET_NSA  REAL DEFAULT (0),
    WET_NVOL REAL DEFAULT (0),
    WET_MXSA REAL DEFAULT (0),
    WET_MXVOL REAL DEFAULT (0),
    WET_VOL  REAL DEFAULT (0),
    WET_SED  REAL DEFAULT (0),
    WET_NSED REAL DEFAULT (0),
    WET_K    REAL DEFAULT (0),
    PSETLW1  REAL DEFAULT (0),
    PSETLW2  REAL DEFAULT (0),
    NSETLW1  REAL DEFAULT (0),
    NSETLW2  REAL DEFAULT (0),
    CHLAW    REAL DEFAULT (0),
    SECCIW   REAL DEFAULT (0),
    WET_NO3  REAL DEFAULT (0),
    WET_SOLP REAL DEFAULT (0),
    WET_ORGN REAL DEFAULT (0),
    WET_ORGP REAL DEFAULT (0),
    PNDEVCOEFF REAL DEFAULT (0),
    WETEVCOEFF REAL DEFAULT (0),
    PND_D50  REAL DEFAULT(10),
    HUC14      TEXT
    );
    """
    
    _LAKECREATESQL= \
    """
    CREATE TABLE lake (
    OID       INTEGER,
    SUBBASIN  INTEGER,
    LAKE_AREA REAL,
    HUC14      TEXT
    );
    """
    
    _PLAYACREATESQL= \
    """
    CREATE TABLE playa (
    OID        INTEGER,
    SUBBASIN   INTEGER,
    PLAYA_AREA REAL,
    HUC14      TEXT
    );
    """
    
    _MONITORINGPOINTCREATESQL= \
    """
    CREATE TABLE MonitoringPoint (
        OBJECTID   INTEGER,
        Shape      BLOB,
        POINTID    INTEGER,
        GRID_CODE  INTEGER,
        Xpr        REAL,
        Ypr        REAL,
        Lat        REAL,
        Long_      REAL,
        Elev       REAL,
        Name       TEXT,
        Type       TEXT,
        Subbasin   INTEGER,
        HUC14      TEXT,
        HydroID    INTEGER,
        OutletID   INTEGER
    );
    """
    
    _SUBMAPPINGCREATESQL= \
    """
    CREATE TABLE submapping (
        SUBBASIN    INTEGER,
        HUC_ID      TEXT,
        IsEnding    INTEGER
        );
    """
    
    _MASTERPROGRESSCREATESQL= \
    """
    CREATE TABLE MasterProgress (
        WorkDir            TEXT,
        OutputGDB          TEXT,
        RasterGDB          TEXT,
        SwatGDB            TEXT,
        WshdGrid           TEXT,
        ClipDemGrid        TEXT,
        SoilOption         TEXT,
        NumLuClasses       INTEGER,
        DoneWSDDel         INTEGER,
        DoneSoilLand       INTEGER,
        DoneWeather        INTEGER,
        DoneModelSetup     INTEGER,
        OID                INTEGER,
        MGT1_Checked       INTEGER,
        ArcSWAT_V_Create   TEXT,
        ArcSWAT_V_Curr     TEXT,
        AccessExePath      TEXT,
        DoneModelRun       INTEGER
    );
    """
    
    _ELEVATIONBANDCREATESQL = \
    """
    CREATE TABLE ElevationBand (
    OID INTEGER, 
    SUBBASIN INTEGER, 
    ELEVB1 REAL, 
    ELEVB2 REAL, 
    ELEVB3 REAL, 
    ELEVB4 REAL, 
    ELEVB5 REAL, 
    ELEVB6 REAL, 
    ELEVB7 REAL, 
    ELEVB8 REAL, 
    ELEVB9 REAL, 
    ELEVB10 REAL, 
    ELEVB_FR1 REAL, 
    ELEVB_FR2 REAL, 
    ELEVB_FR3 REAL, 
    ELEVB_FR4 REAL, 
    ELEVB_FR5 REAL, 
    ELEVB_FR6 REAL, 
    ELEVB_FR7 REAL, 
    ELEVB_FR8 REAL, 
    ELEVB_FR9 REAL, 
    ELEVB_FR10 REAL
    );
    """

if __name__ == '__main__': 
    # pattern for choosing region (2 to 12 digits)
    selection = ''
    if len(sys.argv) > 1:
        selection = sys.argv[1]
    if selection == '':
        selection = '11010001'
    print('Selection is {0}'.format(selection))
    isPlus = False
    isCDL = True
    #modelsDir = 'I:/Models/'
    modelsDir = 'G:/HUCModels/'
    # set true for HUC8 projects.
    is8 = True
    # set true for HUC10 projects.
    is10 = True
    # set true for HUC12 projects
    is12 = True
    # set true for HUC14 projects
    is14 = True
    sequence = []
    if is14:
        sequence.append(14)
    if is12:
        sequence.append(12)
    if is10:
        sequence.append(10)
    if is8:
        sequence.append(8)
    for scale in sequence:
        HUCScale = str(scale)
        baseDir = modelsDir + ('SWAT/Fields_CDL/HUC{0}/' if isCDL else 'SWAT/Fields/HUC{0}/').format(HUCScale)
        mergeDir = baseDir + 'Merged' + selection
        sourceDir = baseDir + selection
        if not os.path.isdir(sourceDir):
            print('Cannot find collection {0} to merge'.format(sourceDir))
            continue
        if MergeDbs.runMerge:
            print('Merging HUC{0}'.format(HUCScale))
            m = MergeDbs(mergeDir, sourceDir, selection, isPlus)
            mainDb = mergeDir + '/HUC{0}_{1}.sqlite'.format(HUCScale, selection)
            refDb = sourceDir + '/QSWATRef2012.sqlite'
            waterBodiesFile = sourceDir + '/WaterBodies.sqlite'
            mainShapesDir = os.path.join(mergeDir, 'Watershed/Shapes')
            shutil.copy(refDb, mergeDir)
            shutil.copy(waterBodiesFile, mergeDir)
            m.makeProjNames()
            if scale == 14:
                upCollectionDir = None
                fromtoFile = mergeDir + '/fromto{0}_14.csv'.format(selection)
                if os.path.isfile(fromtoFile):
                    os.remove(fromtoFile)
            else:
                upCollectionDir = modelsDir + ('SWAT/Fields_CDL/HUC{0}/' if isCDL else 'SWAT/Fields/HUC{0}/').format(str(scale+2)) + selection
                upFromto = upCollectionDir + '/fromto.csv'
                if not os.path.isfile(upFromto):
                    print('Cannot find file {0}'.format(upFromto))
                    continue
                fromtoFile = mergeDir + '/fromto{0}_{1}.csv'.format(selection, HUCScale)
                m.patchFromto(upFromto, fromtoFile, scale, selection)
                #shutil.copy(upFromto, fromtoFile)
            m.makeWatershedTable(mainDb, mainShapesDir, HUCScale, upCollectionDir)
            print('Watershed table written')
            if not m.makeOIMap(fromtoFile):
                exit()
            # print('OIMap created: {0}'.format(m.oiMap))
            m.makeDownstreamMap(scale)
            print('Downstream map written')
            m.makeDrainage()
            print('Drainage map written')
            m.makeReachTable(mainDb, mainShapesDir, HUCScale, fromtoFile)
            print('Reach table written')
            m.makeWaterBodiesTables(mainDb)
            print('pnd, res, lake and playa tables written')
            m.makeMonitoringPoint(mainDb, mainShapesDir, HUCScale)
            print('MonitoringPoint table written')
            m.makeHRUsTable(mainDb)
            print('hrus table written')
            m.makeElevationBand(mainDb)
            print('ElevationBand table written')
            m.makeMasterProgress(mainDb, mergeDir)
            print('MasterProgress table written')
        if MergeDbs.runPaths:
            fromtoActFile = mergeDir + '/fromto{0}_{1}_act.csv'.format(selection, HUCScale)
            calcConnections(fromtoActFile, '')
            if scale != 14:
                f, ext = os.path.splitext(fromtoActFile)
                pathLenFile = f + '_len' + ext
                resultFile = mergeDir + '/comparePaths{0}_{1}.txt'.format(selection, HUCScale)
                HAWQSFile = MergeDbs.HAWQSDir + 'hawqs_v1_from-to_huc{0}_{1}len.csv'.format(HUCScale, selection)
                comparePaths(HAWQSFile, pathLenFile, MergeDbs.pathDiffThreshold, outFile=resultFile)
    print('Done')
    exit()
    
