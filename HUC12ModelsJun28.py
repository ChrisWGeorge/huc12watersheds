# -*- coding: utf-8 -*-
'''
Created on Nov 29, 2019

@author: Chris George
copyright: (C) 2019 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
'''

import sqlite3
import time
import os
import posixpath
import glob
import shutil
import subprocess
import csv
import sys
import math
import traceback
import processing  # type: ignore @UnresolvedImport
from processing.core.Processing import Processing  # type: ignore @UnresolvedImport @UnusedImport
from pathlib import Path
# from copy import copy
from osgeo import gdal, ogr  # type: ignore @UnusedImport
from typing import Dict, List, Set, FrozenSet, Tuple, Optional, Union, Callable, Any, TYPE_CHECKING, KeysView  # @UnusedImport

from qgis.core import QgsVectorLayer, QgsExpression, QgsFeatureRequest, QgsApplication, QgsFields, QgsField, QgsVectorFileWriter, QgsWkbTypes, QgsFeature, QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsGeometry, QgsProject, QgsPointXY, QgsVectorDataProvider, QgsCoordinateTransformContext  # @UnresolvedImport
from PyQt5.QtCore import QVariant  # type: ignore @UnresolvedImport



from CreateProject import CreateProject

app = QgsApplication([], False)
QgsApplication.setPrefixPath('C:/Program Files/QGIS 3.16/apps/qgis-ltr', True)
QgsApplication.initQgis()

# ned this to use processing algorithms

class DummyInterface(object):
    def __init__(self):
        self.destCrs = None

    def __getattr__(self, *args, **kwargs):

        def dummy(*args, **kwargs):
            return DummyInterface()

        return dummy

    def __iter__(self):
        return self

    def next(self):
        raise StopIteration

    def layers(self):
        # simulate iface.legendInterface().layers()
        return QgsProject.instance().mapLayers().values()

iface = DummyInterface()

# initalise processing plugin with dummy iface object

plugin = processing.classFactory(iface)
        
class Files:
    """Common file locations."""
    
    HUC12Dir = 'G:/Users/Public/HUC12Watersheds'
    ModelsDir = 'G:/HUCModels'
    
    HUC8ProjectParent = '/HUC8'
    HUC10ProjectParent ='/HUC10'
    HUC12ProjectParent ='/HUC12'
    HUC14ProjectParent = '/HUC14'
    
    DbDir = HUC12Dir + '/Databases'
    
    DataDir = 'I:/Data'
    HUC12Data = DataDir + '/HUC12Watershed.sqlite'
    channels0Db = DataDir + '/combinedChannels0.sqlite'
    NHDDir = DataDir + '/NHDPlusNationalData'
    CatchmentTable = NHDDir + '/combinedCrosswalk.csv'
    CatchmentPatch = NHDDir + '/catchments_patch.csv'
    FromToTable = NHDDir + '/fromtoComids.csv'
    FromToPatch = NHDDir + '/fromto_patch.csv'
    AreasTable = NHDDir + '/catchmentAreas.csv'
    DSTable = NHDDir + '/huc12ds.csv'
    GDB = NHDDir + '/NHDPlusV21_National_Seamless_Flattened_Lower48.gdb'
    huc12Db = NHDDir + '/HUC12.sqlite'
    catchmentDb = NHDDir + '/Catchment.sqlite'
    flowlineDb = NHDDir + '/Flowline.sqlite'
    demDir = DataDir + '/DEM'
    soilDir = DataDir + '/soil_lkey'
    landuseDir = DataDir + '/landuse'
    # submappingToAccess = DataDir + '/submappingToAccess.exe'
    
class Waterbody():
    
    def __init__(self, nidid: str, name: str, year: int, maxVol: float, normVol: float, surfaceArea: float, drainArea: float, lat: float, lon: float, isReservoir: bool) -> None:
        self.nidid = nidid  # only used with ponds
        self.name = name
        self.year = year
        self.maxVol = maxVol  # ha-m
        self.normVol = normVol  #ha-m
        self.surfaceArea = surfaceArea  # ha
        self.drainArea = drainArea      # sqkm
        self.location = (lat, lon)
        self.isReservoir = isReservoir
        self.volEstimated = False
        self.surfaceAreaEstimated = False 
        
    def addWaterbody(self, w: 'Waterbody', isParallel: bool) -> None:
        """Add waterbody w to existing one."""
        if w.year != 0:
            if self.year == 0:
                self.year = w.year
            else:
                self.year = min(self.year, w.year)
        if not self.isReservoir:
            self.nidid = '{0},{1}'.format(self.nidid, w.nidid)
        if self.name is None:
            self.name = w.name
        elif w.name is not None and w.surfaceArea > self.surfaceArea:
            self.name = w.name
        if w.location == self.location:
            self.maxVol = max(self.maxVol, w.maxVol)
            self.normVol = max(self.normVol, w.normVol)
            self.surfaceArea = max(self.surfaceArea, w.surfaceArea)
            self.drainArea = max(self.drainArea, w.drainArea)
        else:
            self.maxVol = self.maxVol + w.maxVol
            self.normVol = self.normVol + w.normVol
            self.surfaceArea = self.surfaceArea + w.surfaceArea
            self.drainArea = self.drainArea + w.drainArea if isParallel or not self.isReservoir else max(self.drainArea, w.drainArea)
            if w.surfaceArea > self.surfaceArea:
                self.location = w.location
        self.volEstimated = self.volEstimated or w.volEstimated
        self.surfaceAreaEstimated = self.surfaceAreaEstimated or w.surfaceAreaEstimated
                
    @staticmethod
    def estimateDepth(area: float) -> float:
        """Use NHDPlus_V2 user guide method (p 51) to estimate depth from area in sq km."""
        if area < 0.5:
            if area < 0.1:
                if area < 0.01:
                    return 0.45
                else:
                    return 0.64
            else:
                return 1.5
        elif area < 2:
            if area < 1:
                return 2
            else:
                return 3
        else:
            return 0
    
class HUC12_10_8InletData:
    
    def __init__(self, inletId: int, inletHUC12_10_8: str) -> None: 
        """Information about inlet."""
        # tuples upperhuc10_8_6, upperhuc12_10_8, outletid
        self.uppers: List[Tuple[str, str, int]] = []
        self.inletId = inletId
        self.inletPoint: Optional[QgsPointXY] = None
        self.inletHUC12_10_8 = inletHUC12_10_8
        
    def addUpper(self, upper: Tuple[str, str, int]):
        if upper not in self.uppers:
            self.uppers.append(upper)
        
    def asString(self) -> str:  
        return 'inletHUC12_10_8: {2}, inletId: {1}, uppers: {0}' \
            .format(self.uppers, self.inletId, self.inletHUC12_10_8)

class HUC14InletData:
    
    def __init__(self, upper: Tuple[str, int], inletId: int, inletComid: int) -> None: 
        """Information about inlet."""
        # tuple of upper HUC, outletComid
        self.uppers: List[Tuple[str, int]] = [upper]
        self.inletId = inletId
        self.inletPoint: Optional[QgsPointXY] = None
        self.inletComid = inletComid
        
    def addUpper(self, upper: Tuple[str, int]) -> None:
        self.uppers.append(upper)
        
    def asString(self) -> str:
        return 'Uppers: {0} inletId: {1} inletComid: {2}' \
            .format(self.uppers, self.inletId, self.inletComid)
        
class HUC12_10_8Catchments:
    """Generating HUC12 and HUC10 models.
    
    In HUC12_10 models, basins are HUC10_8 watersheds and subbasins are HUC12_10 watersheds.  Channels are mergers of flowlines.
    and in plus models channels are flowlines."""
    
    
    def __init__(self, is10: bool, is12: bool, isPlus: bool, isCDL: bool, withProject: bool, runProject: bool, batFile: str, minHRUha: int) -> None:
        '''
        Constructor
        '''
        ## batch rerun file
        self.batchRerun = ''
        ## HUC10 projects
        self.is10 = is10
        ## HUC12 projects
        self.is12 = is12
        ## QSWAT or QSWAT+ projects
        self.isPlus = isPlus
        # use fields_CDL landuse file and lookup
        self.isCDL = isCDL
        ## include project databases and DEM
        self.withProject = withProject
        ## run project
        self.runProject = runProject
        ## project directories for each huc10_8_6
        self.projectDirs: Dict[str, str] = dict()
        ogr.RegisterAll()
        ogr.UseExceptions()
        ## outlet/inlet relation between huc12_10s, converted to combined point ids
        # huc12_10_8 -> point id -> (huc12_10_8, pointid)
        self.oi: Dict[str, Dict[int, Tuple[str, int]]] = dict()
        ## map of HUC10_8_6 to HUC12_10_8 to SWAT number, area in m^2, and drain area in km^2
        self.huc10_8_6Data: Dict[str, Dict[str, Tuple[int, float, float]]] = dict()
        ## map of HUC12_10_8InletData for huc12_10_8 basin
        self.inlets: Dict[str, List[HUC12_10_8InletData]] = dict()
        ## map from huc10_8_6 to watershed outlets: (huc12_10_8, point id, point)
        self.outlets: Dict[str, List[Tuple[str, int, QgsPointXY]]] = dict()
        ## map from comid to flowline start points for each huc10_8_6
        self.startPoints: Dict[str, Dict[int, QgsPointXY]] = dict()
        ## ignorable oulets: outlets used in HUC14_12_10 model not used in HUC12_8_6 model because there is a better alternative
        # or because the corresponding inlet is removed to avoid a circularity
        self.ignorableOutlets: Dict[str, List[Tuple[str, int]]] = dict()
        ## last used inlet or outlet point id for combined watershed
        # This is kept unique across combined watershed so can be used unchanged in single HUC10_8_6 models.
        # Unlike numbers for subbasins there is no need for a contiguous set from 1, so can use same numbers in combined and single projects.
        self.combinedPointId = Common.basePointId
        ## map of (HUC12_10_8 point id) to point id 
        self.combinedToSinglePointId: Dict[Tuple[str, int], int] = dict()
        ## downstream relation between subbasins within an HUC12_10_8 watershed
        # used to decide if waterbodies in different subbasins drain in parallel or in series, for merging them
        self.HUC12_10_8Ds: Dict[str, Dict[int, int]] = dict()
        ## Common functions
        self.common = Common(is10, is12, False, isPlus, batFile, minHRUha)
        ## crs data
        self.crsProject = self.common.crsProject
        ## crs transform
        self.transformToProjected = self.common.transformToProjected
        ## path of combined watershed shapefile
        self.combinedWshed = ''
        ## path of combined channels shapefile
        self.combinedChannels = ''
        ## path of combined points shapefile
        self.combinedPoints = ''
        ## map from combined to separate HUC10_8_6 SWAT numbers for channels and subbasins: int -> (huc12_10_8, int)
        self.combinedToSingle: Dict[int, Tuple[str, int]] = dict()
        channelFields, channelIndexes = self.common.createChannelsFields(extended=True)
        ## channel shapefile fields
        self.channelFields = channelFields
        ## channel shapefile indexes
        self.channelIndexes = channelIndexes
        wshedFields, wshedIndexes = self.common.createWatershedFields()
        ## watershed shapefile fields
        self.wshedFields = wshedFields
        ## watershed shapefile indexes
        self.wshedIndexes = wshedIndexes
        pointsFields, pointsIndexes = self.common.createPointsFields()
        ## points shapefile fields
        self.pointsFields = pointsFields
        ## points shapefile indexes
        self.pointsIndexes = pointsIndexes
        ## connection to combinedChannels0.sqlite
        self.channels0Conn: Optional[Any] = None
        ## water bodies database
        self.waterBodiesDb = ''
        ## water bodies database for previous scale
        self.HUC14_12_10WaterBodiesDb = ''
        
    def getSinglePointId(self, huc12_10_8: str, pointId: int) -> int:
        """Return single point id for huc12_10_8 plus inlet or outlet point id from huc12_10_8 model, and store mapping in self.combinedToSinglePointId."""
        singlePointId = self.combinedToSinglePointId.get((huc12_10_8, pointId), -1)
        if singlePointId < 0:
            self.combinedPointId += 1
            self.combinedToSinglePointId[(huc12_10_8, pointId)] = self.combinedPointId
            return self.combinedPointId
        else:
            return singlePointId
        
    def populateTables(self, HUC14_12_10OI: str, selection: str) -> None:
        """Create tables."""
        # build oi table
        self.oi = dict()
        with open(HUC14_12_10OI, 'r', newline='') as csvFile:
            reader = csv.reader(csvFile)
            _ = next(reader)  # skip header
            for line in reader:
                outHUC12_10_8 = line[0]
                outletId = int(line[1])
                inHUC12_10_8 = line[2]
                inletId = int(line[3])
                if outHUC12_10_8.startswith(selection) and inHUC12_10_8.startswith(selection):
                    self.oi.setdefault(outHUC12_10_8, dict())[outletId] =  (inHUC12_10_8, inletId)
            
    def form(self, selection: str, withShapes: bool) -> Optional[str]:
        """Create HUC12_10 project"""
        #time1 = time.process_time()
        maxLen = 10 if self.is12 else 8 if self.is10 else 6
        if len(selection) > maxLen:
            Common.information('No HUC{0} projects possible'.format(maxLen+2))
            return None
        HUC14_12_10, HUC12_10_8 = ('HUC14', 'HUC12') if self.is12 else ('HUC12', 'HUC10') if self.is10 else ('HUC10', 'HUC8')
        # Choose HUC14 or HUC12 or HUC10 project to use as basis
        # Choose longest directory name consistent with selection (selection starts with it)
        # merely because this gives the smallest amount of data to search.
        upperDir0 = Files.ModelsDir + ('/SWATPlus' if self.isPlus else '/SWAT') + ('/Fields_CDL' if self.isCDL else '/Fields')
        upperDir = upperDir0 + (Files.HUC14ProjectParent if self.is12 
                        else Files.HUC12ProjectParent if self.is10 
                        else Files.HUC10ProjectParent)
        HUC14Dir = upperDir0 + Files.HUC14ProjectParent
        parentPattern = upperDir + '/*'
        nameLength = 0
        longest = ''
        for f in glob.iglob(parentPattern):
            if not os.path.isdir(f):
                continue
            name = os.path.split(f)[1]
            if not selection.startswith(name):
                continue
            if len(name) > nameLength:
                longest = f 
                nameLength = len(name)
        if os.path.isdir(longest):
            HUC14_12_10Parent = longest
            Common.information('{0} chosen as {1} projects to base {2} {3} projects on'.format(HUC14_12_10Parent, HUC14_12_10, selection, HUC12_10_8))
        else:
            Common.error('{3} projects need corresponding {2} projects in {0} to exist, but none found to equal or extend {1}'.format(upperDir, selection, HUC14_12_10, HUC12_10_8))
            return None
        HUC14_12_10Channels = HUC14_12_10Parent + '/combinedChannels.shp'
        if not os.path.isfile(HUC14_12_10Channels):
            Common.error('Cannot find {1} combined channel shapefile {0}'.format(HUC14_12_10Channels, HUC14_12_10))
            return None
        #print('Base channels file: {0}'.format(HUC14_12_10Channels))
        HUC14Pattern = HUC14Dir + '/*'
        nameLength = 0
        HUC14Channels0 = ''
        for f in glob.iglob(HUC14Pattern):
            if not os.path.isdir(f):
                continue
            name = os.path.split(f)[1]
            if not selection.startswith(name):
                continue
            if len(name) > nameLength and os.path.isfile(f + '/combinedChannels0.sqlite'):
                HUC14Channels0 = f + '/combinedChannels0.sqlite'
                nameLength = len(name)
        if os.path.isfile(HUC14Channels0):
            #Common.information('{0} chosen as Channels0 spatialite'.format(HUC14Channels0))
            self.channels0Conn = sqlite3.connect(HUC14Channels0)  # @UndefinedVariable
            assert self.channels0Conn is not None
            self.channels0Conn.enable_load_extension(True)
            self.channels0Conn.execute("SELECT load_extension('mod_spatialite')")
        else:
            Common.error('{2} projects need Channels0 spatialite to exist, but none found in folders that equal or are within {0}/{1}'.format(HUC14Dir, selection, HUC12_10_8))
            return None
        HUC14_12_10Wshed = HUC14_12_10Parent + '/combinedWshed.shp'
        if not os.path.isfile(HUC14_12_10Wshed):
            Common.error('Cannot find {1} combined watershed shpefile {0}'.format(HUC14_12_10Wshed, HUC14_12_10))
            return None
        HUC14_12_10OI = HUC14_12_10Parent + '/oi.csv'
        if not os.path.isfile(HUC14_12_10OI):
            Common.error('Cannot find {1} inlet/outlet connections file {0}'.format(HUC14_12_10OI, HUC14_12_10))
            return None
        self.HUC14_12_10WaterBodiesDb = HUC14_12_10Parent + '/WaterBodies.sqlite'
        if not os.path.isfile(self.HUC14_12_10WaterBodiesDb):
            Common.error('Cannot find {1} water bodies file {0}'.format(self.HUC14_12_10WaterBodiesDb, HUC14_12_10))
            return None
        #print('Upper waterbodies database is {0}'.format(self.HUC14_12_10WaterBodiesDb))
        extDir = '/Fields_CDL' if self.isCDL else '/Fields'
        SWATDir = '/SWATPlus' + extDir if self.isPlus else '/SWAT' + extDir
        parent = Files.ModelsDir + SWATDir + (Files.HUC12ProjectParent if self.is12 
                  else Files.HUC10ProjectParent if self.is10
                  else Files.HUC8ProjectParent) + '/' + selection
        if not os.path.isdir(parent):
            os.makedirs(parent)
        problemsFile = posixpath.join(parent, 'problems.txt')
        if not (withShapes or self.withProject):
            return problemsFile
        self.batchRerun = posixpath.join(parent, 'rerun.bat')
        if os.path.isfile(self.batchRerun):
            os.remove(self.batchRerun)
        self.combinedChannels = parent + '/combinedChannels.shp'
        combinedChannelsLayer = QgsVectorLayer('LineString?crs=epsg:5072', 'channels_layer', 'memory')
        channelsProvider = combinedChannelsLayer.dataProvider()
        channelsProvider.addAttributes(self.channelFields.toList())
        combinedChannelsLayer.updateFields()
        self.combinedWshed = parent + '/combinedWshed.shp'
        combinedWshedLayer = QgsVectorLayer('Polygon?crs=epsg:5072', 'wshed_layer', 'memory')
        wshedProvider = combinedWshedLayer.dataProvider()
        wshedProvider.addAttributes(self.wshedFields.toList())
        combinedWshedLayer.updateFields()
        self.combinedPoints = parent + '/combinedPoints.shp'
        self.waterBodiesDb = parent + '/WaterBodies.sqlite'
        if not os.path.isfile(self.waterBodiesDb) or withShapes:
            shutil.copy(posixpath.join(Files.DbDir, 'WaterBodies.sqlite'), self.waterBodiesDb)
        with open(problemsFile, 'w') as problems:
            self.populateTables(HUC14_12_10OI, selection)
            self.projectDirs = dict()
            self.outlets = dict()
            self.ignorableOutlets = dict()
            self.inlets = dict()
            self.huc10_8_6Data = dict()
            # make a project for each huc10_8 prefix amongst the huc12_10s
            # use HUC14_12_10 combined watershed to create HUC12_10 combined watershed
            # and to make map of HUC10_8 and HUC12_10 
            self.makeCombinedWshed(HUC14_12_10Wshed, combinedWshedLayer, selection)
            # use HUC14_12_10 combined channel network to create HUC12_10 combined channel network
            self.makeCombinedChannels(HUC14_12_10Channels, HUC14Channels0, combinedChannelsLayer, selection, problems)
            self.fixChannels(combinedChannelsLayer, problems)
            self.makeCombinedPoints(problems)
            self.mapCombinedToSingle(combinedChannelsLayer)
            self.makeWaterBodies(problems, parent, selection)
            for huc10_8_6 in sorted(self.huc10_8_6Data):
                Common.information('Creating project {0}'.format(huc10_8_6))
                project = CreateProject(huc10_8_6, parent, Files.DbDir, self.isPlus, self.withProject, False)
                projDir = project.projDir
                self.projectDirs[huc10_8_6] = projDir
                self.writeWatershedShapefile(huc10_8_6, combinedWshedLayer, projDir)
                self.writeChannelsShapefile(huc10_8_6, combinedChannelsLayer, combinedWshedLayer, projDir, problems)
#  TODO:          if self.isPlus:
#                     self.writeChannelsShapefile(huc10_8_6, projDir, problems)
                if self.withProject:
                    region = huc10_8_6[:2]
                    Common.addRaster(region, 'DEM', self.isPlus, self.isCDL, projDir)
                    Common.addRaster(region, 'soil', self.isPlus, self.isCDL, projDir)
                    Common.addRaster(region, 'landuse', self.isPlus, self.isCDL, projDir)
                projDir = self.projectDirs[huc10_8_6]
                pointsFile = projDir + '/Watershed/Shapes/points.shp'
                self.writePointsShapefile(huc10_8_6, pointsFile)
            error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(combinedWshedLayer, self.combinedWshed, QgsCoordinateTransformContext(), self.common.options)
            if error != 0:  # QgsVectorFileWriter.NoError
                Common.error('Failed to write watershed file {0}: {1}'.format(self.combinedWshed, msg))
            error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(combinedChannelsLayer, self.combinedChannels, QgsCoordinateTransformContext(), self.common.options)
            if error != 0:  # QgsVectorFileWriter.NoError
                Common.error('Failed to write channels file {0}: {1}'.format(self.combinedChannels, msg))
        self.alignInletsOutlets(selection, parent) 
        #time2 = time.process_time()
        #formTime = time2 - time1
        #Common.information('Forming projects took {0} seconds'.format(int(formTime + 0.5)))
        return problemsFile
    
    def makeCombinedWshed(self, HUC14_12_10Wshed: str, combinedWshedLayer: QgsVectorLayer, selection: str):
        """Make HUC12_10 combined watershed from HUC14_12_10.  And make huc10_8Data."""
        HUC14_12_10Layer = QgsVectorLayer(HUC14_12_10Wshed, 'HUC14_12_10', 'ogr')
        HUC14_12_10Provider = HUC14_12_10Layer.dataProvider()
        HUC14_12_10Fields = HUC14_12_10Provider.fields()
        huc12_10_8Index = HUC14_12_10Fields.indexFromName(self.common.HUC14_12_10_8String)
        combinedWshedProvider = combinedWshedLayer.dataProvider()
        polys: Dict[str, List[QgsFeature]] = dict()
        for feature in HUC14_12_10Provider.getFeatures():
            huc12_10_8 = feature[huc12_10_8Index]
            if huc12_10_8.startswith(selection):
                polys.setdefault(huc12_10_8, []).append(feature)
        # merge polygons and write to HUC12_10_8 combined watershed
        areaIndex = HUC14_12_10Fields.indexFromName('Area')
        SWATNum = 0
        for huc12_10_8, features in polys.items():
            huc12_10_8Data = self.huc10_8_6Data.setdefault(huc12_10_8[:-2], dict())
            poly = QgsGeometry.unaryUnion([f.geometry().makeValid() for f in features])
            if poly.isEmpty():
                # just collect the polygons into a multipolygon
                f0 = features.pop()
                poly = f0.geometry()
                area = f0[areaIndex]
                for feature in features:
                    nexArea = feature[areaIndex]
                    nextGeom = feature.geometry().makeValid()
                    merge = Common.polyCombine(poly, nextGeom)
                    poly = merge
                    area += nexArea
            else:
                area = poly.area()
            huc12_10_8Feature = QgsFeature(self.wshedFields)
            SWATNum += 1
            huc12_10_8Feature.setAttributes([SWATNum, area, SWATNum, huc12_10_8[:-2], huc12_10_8])
            huc12_10_8Feature.setGeometry(poly)
            combinedWshedProvider.addFeatures([huc12_10_8Feature])
            huc12_10_8Data[huc12_10_8] = (SWATNum, area, 0.0)  # drain area added later
        
    def makeCombinedChannels(self, HUC14_12_10Channels: str, HUC14Channels0: str, combinedChannelsLayer: QgsVectorLayer, selection: str, problems):  # @UnusedVariable
        """Make combined network shapefile."""        
        HUC14_12_10Layer = QgsVectorLayer(HUC14_12_10Channels, 'channels', 'ogr')
        HUC14_12_10Provider = HUC14_12_10Layer.dataProvider()
        HUC14_12_10Fields = HUC14_12_10Provider.fields()
        linkIndex = HUC14_12_10Fields.indexFromName('LINKNO')
        dsLinkIndex = HUC14_12_10Fields.indexFromName('DSLINKNO')
        dsNodeIndex = HUC14_12_10Fields.indexFromName('DSNODEID')
        orderIndex = HUC14_12_10Fields.indexFromName('streamorde')
        drainAreaIndex = HUC14_12_10Fields.indexFromName('TotDASqKm')
        nameIndex = HUC14_12_10Fields.indexFromName('NAME')
        huc12_10_8Index = HUC14_12_10Fields.indexFromName(self.common.HUC14_12_10_8String)
        huc14_12_10Index = HUC14_12_10Fields.indexFromName(self.common.upperString)
        lengthIndex = HUC14_12_10Fields.indexFromName('Length')
        sourceXIndex = HUC14_12_10Fields.indexFromName('SourceX')
        sourceYIndex = HUC14_12_10Fields.indexFromName('SourceY')
        outletXIndex = HUC14_12_10Fields.indexFromName('OutletX')
        outletYIndex = HUC14_12_10Fields.indexFromName('OutletY')
#         HUC14Layer0 = QgsVectorLayer(HUC14Channels0, 'channels0', 'ogr')
#         HUC14Provider0 = HUC14Layer0.dataProvider()
#         HUC14Fields0 = HUC14Provider0.fields()
#         orderIndex0 = HUC14Fields0.indexFromName('streamorde')
        dsSWAT: Dict[int, int] = dict()
        self.HUC12_10_8Ds = dict()
                    
        def getBest(candidates: List[QgsFeature]) -> QgsFeature:
            """Find feature from candidates with the highest order, and then the greatest drainage area if more than one with same highest order"""
            if len(candidates) == 1:
                #print('Best by default {0}'.format(candidates[0][linkIndex]))
                return candidates[0]
            currentBestOrder = -10  # order can be -9 for coastline channels
            currentOrderCandidates: List[QgsFeature] = []
            for feature in candidates:
                order = feature[orderIndex]
                if order < currentBestOrder:
                    continue
                if order > currentBestOrder:
                    currentBestOrder = order
                    currentOrderCandidates = [feature]
                else:
                    currentOrderCandidates.append(feature) 
            if len(currentOrderCandidates) == 1:
                #print('Best on order {0}'.format(currentOrderCandidates[0][linkIndex]))
                return currentOrderCandidates[0]
            candidateLinks = [f[linkIndex] for f in currentOrderCandidates]  # @UnusedVariable (used in print below)
            currentBestFeature = currentOrderCandidates.pop()
            currentBestArea = currentBestFeature[drainAreaIndex]
            for feature in currentOrderCandidates:
                area = feature[drainAreaIndex]
                if area > currentBestArea:
                    currentBestFeature = feature
                    currentBestArea = area
            #print('Best on area {0} from {1}'.format(currentBestFeature[linkIndex], candidateLinks))
            return currentBestFeature
        
        def extendChannel(features: List[QgsFeature], inletFeature: QgsFeature, channel: List[QgsFeature]) -> None:
            """Add inletFeature and any downstream from it that are in features to channel, unless already there."""
            if inletFeature in channel:
                return  # no need to search downstream since orders can only increase downstream
            channel.append(inletFeature)
            #print('channel {0}'.format([f[linkIndex] for f in channel]))
            dsLink = inletFeature[dsLinkIndex]
            for dsFeature in features:
                if dsFeature[linkIndex] == dsLink:
                    extendChannel(features, dsFeature, channel)
        
        def linkInletToChannel(link: int, channelLinks: List[int], soFar: List[int], huc12_10_8Features: List[QgsFeature]) -> Tuple[bool, List[int]]:
            """Add inlet link and its links downchannel to channel if necessary, returning list of link values of features to add."""
            if link in channelLinks:
                return True, soFar
            elif link > Common.basePointId:  # outlet
                return False, []
            else:
                soFar.append(link)
                for f in huc12_10_8Features:
                    if link == f[linkIndex]:
                        return linkInletToChannel(f[dsLinkIndex], channelLinks, soFar, huc12_10_8Features)
                return False, []
            
        def removeLowerOrder(HUC14_12_10: str) -> QgsGeometry:
            """Collect geometries from channels0 of channels with HUC14_12_10 attribute,
            remove those below minimum order, merge geometries of remainder and return.
            Minimum order is 2 for HUC12, 3 for HUC10, 4 for HUC8."""
#             if self.is12:
#                 minOrder = 2
#                 exp = QgsExpression('"{0}" = {1}'.format(self.common.upperString, HUC14_12_10))
#             elif self.is10:
#                 minOrder = 3
#                 exp = QgsExpression('"{0}" = {1}'.format(self.common.upperString, HUC14_12_10))
#             else:
#                 minOrder = 4
#                 exp = QgsExpression('LEFT("HUC12", 10) = {0}'.format(HUC14_12_10))
#             channels1 = [channel for channel in HUC14Provider0.getFeatures(QgsFeatureRequest(exp))]
#             if len(channels1) == 1:
#                 return channels1[0].geometry()
#             channels2 = []
#             while len(channels2) == 0:
#                 channels2 = [channel for channel in channels1 if int(channel[orderIndex0]) >= minOrder]
#                 minOrder -= 1
#             return QgsGeometry.unaryUnion([channel.geometry() for channel in channels2])
            if self.is12:
                minOrder = 2
                sql1 = 'SELECT AsText(geometry) FROM combinedchannels0 WHERE HUC14 = "{0}" AND streamorde >= {{0}}'.format(HUC14_12_10)
            elif self.is10:
                minOrder = 3
                sql1 = 'SELECT AsText(geometry) FROM combinedchannels0 WHERE HUC12 = "{0}" AND streamorde >= {{0}}'.format(HUC14_12_10)
            else: # 8
                minOrder = 4
                sql1 = 'SELECT AsText(geometry) FROM combinedchannels0 WHERE HUC10 = "{0}" AND streamorde >= {{0}}'.format(HUC14_12_10)
            geoms: List[QgsGeometry] = []
            assert self.channels0Conn is not None
            while len(geoms) == 0:
                geoms = [QgsGeometry.fromWkt(row[0]) for row in self.channels0Conn.execute(sql1.format(minOrder))]
                minOrder -= 1
            return QgsGeometry.unaryUnion(geoms)
        
        def removeUpstream(features: List[QgsFeature]) -> List[QgsFeature]:
            """Remove features which have a downstream link also in features,
            but make sure non-empty list is returned, in case there is a circularity."""
            links = [feature[linkIndex] for feature in features]
            for feature in features:
                if len(links) == 1:
                    break
                if feature[dsLinkIndex] in links:
                    links.remove(feature[linkIndex])
            return [feature for feature in features if feature[linkIndex] in links]
        
        def reachableFrom(target: int, source: int) -> bool:
            """Return True if target reachable from source via dsSWAT relation,
            i.e. return True if target is downstream from source.
            
            Algorithm assumes dsSWAT not circular, so reachableFrom(t, s) should be checked 
            false before defining dsSWAT[t] = s"""
            nxt = dsSWAT.get(source, -1)
            if nxt == -1:
                return False
            elif nxt == target:
                return True
            else:
                return reachableFrom(target, nxt)
            
        def makeDs(huc12_10_8: str, channels: List[QgsFeature]) -> None:
            """Create or extend ds relation for the channels in self.HUC12_10_8Ds."""
            ds = self.HUC12_10_8Ds.setdefault(huc12_10_8, dict())
            for channel in channels:
                link = channel[linkIndex]
                dsLink = channel[dsLinkIndex]
                if link < Common.basePointId and dsLink > 0:
                    ds[link] = dsLink
                    
        # code for makeCombinedChannels
        combinedChannelsProvider = combinedChannelsLayer.dataProvider()
        # necessary to fix list for later comprehensions to work
        features = list(HUC14_12_10Provider.getFeatures())
        for huc12_10_8Data in self.huc10_8_6Data.values():
            for huc12_10_8, (SWATNum, area, _) in huc12_10_8Data.items():
                huc10_8_6 = huc12_10_8[:-2]
                print('Forming channel for HUC12_10_8 {0}'.format(huc12_10_8))
                # HUC14_12_10 combined channels has local inlet and outlet numnbers for each HUC12_10_8
                # dsNodeId -2 for closed basins, with dummy streams which we ignore
                huc12_10_8Features = [f for f in features if f[huc12_10_8Index] == huc12_10_8 and f[dsNodeIndex] != -2]
                makeDs(huc12_10_8, huc12_10_8Features)
                inletFeatures = [f for f in huc12_10_8Features if f[linkIndex] > Common.basePointId]
                #print('Inlet features: {0}'.format([f[linkIndex] for f in inletFeatures]))
                exitFeatures = [f for f in huc12_10_8Features if f[dsLinkIndex] < 0]
                #print('Exit features: {0}'.format([f[linkIndex] for f in exitFeatures]))
                if len(exitFeatures) == 0:  # check if closed basin
                    if len(huc12_10_8Features) > 0:
                        problems.write('No exit channel in HUC12_10_8 {0}\n'.format(huc12_10_8))
                        # use all features, but try to remove features with a downstream link also in features
                        exitFeatures = removeUpstream(huc12_10_8Features)
                dsSWATNum = -1
                # First strategy was to include channels upstream from outlet, choosing best at junctions
                # This can leave some areas empty of channels
                # Alternative strategy is to include all channels with order at least 2 less than exit channel's order,
                # plus inlet channels, so that all inlets are connected
                # However, these channels are merged, and merged channel given maximum order of its components
                # so that an order two channel, say, may have several order 1 components
                # So we remake each channels geometry by removing its low order components
                # from the original channel0 map and re-merging them
                channel = huc12_10_8Features
                if len(channel) == 0:
                    # closed basin
                    dsNodeId = -2
                    dsSWATNum = -1
                    lnarea = math.log(area)
                    lnlength = 1.54 + 0.445 * lnarea  # TODO: 03 catchment formula
                    length = math.exp(lnlength)
                    # choose an arbitrary dummy channel
                    channels = [f for f in features if f[huc12_10_8Index] == huc12_10_8 and f[dsNodeIndex] == -2]
                    if len(channels) == 0:
                        Common.error('Cannot find any channels in closed basin {0}'.format(huc12_10_8))
                        continue
                    channel0 = channels[0]
                    line = channel0.geometry()
                    sourcePoint = Common.firstPoint(line)
                    outletPoint = Common.lastPoint(line)
                    order = 1
                    drainArea = area / 1E6  # convert to sq km
                    # add drain area to HUC12_10Data
                    huc12_10_8Data[huc12_10_8] = (SWATNum, area, drainArea)
                    name = ''
                else:
                    exitFeature = getBest(exitFeatures)
                    outletData = (huc12_10_8, exitFeature[dsNodeIndex], QgsPointXY(exitFeature[outletXIndex], exitFeature[outletYIndex]))
                    self.outlets.setdefault(huc10_8_6, []).append(outletData)
                    #print('Possible outlet {0}'.format(outletData))
                    # mark any other outlets as ignorable, to avoid later error failing to match with inlet
                    exitFeatures.remove(exitFeature)
                    if len(exitFeatures) > 0:
                        for exitFeature2 in exitFeatures:
                            # don't mark as ignorable if it shares its dsNodeId with the one to keep, or that will be ignorable
                            if exitFeature2[dsNodeIndex] != exitFeature[dsNodeIndex]:  
                                self.ignorableOutlets.setdefault(huc10_8_6, []).append((huc12_10_8, exitFeature2[dsNodeIndex]))
                                #print('ignorable outlet: {0}'.format((huc12_10_8, exitFeature2[dsNodeIndex])))
                    dsNodeId = exitFeature[dsNodeIndex]
                    # add all inlets even if not currently in channel, since selected channel might not have included them
                    #print('Channel is {0}'.format([f[linkIndex] for f in channel]))
                    for f in inletFeatures:
                        inletData = HUC12_10_8InletData(f[linkIndex], huc12_10_8)
                        inletData.inletPoint = QgsPointXY(f[sourceXIndex], f[sourceYIndex])
                        self.inlets.setdefault(huc10_8_6, []).append(inletData)
                        #print('Possible inlet {0}'.format(inletData.asString()))
                    #print('Channel is {0}'.format([f[linkIndex] for f in channel]))
                    # before merging channels, find furthest source point from channel exit,
                    # and calculate length along path from there to exit
                    # Note this might involve a low order channel segmnent that is later removed
                    # but the eventual channel selection is for visual consumption
                    # while the longer the channels we use to calculate slopes the better
                    candidates2 = {i: QgsPointXY(channel[i][sourceXIndex], channel[i][sourceYIndex]) for i in range(len(channel))}
                    outletPoint = QgsPointXY(exitFeature[outletXIndex], exitFeature[outletYIndex])
                    index, sourcePoint = Common.furthest(candidates2, outletPoint)  # type: ignore
                    sourceFeature = channel[index]
                    # calculate length of path from source to outlet
                    length = sourceFeature[lengthIndex]
                    currentFeature = sourceFeature
                    outletLink = exitFeature[linkIndex]
                    chain = [currentFeature[linkIndex]]
                    while True:
                        currentLink = currentFeature[linkIndex]
                        if currentLink == outletLink:
                            break
                        nextLink = currentFeature[dsLinkIndex]
                        if nextLink in chain:
                            chain.append(nextLink)
                            Common.error('Circular channel.  Links are {0}'.format(chain))
                            break
                        chain.append(nextLink)
                        found = False
                        for f in channel:
                            if f[linkIndex] == nextLink:
                                currentFeature = f
                                length += f[lengthIndex]
                                found = True
                                break
                        if not found:
                            # nextLink is not in channel: we have found a different exit, but we can use it
                            break
                    # merge channel segments
                    feature = channel.pop(0)
                    #print('dsNode is {0}'.format(dsNodeId))
                    order = feature[orderIndex]
                    drainArea = feature[drainAreaIndex]
                    # add drain area to HUC12_10Data
                    huc12_10_8Data[huc12_10_8] = (SWATNum, area, drainArea)
                    #print('Drainage area for {0} in {1} set to {2}'.format(SWATNum, huc12_10_8, drainArea))
                    name = feature[nameIndex]
                    nameOrder = order
                    # instead of just using the geometry of the feature we use the original channel0 channels,
                    # removing low order ones and merging
                    line = removeLowerOrder(feature[huc14_12_10Index])
                    #print('Link {0} has type {1} WKBType {2}'.format(feature[linkIndex], line.type(), line.wkbType()))
                    for feature in channel:
                        if name == '' or feature[orderIndex] > nameOrder:  # give preference to higher order flowlines for choosing name of channel
                            name = feature[nameIndex]
                            nameOrder = feature[orderIndex]
                        nextGeom = removeLowerOrder(feature[huc14_12_10Index])
                        #print('Link {0} has type {1} WKBType {2}'.format(feature[linkIndex], nextGeom.type(), nextGeom.wkbType()))
                        merge = line.combine(nextGeom)
                        if merge is None:
                            merge = Common.linkCombine(line, nextGeom)
                        line = merge
                        order = max(order, feature[orderIndex])
                huc12_10_8Feature = QgsFeature(self.channelFields)
                # dsLink and dsNodeId set properly when is12 in fixChannels
                assert sourcePoint is not None
                huc12_10_8Feature.setAttributes([SWATNum, dsSWATNum, dsNodeId, SWATNum, order, drainArea, name, huc12_10_8[:-2],
                                               huc12_10_8, length, sourcePoint.x(), sourcePoint.y(), outletPoint.x(), outletPoint.y()])
                #print('Channel length: {0}'.format(line.length()))
                huc12_10_8Feature.setGeometry(line)
                ok, _ = combinedChannelsProvider.addFeatures([huc12_10_8Feature])
                if not ok:
                    Common.error('Failed to add combined channel {0} for {1} '.format([f[linkIndex] for f in channel], huc12_10_8))
                    for error in combinedChannelsProvider.errors():
                        print(error)
                        
        
    def fixChannels(self, combinedChannelsLayer: QgsVectorLayer, problems):
        """Correct dsLink and dsNodeId fields.  Remove internal inlets/outlets and and ignorable outlets"""
        
        def upperIsIgnorable(upper: Tuple[str, str, int]) -> bool:
            """Return True if upper matches an ignorable."""
            HUC10_8_6, HUC12_10_8, upperId = upper
            for (outletHUC, outletId) in self.ignorableOutlets.get(HUC10_8_6, []):
                if outletHUC == HUC12_10_8 and outletId == upperId:
                    return True
            return False
        
        def reachable(dsHUC10_8_6: str, huc10_8_6: str, hucUs: Dict[str, Set[str]]) -> bool:
            """Return True if dsHUC10_8_6 is reachable by going upstream from huc10_8_6.
            If so, adding a downstream connection from huc10_8_6 to dsHUC10_8_6 would introduce a circularity.
            Assumes hucUs is not already circular, or may not terminate."""
            us = hucUs.get(huc10_8_6, set())
            if dsHUC10_8_6 in us:
                return True
            for huc in us:
                if reachable(dsHUC10_8_6, huc, hucUs):
                    return True
            return False
            
        channelsProvider = combinedChannelsLayer.dataProvider()
        linkIndex = self.channelIndexes['LINKNO']
        dsLinkIndex = self.channelIndexes['DSLINKNO']
        dsNodeIndex = self.channelIndexes['DSNODEID']
        huc10_8_6Index = self.channelIndexes[self.common.HUC12_10_8_6String]
        huc12_10_8Index = self.channelIndexes[self.common.HUC14_12_10_8String]
        # upstream relation for HUC10_8_6s: used to detect circularities between projects
        hucUs: Dict[str, Set[str]] = dict()
        # downstream relation for SWATNums
        ds: Dict[int, int] = dict()
        maxSWATNum = 0
        mmap: Dict[int, Dict[int, Any]] = dict()
        #print('There are {0} channels'.format(channelsProvider.featureCount()))
        for feature in channelsProvider.getFeatures():
            SWATNum = feature[linkIndex]
            maxSWATNum = max(maxSWATNum, SWATNum)
            dsSWATNum = feature[dsLinkIndex]
            dsNodeId = feature[dsNodeIndex]
            huc12_10_8 = feature[huc12_10_8Index]
            huc10_8_6 = feature[huc10_8_6Index]
            # dsLink, dsNode values
            if True: # self.is12:
                oiData = self.oi.get(huc12_10_8, None)
                #print('oiData for HUC {0}: {1}.  dsNodeId is {2}'.format(huc12_10_8, oiData, dsNodeId))
                if oiData is not None:
                    (dsHUC12_10_8, dsPointId) = oiData.get(dsNodeId, (None, 0))
                    if dsHUC12_10_8 is not None:
                        dsHUC10_8_6 = dsHUC12_10_8[:-2]
                        if dsHUC12_10_8.startswith(huc10_8_6):
                            #print('Removing outlet {0} {1} to inlet {2} {3}'.format(huc12_10_8, dsNodeId, dsHUC12_10_8, dsPointId))
                            # remove internal outlet
                            outlets = self.outlets.get(huc10_8_6, [])
                            #print('Outlets for {0}: {1}'.format(huc10_8_6, outlets))
                            for i in range(len(outlets)):
                                outletHUC, outletId, _ = outlets[i]
                                if outletHUC == huc12_10_8 and outletId == dsNodeId:
                                    del outlets[i]
                                    break
                            #print('  after removing internal: {0}'.format(outlets))
                            # fill in outlet (upper) details of possible inlet:
                            for inlet in self.inlets.get(dsHUC10_8_6, []):
                                if inlet.inletHUC12_10_8 == dsHUC12_10_8 and inlet.inletId == dsPointId:
                                    inlet.addUpper((huc10_8_6, huc12_10_8, dsNodeId))
                                    break
                            dsHUC12_10_8Data = self.huc10_8_6Data.get(dsHUC10_8_6, dict())
                            (dsSWATNum, _, _) = dsHUC12_10_8Data.get(dsHUC12_10_8, (-1, 0, 0))
                            if dsSWATNum < 0:
                                problems.write('Failed to find data for HUC12_10_8 {0}\n'.format(dsHUC12_10_8))
                        else:    # different projects: check if we have a circularity
                            if reachable(dsHUC10_8_6, huc10_8_6, hucUs):
                                print('Circularity between {0} and {1}: removing inlet to {1} from {0}'.format(huc10_8_6, dsHUC10_8_6))
                                problems.write('Circularity between {0} and {1}: removing inlet to {1} from {0}\n'.format(huc10_8_6, dsHUC10_8_6))
                                # inlets list indexes to delete
                                indexesToDelete: Set[int] = set()
                                inlets = self.inlets.get(dsHUC10_8_6, [])
                                for i in range(len(inlets)):
                                    inlet = inlets[i]
                                    if inlet.inletHUC12_10_8 == dsHUC12_10_8 and inlet.inletId == dsPointId:
                                        #print('Removing upper from {0} to inlet {1}'.format(huc12_10_8, inlet.asString()))
                                        # remove the upper
                                        inlet.uppers = [upper for upper in inlet.uppers if upper[1] != huc12_10_8]
                                        if len(inlet.uppers) == 0:
                                            indexesToDelete.add(i)
                                        break
                                inlets = [inlets[i] for i in range(len(inlets)) if i not in indexesToDelete]
                                if len(inlets) == 0:
                                    del self.inlets[dsHUC10_8_6]
                                else:
                                    self.inlets[dsHUC10_8_6] = inlets
                                # make the outlet ignorable so not reported later as unmatched
                                ignorables = self.ignorableOutlets.get(huc10_8_6, [])
                                if (huc12_10_8, dsNodeId) not in ignorables:
                                    ignorables.append((huc12_10_8, dsNodeId))
                                    self.ignorableOutlets[huc10_8_6] = ignorables
                            else:
                                hucUs.setdefault(dsHUC10_8_6, set()).add(huc10_8_6)
                        # The loop above to fill in uppers only works for internal outlet-inlet pairs
                        # Check for external upper data
                        for upperHUC12_10_8, upperData in self.oi.items():
                            for upperId, (lowerHUC12_10_8, lowerId) in upperData.items():
                                lowerHUC10_8_6 = lowerHUC12_10_8[:-2]
                                for inlet in self.inlets.get(lowerHUC10_8_6, []):
                                    if inlet.inletId == lowerId and inlet.inletHUC12_10_8 == lowerHUC12_10_8:
                                        inlet.addUpper((upperHUC12_10_8[:-2], upperHUC12_10_8, upperId))
                if dsSWATNum > 0:
                    mmap[feature.id()] = {dsLinkIndex: dsSWATNum, dsNodeIndex: -1}
                    ds[SWATNum] = dsSWATNum
        # remove uppers for internal outlet-inlet pairs and for pairs where the outlet is ignorable
        for huc in self.inlets:
            inlets = self.inlets[huc]
            for inlet in inlets:
                inlet.uppers = [upper for upper in inlet.uppers if not upperIsIgnorable(upper) and upper[0] != huc]
            inlets = [inlet for inlet in inlets if len(inlet.uppers) > 0]
            #print('Inlets for {0}:'.format(huc))
            #for inlet in inlets:        
            #    print(inlet.asString())
            self.inlets[huc] = inlets
        if self.is12:
            # renumber outlet ids
            newOutlets: Dict[str, List[Tuple[str, int, QgsPointXY]]] = dict()
            for huc10_8_6, outlets in self.outlets.items():
                if len(outlets) > 0:
                    newOutlets[huc10_8_6] = []
                    for (huc12_10_8, outletId, pt) in outlets:
                        pointId = self.getSinglePointId(huc12_10_8, outletId)
                        exp = QgsExpression('"HUC12" = {0} AND "DSNODEID" = {1}'.format(huc12_10_8, outletId))
                        for feature in channelsProvider.getFeatures(QgsFeatureRequest(exp).setFlags(QgsFeatureRequest.NoGeometry)):
                            mmap[feature.id()]  = {dsNodeIndex: pointId}
                            print('Outlet {0} {1} renumbered to {2}'.format(huc12_10_8, outletId, pointId))
                            break
                        newOutlets[huc10_8_6].append((huc12_10_8, pointId, pt))
            self.outlets = newOutlets   
        # change attributes before adding features, as latter changes feature ids used in  mmap
        channelsProvider.changeAttributeValues(mmap)
        # add inlets with zero-length channels, renumbering all point ids if is12
        inletFeatures: List[QgsFeature] = []
        for huc10_8_6, inlets in self.inlets.items():
            for inlet in inlets:
                ## add zero length channel
                pointId = self.getSinglePointId(inlet.inletHUC12_10_8, inlet.inletId) if self.is12 else inlet.inletId
                huc12_10_8Data = self.huc10_8_6Data[huc10_8_6]
                (inletSWATNum, _, drainArea) = huc12_10_8Data[inlet.inletHUC12_10_8]
                #print('Drainage area for inlet to {0} in {1} is {2}'.format(inletSWATNum, inlet.inletHUC12_10_8, drainArea))
                zfeature = QgsFeature(channelsProvider.fields())
                assert inlet.inletPoint is not None
                zfeature.setGeometry(QgsGeometry.fromPolylineXY([inlet.inletPoint, inlet.inletPoint]))
                if self.isPlus:
                    zfeature.setAttributes([pointId, inletSWATNum, 100, -1, pointId, -1, pointId, 1, drainArea, '', inlet.inletHUC12_10_8[:-2],
                                            inlet.inletHUC12_10_8, 0, inlet.inletPoint.x(), inlet.inletPoint.y(), inlet.inletPoint.x(), inlet.inletPoint.y()])
                else:
                    zfeature.setAttributes([pointId, inletSWATNum, pointId, pointId, 1, drainArea, '', inlet.inletHUC12_10_8[:-2],
                                            inlet.inletHUC12_10_8, 0, inlet.inletPoint.x(), inlet.inletPoint.y(), inlet.inletPoint.x(), inlet.inletPoint.y()])
                inletFeatures.append(zfeature)
                inlet.inletId = pointId
        channelsProvider.addFeatures(inletFeatures)
        
    def makeCombinedPoints(self, problems: Any) -> None:  # @UnusedVariable
        """Create shapefile containing the huc10_8_6 inlets and outlets."""
        # create shapefile
        pointsLayer = QgsVectorLayer('Point?crs=epsg:5072', 'points_layer', 'memory')
        pointsProvider = pointsLayer.dataProvider()
        pointsProvider.addAttributes(self.pointsFields.toList())
        pointsLayer.updateFields()
        features: List[QgsFeature] = []
        for outlets in self.outlets.values():
            for (_, pointId, pt) in outlets:
                feature = QgsFeature(self.pointsFields)
                feature.setAttributes([pointId, 0, 0, 0, ''])
                feature.setGeometry(QgsGeometry.fromPointXY(pt))
                features.append(feature)
        for inlets in self.inlets.values():
            for inletData in inlets:
                feature = QgsFeature(self.pointsFields)
                feature.setAttributes([inletData.inletId, 1, 0, 0, ''])
                assert inletData.inletPoint is not None
                feature.setGeometry(QgsGeometry.fromPointXY(inletData.inletPoint))
                features.append(feature)
        pointsProvider.addFeatures(features)
        QgsVectorFileWriter.writeAsVectorFormatV2(pointsLayer, self.combinedPoints, QgsCoordinateTransformContext(), self.common.options)
        
    def makeWaterBodies(self, problems: Any, parent: str, selection: str) -> None:
        """Create water bodies sqlite file."""
        
        def mergeReservoirs(reservoirs: List[Tuple[int, Waterbody]], ds: Dict[int, int]) -> Waterbody:
            """Merge reservoirs with subbasins according to whether subbasins drain in series or in parallel."""
            
            def isDownstream(sub1: int, sub2: int) -> bool:
                """Return true if sub1 is downstream of sub2."""
                sub = ds.get(sub2, -1)
                if sub == sub1:
                    return True
                elif sub == -1:
                    return False
                else:
                    return isDownstream(sub1, sub)
                
            ## first merge reservoirs in series
            count = len(reservoirs)
            removed: Set[int] = set()
            for i in range(count):
                if i not in removed:
                    subi, resi = reservoirs[i]
                    for j in range(i+1, count):
                        if j not in removed:
                            subj, resj = reservoirs[j]
                            if isDownstream(subj, subi):
                                resj.addWaterbody(resi, False)
                                removed.add(i)
                                break
                            elif isDownstream(subi, subj):
                                resi.addWaterbody(resj, False)
                                removed.add(j)
            # remove merged reservoirs
            for i in sorted(removed, reverse=True):
                reservoirs.pop(i)
            # now have a list of reservoirs to be merged in parallel
            reservoir0 = reservoirs.pop(0)[1]
            for _, reservoir in reservoirs:
                reservoir0.addWaterbody(reservoir, True)
            return reservoir0
        
        ponds: Dict[str, List[Waterbody]] = dict()
        reservoirs: Dict[str, List[Tuple[int, Waterbody]]] = dict() 
        wetlands: Dict[str, float] = dict()
        playas: Dict[str, float] = dict()
        with sqlite3.connect(self.HUC14_12_10WaterBodiesDb) as waterConn0:  # @UndefinedVariable
            sqlPnd = 'SELECT * FROM ponds'
            sqlRes = 'SELECT * FROM reservoirs'
            sqlWet = 'SELECT * FROM wetlands'
            sqlPlaya = 'SELECT * FROM playas'
            for row in waterConn0.execute(sqlPnd):
                if row[0].startswith(selection):
                    pond = Waterbody(row[3], row[4], 0, row[6], row[8], row[7], row[10], row[11], row[12], False)
                    pond.volEstimated = bool(row[13])
                    pond.surfaceAreaEstimated = bool(row[14])
                    ponds.setdefault(row[0], []).append(pond)
            for row in waterConn0.execute(sqlRes):
                if row[0].startswith(selection):
                    reservoir = Waterbody('', row[3], row[4], row[6], row[8], row[7], row[10], row[11], row[12], True)
                    reservoirs.setdefault(row[0], []).append((row[2], reservoir))
            for row in waterConn0.execute(sqlWet):
                if row[0].startswith(selection):
                    wetlands[row[0]] = wetlands.get(row[0], 0) + row[3]
            for row in waterConn0.execute(sqlPlaya):
                if row[0].startswith(selection):
                    playas[row[0]] = playas.get(row[0], 0) + row[3]
        with sqlite3.connect(self.waterBodiesDb) as waterConn:  # @UndefinedVariable
            waterConn.execute('DELETE FROM ponds')
            waterConn.execute('DELETE FROM reservoirs')
            waterConn.execute('DELETE FROM wetlands')
            waterConn.execute('DELETE FROM playas')
            sqlPnd1 = 'INSERT INTO ponds VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
            sqlRes1 = 'INSERT INTO reservoirs VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)'
            sqlWet1 = 'INSERT INTO wetlands VALUES(?,?,?,?)'
            sqlPlaya1 = 'INSERT INTO playas VALUES(?,?,?,?)'
            resAreas: Dict[str, Dict[int, float]] = dict()
            pondAreas: Dict[str, Dict[int, float]] = dict()
            for huc12_10_8, pondList in ponds.items():
                huc10_8_6 = huc12_10_8[:-2]
                SWATNum, _, _ = self.huc10_8_6Data[huc10_8_6][huc12_10_8]
                # need to use SWAT basin number for the individual project, even though this is a combined file, 
                # since we don't create individual water bodies tables for projects, nor put subbasin numbers in the points file
                SWATBasin = self.combinedToSingle[SWATNum][1]
                pond0 = pondList.pop(0)
                for pond in pondList:
                    pond0.addWaterbody(pond, False)
                waterConn.execute(sqlPnd1, (huc10_8_6, huc12_10_8, SWATBasin, pond0.nidid, pond0.name, pond0.surfaceArea * 1.1, pond0.maxVol, pond0.surfaceArea, 
                                            pond0.normVol, pond0.normVol * 0.8, pond0.drainArea, 
                                            pond0.location[0], pond0.location[1], pond0.volEstimated, pond0.surfaceAreaEstimated))
                pondAreas.setdefault(huc10_8_6, dict())[SWATBasin] = pond0.surfaceArea
            for huc12_10_8, resList in reservoirs.items():
                huc10_8_6 = huc12_10_8[:-2]
                SWATNum, _, _ = self.huc10_8_6Data[huc10_8_6][huc12_10_8]
                SWATBasin = self.combinedToSingle[SWATNum][1]
                reservoir = mergeReservoirs(resList, self.HUC12_10_8Ds.get(huc12_10_8, dict()))
                waterConn.execute(sqlRes1, (huc10_8_6, huc12_10_8, SWATBasin, reservoir.name, reservoir.year, reservoir.surfaceArea * 1.1, reservoir.maxVol, 
                                            reservoir.surfaceArea, reservoir.normVol, reservoir.normVol * 0.8, reservoir.drainArea,
                                            reservoir.location[0], reservoir.location[1]))
                resAreas.setdefault(huc10_8_6, dict())[SWATBasin] = reservoir.surfaceArea
            for huc12_10_8, area in wetlands.items():
                huc10_8_6 = huc12_10_8[:-2]
                SWATNum, _, _ = self.huc10_8_6Data[huc10_8_6][huc12_10_8]
                SWATBasin = self.combinedToSingle[SWATNum][1]
                waterConn.execute(sqlWet1, (huc10_8_6, huc12_10_8, SWATBasin, area))
            for huc12_10_8, area in playas.items():
                huc10_8_6 = huc12_10_8[:-2]
                SWATNum, _, _ = self.huc10_8_6Data[huc10_8_6][huc12_10_8]
                SWATBasin = self.combinedToSingle[SWATNum][1]
                waterConn.execute(sqlPlaya1, (huc10_8_6, huc12_10_8, SWATBasin, area))
            # replaced by fixBasindata
            #===================================================================
            # # if not SWATPlus or runProject (since then about to be written anyway, and code now corrected)
            # # and if project database exists
            # # rewrite the subbasin areas in hrus table to exclude reservoirs and ponds
            # # This is a temporary measure to correct old projects
            # if not self.isPlus and not self.runProject:
            #     for huc10_8_6, data in self.huc10_8_6Data.items():
            #         projDb = parent + '/huc{0}/huc{0}.sqlite'.format(huc10_8_6)
            #         if os.path.isfile(projDb):
            #             projConn = sqlite3.connect(projDb)
            #             # check we have an hrus table
            #             sql = "SELECT name FROM sqlite_master WHERE name = 'hrus'"
            #             rows = projConn.execute(sql).fetchall()
            #             if len(rows) > 0:
            #                 for (SWATNum, area, _) in data.values():
            #                     SWATBasin = self.combinedToSingle[SWATNum][1]
            #                     resArea = resAreas.get(huc10_8_6, dict()).get(SWATBasin, 0)
            #                     pondArea = pondAreas.get(huc10_8_6, dict()).get(SWATBasin, 0)
            #                     waterbodiesArea = resArea + pondArea
            #                     hrusArea = max(area / 1E4 - waterbodiesArea, 0)  # total area of HRUs in hectares
            #                     self.common.fixHRUsTable(projDb, projConn, SWATNum, hrusArea, problems, self.batchRerun)
            #                     self.common.fixWater(projConn, waterConn, huc10_8_6, SWATNum, area)
            #                 projConn.commit()
            #===================================================================
            waterConn.commit()
                
    def mapCombinedToSingle(self, combinedChannelsLayer: QgsVectorLayer):
        """set up maps from combined to single channel and subbasin numbers."""
        self.combinedToSingle = {-1: ('', -1)}
        provider = combinedChannelsLayer.dataProvider()
        linkIndex = self.channelIndexes['LINKNO']
        dsNodeIndex = self.channelIndexes['DSNODEID']
        huc12_10_8Index = self.channelIndexes[self.common.HUC14_12_10_8String]
        SWATNums: Dict[str, int] = dict()
        for feature in provider.getFeatures():
            huc12_10_8 = feature[huc12_10_8Index]
            huc10_8_6 = huc12_10_8[:-2]
            link = feature[linkIndex]
            if link < Common.basePointId:  # don't want to renumber inlet/outlet point ids
                SWATNum = SWATNums.setdefault(huc10_8_6, 0)    
                SWATNum += 1
                SWATNums[huc10_8_6] = SWATNum
                self.combinedToSingle[link] = (huc12_10_8, SWATNum)
            else:
                self.combinedToSingle[link] = (huc12_10_8, link)
            dsNodeId = feature[dsNodeIndex]
            if dsNodeId > 0:
                self.combinedToSingle[dsNodeId] = (huc12_10_8, dsNodeId)
            
    def writeChannelsShapefile(self, huc10_8_6: str, combinedChannelsLayer: QgsVectorLayer, combinedWshedLayer, projDir: str, problems: Any) -> None:  # @UnusedVariable
        """Write channels shapefile for huc10_8_6 project."""
        
        channelsLayer = QgsVectorLayer('LineString?crs=epsg:5072', 'channels_layer', 'memory')
        channelsProvider = channelsLayer.dataProvider()
        channelsProvider.addAttributes(self.channelFields.toList())
        channelsLayer.updateFields()
        linkIndex = self.channelIndexes['LINKNO']
        dsLinkIndex = self.channelIndexes['DSLINKNO']
        dsNodeIndex = self.channelIndexes['DSNODEID']
        wsnoIndex = self.channelIndexes['WSNO']
        huc10_8_6Index = self.channelIndexes[self.common.HUC12_10_8_6String]
        huc12_10_8Index = self.channelIndexes[self.common.HUC14_12_10_8String]
        if self.is12:
            exp = QgsExpression('left("HUC12", 10) = {0}'.format(huc10_8_6))
        elif self.is10:
            exp = QgsExpression('left("HUC10", 8) = {0}'.format(huc10_8_6))
        else:
            exp = QgsExpression('left("HUC8", 6) = {0}'.format(huc10_8_6))
        features = [feature for feature in combinedChannelsLayer.dataProvider().getFeatures(QgsFeatureRequest(exp))]
        if self.isPlus:
            subbasinsFile = posixpath.join(self.projectDirs[huc10_8_6], 'submapping.csv')
            subFile = open(subbasinsFile, 'w')
            subFile.write('SUBBASIN,HUC_ID,IsEnding1,IsEnding2\n')
        else:
            subbasinsFile = posixpath.join(self.projectDirs[huc10_8_6], 'submapping.csv')
            subFile = open(subbasinsFile, 'w')
            subFile.write('SUBBASIN,HUC_ID,IsEnding\n')
        for feature in features:
            huc12_10_8 = feature[huc12_10_8Index]
            huc10_8_6 = feature[huc10_8_6Index]
            link = feature[linkIndex]
            dsLink = feature[dsLinkIndex]
            dsNode = feature[dsNodeIndex]
            if link >= Common.basePointId:
                # zero length channel to inlet.  dsLink will be within huc10_8_6
                singleLink = dsNode
                singleDsLink = self.combinedToSingle[dsLink][1]
                singleDsNodeId = singleLink
            else:
                singleLink = self.combinedToSingle[link][1]
                if dsLink < 0:
                    singleDsLink = -1
                    singleDsNodeId  = dsNode
                else:
                    _, singleDsLink = self.combinedToSingle[dsLink]
                    singleDsNodeId = -1
            feature.setAttribute(linkIndex, singleLink)
            feature.setAttribute(dsLinkIndex, singleDsLink) 
            feature.setAttribute(dsNodeIndex, singleDsNodeId) 
            feature.setAttribute(wsnoIndex, singleLink)
            isEnding = 1 if singleDsLink < 0 else 0
            if singleLink < Common.basePointId:  # otherwise can include zero-length inlet channels
                self.startPoints.setdefault(huc10_8_6, dict())[singleLink] = Common.firstPoint(feature.geometry())
                if self.isPlus:
                    subFile.write('{0},{1},{2},{3}\n'.format(singleLink, huc12_10_8, isEnding, 0)) # TODO: second isEnding
                else:
                    subFile.write('{0},{1},{2}\n'.format(singleLink, huc12_10_8, isEnding))
        subFile.close()
        channelsProvider.addFeatures(features)
        if self.isPlus:
            channelsShapefile = projDir + '/Watershed/Shapes/channels.shp'
        else:
            channelsShapefile = projDir + '/Watershed/Shapes/channels.shp'
        error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(channelsLayer, channelsShapefile, QgsCoordinateTransformContext(), 
                                                               self.common.options)
        if error != 0:  # QgsVectorFileWriter.NoError
            Common.error('Failed to write channels file {0}: {1}'.format(channelsShapefile, msg))
        return
            
    def writeWatershedShapefile(self, huc10_8_6: str, combinedWshedLayer: QgsVectorLayer, projDir: str) -> None: 
        """Write watershed ahapefile for huc10_8 project. """
        subbasinsLayer = QgsVectorLayer('Polygon?crs=epsg:5072', 'subbasins_layer', 'memory')
        subbasinsProvider = subbasinsLayer.dataProvider()
        subbasinsProvider.addAttributes(self.wshedFields.toList())
        subbasinsLayer.updateFields()
        if self.is12:
            exp = QgsExpression('left("HUC12", 10) = {0}'.format(huc10_8_6))
        elif self.is10:
            exp = QgsExpression('left("HUC10", 8) = {0}'.format(huc10_8_6))
        else:
            exp = QgsExpression('left("HUC8", 6) = {0}'.format(huc10_8_6))
        polyIndex = self.wshedIndexes['PolygonId']
        subIndex = self.wshedIndexes['Subbasin']
        features = [feature for feature in combinedWshedLayer.dataProvider().getFeatures(QgsFeatureRequest(exp))]
        for feature in features:
            (_, SWATNum) = self.combinedToSingle[feature[polyIndex]]
            feature.setAttribute(polyIndex, SWATNum)
            feature.setAttribute(subIndex, SWATNum)
        subbasinsProvider.addFeatures(features)
        if self.isPlus:
            subbasinsFile = projDir + '/Watershed/Shapes/subbasins.shp'
        else:
            subbasinsFile = projDir + '/Watershed/Shapes/demwshed.shp'
        error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(subbasinsLayer, subbasinsFile, QgsCoordinateTransformContext(), 
                                                               self.common.options)
        if error != 0:  # QgsVectorFileWriter.NoError
            Common.error('Failed to write subbasins file {0}: {1}'.format(subbasinsFile, msg))
        
    def writePointsShapefile(self, huc10_8_6: str, pointsFile: str) -> None:
        """Create shapefile containing the huc10_8's internal inlets and outlets plus any combined ones that are inlets or outlets for this huc10_8_6."""
        # create shapefile
        # note if the order of fields is changed the call below of setAttributes will need changing
        pointsLayer = QgsVectorLayer('Point?crs=epsg:5072', 'points_layer', 'memory')
        pointsProvider = pointsLayer.dataProvider()
        pointsProvider.addAttributes(self.pointsFields.toList())
        pointsLayer.updateFields()
        features: List[QgsFeature] = []
        # generate new ids for reservoirs and ponds
        maxPointId = 0
        for (_, outletId, pt) in self.outlets.get(huc10_8_6, []):
            feature = QgsFeature(self.pointsFields)
            feature.setAttributes([outletId, 0, 0, 0, ''])
            feature.setGeometry(QgsGeometry.fromPointXY(pt))
            features.append(feature)
            maxPointId = max(maxPointId, outletId)
        for inletData in self.inlets.get(huc10_8_6, []):
            feature = QgsFeature(self.pointsFields)
            feature.setAttributes([inletData.inletId, 1, 0, 0, ''])
            assert inletData.inletPoint is not None
            feature.setGeometry(QgsGeometry.fromPointXY(inletData.inletPoint))
            features.append(feature)
            maxPointId = max(maxPointId, inletData.inletId)
        # add reservoirs and ponds
        with sqlite3.connect(self.waterBodiesDb) as waterConn:  # @UndefinedVariable
            sql = "SELECT RES_NAME, RES_LAT, RES_LONG FROM reservoirs WHERE HUC12_10_8_6 LIKE '{0}'".format(huc10_8_6)
            for row in waterConn.execute(sql):
                maxPointId += 1
                geom = QgsGeometry.fromPointXY(QgsPointXY(float(row[2]), float(row[1])))
                geom.transform(self.transformToProjected)
                feature = QgsFeature(self.pointsFields)
                feature.setAttributes([maxPointId, 0, 1, 0, '' if row[0] is None else row[0]])
                feature.setGeometry(geom)
                features.append(feature)
            sql = "SELECT PND_NAME, PND_LAT, PND_LONG FROM ponds WHERE HUC12_10_8_6 LIKE '{0}'".format(huc10_8_6)
            for row in waterConn.execute(sql):
                maxPointId += 1
                geom = QgsGeometry.fromPointXY(QgsPointXY(float(row[2]), float(row[1])))
                geom.transform(self.transformToProjected)
                feature = QgsFeature(self.pointsFields)
                feature.setAttributes([maxPointId, 0, 2, 0, '' if row[0] is None else row[0]])
                feature.setGeometry(geom)
                features.append(feature)
        pointsProvider.addFeatures(features)
        QgsVectorFileWriter.writeAsVectorFormatV2(pointsLayer, pointsFile, QgsCoordinateTransformContext(), self.common.options)
        
    def alignInletsOutlets(self, selection: str, parent: str) -> None:
        """Create inletsOutlets csv file matching inlet ids to outlet ids."""
        outletInletFile = posixpath.join(parent, 'oi.csv')
        fromToFile = posixpath.join(parent, 'fromto.csv')
        with open(outletInletFile, 'w') as oiFile, open(fromToFile, 'w') as fromTo:
            oiFile.write('OutletHUC,OutletId,InletHUC,InletId\n')
            fromTo.write('OutletHUC,InletHUC\n')
            hucPairs: Set[Tuple[str, str]] = set()
            if self.is12:
                for huc10_8_6, inlets in self.inlets.items():
                    for inlet in inlets:
                        for (upperHUC10_8_6, outletHUC12_10_8, outletId) in inlet.uppers:
                            # should only have external upper values left, but we'll check
                            if upperHUC10_8_6 == huc10_8_6:
                                print('Internal error: inlet {0} has an internal comnnection'.format(inlet.asString()))
                                continue
                            # outlets need renumbering to single
                            singleOutletId = self.getSinglePointId(outletHUC12_10_8, outletId)
                            oiFile.write('{0},{1},{2},{3}\n'.format(upperHUC10_8_6, singleOutletId, huc10_8_6, inlet.inletId))
                            hucPairs.add((upperHUC10_8_6, huc10_8_6))
            else:
                for huc10_8_6, inlets in self.inlets.items():
                    for inlet in inlets:
                        for upperHUC10_8_6, _, outletId in inlet.uppers:
                            oiFile.write('{0},{1},{2},{3}\n'.format(upperHUC10_8_6, outletId, huc10_8_6, inlet.inletId))
                            hucPairs.add((upperHUC10_8_6, huc10_8_6))
            for upperHUC, huc in hucPairs:
                fromTo.write('{0},{1}\n'.format(upperHUC, huc))
            # check we have included all external connections from previous level
            for outHUC12_10_8, oiData in self.oi.items():
                if outHUC12_10_8.startswith(selection):
                    for outletId, (inHUC12_10_8, inletId) in oiData.items():
                        if inHUC12_10_8.startswith(selection):
                            outHUC10_8_6 = outHUC12_10_8[:-2]
                            inHUC10_8_6 = inHUC12_10_8[:-2]
                            if outHUC10_8_6 == inHUC10_8_6:
                                continue  # internal
                            if (outHUC12_10_8, outletId) in self.ignorableOutlets.get(outHUC10_8_6, []):
                                continue # ignorable
                            found = False
                            for inlet in self.inlets.get(inHUC10_8_6, []):
                                expectedInletId = self.getSinglePointId(inlet.inletHUC12_10_8, inletId) if self.is12 else inletId
                                if inlet.inletId == expectedInletId and inlet.inletHUC12_10_8 == inHUC12_10_8:
                                    for upper in inlet.uppers:
                                        if upper[1] == outHUC12_10_8 and upper[2] == outletId:
                                            found = True
                                            break
                                if found:
                                    break
                            if not found:
                                Common.error('Outlet-inlet item ({0}, {1}, {2}, {3}) not found'.format(outHUC12_10_8, outletId, inHUC12_10_8, inletId))
    
    def deleteProjects(self, delete: List[str], selection: str) -> None:
        extDir = '/Fields_CDL' if self.isCDL else '/Fields'
        SWATDir = '/SWATPlus' + extDir if self.isPlus else '/SWAT' + extDir
        parent = Files.ModelsDir + SWATDir + (Files.HUC12ProjectParent if self.is12 
                  else Files.HUC10ProjectParent if self.is10
                  else Files.HUC8ProjectParent) + '/' + selection
        fromToFile = posixpath.join(parent, 'fromto.csv')
        fromToFile2 = posixpath.join(parent, 'fromto2.csv')
        with open(fromToFile, 'r', newline='') as f, open(fromToFile2, 'w', newline='') as f2:
            reader = csv.reader(f)
            writer = csv.writer(f2)
            for row in reader:
                if row[0] not in delete and row[1] not in delete:
                    writer.writerow(row)
        os.remove(fromToFile)
        os.rename(fromToFile2, fromToFile)
        for huc10_8_6 in delete:
            Common.information('Deleting project huc{0}'.format(huc10_8_6))
            projDir = posixpath.join(parent, 'huc' + huc10_8_6)
            if os.path.isdir(projDir):
                try:
                    shutil.rmtree(projDir)
                    Common.information('Deleted directory {0}'.format(projDir))
                except:
                    Common.information('Please delete directory {0}'.format(projDir))
            if not self.isPlus:
                qgsFile = projDir + '.qgs'
                if os.path.isfile(qgsFile):
                    os.remove(qgsFile)
                backup = qgsFile + '~'
                if os.path.isfile(backup):
                    os.remove(backup)
                
    def addExternalInlet(self, huc10_8_6: str) -> None:
        """Add inlet point to channel with furthest start point."""
        outlets = self.outlets.get(huc10_8_6, None)
        if outlets is None:
            Common.error('Failed to find outlet point for project huc{0}'.format(huc10_8_6))
            return
        # arbitrarily select an outlet: almost certainly only one
        for _, _, outletPoint in outlets:
            break
        channel, inletPoint = Common.furthest(self.startPoints.get(huc10_8_6, dict()), outletPoint)
        if inletPoint is None:
            Common.error('Failed to find inlet point for project huc{0}'.format(huc10_8_6))
            return
        inletId = Common.basePointId # 100000: points already added start from id 100001
        # add inlet to points shapefile
        projDir = self.projectDirs[huc10_8_6]
        pointsFile = projDir + '/Watershed/Shapes/points.shp'
        pointsLayer = QgsVectorLayer(pointsFile, 'points', 'ogr')
        pointsProvider = pointsLayer.dataProvider()
        point = QgsFeature(pointsProvider.fields())
        point.setAttributes([inletId, 1, 0, 0, ''])
        point.setGeometry(QgsGeometry.fromPointXY(inletPoint))
        pointsProvider.addFeatures([point])
        # add zero length channel
        channelsFile = projDir + '/Watershed/Shapes/channels.shp'
        channelsLayer = QgsVectorLayer(channelsFile, 'channels', 'ogr')
        channelsProvider = channelsLayer.dataProvider()
        zfeature = QgsFeature(channelsProvider.fields())
        zfeature.setGeometry(QgsGeometry.fromPolylineXY([inletPoint, inletPoint]))
        if self.isPlus:
            zfeature.setAttributes([inletId, channel, 100, -1, inletId, -1, 0, 0, 0, '', huc10_8_6, 0,
                                    0, inletPoint.x(), inletPoint.y(), inletPoint.x(), inletPoint.y()])
        else:
            zfeature.setAttributes([inletId, channel, inletId, 0, 0, 0, '', huc10_8_6, 0,
                                    0, inletPoint.x(), inletPoint.y(), inletPoint.x(), inletPoint.y()])
        channelsProvider.addFeatures([zfeature])
                


class HUC14Catchments:
    """Generating HUC14 models.
    
    In HUC14 models, basins are HUC12 watersheds and subbasins are mergers of catchments."""
    
    thresholdDivisor = 4
    
    mergeThreshold = 30.0    # maximum total area in km^2 for two catchments to be merged
    tinyThreshold = 5.0      # maximum area of catchment in km^2 to be considered tiny
    junctionThreshold = 0.15  # maximum fraction of catchment area to downstream catchment area for it to be merged as the last junction
    
    mainBifurcatePercent = 60  # percentage of flow sent to main channel when there is a genuine bifurcation
    
    # channels that share a starting point with another channel, which can cause a stream with 
    # two directions if both are merged into the same channel.
    # If included in channelsToOmit the channel's geometry is ignored rather than added to the one downstream,
    # which breaks the loop.
    # Such channels emerge from running QSWAT and getting a message about a stream with a loop.
    # The run must be done again with makeOriginals set True so the comids of channels in the loop can be found,
    # and then once more after adding one of the two channels to channelsToOmit.
    channelsToOmit = {19334029, 6764304, 6778829, 9343403}
    
    #flowlines that are terminals but should not be
    terminalsToCancel = {5860393}
    
    def __init__(self, isPlus: bool, isCDL: bool, withProject: bool, runProject: bool, makeOriginals: bool, batFile: str, minHRUha: int) -> None:
        '''
        Constructor
        '''
        ## file for rerun commands
        self.batchRerun = ''
        ## QSWAT or QSWAT+ projects
        self.isPlus = isPlus
        # use fields_CDL landuse file and lookup
        self.isCDL = isCDL
        ## include project databases, DEM, landuse and soils
        self.withProject = withProject
        ## run project
        self.runProject = runProject
        # make copies of flowline and catchment maps
        self.makeOriginals = makeOriginals
        ## table mapping comids to area in sq km for each HUC12 basin
        self.catchmentAreas: Dict[str, Dict[int, float]] = dict()
        ## table mapping flowline comids to lengths in km
        self.flowLengths: Dict[int, float] = dict()
        ## table mapping comids to drainage areas at downstream end in sq km
        self.drainageAreas: Dict[int, float] = dict()
        ## map from catchment to downstream, catchment for huc12 basin
        self.dsCatchments: Dict[str, Dict[int, List[int]]] = dict()
        ## copy of dsCatchments made after reduction but before merging, to match channels map based on flowlines
        self.dsCatchments0: Dict[str, Dict[int, List[int]]] = dict()
        ## map from comid to list of targets for huc12 basin
        self.bifurcations: Dict[str, Dict[int, List[int]]] = dict()
        ## map from comid to list of (outlet number, old downstream comid) for huc12 basin
        self.terminals: Dict[str, Dict[int, List[Tuple[int, int]]]] = dict()
        ## set of divergence 2 comids for huc12 basin
        self.minors: Dict[str, Set[int]] = dict()
        ## mapping from comid to HUC
        self.comidToHUC: Dict[int, str] = dict()
        ## set of streams which start within catchment (startflag = 1)
        self.starters: Dict[str, Set[int]] = dict()
        ## set of divergence 9 comids for huc12 basin
        self.coastIds: Dict[str, Set[int]] = dict()
        ## map of HUC14InletData for huc12 basin
        self.inlets: Dict[str, Set[HUC14InletData]] = dict()
        ## map of minor-major pairs for huc12 basin (not used if isPlus)
        self.minorMajor: Dict[str, Set[Tuple[int, int]]] = dict()
        ## map of comid to reservoir for each huc12 basin
        self.reservoirs: Dict[str, Dict[int, Waterbody]] = dict()
        ## map of comid to Pond for each huc12 basin.  
        # Keep list until can compare drainage with subbasin area to see which are eliminated
        self.ponds: Dict[str, Dict[int, List[Waterbody]]] = dict()
        ## nidids of included ponds
        self.includedPonds: List[str] = []
        ## nidids of not included ponds
        self.excludedPonds: List[str] = []
        ## formula to estimate pond surface area from volume
        self.pondAreaFun: Callable[[float], float]
        ## formula to estimate pond volume from surface area
        self.pondVolFun: Callable[[float], float]
        ## HUC8 area for current project
        # ponds with drainage area larger than this are ignored
        self.huc8Area = 0.0
        ## map of comid to wetland area for each huc12 basin
        self.wetlands: Dict[str, Dict[int, float]] = dict()
        ## map of comid to playa area for each huc12 basin
        self.playas: Dict[str, Dict[int, float]] = dict()
        ## project directories
        self.projectDirs: Dict[str, str] = dict()
        ogr.RegisterAll()
        ogr.UseExceptions()
        ## connection to huc12
        self.huc12Conn = sqlite3.connect(Files.huc12Db)  # @UndefinedVariable
        self.huc12Conn.enable_load_extension(True)
        self.huc12Conn.execute("SELECT load_extension('mod_spatialite')")
        ## connection to catchment
        self.catchmentConn = sqlite3.connect(Files.catchmentDb)  # @UndefinedVariable
        self.catchmentConn.enable_load_extension(True)
        self.catchmentConn.execute("SELECT load_extension('mod_spatialite')")
        ## connection to flowline
        self.flowlineConn = sqlite3.connect(Files.flowlineDb)  # @UndefinedVariable
        self.flowlineConn.enable_load_extension(True)
        self.flowlineConn.execute("SELECT load_extension('mod_spatialite')")
        ## current channels layer
        self.channelsLayer: Optional[QgsVectorLayer] = None
        ## current streams layer 
        self.streamsLayer: Optional[QgsVectorLayer] = None
        ## streamsLayer indexes
        self.streamsIndexes: Dict[str, int] = dict()
        ## current wshed layer
        self.subbasinsLayer: Optional[QgsVectorLayer] = None
        ## subbasinsLayer indexes
        self.subbasinsIndexes: Dict[str, int] = dict()
        ## last used inlet or outlet id for each HUC12  
        self.pointIds: Dict[str, int] = dict()
        ## map from huc12 to exit points: (comid, point id, point)
        self.outlets: Dict[str, Set[Tuple[int, int, QgsPointXY]]] = dict()
        ## map from comid to flowline start points for each HUC12
        self.startPoints: Dict[str, Dict[int, QgsPointXY]] = dict()
        ## map from merged comid to new comid for each huc
        self.mergeTargets: Dict[str, Dict[int, int]] = dict()
        ## map from comid to SWAT identifier for each huc12: used for final catchments and channels
        self.SWATIds: Dict[str, Dict[int, int]] = dict()
        ## last SWAT identifer used for each huc12
        self.lastSWATNum: Dict[str, int] = dict()
        ## upper HUCs for each huc, plus flag to prevent multiple evaluation
        self.upperHUCs: Dict[str, Tuple[Set[str], bool]] = dict()
        ## for each huc12 a set of SWAT ids of polygons acually appearing in watershed
        # isolated flowlines will not get into the streams shapefile or polygon shapefile but will be given a SWAT id
        # we need to remove them from the channels shapefile, using this list of ones to keep
        self.SWATPolygons: Dict[str, Set[int]] = dict()
        ## catchment polygons missing from polygon shapefile for each huc12 (because too small, or because unattached)
        self.noCatchments: Dict[str, Set[int]] = dict()
        ## Empty HUC12s.  Stored so we don't try to connect them to inlets
        self.emptyHUC12s: Set[str] = set()
        ## cacthment polygons in closed HUC12s (indicated by negative comids)
        self.closedCatchments: Dict[str, Set[int]] = dict()
        ## centroids of closed catchments
        self.centroids: Dict[int, QgsPointXY] = dict()
        ## crs data
        self.common = Common(False, False, True, isPlus, batFile, minHRUha)
        self.crsProject = self.common.crsProject
        ## crs transform
        self.transformToProjected = self.common.transformToProjected
        ## combined watershed shapefile writer
        self.combinedWshedLayer:  Optional[QgsVectorLayer] = None
        ## combined streams shapefile writer
        self.combinedStreamsLayer:  Optional[QgsVectorLayer] = None
        # combinedChannels0 file
        self.combinedChannels0 = ''
        ## fields for combinedChannels0 file
        self.channelFields0 = self.common.createChannelsFields(is14_0=True)[0]
        channelFields, channelIndexes = self.common.createChannelsFields()
        ## channel shapefile fields
        self.channelFields = channelFields
        ## channel shapefile indexes
        self.channelIndexes = channelIndexes
        # combinedChannels0 file
        self.combinedChannels0Features: List[QgsFeature] = []
        ## combined original watershed shapefile writer
        self.combinedWshed0Layer:  Optional[QgsVectorLayer] = None
        ## catchment table reduced to this model's selection
        self.selectcatchTable = ''
        ## water table reduced to this model's selection
        self.selectWaterTable = ''
        ## path of comids database
        self.comidsDb = ''
        ## water bodies database
        self.waterBodiesDb = ''
        ## connection to WaterBodiesDb
        self.waterConn: Any = None
        ## total catchments in each HUC12
        self.totalCatchments: Dict[str, int] = dict()
#         ## count of HUC12s with catchments belonging to other HUC12s, since less than 5% overlap
#         self.extraCatchmentsCount = 0
        
        
    @staticmethod   
    def importCatchmentTable() -> None:
        """Import CatchmentTable to provide COMID - HUC12 name mapping in database.
        Also apply catchments_patch.csv"""
        conn = sqlite3.connect(Files.HUC12Data)  # @UndefinedVariable
        sql = 'DROP TABLE IF EXISTS catchments'
        cursor = conn.cursor()
        cursor.execute(sql)
        sql = 'CREATE TABLE catchments (comid INTEGER PRIMARY KEY UNIQUE, huc12 TEXT)'
        cursor.execute(sql)
        sql = 'CREATE INDEX IF NOT EXISTS  hucindex ON catchments (huc12)'
        cursor.execute(sql)
        sql = 'INSERT INTO catchments VALUES (?, ?)'
        with open(Files.CatchmentTable, 'r') as catchmentFile:
            table = csv.reader(catchmentFile)
            next(table)
            for rec in table:
                cursor.execute(sql, (rec[0], rec[1]))
        # patch wrongly assigned comids
        sql1 = 'UPDATE catchments SET huc12=? WHERE comid=?'
        with open(Files.CatchmentPatch, 'r') as csvFile:
            reader = csv.reader(csvFile)
            next(reader)  # skip header
            for line in reader:
                cursor.execute(sql1, (line[1], line[0]))
        conn.commit()
        
    @staticmethod
    def patchCatchmentsTable() -> None:
        """Apply catchments_patch.csv to catchments table."""
        conn = sqlite3.connect(Files.HUC12Data)  # @UndefinedVariable
        cursor = conn.cursor()
        sql1 = 'UPDATE catchments SET huc12=? WHERE comid=?'
        with open(Files.CatchmentPatch, 'r') as csvFile:
            reader= csv.reader(csvFile)
            next(reader)  # skip header
            for line in reader:
                cursor.execute(sql1, (line[1], line[0]))
        conn.commit()
        
    @staticmethod
    def patchFromToTable() -> None:
        """Apply fromto_patch.csv to catchments table."""
        conn = sqlite3.connect(Files.HUC12Data)  # @UndefinedVariable
        cursor = conn.cursor()
        sql1 = 'UPDATE fromto SET tocomid=? WHERE fromcomid=?'
        with open(Files.FromToPatch, 'r') as csvFile:
            reader= csv.reader(csvFile)
            next(reader)  # skip header
            for line in reader:
                cursor.execute(sql1, (line[1], line[0]))
        conn.commit()
        
    @staticmethod 
    def createFromToMajorTable() -> None:
        """Create fromtomajor, like fromto but with all divergence 2 flowlines removed."""
        conn = sqlite3.connect(Files.HUC12Data)  # @UndefinedVariable
        cursor = conn.cursor()
        sql = 'DROP TABLE IF EXISTS fromtomajor'
        cursor.execute(sql)
        sql = 'DROP TABLE IF EXISTS flowlinemajor'
        cursor.execute(sql)
        sql = "ATTACH '{0}' as Flowline".format(Files.flowlineDb)
        cursor.execute(sql)
        sql = 'CREATE TABLE flowlinemajor AS SELECT comid FROM Flowline.flowline WHERE NOT divergence = 2'
        cursor.execute(sql)
        sql = 'CREATE INDEX flowlinemajor_comid ON flowlinemajor (comid)'
        cursor.execute(sql)
        sql = 'CREATE TABLE fromtomajor1 AS SELECT fromcomid, tocomid FROM fromto INNER JOIN flowlinemajor on fromto.tocomid = flowlinemajor.comid'
        cursor.execute(sql)
        sql = 'CREATE INDEX fromtomajor1_fromcomid ON fromtomajor1 (fromcomid)'
        cursor.execute(sql)
        sql = 'CREATE TABLE fromtomajor AS SELECT fromtomajor1.* FROM fromtomajor1 INNER JOIN flowlinemajor on fromtomajor1.fromcomid = flowlinemajor.comid'
        cursor.execute(sql)
        sql = 'DROP TABLE fromtomajor1'
        cursor.execute(sql)
        sql = 'DETACH Flowline'
        cursor.execute(sql)
        sql = 'CREATE INDEX fromtomajor_fromcomid ON fromtomajor (fromcomid)'
        cursor.execute(sql)
        sql = 'CREATE INDEX fromtomajor_tocomid ON fromtomajor (tocomid)'
        cursor.execute(sql)
        conn.commit()
        
    @staticmethod   
    def importFromToTable() -> None:
        """Import FromToTable to provide COMID - COMID mapping in database."""
        conn = sqlite3.connect(Files.HUC12Data)  # @UndefinedVariable
        sql = 'DROP TABLE IF EXISTS fromto'
        cursor = conn.cursor()
        cursor.execute(sql)
        sql = 'CREATE TABLE fromto (fromcomid INTEGER, tocomid INTEGER)'
        cursor.execute(sql)
        sql = 'CREATE INDEX fromto_fromcomid ON fromto (fromcomid)'
        cursor.execute(sql)
        sql = 'CREATE INDEX fromto_tocomid ON fromto (tocomid)'
        cursor.execute(sql)
        sql = 'INSERT into fromto VALUES (?, ?)'
        with open(Files.FromToTable, 'r') as fromToFile:
            table = csv.reader(fromToFile)
            next(table)
            for rec in table:
                fromId = rec[0]
                toId = rec[1]
                if int(fromId) != 0:
                    cursor.execute(sql, (fromId, toId))
        conn.commit()
        
    @staticmethod   
    def importAreasTable() -> None:
        """Import AreasTable to provide COMID - HUC12 area mapping in database."""
        conn = sqlite3.connect(Files.HUC12Data)  # @UndefinedVariable
        sql = 'DROP TABLE IF EXISTS areas'
        cursor = conn.cursor()
        cursor.execute(sql)
        sql = 'CREATE TABLE areas (comid INTEGER PRIMARY KEY, area REAL)'
        cursor.execute(sql)
        sql = 'INSERT into areas VALUES (?, ?)'
        with open(Files.AreasTable, 'r') as areasFile:
            table = csv.reader(areasFile)
            next(table)
            for rec in table:
                cursor.execute(sql, (rec[0], rec[1]))
        conn.commit()
        
    @staticmethod   
    def importDsTable() -> None:
        """Import huc12ds to provide HUC12 - HUC12 mapping in database."""
        conn = sqlite3.connect(Files.HUC12Data)  # @UndefinedVariable
        sql = 'DROP TABLE IF EXISTS huc12ds'
        cursor = conn.cursor()
        cursor.execute(sql)
        sql = 'CREATE TABLE huc12ds (huc12 TEXT, ds TEXT)'
        cursor.execute(sql)
        sql = 'CREATE INDEX hucdsindex ON huc12ds (ds)'
        cursor.execute(sql)
        sql = 'CREATE INDEX huc12index ON huc12ds (huc12)'
        cursor.execute(sql)
        sql = 'INSERT into huc12ds VALUES (?, ?)'
        with open(Files.DSTable, 'r') as dsFile:
            table = csv.reader(dsFile)
            next(table)
            for rec in table:
                cursor.execute(sql, (rec[0], rec[1]))
        conn.commit()
        
    def populateTables(self, selection: str, cursor: Any, problems: Any) -> None:
        """Populate catchmentAreas, noCatchments. closedCatchments, madOutlets and dsCatchments for HUC12 watersheds starting with selection'"""
        
        def inletForTarget(target: int, inlets: Set[HUC14InletData]) -> Optional[HUC14InletData]:
            """Return inlet data if there is an inlet with inletComid equal to target, else None."""
            for inlet in inlets:
                if inlet.inletComid == target:
                    return inlet
            return None
        
        def targetInTerminal(target: int, terminal: List[Tuple[int, int]]) -> bool:
            """Return True if target already in a terminal pair."""
            for _, toId in terminal:
                if target == toId:
                    return True
            return False
        
        def reduceTerminals():
            """If a terminal for a comid has a -1 toId and also a toId that is not -1, the -1 is irrelevant and should be removed,
            or an unused outlet will be generated.  A -1 toId indicates a terminal with no corresponding inlet, while a toId that is
            not -1 indicates a terminal with a corresponding inlet."""
            fixes: Dict[str, Dict[int, List[Tuple[int, int]]]] = dict()
            for huc12, terminals in self.terminals.items():
                for comid, terminal in terminals.items():
                    if len(terminal) > 1:
                        terminal0 = [(ptId, toId) for (ptId, toId) in terminal if toId != -1]
                        if len(terminal0) == 0:  # all toIds -1: just use first
                            fixHUC = fixes.setdefault(huc12, dict())
                            fixHUC[comid] = [terminal[0]]
                        elif len(terminal0) < len(terminal):
                            fixHUC = fixes.setdefault(huc12, dict())
                            fixHUC[comid] = terminal0
            for huc12, terminals in fixes.items():
                for comid, terminal in terminals.items():
                    self.terminals[huc12][comid] = terminal
        
        # code of populateTables
        # set up pond area function
        row = cursor.execute("SELECT intercept, logvol FROM pondvolarea where Region LIKE '{0}%'".format(selection[:2])).fetchone()
        self.pondAreaFun = lambda v: math.exp(float(row[0]) + float(row[1]) * math.log(v))
        self.pondVolFun = lambda a: math.exp((math.log(a) - float(row[0])) / float(row[1]))
        self.selectcatchTable = 'selectcatch' + selection
        cursor.execute('DROP TABLE IF EXISTS {0}'.format(self.selectcatchTable))
        select = selection if len(selection) == 12 else selection + '%'
        sql0 = "CREATE TABLE {1} AS SELECT comid, huc12 FROM catchments WHERE huc12 LIKE '{0}'".format(select, self.selectcatchTable)
        #sql0 = "CREATE TABLE {2} AS SELECT comid, huc12 FROM catchments WHERE huc12 LIKE '{0}' OR huc12 = '{1}'".format(selection[0], selection[1], self.selectcatchTable)
        cursor.execute(sql0)
        sql02 = 'CREATE INDEX comidindex{0} ON {1} (comid)'.format(selection, self.selectcatchTable)
        cursor.execute(sql02)
        self.selectReservoirTable = 'selectreservoir' + selection
        cursor.execute('DROP TABLE IF EXISTS {0}'.format(self.selectReservoirTable))
        sql03 = "CREATE TABLE {1} AS SELECT * FROM reservoirs WHERE huc12 LIKE '{0}'".format(select, self.selectReservoirTable)
        cursor.execute(sql03)
        sql04 = 'CREATE INDEX reservoircomidindex{0} ON {1} (comid)'.format(selection, self.selectReservoirTable)
        cursor.execute(sql04)
        self.selectPondTable = 'selectpond' + selection
        cursor.execute('DROP TABLE IF EXISTS {0}'.format(self.selectPondTable))
        sql03 = "CREATE TABLE {1} AS SELECT * FROM ponds WHERE huc12 LIKE '{0}'".format(select, self.selectPondTable)
        cursor.execute(sql03)
        sql04 = 'CREATE INDEX pondcomidindex{0} ON {1} (comid)'.format(selection, self.selectPondTable)
        cursor.execute(sql04)
        self.selectWetlandTable = 'selectwetland' + selection
        cursor.execute('DROP TABLE IF EXISTS {0}'.format(self.selectWetlandTable))
        sql03 = "CREATE TABLE {1} AS SELECT * FROM wetlands WHERE huc12 LIKE '{0}'".format(select, self.selectWetlandTable)
        cursor.execute(sql03)
        sql04 = 'CREATE INDEX wetlandcomidindex{0} ON {1} (comid)'.format(selection, self.selectWetlandTable)
        cursor.execute(sql04)
        self.selectPlayaTable = 'selectplaya' + selection
        cursor.execute('DROP TABLE IF EXISTS {0}'.format(self.selectPlayaTable))
        sql03 = "CREATE TABLE {1} AS SELECT * FROM playas WHERE huc12 LIKE '{0}'".format(select, self.selectPlayaTable)
        cursor.execute(sql03)
        sql04 = 'CREATE INDEX playacomidindex{0} ON {1} (comid)'.format(selection, self.selectPlayaTable)
        cursor.execute(sql04)
        sql1 = 'SELECT comid, huc12 FROM {0}'.format(self.selectcatchTable)
        sql2 = 'SELECT tocomid FROM fromto WHERE fromcomid = ?'
        sql3 = 'SELECT huc12 FROM {0} WHERE comid = ?'.format(self.selectcatchTable)
        sql4 = 'SELECT divergence, terminalfl, startflag, totdasqkm, lengthkm FROM flowline WHERE comid=?'
        sql5 = 'SELECT area FROM areas WHERE comid = ?'
        sql6 = 'SELECT res_dam_name, res_year_completed, res_max_storage_ha_m, res_nid_storage_ha_m, res_normal_storage_ha_m, res_area_sqkm, res_drainage_area_sqkm, res_latitude, res_longitude FROM {0} WHERE comid=?'.format(self.selectReservoirTable)
        sql7 = 'SELECT nidid, dam_name, max_storage_ha_m, nid_storage_ha_m, normal_storage_ha_m, surface_area_ha, drainage_area_sqkm, latitude, longitude FROM {0} WHERE comid=?'.format(self.selectPondTable)
        sql8 = 'SELECT wetaream2 FROM {0} WHERE comid=?'.format(self.selectWetlandTable)
        sql9 = 'SELECT playaaream2 FROM {0} WHERE comid=?'.format(self.selectPlayaTable)
        self.centroids = dict()
        # need to use fetchall since cursor is reused for other loops within this one
        for row1 in cursor.execute(sql1).fetchall():
            currentId = int(row1[0])
            currentHUC = row1[1]
            catchmentCount = self.totalCatchments.setdefault(currentHUC, 0)
            catchmentCount += 1
            self.totalCatchments[currentHUC] = catchmentCount
            self.comidToHUC[currentId] = currentHUC
            _ = self.pointIds.setdefault(currentHUC, Common.basePointId)
            ds = self.dsCatchments.setdefault(currentHUC, dict())
            areas = self.catchmentAreas.setdefault(currentHUC, dict())
            noCatchments = self.noCatchments.setdefault(currentHUC, set())
            closedCatchments = self.closedCatchments.setdefault(currentHUC, set())
            targetsMap = self.bifurcations.setdefault(currentHUC, dict())
            terminals = self.terminals.setdefault(currentHUC, dict())
            minors = self.minors.setdefault(currentHUC, set())
            starters = self.starters.setdefault(currentHUC, set())
            coastIds = self.coastIds.setdefault(currentHUC, set())
            inlets = self.inlets.setdefault(currentHUC, set())
            reservoirs = self.reservoirs.setdefault(currentHUC, dict())
            ponds = self.ponds.setdefault(currentHUC, dict())
            playas = self.playas.setdefault(currentHUC, dict())
            wetlands = self.wetlands.setdefault(currentHUC, dict())
            if currentId < 0:
                closedCatchments.add(currentId)
                ds[currentId] = [-1]
            else:
                row4 = self.flowlineConn.execute(sql4, (currentId,)).fetchone()
                if row4 is None:
                    Common.error('Comid {0} not found in flowline table'.format(currentId), problems)
                    continue
                divergence = int(row4[0])
                if divergence == 9:
                    coastIds.add(currentId)
                elif divergence == 2:
                    minors.add(currentId)
                # use downstream flowline in different HUC12 rather than terminal flag
                #if int(row4[1]) == 1 and currentId not in HUC14Catchments.terminalsToCancel:
                #    # details added later
                #    terminals[currentId] = []
                #    #print('{0} is a terminal'.format(currentId))
                if int(row4[2]) == 1:
                    starters.add(currentId)
                self.drainageAreas[currentId] = float(row4[3])
                self.flowLengths[currentId] = float(row4[4])
                targets: List[int] = []
                for row2 in cursor.execute(sql2, (currentId,)).fetchall():
                    toId = int(row2[0])
                    if toId in targets or 0 - toId in targets:  # beware duplicated entries in fromto, eg for 15714875, 167578939
                        continue
                    makeOutlet = False
                    ignore = False
                    if toId != 0:
                        row3 = cursor.execute(sql3, (toId,)).fetchone()
                        if row3 is None or row3[0] != currentHUC:
                            if row3 is None:
                                toId = -1
                                if toId in targets:  # can get more than one exit from selection, giving multiple -1s
                                    continue
                            else:
                                otherHUC = row3[0]
                                otherUpperHUCs = self.getUppers(otherHUC, cursor)
                                #print('Uppers for {0}: {1}'.format(otherHUC, otherUpperHUCs))
                                if currentHUC not in otherUpperHUCs:
                                    canAdd = self.canAdd(currentHUC, otherHUC, cursor)
                                    #print('canAdd({0}, {1}) returned {2}'.format(currentHUC, otherHUC, canAdd))
                                    if canAdd:
                                        #print('Uppers for other {0} before adding: {1}'.format(otherHUC, self.upperHUCs[otherHUC][0]))
                                        self.upperHUCs[otherHUC][0].add(currentHUC)
                                        #print('Uppers for other {0} after adding: {1}'.format(otherHUC, self.upperHUCs[otherHUC][0]))
                                        # problems.write('Added HUC12 {0} as downstream from {1}\n'.format(otherHUC, currentHUC))
                                    else:
                                        # since no longer an inlet, treat as a starter, so eligible for merging later
                                        self.starters.setdefault(otherHUC, set()).add(toId)
                                        makeOutlet = True
                                if toId != 0:
                                    if not self.isPlus:
                                        if currentId in minors:
                                            ignore = True
                                        else:
                                            row4 = self.flowlineConn.execute(sql4, (toId,)).fetchone()
                                            if row4 is not None and int(row4[0]) == 2:
                                                ignore = True
                                        #print('Ignore from flowline for {0}: {1}'.format(toId, ignore))
                                    if makeOutlet and not ignore:
                                        problems.write('Flow from comid {0} in {1} to comid {2} in {3} inconsistent with huc12ds table: changed to outlet\n'
                                                       .format(currentId, currentHUC, toId, otherHUC))
                                        if not (currentId in coastIds):
                                            # need to leave coastIds downstream links for possible matching later
                                            toId = 1 # will become -1
                                if not ignore and not makeOutlet:
                                    inlets = self.inlets.setdefault(otherHUC, set())
                                    inlet = inletForTarget(toId, inlets)
                                    if inlet is None:
                                        currentPoint = self.pointIds.get(otherHUC, Common.basePointId)
                                        thisPoint = currentPoint+1
                                        self.pointIds[otherHUC] = thisPoint
                                        inlets.add(HUC14InletData((currentHUC, currentId), thisPoint, toId))
                                        #print('Added inlet {0} for {1}, currentId {2}, toId {3}'.format(HUC14InletData(currentHUC, thisPoint, toId).asString(), otherHUC, currentId, toId))
                                    else:
                                        inlet.addUpper((currentHUC, currentId))
                                        #print('Extended inlet {0} for {1}, currentId {2}, toId {3}'.format(inlet.asString(), otherHUC, currentId, toId))
                                toId = 0 - toId  # targets in different HUC12 marked by being negative
                            # make a terminal if necessary
                            # see if there is a terminal to same target already
                            terminal = terminals.setdefault(currentId, [])
                            #print('Terminal for currentId {0}: {1}'.format(currentId, terminal))
                            #print('toId {0}'.format(toId))
                            if not targetInTerminal(toId, terminal):  # avoid multiple terminal pairs
                                found = False
                                for comid, terminal1 in terminals.items():
                                    if comid != currentId:
                                        for ptId, target1 in terminal1:
                                            if toId == target1:
                                                terminal.append((ptId, toId))
                                                #print('Extended terminal with terminal for {0}: {1}'.format(comid, terminal))
                                                found = True
                                                break
                                        if found:
                                            break
                                if not found:
                                    self.pointIds[currentHUC] += 1
                                    terminal.append((self.pointIds[currentHUC], toId))
                                    #print('Added new terminal: {0}'.format(terminal))
                    targets.append(toId)
                if len(targets) == 0:
                    if self.isPlus or currentId not in minors:
                        Common.information('Comid {0} in huc {1} has no downstream comid: setting to zero'.format(currentId, currentHUC))
                    # assumption elsewhere that targets is not empty
                    targets = [0]
                elif len(targets) > 1:
                    # Common.information('Comid {0} in huc {1} has multiple downstream comids {2}'.format(currentId, currentHUC, targets))
                    targetsMap[currentId] = targets
                    #print('Targets from {0}: {1}'.format(currentId, targets))
                ds[currentId] = targets
            row5 = cursor.execute(sql5, (currentId,)).fetchone()
            if row5 is None:
                #Common.information('Comid {0} in huc {1} has no defined area'.format(currentId, currentHUC))
                # happens with very short flow lines, or because flowline is unattached
                area = 0.0
            else:
                area = float(row5[0])
            areas[currentId] = area
            if area == 0:
                noCatchments.add(currentId)
            for row6 in cursor.execute(sql6, (currentId,)).fetchall():
                try:
                    yearString = row6[1]
                    year = 0 if yearString is None or len(yearString) == 0 else int(yearString)
                    # can assume not all volumes are zero (or negative)
                    # Default emergency / principal ratio for volume is 1.1.
                    normVol = float(row6[4])
                    maxVol = max(float(row6[2]), float(row6[3]))
                    if maxVol <= 0:
                        maxVol = normVol * 1.1
                    elif normVol == 0:
                        normVol = maxVol / 1.1
                    reservoir = Waterbody('', row6[0], year, maxVol, normVol, float(row6[5]) * 100, float(row6[6]), float(row6[7]), float(row6[8]), True)
                    if currentId in reservoirs:
                        reservoirs[currentId].addWaterbody(reservoir, False)
                    else:
                        reservoirs[currentId] = reservoir
                except:
                    Common.error('Failed to read reservoir data for comid {0}: {1}: {2}'.format(currentId, row6, traceback.format_exc()))
            for row7 in cursor.execute(sql7, (currentId,)).fetchall():
                try:
                    normVol = float(row7[4])
                    maxVol = max(float(row7[2]), float(row7[3]))
                    area = float(row7[5])
                    volEstimated = False
                    surfaceAreaEstimated = False
                    if normVol == 0 and maxVol == 0:
                        if area == 0:
                            continue
                        else:
                            depth = Waterbody.estimateDepth( area / 100)
                            if depth == 0:  # too large for estimateDepth, area > 2 km2
                                # use pondVolFun
                                normVol = self.pondVolFun(area)
                                maxVol = normVol * 1.1
                            else:
                                normVol = area * depth
                                maxVol = normVol * 1.1
                        volEstimated = True
                    elif maxVol == 0:
                        maxVol = normVol * 1.1
                    elif normVol == 0:
                        normVol = maxVol / 1.1
                    if area == 0:
                        area = self.pondAreaFun(normVol)
                        surfaceAreaEstimated = True
                    pond = Waterbody(row7[0], row7[1], 0, maxVol, normVol, area, float(row7[6]), float(row7[7]), float(row7[8]), False)
                    pond.volEstimated = volEstimated
                    pond.surfaceAreaEstimated = surfaceAreaEstimated
                    ponds.setdefault(currentId, []).append(pond)
                except:
                    Common.error('Failed to read pond data for comid {0}: {1}: {2}'.format(currentId, row7, traceback.format_exc()))
            wetlandArea = 0.0
            for row8 in cursor.execute(sql8, (currentId,)).fetchall():
                try:
                    wetlandArea += float(row8[0])
                except:
                    Common.error('Failed to read wetland data for comid {0}: {1}: {2}'.format(currentId, row8, traceback.format_exc()))
            if wetlandArea > 0:
                wetlands[currentId] = wetlandArea
            playaArea = 0.0
            for row9 in cursor.execute(sql9, (currentId,)).fetchall():
                try:
                    playaArea += float(row9[0])
                except:
                    Common.error('Failed to read playa data for comid {0}: {1}: {2}'.format(currentId, row9, traceback.format_exc()))
            if playaArea > 0:
                playas[currentId] = playaArea
            
        reduceTerminals()
        #print('noCatchments for {0}: {1}'.format(currentHUC, noCatchments))
        #print('Terminals for {1} after populate: {0}'.format(terminals, currentHUC))
        #for huc12, inlets in self.inlets.items():
        #    print('Inlets 1 for {0}'.format(huc12))
        #    for inlet in inlets:
        #        print('  {0}'.format(inlet.asString()))
        self.reduceTargets(cursor, problems)
                
    def reduceTargets(self, cursor: Any, problems: Any) -> None:
        """Remove target with higher divergence when two share the same huc12 exit."""
                
        def removeMinors(dsIds: List[int], minors: Set[int]) -> List[int]:
            """Return dsIds after removing comids in minors"""
            result: List[int] = []
            for comid in dsIds:
                if comid > 0 and comid not in minors:
                    result.append(comid)
                elif comid in {-1, 0}:
                    result.append(comid)
                else:
                    huc = self.comidToHUC[abs(comid)]
                    if abs(comid) not in self.minors[huc]:
                        result.append(comid)
            return result
        
        def getPaths(comids: List[int], ds: Dict[int, List[int]], terminalIds: KeysView[int]) -> List[List[int]]:
            """Return list of possible paths from a comid in comids and terminating at an outlet."""
            
            def getPaths1(comid: int) -> List[List[int]]:
                """Return list of possible paths starting downstream from a comid in comids and terminating at an outlet.
                An outlet is a different huc, indicated by a negative comid, or a zero or a terminal"""
                if comid <= 0 or comid in terminalIds:
                    return [[comid]]
                dsIds = ds[comid]
                if len(dsIds) == 1:
                    return [[comid] + l for l in getPaths1(dsIds[0])]
                return [[comid] + l for l in getPaths1(dsIds[0]) + getPaths1(dsIds[1])]
            
            # code for getPaths
            listsOfPaths = [getPaths1(comid) for comid in comids]
            return [path for paths in listsOfPaths for path in paths]
        
#         def getOutlets(comids: List[int], ds: Dict[int, List[int]], terminalIds: KeysView[int]) -> List[int]:
#             """Return list of outlets starting from comids."""
#             
#             def getOutlet(comid: int) -> List[int]:
#                 """Return list of outlet starting from comid.  An outlet is a different huc, 
#                 indicated by a negative comid, or a terminal."""
#                 #print('Finding outlets for {0}'.format(comid))
#                 if comid <= 0 or comid in terminalIds:
#                     return [comid]
#                 dsIds = ds[comid]
#                 if len(dsIds) == 1:
#                     return getOutlet(dsIds[0])
#                 else:
#                     return getOutlet(dsIds[0]) + getOutlet(dsIds[1])
#                                        
#             # code for getOutlets 
#             listsOfOutlets = [getOutlet(comid) for comid in comids]
#             return [outlet for outlets in listsOfOutlets for outlet in outlets]

        def reducePaths(paths: List[List[int]], sql: str, terminals: Dict[int, List[Tuple[int, int]]]) -> List[List[int]]:
            """Reduce list by removing paths which converge at some comid, i.e. share equivalent comids.  
            Comids are equivalent if they are the same comid in huc12, 
            or the same huc if both diferent from huc12, or the same terminal outlet number."""
            
            def haveCommonComid(path1: List[int], path2: List[int]) -> bool:
                """Return true if there are comids in path1 and path2 respectively that are equivalent."""
            
#         def reduce(outlets: List[int], sql: str, terminals: Dict[int, Tuple[int, List[int]]]) -> List[int]:
#             """Reduce list by removing equivalent outlets.  Outlets are equivalent if they are the same comid in huc12, 
#             or the same huc if both diferent from huc12, or the same terminal outlet number."""
#             
#             def foundIn(outlet: int, outlets: List[int]) -> bool:
#                 """Return true if outlet equivalent to one in outlets."""
            
                def equivalent(comid1: int, comid2: int):
                    """Comids are equivalent if they are the same comid in huc12, , or the same terminal,
                    or have the same huc if both negative."""
                    if comid1 >= 0:
                        if comid2 == comid1:
                            return True
                        else:
                            outlets1 = [term[0] for term in terminals.get(comid1, [(-1, 0)])]
                            outlets2 = [term[0] for term in terminals.get(comid1, [(-2, 0)])]
                            for num in outlets1:
                                if num in outlets2:
                                    return True
                            return False
                    if comid2 >= 0:
                        return False
                    row1 = cursor.execute(sql, (0 - comid1,)).fetchone()
                    row2 = cursor.execute(sql, (0 - comid2,)).fetchone()
                    if row1 is None:
                        return row2 is None
                    return row1[0] == row2[0]
                    
#                 # code for foundIn
#                 for outlet1 in outlets:
#                     # use outlet == outlet1 or same teriminal id instead of equivalent if looking at single HUC12, 
#                     # since then all outlets are equivalent: selectcatch only has current HUC12s
#                     if equivalent(outlet, outlet1):  
#                         return True
#                 return False
#                 
#             # code for reduce
#             result: List[int] = []
#             for outlet in outlets:
#                 if not foundIn(outlet, result):
#                     result.append(outlet)
#             return result

                # code for haveCommonComid
                for comid1 in path1:
                    for comid2 in path2:
                        if equivalent(comid1, comid2):
                            return True
                return False
            
            # code for reducePaths
            result: List[List[int]] = []
            while len(paths) > 0:
                nextPath = paths.pop(0)
                canBeRemoved = False
                for path in paths:
                    if haveCommonComid(nextPath, path):
                        canBeRemoved = True
                        break
                if not canBeRemoved:
                    result.append(nextPath)
            return result
        
        def findTerminal(dsIds: List[int], currentHUC: str) -> Tuple[str, Optional[Tuple[int, int]]]:
            """If dsIds comtains a comid in common with the downstream ids for a terminal,
            return the name of the huc containing the terminal and the terminal
            else return ('', None).  
            
            The currentHUC is used to restrict the search for terminals to those in HUCs for which currentHUC is the same or an upper."""
            for huc12, terminals in self.terminals.items():
                if huc12 == currentHUC or currentHUC in self.getUppers(huc12, cursor):
                    for terminal in terminals.values():
                        for outletId, dsId in terminal:
                            for comid in dsIds:
                                if comid != 0:
                                    if comid == dsId:
                                        return huc12, (outletId, comid)
            return '', None
        
        def getCoastlineHUC(comid: int, currentHUC: str) -> Optional[str]:
            """Return huc12 if comid is in coastline comids for that huc12, and it is not the currentHUC, else None."""
            sql3 = 'SELECT huc12 FROM {0} WHERE comid = ?'.format(self.selectcatchTable)
            row3 = cursor.execute(sql3, (comid,)).fetchone()
            if row3 is None or row3[0] == currentHUC:
                return None
            if comid in self.coastIds[row3[0]]:
                return row3[0]
            return None
            
        # code for reduceTargets
        sql2 = 'SELECT huc12 FROM {0} WHERE comid = ?'.format(self.selectcatchTable)
        # collect terminal points, and set their self.dsCatchment value to zero
        # and also collect points with a divergence 9, since these may be rerouted to terminal outlets
        # and collect minor comids = streams with divergence 2
        # We save changes to minors until they have been matched (and may be made major because unmtached)
        # until we have also removed them from inlets and terminals
        newMinors: Dict[str, Set[int]] = dict()
        for huc12, ds in self.dsCatchments.items():
            #print('ds for {1} before reducing targets: {0}'.format(ds, huc12))
            terminals = self.terminals[huc12]
            #print('Terminals for {1} before coastline added: {0}'.format(terminals, huc12))
            minors = self.minors[huc12]
            # reroute points with downstream comid in common with terminal downstream comid to the terminal if in the same HUC,
            # and in any case to be an outlet
            # Note this must be done before changing the terminal to not route to a coastline in a different HUC: see below
            for coastId in self.coastIds[huc12]:
                dsIds = ds[coastId]
                terminalHUC, term =  findTerminal(dsIds, huc12)
                #print('Coast id {0}, downstream {1}, terminalHUC {2}, dsId {3}, outletId {4}'.format(coastId, dsIds, terminalHUC, dsId, outletId))
                if term is not None:
                    outletId, dsId  = term
                    if terminalHUC == huc12:
                        self.terminals[huc12][coastId] = [(outletId, dsId)]
            #print('Terminals for {1} after coastline added: {0}'.format(terminals, huc12))
            if self.isPlus:
                # distinguish between splits that converge within the huc12 and splits that lead to different outlets
                # we need to repeatedly look for splits that can be removed,
                # since if there is a split at a, and one branch splits at b before all three join again,
                # we will get a false result if we try to remove the split at a before the split at b
                reduced = True  # flag to show some change in last cycle
                actualBifurcations: Dict[int, List[int]] = dict()
                removed: Set[int] = set()
                while reduced:
                    reduced = False
                    #print('Bifurcations: {0}'.format(self.bifurcations[huc12]))
                    for comid in self.bifurcations[huc12]:
                        targets = ds[comid]
                        if len(targets) > 1:
#                             outlets0 = getOutlets(targets, ds, terminals.keys())
#                             #print('Outlets from {0}: {1}'.format(comid, outlets0))
#                             outlets = reduce(outlets0, sql2, terminals)
#                             #print('  After reduce: {0}'.format(outlets))
#                             if len(outlets) == 1:
                            paths0 = getPaths(targets, ds, terminals.keys())
                            #print('Paths from {0}:'.format(comid))
                            # for path in paths0:
                            #     print('{0}'.format(path))
                            paths = reducePaths(paths0, sql2, terminals)
                            #print('Reduced paths from {0}:'.format(comid))
                            # for path in paths:
                            #     print('{0}'.format(path))
                            if len(paths) == 1:
                                # can remove the split
                                dsIds = []
                                # make the split by collecting the divergence != 2 branches: should only be one
                                for downId in targets:
                                    if downId in minors:
                                        removed.add(downId)
                                        continue
                                    dsIds.append(downId)
                                if len(dsIds) > 1:
                                    # eg one or more negative ones escaped the minors check.  
                                    # The paths are equivalent, so select a positive one (ie within this huc, and will be not minor) if possible
                                    # and mark other to be removed
                                    if dsIds[0] >= 0:
                                        removed.add(dsIds[1])
                                        dsIds = [dsIds[0]]
                                    else:
                                        removed.add(dsIds[0])
                                        dsIds = [dsIds[1]]
                                #print('{0} selected as downstream from {1}'.format(dsIds[0], comid))
                                ds[comid] = dsIds
                                if comid in actualBifurcations:
                                    del actualBifurcations[comid]
                                    # removing comid from self.bifurcations not allowed since looping on this
                                    # del self.bifurcations[huc12][comid]
                                    reduced = True
                                    #print('removed split at {0}'.format(comid))
                            else:
                                # put minor comid second if necessary
                                if abs(targets[0]) in minors:
                                    if abs(targets[1]) in minors:
                                        Common.error('{0} and {1} downstrem from {2} in huc12 {3} both with divergence 2'.
                                                              format(abs(targets[0]), abs(targets[1]), comid, huc12),
                                                              problems)
                                    else:
                                        targets = [targets[1], targets[0]]
                                        ds[comid] = targets
                                actualBifurcations[comid] = [path[-1] for path in paths]
                for comid, outlets in actualBifurcations.items():
                    problems.write('Genuine bifurcation in huc12 {0} from {1}\n'.format(huc12, comid))
                    for outlet in outlets:
                        row = cursor.execute(sql2, (abs(outlet),)).fetchone()
                        if row is None:
                            problems.write('    {0} in None\n'.format(abs(outlet)))
                        else:
                            problems.write('    {0} in HUC12 {1}\n'.format(abs(outlet), row[0]))
                # delete removed comids from inlets and terminals
                #print('{0} removed from {1}'.format(removed, huc12))
                for comid in removed:
                    if comid < 0:
                        row = cursor.execute(sql2, (abs(comid),)).fetchone()
                        if row is not None:
                            inlets = self.inlets.get(row[0], set())
                            toRemove: Set[HUC14InletData] = set()
                            #print('Inlets before removal:')
                            #for inlet in inlets:
                            #    print(inlet.asString())
                            for inlet in inlets:
                                for comid in removed:
                                    if inlet.inletComid == abs(comid):
                                        toRemove.add(inlet)
                            for inlet in toRemove:
                                inlets.remove(inlet)
                            #print('Inlets after removal:')
                            #for inlet in inlets:
                            #    print(inlet.asString())
                terminals = self.terminals.get(huc12, dict())
                for terminal in terminals.values():
                    termsToRemove = []
                    for (outletId, dsId) in terminal:
                        if dsId in removed:
                            termsToRemove.append((outletId, dsId))
                    for term in termsToRemove:
                        terminal.remove(term)
            else:  # not self.isPlus
                #print('ds for {1} before matching majors and minors: {0}'.format(ds, huc12))
                # remove divergence 2 comids from dsCatchments map
                # but first make set of paired minor-major catchments
                # later their catchments will be merged, so minor area not lost when minor is removed
                minorMajorPairs: Set[Tuple[int, int]] = set()
                self.minorMajor[huc12] = minorMajorPairs
                makeMajor: Set[int] = set()
                for minor in minors:
                    # find upstream comid, and parallel major
                    major = -1
                    for comid, targets in ds.items():
                        if minor in targets:
                            for target in targets:
                                if target > 0 and target not in minors:
                                    major = target
                                    minorMajorPairs.add((minor, major))
                                    break
                            if major < 0:
                                # parallel downstream perhaps outside huc12
                                # use upstream if not minor
                                if comid not in minors:
                                    major = comid
                                    minorMajorPairs.add((minor, major))
                    if major < 0:
                        # upper comid may have been outside huc12
                        # use downstream major instead
                        targets = ds.get(minor, [])
                        for target in targets:
                            if target > 0 and target not in minors:
                                major = target
                                minorMajorPairs.add((minor, major))
                                break
                    if major < 0:
                        problems.write('Could not find major for minor {0} in huc12 {1}\n'.format(minor, huc12))
                        # treat it as a major instead, else can have catchment with no stream
                        makeMajor.add(minor)
                minors2 = minors.difference(makeMajor)
                newMinors[huc12] = minors2
                # if a minor comid has a non-minor downstream comid, with no upstream except the minor,
                # make it a starter,
                # since it will effectively become one when minor is then removed
                for minor in minors2:
                    majorTargets = [t for t in ds.get(minor, []) if t not in minors and t > 0]
                    for target in majorTargets:
                        upIds = {upId for upId in ds if target in ds[upId] and upId != minor}
                        if len(upIds) == 0:
                            self.starters[huc12].add(target)
                    # remove minor from ds
                    if minor in ds:
                        del ds[minor]
                for comid in ds:
                    ds[comid] = removeMinors(ds[comid], minors2)
                    # also remove any -1 entries if still have a pair, since other must be major or also -1
                    targets = ds[comid]
                    if len(targets) > 1:
                        targets = [t for t in targets if t != -1]
                        if len(targets) == 0:
                            targets = [-1]
                        ds[comid] = targets
                #print('Minors: {0}'.format(minors2))
                #print('ds for {1}: {0}'.format(ds, huc12))
                # remove minors from inlets map
                inlets = self.inlets.get(huc12, set())
                toRemove = set()
                for inlet in inlets:
                    if inlet.inletComid in minors2:
                        toRemove.add(inlet)
                for inlet in toRemove:
                    inlets.remove(inlet)
                #print('Inlets 2 for {0}'.format(huc12))
                #for inlet in inlets:
                #    print('  {0}'.format(inlet.asString()))
                # remove minors from terminals
                # these must be all minors, including those made major, to avoid split outlets:
                terminals = self.terminals.get(huc12, dict())
                for minor in minors:
                    if minor in terminals:
                        del terminals[minor]
                comidsToRemove = []
                for comid, terminal in terminals.items():
                    termsToRemove = []
                    for (outletId, dsId) in terminal:
                        absId = abs(dsId)
                        if absId > 1:
                            huc = self.comidToHUC[absId]
                            if absId in self.minors[huc]:
                                termsToRemove.append((outletId, dsId))
                    for term in termsToRemove:
                        terminal.remove(term)
                    if len(terminal) == 0:
                        comidsToRemove.append(comid)
                for comid in comidsToRemove:
                    del terminals[comid]
                #print('Terminals for {1} after minors removed: {0}'.format(terminals, huc12))
#                 # if the dsId for a terminal is coastline in a different HUC12, change it to -1
#                 # this gives precedence to outlets over coastline flows across HUC12 boundaries
#                 # and eventually removes a loop in HUC10 model 01100004
#                 # note this must be done after rerouting coastlines to terminals: see above
#                 for comid, terminal in terminals.items():
#                     for i in range(len(terminal)):
#                         (outlet, dsId) = terminal[i]
#                         if dsId < 0 and dsId != -1:
#                             otherHUC = getCoastlineHUC(0 - dsId, huc12)
#                             if otherHUC is not None:
#                                 terminal[i] = outlet, -1
#                                 ds[comid] = [-1]
#                                 inlets = self.inlets.get(otherHUC, set())
#                                 toRemove = set()
#                                 for inlet in inlets:
#                                     if inlet.inletComid == 0 - dsId:
#                                         toRemove.add(inlet)
#                                 for inlet in toRemove:
#                                     inlets.remove(inlet)
#                 #print('Terminals for {1} after terminal to coastline removed: {0}'.format(terminals, huc12)) 
        # update minors map for any that have been made major
        for huc12, minors2 in newMinors.items():
            self.minors[huc12] = minors2
                     
        
#     def extendTargets(self, cursor: Any) -> None:
#         """Extend targets list into lists of paths, terminating where they become zero or enter another huc12 or exit into ocean etc."""
#         
#         def fullPath(huc12: str, comid: int, soFar: List[int], ds: Dict[int, List[int]]) -> List[Tuple[List[int], Optional[str]]]:
#             if comid in soFar:
#                 soFar.append(comid)
#                 Common.error('Loop in path of huc12 {0}: {1}'.format(huc12, soFar))
#                 return [([], '')]
#             result = soFar
#             result.append(comid)
#             nextId = comid
#             if nextId == 0:
#                 return [(result, '')]
#             elif nextId < 0:
#                 sql = 'SELECT huc12 FROM {0} WHERE comid = ?'.format(self.selectcatchTable)
#                 row = cursor.execute(sql, (abs(nextId),)).fetchone()
#                 if row is None:
#                     return [(result, None)]
#                 else:
#                     return [(result, row[0])]
#             else:
#                 downIds: List[int] = ds.get(nextId, [])
#                 if downIds == []:
#                     return [(result, '')]
#                 paths = []
#                 for dsId in downIds:
#                     ps = fullPath(huc12, dsId, result, ds)
#                     print('Split path from {0}: {1}'.format(dsId, ps))
#                     paths.extend(ps)
#                 return paths
#             
#         for huc12, targetsMap in self.bifurcations.items():
#             ds = self.dsCatchments[huc12]
#             paths: List[Tuple[List[int], Optional[str]]] = []
#             for fromId in targetsMap:
#                 for target, _ in targetsMap[fromId]:
#                     paths.extend(fullPath(huc12, target[0], [], ds))
#                 targetsMap[fromId] = paths
#                 print('Paths from {0}: '.format(fromId, paths))
#                 
#                         
#     def reduceTargets(self, problems: Any) -> None:
#         """Resolve bifurcations into single link using divergence field from flowline.  Update dsCatchments."""
#         
#         sql = 'SELECT divergence FROM flowline WHERE comid=?'
#         for huc12, targetsMap in self.bifurcations.items():
#             ds = self.dsCatchments[huc12]
#             for fromId, targets in targetsMap.items():
#                 dsIds = ds[fromId]
#                 assert len(targets) > 1
#                 # collect paths ending in different HUC, using divergence to select the major one when two end in same HUC
#                 for i in range(len(targets)):
#                     for j in range(i+1, len(targets)):
#                         if targets[i][1] == targets[j][1]:
#                             absTarget1 = abs(targets[i][0][0])
#                             row1 = self.flowlineConn.execute(sql, (absTarget1,)).fetchone()
#                             divergence1 = int(row1[0])
#                             if divergence1 == 2:  # minor
#                                 dsIds.remove(targets[i][0][0])
#                             else:
#                                 dsIds.remove(targets[j][0][0])
#                 if len(dsIds) > 1:
#                     problems.write('Channel {0} in HUC12 {1} has a bifurcation:\n'.
#                                        format(fromId, huc12))
#                     for target in targets:
#                         path = target[0]
#                         start = path[0]
#                         if start in dsIds:
#                             problems.write('   Path {0} leading to {1}\n'.format(path, target[1]))
            
    def mergeBasins(self, selection: str, withShapes: bool) -> str:
        """Merge HUC12 watershed selected by selection, and write stats to stats.csv"""
        # self.setLayers()
        extDir = '/Fields_CDL' if self.isCDL else '/Fields'
        SWATDir = '/SWATPlus' + extDir if self.isPlus else '/SWAT' + extDir
        parent = Files.ModelsDir + SWATDir + Files.HUC14ProjectParent + '/' + selection
        if not os.path.exists(parent):
            os.makedirs(parent)
        statsFile = posixpath.join(parent, 'stats.csv')
        problemsFile = posixpath.join(parent, 'problems.txt')
        self.batchRerun = posixpath.join(parent, 'rerun.bat')
        if os.path.isfile(self.batchRerun):
            os.remove(self.batchRerun)
        self.comidsDb = parent + '/COMIDS' + selection + '.sqlite'
        if os.path.isfile(self.comidsDb):
            os.remove(self.comidsDb)
        shutil.copy(posixpath.join(Files.DbDir, 'COMIDS.sqlite'), self.comidsDb)
        self.waterBodiesDb = parent + '/WaterBodies.sqlite'
        if not os.path.isfile(self.waterBodiesDb) or withShapes:
            shutil.copy(posixpath.join(Files.DbDir, 'WaterBodies.sqlite'), self.waterBodiesDb)
        self.waterConn = sqlite3.connect(self.waterBodiesDb)  # @UndefinedVariable
        if withShapes:
            sql = 'DELETE FROM reservoirs;'
            self.waterConn.execute(sql)
            sql = 'DELETE FROM ponds;'
            self.waterConn.execute(sql)
            sql = 'DELETE FROM wetlands;'
            self.waterConn.execute(sql)
            sql = 'DELETE FROM playas;'
            self.waterConn.execute(sql)
            self.waterConn.commit()
        if not withShapes:
            if self.withProject:
                pattern = parent + '/huc*'
                for d in glob.iglob(pattern):
                    if not os.path.isdir(d):
                        continue
                    huc12 = os.path.split(d)[1][3:]
                    project = CreateProject(huc12, parent, Files.DbDir, self.isPlus, self.withProject, True)
                    projDir = project.projDir
                    region = huc12[:2]
                    Common.addRaster(region, 'DEM', self.isPlus, self.isCDL, projDir)
                    Common.addRaster(region, 'soil', self.isPlus, self.isCDL, projDir)
                    Common.addRaster(region, 'landuse', self.isPlus, self.isCDL, projDir)
            return problemsFile
        wshedFields, _ = self.common.createWatershedFields()
        # QgsVectorFileWriter.create generates a writer that only flushes when program terminates
        self.combinedWshedLayer = QgsVectorLayer('Polygon?crs=epsg:5072', 'combinedWshed', 'memory')
        combinedWshedProvider = self.combinedWshedLayer.dataProvider()
        combinedWshedProvider.addAttributes(wshedFields.toList())
        self.combinedWshedLayer.updateFields()
        # QgsVectorFileWriter.create(combinedWshed, wshedFields, QgsWkbTypes.Polygon, self.crsProject, 
        #                                                       QgsCoordinateTransformContext(), self.common.options)
        self.combinedStreamsLayer = QgsVectorLayer('LineString?crs=epsg:5072', 'combinedStreams', 'memory')
        combinedStreamsProvider = self.combinedStreamsLayer.dataProvider()
        combinedStreamsProvider.addAttributes(self.channelFields.toList())
        self.combinedStreamsLayer.updateFields()
        # QgsVectorFileWriter.create(combinedStreams, self.channelFields, QgsWkbTypes.LineString, self.crsProject, 
        #                                                       QgsCoordinateTransformContext(), self.common.options)
        self.combinedChannels0 = parent + '/combinedChannels0.shp'
        if self.makeOriginals:
            self.combinedWshed0Layer = QgsVectorLayer('Polygon?crs=epsg:5072', 'combinedWshed0', 'memory')
            combinedWshed0Provider = self.combinedWshed0Layer.dataProvider()
            combinedWshed0Provider.addAttributes(wshedFields.toList())
            self.combinedWshed0Layer.updateFields()
            # QgsVectorFileWriter.create(combinedWshed0, wshedFields, QgsWkbTypes.Polygon, self.crsProject, 
            #                                                       QgsCoordinateTransformContext(), self.common.options)
            
        hucCount = 0
        hucArea = 0.0
        totalCatchmentCount = 0
        totalSubCount = 0
        totalStreamCount = 0
        subMin = float('inf')
        subMax = 0.0
        with open(statsFile, 'w') as stats, open(problemsFile, 'w') as problems:
            stats.write('HUC12, NumCatchments, Area, NumSubs, NumStreams, MinSize, MaxSize, MeanSize\n')
#             self.extraCatchmentsCount = 0
            with sqlite3.connect(Files.HUC12Data) as conn, sqlite3.connect(self.comidsDb) as comidsConn:  # @UndefinedVariable
                cursor = conn.cursor()
                comidsCursor = comidsConn.cursor()
                #time1 = time.process_time()
                self.populateTables(selection, cursor, problems)
                #time2 = time.process_time()
                # self.extendTargets(cursor)
                # self.reduceTargets(problems)
                #Common.information('Populating tables took {0} seconds'.format(int((time2 - time1) + 0.5)))
                for huc12 in sorted(self.catchmentAreas):
                    #time4a = time.process_time()
                    project = CreateProject(huc12, parent, Files.DbDir, self.isPlus, self.withProject, False)
                    self.projectDirs[huc12] = project.projDir
                    self.mergeTargets[huc12] = dict()
                    projDir = project.projDir
                    del project
                    # may have already been set by populateTables
                    self.pointIds.setdefault(huc12, Common.basePointId)
                    selectHUC8 = huc12[:8]
                    sql8 = "SELECT area from huc8areas WHERE huc8 LIKE '{0}'".format(selectHUC8)
                    row8 = cursor.execute(sql8).fetchone()
                    self.huc8Area = float(row8[0])
                    Common.information('HUC12 {0}'.format(huc12))
                    #time4b = time.process_time()
                    #Common.information('Creating project directories took {0} milliseconds'.format(int((time4b - time4a) * 1000 + 0.5)))
                    self.createWatershedShapefile(huc12, problems)
                    #time5 = time.process_time()
                    #Common.information('Creating subbasins shapefile took {0} milliseconds'.format(int((time5 - time4b) * 1000 + 0.5)))
                    if self.subbasinsLayer is None:
                        if os.path.isdir(projDir):
                            try:
                                shutil.rmtree(projDir)
                                #print('Deleted directory {0}'.format(projDir))
                                if not self.isPlus and os.path.isfile(projDir + '.qgs'):
                                    os.remove(projDir + '.qgs')
                            except:
                                Common.information('Please delete directory {0}'.format(projDir))
                        continue
                    self.createStreamsShapefile(huc12, cursor, problems)
                    #time6 = time.process_time()
                    #Common.information('Creating streams shapefile took {0} milliseconds'.format(int((time6 - time5) * 1000 + 0.5)))
                    if self.streamsLayer is None:
                        if os.path.isdir(projDir):
                            try:
                                shutil.rmtree(projDir)
                                #print('Deleted directory {0}'.format(projDir))
                                if not self.isPlus and os.path.isfile(projDir + '.qgs'):
                                    os.remove(projDir + '.qgs')
                            except:
                                Common.information('Please delete directory {0}'.format(projDir))
                        continue
                    area, subCount, minArea, maxArea, streamCount = self.mergeBasin(huc12, cursor, comidsCursor, stats, problems)
                    # time7 = time.process_time()              
                    # Common.information('Merging catchments took {0} milliseconds'.format(int((time7 - time6) * 1000 + 0.5)))
                    # reduce channel count by 1 zero length channel for each inlet
                    streamCount -= len(self.inlets.get(huc12, set()))
                    if subCount != streamCount:
                        # check against layers
                        expr = QgsExpression('"LINKNO" < {0}'.format(Common.basePointId))
                        streamCount2 = len(list(self.streamsLayer.dataProvider().getFeatures(QgsFeatureRequest(expr))))
                        subCount2 = self.subbasinsLayer.dataProvider().featureCount()
                        if subCount2 != streamCount2:
                            problems.write('HUC12 {0} has {1} subbasins and {2} non-inlet streams\n'.format(huc12, subCount2, streamCount2))
                        subCount = subCount2
                        streamCount = streamCount2
                    if subCount == 0 or streamCount == 0:
                        problems.write('HUC12 {0} has {1} subbasins and {2} non-inlet streams.  Is it missing?\n'.
                                       format(huc12, subCount, streamCount))
                    totalCatchmentCount += self.totalCatchments[huc12]
                    totalSubCount += subCount
                    totalStreamCount += streamCount
                    hucCount += 1
                    hucArea += area
                    subMin = min(subMin, minArea)
                    subMax = max(subMax, maxArea)
                
                # have to process all channels files before we can assume all inlet points are defined
                for huc12 in self.catchmentAreas:
                    #time7 = time.process_time()
                    projDir = self.projectDirs[huc12]
                    if os.path.isdir(projDir):  # could have ben removed as empty
                        pointsFile = projDir + '/Watershed/Shapes/points.shp'
                        self.createPointsShapefile(huc12, pointsFile, problems)
                        #time8 = time.process_time()
                        #Common.information('Creating points shapefile took {0} milliseconds'.format(int((time8 - time7) * 1000 + 0.5)))
                        if self.withProject:
                            region = huc12[:2]
                            Common.addRaster(region, 'DEM', self.isPlus, self.isCDL, projDir)
                            Common.addRaster(region, 'soil', self.isPlus, self.isCDL, projDir)
                            Common.addRaster(region, 'landuse', self.isPlus, self.isCDL, projDir)
                        #time9 = time.process_time()
                        #Common.information('Creating DEM took {0} milliseconds'.format(int((time9 - time8) * 1000 + 0.5)))
                meanArea = 0.0 if totalSubCount == 0 else hucArea / totalSubCount
                stats.write('huc{0}, {7}, {1:.1F}, {2}, {3}, {4:.1F}, {5:.1F}, {6:.1F}\n'.
                        format(selection, hucArea, totalSubCount, totalStreamCount, subMin, subMax, meanArea, totalCatchmentCount))
#                 problems.write('Count of HUC12s with external catchments: {0}\n'.format(self.extraCatchmentsCount))
                conn.commit()
                sql = 'DROP TABLE IF EXISTS {0}'.format(self.selectcatchTable)
                cursor.execute(sql)
                sql = 'DROP TABLE IF EXISTS {0}'.format(self.selectReservoirTable)
                cursor.execute(sql)
                sql = 'DROP TABLE IF EXISTS {0}'.format(self.selectPondTable)
                cursor.execute(sql)
                sql = 'DROP TABLE IF EXISTS {0}'.format(self.selectWetlandTable)
                cursor.execute(sql)
                sql = 'DROP TABLE IF EXISTS {0}'.format(self.selectPlayaTable)
                cursor.execute(sql)
                # huc12 database can get very large, and select... tables are regularly generated and removed
                sql = 'VACUUM'
                cursor.execute(sql)
                conn.commit()
            self.alignInletsOutlets(parent, problems)
            if not self.isPlus:
                self.fixChannels(parent, problems)
                self.fixCombinedChannels0()
#         if not self.isPlus and self.withProject:
#             # update submapping and Channels tables
#             command = '{0} {1}'.format(Files.submappingToAccess, self.projectDirs[huc12] + '/huc' + huc12 + '.mdb')
#             os.system(command)
            combinedWshed = parent + '/combinedWshed.shp'
            combinedStreams = parent + '/combinedStreams.shp'
            error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(self.combinedWshedLayer, combinedWshed, QgsCoordinateTransformContext(), 
                                                                   self.common.options)
            if error != 0:  # QgsVectorFileWriter.NoError
                Common.error('Failed to write subbasins file {0}: {1}'.format(combinedWshed, msg),
                             problems)
            error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(self.combinedStreamsLayer, combinedStreams, QgsCoordinateTransformContext(), 
                                                                   self.common.options)
            if error != 0:  # QgsVectorFileWriter.NoError
                Common.error('Failed to write subbasins file {0}: {1}'.format(combinedStreams, msg),
                             problems)
            if self.makeOriginals:
                combinedWshed0 = parent + '/combinedWshed0.shp'
                assert self.combinedWshed0Layer is not None
                error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(self.combinedWshed0Layer, combinedWshed0, QgsCoordinateTransformContext(), 
                                                                       self.common.options)
                if error != 0:  # QgsVectorFileWriter.NoError
                    Common.error('Failed to write subbasins file {0}: {1}'.format(combinedWshed0, msg),
                                 problems)
            self.markPonds()
        #time3 = time.process_time()
        #Common.information('Merging catchments took {0} seconds'.format(int((time3 - time2) + 0.5)))
        return problemsFile
             
    def mergeBasin(self, huc12: str, cursor: Any, comidsCursor: Any, stats: Any, problems: Any) -> Tuple[float, int, float, float, int]:
        """Merge downstream catchments for an HUC basin by area into a map of merged nodes to downstream merged nodes."""
            
        def getJunctions(currentDs: Dict[int, List[int]]) -> Set[FrozenSet[int]]:
            """Return set of sets of catchments which share a common downstream catchment"""
            junctions: Set[FrozenSet[int]] = set()
            for dsIds in currentDs.values():
                for dsId in dsIds:
                    if dsId > 0:
                        upIds = {upId for upId in currentDs.keys() if dsId in currentDs[upId]}
                        if len(upIds) > 1:
                            junctions.add(frozenset(upIds))
            return junctions
                    
        def isLastComponent(junctions: Set[FrozenSet[int]], comid: int) -> bool:
            """Return true if there is a junction with comid as the last component."""
            for junction in junctions:
                if len(junction) == 1 and comid in junction:
                    return True
            return False
        
        def isJunctionComponent(comid: int, junctions: Set[FrozenSet[int]]) -> bool:
            """Return True if comid flows into a junction."""
            for junction in junctions:
                if comid in junction:
                    return True
            return False
            
        def removeJunctionComponent(junctions: Set[FrozenSet[int]], comid: int) -> None:
            """Remove comid from any junctions it is in.  Remove singular junctions."""
            for junction in junctions:
                if comid in junction:
                    junctions.remove(junction)
                    junction1 = frozenset({x for x in junction if x != comid})
                    if len(junction1) > 1:
                        junctions.add(junction1)
                    return
                
        def isInlet(comid: int, inlets: Set[HUC14InletData]) -> bool:
            """Return true if comid is an inlet comid."""
            for inlet in inlets:
                if comid == inlet.inletComid:
                    return True
            return False
        
        def moveInlet(comid1: int, comid2, inlets: Set[HUC14InletData]) -> None:
            """Replace comid1 in inlets with comid2."""
            for inlet in inlets:
                if comid1 == inlet.inletComid:
                    inlet.inletComid = comid2 
        
        def removeStarter(catchment: int, dsCatchment: int, starters: Set[int], junctions: Set[FrozenSet[int]], provider: QgsVectorDataProvider, 
                          toDelete: List[int], startPoints: Dict[int, QgsPointXY], inlets: Set[HUC14InletData]) -> None:
            """Remove starter channel.  Make downstream comid (if any) a starter unless catchment is a junction component
            or downstream comid is an inlet comid."""
            if isJunctionComponent(catchment, junctions):
                removeJunctionComponent(junctionCatchments, catchment)
            elif dsCatchment > 0:
                if not isInlet(dsCatchment, inlets):
                    starters.add(dsCatchment)
                startPoints[dsCatchment] = startPoints[catchment]
            exp = QgsExpression('"LINKNO" = {0}'.format(catchment))
            link = next(provider.getFeatures(QgsFeatureRequest(exp)))  # type: ignore
            toDelete.append(link.id())
            
        def isTinyInlet(comid: int, inlets: Set[HUC14InletData], currentAreas: Dict[int, float]) -> bool:
            """Return true if comid is an inlet and its catchment area is tiny."""
            return currentAreas[comid] < HUC14Catchments.tinyThreshold and isInlet(comid, inlets)
        
        def terminalPartner(catchment: int, terminals: Dict[int, List[Tuple[int, int]]], areas: Dict[int, float]) -> Optional[int]:
            """If catchment is tiny and a terminal, and there is a terminal to same outlet for a different catchment,
            return the other catchment, as a candidate for merging with catchment, else return None."""
            if areas[catchment] >= HUC14Catchments.tinyThreshold:
                return None
            terminal = terminals.get(catchment, [])
            if len(terminal) == 0:
                return None
            for partner, partnerTerminal in terminals.items():
                if partner != catchment and terminal[0] in partnerTerminal:
                    return partner
            return None
        
        def dsReplace(minor: int, major: int, ds: Dict[int, List[int]]) -> None:
            """Replace any occurrence of minor as a target in downstream map with major."""
            for comid in ds:
                targets = ds[comid]
                for i in range(len(targets)):
                    if targets[i] == minor:
                        if major in targets:
                            del targets[i]
                        else:
                            targets[i] = major
                            
        def terminalsReplace(minor: int, major: int, terminals: Dict[int, List[Tuple[int, int]]]) -> None:
            """Any terminal for minor replaced by same terminal for major, unless major already has one"""
            if major in terminals:
                if minor in terminals:
                    del terminals[minor]
            else:
                terminal = terminals.get(minor, None)
                if terminal is not None:
                    terminals[major] = terminal
                    del terminals[minor]
                        
        # code of mergeBasin
        areas = self.catchmentAreas[huc12]
        starters = self.starters[huc12]
        noCatchments = self.noCatchments[huc12]
        ds = self.dsCatchments[huc12]
        terminals = self.terminals[huc12]
        currentDs: Dict[int, List[int]] = dict()
        currentAreas: Dict[int, float] = dict()
        mergeTargets = self.mergeTargets[huc12]
        inlets = self.inlets.get(huc12, set())
        startPoints = self.startPoints[huc12]
        # copy ds into current
        for source, targets in ds.items():
            currentDs[source] = targets
        #print('CurrentDs: {0}'.format(currentDs))
        # copy areas into current, and calculate totalArea 
        totalArea = 0.0      
        for catchment, area in areas.items():
            currentAreas[catchment] = area
            totalArea += area
        #print('Original current areas: {0}'.format(currentAreas))
        upperBound = HUC14Catchments.mergeThreshold
#         junctionThreshold = HUC14Catchments.junctionThreshold
        #mergeTime = 0
        #mergeCatchmentTime = 0
        #mergeChannelTime = 0
        assert self.subbasinsLayer is not None
        subbasinsProvider = self.subbasinsLayer.dataProvider()
        mergedSubbasins: Dict[int, QgsFeature] = dict()
        subbasinsToDelete: List[int] = []
        assert self.streamsLayer is not None
        streamsProvider = self.streamsLayer.dataProvider()
        mergedStreams: Dict[int, QgsFeature] = dict()
        streamsToDelete: List[int] = []
        junctionCatchments = getJunctions(currentDs)
        if not self.isPlus:
            # first merge the paired minor-major catchments, minor into major, and remove noCatchment streams
            for minor, major in self.minorMajor[huc12]:
                currentAreas[major] += currentAreas[minor]
                del currentAreas[minor]
                #print('Removed minor {0} for major {1}'.format(minor, major))
                self.mergeSubbasins(huc12, minor, major, noCatchments, subbasinsProvider, mergedSubbasins, subbasinsToDelete)
                # replace minor with major in currentDs
                dsReplace(minor, major, currentDs)
                terminalsReplace(minor, major, terminals)
        #print('Terminals after fixing minors: {0}'.format(terminals))
        # loop continues until changed is false
        changed = True
        # initally concentrate on removing starters
        removingStarters = True
        while changed:
            changed = False
            #print('CurrentDs: {0}'.format(currentDs))
            #print('Current areas: {0}'.format(currentAreas))
            #print('Junctions: {0}'.format(junctionCatchments))
            #time1 = time.process_time()
            # find next catchment pair to merge: smallest with area beneath tiny threshold, 
            # or pair with smallest combined area
            tinyArea = HUC14Catchments.tinyThreshold
            tinyUpCatchment = -1
            tinyUpArea = 0.0
            tinyDownCatchment = -1
            tinyDownArea = 0.0
            smallestArea = totalArea
            smallestAreaPair = (-1, -1)
            for catchment, dsCatchments in currentDs.items():
                if catchment < -1:  # closed catchment
                    continue
                if removingStarters and catchment not in starters:
                    continue
                if len(dsCatchments) > 1:  # bifurcation
                    continue
                if catchment in terminals:
                    # if catchment is tiny and is a terminal and there is another catchment to same terminal outlet
                    # can be removed by merging with other terminal
                    dsCatchment = terminalPartner(catchment, terminals, currentAreas)
                    if dsCatchment is None:
                        continue
                    #print('Terminal partner of {0} is {1}, no catchment is {2}'.format(catchment, dsCatchment, dsCatchment in noCatchments))
                elif catchment not in starters and isJunctionComponent(catchment, junctionCatchments) and currentAreas[catchment] >= HUC14Catchments.tinyThreshold:
                    # check for tiny downstream outlet
                    dsCatchment = dsCatchments[0]
                    if dsCatchment in terminals and currentAreas[dsCatchment] < HUC14Catchments.tinyThreshold:
                        pass
                    else:
                        continue
#                 if catchment in terminals or len(dsCatchments) > 1 or \
#                     (catchment not in starters and isJunctionComponent(catchment, junctionCatchments) and 
#                      not isTinyInlet(catchment, inlets, currentAreas)):
#                     # terminal or bifurcation  or non-starter into junction - cannot merge unless a tiny inlet
#                     # but if downstream catchment is tiny and itself in a junction break the normal no merging across junctions rule
#                     # This avoids tiny catchments persisting between junctions
#                     dsCatchment = dsCatchments[0]
#                     if dsCatchment > 0 and areas[dsCatchment] < HUC14Catchments.tinyThreshold and isJunctionComponent(dsCatchment, junctionCatchments):
#                         pass
#                     else:
#                         # or if catchment is tiny and is a terminal and there is another catchment to same terminal outlet
#                         # can be removed by merging with other terminal
#                         dsCatchment = terminalPartner(catchment, terminals, currentAreas)
#                         if dsCatchment is None:
#                             continue
                else:
                    dsCatchment = dsCatchments[0]
                thisArea = currentAreas[catchment]
                if dsCatchment <= 0:
                    # check if outlet catchment is tiny
                    if thisArea < tinyArea:
                        tinyUpCatchment = catchment
                        tinyUpArea = thisArea
                        tinyArea = thisArea
                        tinyDownCatchment = -1
                        tinyDownArea = totalArea # ensure we will find the upper one smaller
                    continue
                downArea = currentAreas[dsCatchment]
                area = thisArea + downArea
                if thisArea < tinyArea or downArea < tinyArea:
                    tinyUpCatchment = catchment
                    tinyUpArea = thisArea
                    tinyArea = min(thisArea, downArea)
                    tinyDownCatchment = dsCatchment
                    tinyDownArea = downArea
                if area < smallestArea:
                    smallestArea = area
                    smallestAreaPair = (catchment, dsCatchment)
            if tinyUpCatchment == -1 and smallestAreaPair == (-1, -1):
                # no more candidates to merge
                if removingStarters:
                    removingStarters = False
                    changed = True
                continue
            if tinyUpCatchment != -1:
                # check if any upstream catchment is smaller than downstream one, but try not to remove a junction component
                # unless the catchment is a starter
                if removingStarters:
                    preferUpper = False # don't consider upper catchment, since we know this is a starter
                elif catchment in terminals:
                    preferUpper = False # dscatchment is in fact a terminal partner, and is not downstream from catchment
                elif tinyDownArea < tinyUpArea:
                    preferUpper = False # already have the lower is tiny, and moving up would lose it
                                        # could add code to look for one donstream from lower, perhaps TODO
                else:
                    tinyUpperCatchment = -1
                    tinyUpperArea = totalArea
                    upperIsJunction = tinyUpCatchment not in starters and isJunctionComponent(tinyUpCatchment, junctionCatchments)
                    for catch, dsCatches in currentDs.items():
                        lowerIsJunction = catch not in starters and isJunctionComponent(catch, junctionCatchments)
                        if catch in terminals or len(dsCatches) > 1 or isLastComponent(junctionCatchments, catch):
                            continue
                        else:
                            dsCatch = dsCatches[0]
                        if dsCatch == tinyUpCatchment:
                            area = currentAreas[catch]
                            if area < tinyUpperArea:
                                tinyUpperCatchment = catch
                                tinyUpperArea = area
                    if tinyUpperCatchment == -1:
                        # no possible upper found
                        preferUpper = False
                    # Have two possible merges: lower: tinyUpCatchment with tinyDownCatchment, or upper: tinyUpperCatchment with tinyCatchment(=dscatch)
                    # First prefer merge which does not put a junction within the merged area, i.e. upstream is not in a junction
                    elif lowerIsJunction == upperIsJunction:  # both true or both false: choose by area
                        preferUpper = tinyUpperArea < tinyDownArea
                    else: 
                        preferUpper = lowerIsJunction
                if preferUpper:
                    catchment =  tinyUpperCatchment
                    dsCatchment = tinyUpCatchment
                    smallestArea = tinyUpArea + tinyUpperArea
                elif tinyDownCatchment == -1:
                    # seem to have an huc12 consisting of only one tiny catchment
                    # or an odd no catchment stream that does not belong, eg 1726271 in 010200040309
                    if tinyUpCatchment in noCatchments:
                        catchment = tinyUpCatchment
                        dsCatchment = -1
                        smallestArea = 0
                        # treat it as a starter since nothing downstream to merge
                        starters.add(catchment) 
                    else:
                        Common.error('Comid {0} in {1} is stray or single stream'.format(tinyUpCatchment, huc12))
                        if removingStarters:
                            removingStarters = False
                            changed = True
                        continue
                else:
                    catchment = tinyUpCatchment
                    dsCatchment = tinyDownCatchment
                    smallestArea = tinyUpArea + tinyDownArea
            elif smallestArea < upperBound:
                catchment, dsCatchment = smallestAreaPair
            else:
                # no more candidates to merge
                if removingStarters:
                    removingStarters = False
                    changed = True
                continue
            # merge catchment and dsCatchment
            assert catchment in noCatchments or dsCatchment > 0, 'attempting to merge {0} in {1} into exit'.format(catchment, huc12)
            #print('Merging {0} into {1}'.format(catchment, dsCatchment))
            #print('Current areas: {0}'.format(currentAreas))
            #print('Junctions: {0}'.format(junctionCatchments))
            # use dsCatchment as id for merger
            area = currentAreas[catchment]
            if dsCatchment > 0:
                mergeTargets[catchment] = dsCatchment
            # update terminals
            terminal = terminals.get(catchment, None)
            if terminal is not None:
                if dsCatchment not in terminals:  # catchment may be being merged with another terminal to same outlet
                    terminals[dsCatchment] = terminal
                del terminals[catchment]
                #print('Terminals for {1} adjusted: {0}'.format(terminals, huc12))
                # remove the corresponding outlet entry, 
                # else if the partner is later removed the outlet will remain but nothing attached to it
                outlets = self.outlets.get(huc12, set())
                outletToRemove: Optional[Tuple[int, int, QgsPointXY]] = None
                for outlet in outlets:
                    (comid, _, _) = outlet
                    if comid == catchment:
                        outletToRemove = outlet
                        # outlet is to be removed, and needs to be removed from corresponding inlet's uppers
                        for HUC, (upperHUCs, _) in self.upperHUCs.items():
                            if huc12 in upperHUCs:
                                inletsList = list(self.inlets.get(HUC, set()))
                                for j in range(len(inletsList)):
                                    inlet = inletsList[j]
                                    if inlet.inletComid in [abs(x) for x in currentDs[catchment]]:
                                        uppers = inlet.uppers
                                        for i in range(len(uppers)):
                                            (HUC1, comid1) = uppers[i]
                                            if HUC1 == huc12 and comid1 == comid:
                                                #print('Removing upper {0} for removed outlet on comid {1}'.format(uppers[i], comid))
                                                del uppers[i]
                                                break
                                    if len(inlet.uppers) == 0:
                                        del inletsList[j]
                                        break
                                self.inlets[HUC] = set(inletsList)
                        break
                if outletToRemove is not None:
                    outlets.remove(outletToRemove)
            # update targets in currentDs
            for source, targets in currentDs.items():
                if len(targets) > 1:
                    continue
                else:
                    target = targets[0]
                if target == catchment:
                    currentDs[source] = [dsCatchment]
                    #print('down from {0} replaced by {1}'.format(source, catchment))
            # move inlet if any
            if dsCatchment > 0:
                moveInlet(catchment, dsCatchment, inlets)
                currentAreas[dsCatchment] = smallestArea
            del currentDs[catchment]
            #print('Current ds: {0}'.format(currentDs))
            del currentAreas[catchment]
            #time2 = time.process_time()
            #print(huc12 + '_7')
            self.mergeSubbasins(huc12, catchment, dsCatchment, noCatchments, subbasinsProvider, mergedSubbasins, subbasinsToDelete)
            #print(huc12 + '_8')
            #time3 = time.process_time()
            if catchment in starters:
                #print('Removing starter {0}'.format(catchment))
                removeStarter(catchment, dsCatchment, starters, junctionCatchments, streamsProvider, streamsToDelete, 
                              startPoints, inlets)
            else:
                #print('Merging {0}'.format(catchment))
                self.mergeStreams(catchment, dsCatchment, streamsProvider, mergedStreams, streamsToDelete, startPoints)
            changed = True
            #print('After merge {0} merged subbasins and {1} subbasins to delete'.format(len(mergedSubbasins), len(subbasinsToDelete)))
            #print('After merge {0} merged channels and {1} channels to delete'.format(len(mergedStreams), len(streamsToDelete)))
            #print(huc12 + '_9')
            #time4 = time.process_time()
            #mergeTime += time2 - time1
            #mergeCatchmentTime += time3 - time2
            #mergeChannelTime += time4 - time3
        #time5 = time.process_time()
        # use channels since very small subbasins may not exist
        self.setSWATIds(huc12, mergeTargets, currentDs, comidsCursor)
        subbasinsProvider.addFeatures(mergedSubbasins.values())
        subbasinsProvider.deleteFeatures(subbasinsToDelete)
        #time6 = time.process_time()
        #mergeCatchmentTime += time6 - time5
        streamsProvider.addFeatures(mergedStreams.values())
        streamsProvider.deleteFeatures(streamsToDelete)
        #print(huc12 + '_15')
        #time7 = time.process_time()
        #mergeChannelTime += time7 - time6
        #Common.information('Merging catchments took {0} milliseconds'.format(int(mergeTime * 1000 + 0.5)))
        #Common.information('Merging catchment shapes took {0} milliseconds'.format(int(mergeCatchmentTime * 1000 + 0.5)))
        #Common.information('Merging channel shapes took {0} milliseconds'.format(int(mergeChannelTime * 1000 + 0.5)))
        finalAreas = self.fixSubbasins(huc12, problems)
        # Save memory layer to file
        projDir = self.projectDirs[huc12]
        if self.isPlus:
            subbasinsFile = projDir + '/Watershed/Shapes/subbasins.shp'
            streamsFile = projDir + '/Watershed/Shapes/streams.shp'
        else:
            subbasinsFile = projDir + '/Watershed/Shapes/' + 'demwshed.shp'
            streamsFile = projDir + '/Watershed/Shapes/' + 'streams.shp'
        error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(self.subbasinsLayer, subbasinsFile, QgsCoordinateTransformContext(), 
                                                               self.common.options)
        if error != 0:  # QgsVectorFileWriter.NoError
            Common.error('Failed to write subbasins file {0}: {1}'.format(subbasinsFile, msg),
                         problems)
        #print(huc12 + '_16')
        streamCount = self.fixStreams(huc12, currentDs, terminals, cursor, problems)
        error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(self.streamsLayer, streamsFile, QgsCoordinateTransformContext(), self.common.options)
        if error != 0:  # QgsVectorFileWriter.NoError
            Common.error('Failed to write streams file {0}: {1}'.format(streamsFile, msg), problems)
        #print(huc12 + '_17')
        #print('Structure: {0}'.format(currentDs))
        #print('Areas: {0}'.format(currentAreas))
        minArea, maxArea, meanArea = Common.minMaxMean(finalAreas)
        #print(huc12 + '_18')
        stats.write('huc{0}, {7}, {1:.1F}, {2}, {3}, {4:.1F}, {5:.1F}, {6:.1F}\n'.
                    format(huc12, totalArea, len(finalAreas), streamCount, minArea, maxArea, meanArea, self.totalCatchments[huc12]))
        #print(huc12 + '_19')
        return totalArea, len(finalAreas), minArea, maxArea, streamCount 
    
    def setSWATIds(self, huc12: str, mergeTargets: Dict[int, int], currentDs: Dict[int, List[int]], comidsCursor: Any) -> None:
        """Add all comids in mergeTargets to self.SWATIds map, generating SWATIds for each merge group.
        Also add or replace entries in comids table."""
        SWATIds: Dict[int, int] = dict()
        noCatchments = self.noCatchments[huc12]
        # invert mergedTargets map
        finalTargets: Dict[int, Set[int]] = dict()
        for source in mergeTargets:
            try:
                finalTarget = HUC14Catchments.finalTarget(source, mergeTargets, huc12)
            except ValueError:
                return
            finalTargets.setdefault(finalTarget, set()).add(source)
        # make set of all catchmnents not involved in mergers
        currentCatchments = set(currentDs.keys())
        currentCatchments.difference_update(noCatchments)
        for source, target in mergeTargets.items():
            currentCatchments.discard(source)
            currentCatchments.discard(target)
        #print('No catchments: {0}'.format(self.noCatchments[huc12]))
        #print('Adding unmerged catchments {0}'.format(currentCatchments))
        #print('Merge targets: {0}'.format(mergeTargets))
        # add unmerged catchments to finalTargets map
        for catchment in currentCatchments:
            finalTargets[catchment] = set()
        targetsList = sorted(list(finalTargets.keys()))
        areas = self.catchmentAreas.get(huc12, dict())
        huc10 = huc12[:-2]
        huc8 = huc10[:-2]
        sql = 'INSERT INTO comids VALUES (?,?,?,?,?,?,?,?)'
        # map targets to SWATIds in order so this process is repeatable
        lastNum = 0
        for i in range(len(targetsList)):
            finalTarget = targetsList[i]
            # if all in noCatchments can ignore
            if finalTarget in noCatchments:
                found = False 
                for source in finalTargets[finalTarget]:
                    found = source not in noCatchments
                    if found:
                        break
                if not found:
                    continue
            lastNum += 1
            SWATIds[finalTarget] = lastNum
            huc14 = huc12 + '{0:0>2d}'.format(lastNum)
            comidsCursor.execute(sql, (finalTarget, huc14, huc12, huc10, huc8, self.flowLengths.get(finalTarget, 0), 
                                 areas.get(finalTarget, 0), self.drainageAreas.get(finalTarget, 0)))
            for source in finalTargets[finalTarget]:
                SWATIds[source] = lastNum
                comidsCursor.execute(sql, (source, huc14, huc12, huc10, huc8, self.flowLengths.get(source, 0), 
                                     areas.get(source, 0), self.drainageAreas.get(source, 0)))
        self.lastSWATNum[huc12] = lastNum
        self.SWATIds[huc12] = SWATIds
    
    def mergeSubbasins(self, huc12: str, catchment: int, dsCatchment: int, noCatchments: Set[int], provider: QgsVectorDataProvider, 
                       merged: Dict[int, QgsFeature], toDelete: List[int]) -> None:
        """Merge subbasins."""
        indexes = self.subbasinsIndexes
        if catchment in noCatchments:
            # nothing to do: no subbasin polygon to merge
            return
        assert self.subbasinsLayer is not None
        # look first in merged, since these are up to date
        poly1 = merged.get(catchment, None)
        if poly1 is None:
            expr1 = QgsExpression('"PolygonId" = {0}'.format(catchment))
            try:
                poly1 = next(provider.getFeatures(QgsFeatureRequest(expr1)))  # type: ignore
                fromMerged1 = False
            except:
                Common.information('Failed to find catchment {0} when merging with downstream {1}'.format(catchment, dsCatchment))
                # nothing to do
                return
        else:
            fromMerged1 = True
        if dsCatchment in noCatchments:
            if not catchment in noCatchments:
                noCatchments.remove(dsCatchment)
            # need to set the basin of the upstream polygon to dsCatchment
            if fromMerged1:
                poly1[indexes['PolygonId']] = dsCatchment
                merged[dsCatchment] = poly1
                del merged[catchment]
            else:
                mmap = {poly1.id(): {indexes['PolygonId'] : dsCatchment}}
                provider.changeAttributeValues(mmap)
            return
        poly2 = merged.get(dsCatchment, None)
        if poly2 is None:
            expr2 = QgsExpression('"PolygonId" = {0}'.format(dsCatchment))
            try:
                poly2 = next(provider.getFeatures(QgsFeatureRequest(expr2)))  # type: ignore
                fromMerged2 = False
            except:
                Common.information('Failed to find downstream catchment {0} when merging with upstream {1}'.format(dsCatchment, catchment))
                # need to set the basin of the upstream polygon to dsCatchment
                mmap = {poly1.id(): {indexes['PolygonId'] : dsCatchment}}
                provider.changeAttributeValues(mmap)
                return
        else:
            fromMerged2 = True
        poly = QgsFeature(provider.fields())
        # use lower polygon id as id for merger
        poly[indexes['PolygonId']] = dsCatchment
        poly[indexes['Area']] = poly1[indexes['Area']] + poly2[indexes['Area']]
        poly[indexes['HUC12']] = poly2[indexes['HUC12']]
        geom1 = poly1.geometry().makeValid()
        geom2 = poly2.geometry().makeValid()
        geom = geom1.combine(geom2)
        if geom is None:
            geom = Common.polyCombine(geom1, geom2)
        poly.setGeometry(geom)
        merged[dsCatchment] = poly
        if not fromMerged1:
            toDelete.append(poly1.id())
        else:
            del merged[catchment]
        if not fromMerged2:
            toDelete.append(poly2.id())
        # add water bodies in catchment to dsCatchment
        if catchment in self.reservoirs[huc12]:
            if dsCatchment in self.reservoirs[huc12]:
                self.reservoirs[huc12][dsCatchment].addWaterbody(self.reservoirs[huc12][catchment], False)
            else:
                self.reservoirs[huc12][dsCatchment] = self.reservoirs[huc12][catchment]
        self.ponds[huc12].setdefault(dsCatchment, []).extend(self.ponds[huc12].get(catchment, []))
        if catchment in self.wetlands[huc12]:
            if dsCatchment in self.wetlands[huc12]:
                self.wetlands[huc12][dsCatchment] += self.wetlands[huc12][catchment]
            else:
                self.wetlands[huc12][dsCatchment] = self.wetlands[huc12][catchment]
        if catchment in self.playas[huc12]:
            if dsCatchment in self.playas[huc12]:
                self.playas[huc12][dsCatchment] += self.playas[huc12][catchment]
            else:
                self.playas[huc12][dsCatchment] = self.playas[huc12][catchment]
        
    def mergeStreams(self, catchment: int, dsCatchment: int, provider: QgsVectorDataProvider, 
                      merged: Dict[int, QgsFeature], toDelete: List[int], startPoints: Dict[int, QgsPointXY]) -> None:
        """Merge streams."""
        #print('Merging streams {0} and {1}'.format(catchment, dsCatchment))
        indexes = self.streamsIndexes
        # look first in merged, since these are up to date
        link1 = merged.get(catchment, None)
        if link1 is None:
            expr1 = QgsExpression('"LINKNO" = {0}'.format(catchment))
            link1 = next(provider.getFeatures(QgsFeatureRequest(expr1)))  # type: ignore
            fromMerged1 = False
        else:
            fromMerged1 = True
        link2 = merged.get(dsCatchment, None)
        if link2 is None:
            expr2 = QgsExpression('"LINKNO" = {0}'.format(dsCatchment))
            link2 = next(provider.getFeatures(QgsFeatureRequest(expr2)))  # type: ignore
            fromMerged2 = False
        else:
            fromMerged2 = True
        link = QgsFeature(provider.fields())
        # use lower link id as id for merger
        link[indexes['LINKNO']] = dsCatchment
        # start point for merger will be start of upper stream
        startPoints[dsCatchment] = startPoints[catchment]
        del startPoints[catchment]
        if self.isPlus:
            link[indexes['DSLINKNO1']] = -1   # fixed later
            link[indexes['PERCENT1']] = 100   # fixed later
            link[indexes['DSLINKNO2']] = -1   # fixed later
            link[indexes['DSNODEID1']] = -1   # fixed later
            link[indexes['DSNODEID2']] = -1   # fixed later
        else:   
            link[indexes['DSLINKNO']] = -1   # fixed later
            link[indexes['DSNODEID']] = -1   # fixed later
        link[indexes['WSNO']] = dsCatchment  # type: ignore
        link[indexes['streamorde']] = max(link1[indexes['streamorde']], link2[indexes['streamorde']])  # type: ignore
        # drainage areas are at outlet, so dsCatchemnt is appropriate
        link[indexes['TotDASqKm']] = link2[indexes['TotDASqKm']]  # type: ignore
        # try to choose a non-empty name
        if link2[indexes['NAME']] == '':  # type: ignore
            link[indexes['NAME']] = link1[indexes['NAME']]  # type: ignore
        else:
            link[indexes['NAME']] = link2[indexes['NAME']]  # type: ignore
        # flowlines are type lineZM: remove ZM
        geom1 = link1.geometry()
        if geom1.isMultipart():
            geom1 = QgsGeometry.fromMultiPolylineXY(geom1.asMultiPolyline())
        else:
            geom1 = QgsGeometry.fromPolylineXY(geom1.asPolyline())
        geom2 = link2.geometry()
        if geom2.isMultipart():
            geom2 = QgsGeometry.fromMultiPolylineXY(geom2.asMultiPolyline())
        else:
            geom2 = QgsGeometry.fromPolylineXY(geom2.asPolyline())
        if catchment in HUC14Catchments.channelsToOmit:
            geom = geom2
        else:
            geom = geom1.combine(geom2)
            if geom is None:
                geom = Common.linkCombine(geom1, geom2)
        link.setGeometry(geom)
        merged[dsCatchment] = link 
        if not fromMerged1:
            toDelete.append(link1.id())
        else:
            del merged[catchment]
        if not fromMerged2:
            toDelete.append(link2.id())
        
    def fixSubbasins(self, huc12: str, problems: Any) -> Dict[int, float]:
        """Set PolygonID and Subbasin fields to SWAT ids, and HUC14 field"""
        SWATPolygons = self.SWATPolygons.setdefault(huc12, set())
        indexes = self.subbasinsIndexes
        SWATIds = self.SWATIds[huc12]
        assert self.subbasinsLayer is not None
        provider = self.subbasinsLayer.dataProvider()
        areas: Dict[int, float] = dict()
        mmap : Dict[int, Dict[int, Any]] = dict()
        # replaced by fixBasindata
        #=======================================================================
        # # if not SWATPlus or runProject (since then about to be written anyway, and code now corrected)
        # # and if project database exists
        # # rewrite the subbasin areas in hrus table to exclude reservoirs and ponds
        # # This is a temporary measure to correct old projects
        # projConn = None
        # if not self.isPlus and not self.runProject:
        #     projDir = self.projectDirs[huc12]
        #     projDb = os.path.join(projDir, 'huc{0}.sqlite'.format(huc12))
        #     if os.path.isfile(projDb):
        #         projConn = sqlite3.connect(projDb)
        #         # check we have an hrus table
        #         sql = "SELECT name FROM sqlite_master WHERE name = 'hrus'"
        #         rows = projConn.execute(sql).fetchall()
        #         if len(rows) == 0:
        #             projConn = None
        #=======================================================================
        for poly in provider.getFeatures():
            polyId = int(poly[indexes['PolygonId']])
            area = float(poly[indexes['Area']])
            areas[polyId] = area / 1E6  # convert to sq km
            SWATId = self.getSWATNum(polyId, huc12, SWATIds)
            huc14 = huc12 + '{0:0>2d}'.format(SWATId)
            mmap[poly.id()] = {indexes['PolygonId']: SWATId, 
                               indexes['Subbasin']: SWATId,
                               indexes[self.common.HUC14_12_10_8String]: huc14}
            SWATPolygons.add(SWATId)
            # add any water bodies
            reservoirs = self.reservoirs.get(huc12, dict())
            reservoir = reservoirs.get(polyId, None)
            if reservoir is None:
                resArea = 0.0  # @UnusedVariable
            else:
                resArea = reservoir.surfaceArea  # @UnusedVariable
                self.writeReservoir(huc12, huc14, SWATId, reservoir)
            pondMap = self.ponds.get(huc12, dict())
            ponds = pondMap.get(polyId, [])
            if len(ponds) > 0:
                pondArea = self.writePond(huc12, huc14, SWATId, ponds)  # @UnusedVariable
            else:
                pondArea = 0  # @UnusedVariable
            wetlands = self.wetlands.get(huc12, dict())
            wetlandArea = wetlands.get(polyId, 0)
            if wetlandArea > 0:
                self.writeWetland(huc12, huc14, SWATId, wetlandArea)
            playas = self.playas.get(huc12, dict())
            playaArea = playas.get(polyId, 0)
            if playaArea > 0:
                self.writePlaya(huc12, huc14, SWATId, playaArea)
        # replaced by fixBasindata
        #=======================================================================
        #     if projConn is not None:
        #         waterbodiesArea = resArea + pondArea
        #         hrusArea = max(area / 1E4 - waterbodiesArea, 0)  # total area of HRUs in hectares
        #         self.common.fixHRUsTable(projDb, projConn, SWATId, hrusArea, problems, self.batchRerun)
        #         self.common.fixWater(projConn, self.waterConn, huc12, SWATId, area)
        # if projConn is not None:
        #     projConn.commit()
        #=======================================================================
        self.waterConn.commit()
        provider.changeAttributeValues(mmap)
        # copy features to combined
        assert self.combinedWshedLayer is not None
        features = list(provider.getFeatures())
        OK = self.combinedWshedLayer.dataProvider().addFeatures(features)
        if not OK:
            Common.error('Failed to add subbasins for {0} to combined watershed shapefile'.format(huc12))
        return areas
    
    def writeReservoir(self, huc12: str, huc14: str, SWATId: int, reservoir: Waterbody) -> None:
        """Write reservoir data to WaterBodiesDb.  
        Default initial volume is 80% of principal valume."""
        eVol = reservoir.maxVol
        pVol = reservoir.normVol
        pArea = reservoir.surfaceArea
        eArea = pArea * 1.1
        sql = 'INSERT INTO reservoirs VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)'
        self.waterConn.execute(sql, (huc12, huc14, SWATId, reservoir.name, reservoir.year, eArea, eVol, pArea, pVol, pVol * 0.8, reservoir.drainArea,
                                     reservoir.location[0], reservoir.location[1]))
        
    def writePond(self, huc12: str, huc14: str, SWATId: int, ponds: List[Waterbody]) -> float:
        """Write pond data to WaterBodiesDb.
        Omit ponds with drainage area > huc8Area.  
        Default initial volume is 80% of principal valume.
        Return pond surface area in ha"""
        
        def mergePonds() -> Optional[Waterbody]:
            """Merge ponds with drainage area < subAreaKm2."""
            pond = None
            while len(ponds) > 0:
                pond0 = ponds.pop(0)
                if pond0.drainArea <= self.huc8Area:
                    pond = pond0
                    self.includedPonds.append(pond0.nidid)
                    break
                self.excludedPonds.append(pond0.nidid)
            if pond is None:
                return None
            for pond1 in ponds:
                if pond1.drainArea <= self.huc8Area:
                    pond.addWaterbody(pond1, False)
                    self.includedPonds.append(pond1.nidid)
                else:
                    self.excludedPonds.append(pond1.nidid)
            return pond
        
        pond = mergePonds()
        if pond is None:
            return 0
        eVol = pond.maxVol
        pVol = pond.normVol
        pArea = pond.surfaceArea
        eArea = pArea * 1.1
        sql = 'INSERT INTO ponds VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        self.waterConn.execute(sql, (huc12, huc14, SWATId, pond.nidid, pond.name, eArea, eVol, pArea, pVol, pVol * 0.8, pond.drainArea,
                                     pond.location[0], pond.location[1], pond.volEstimated, pond.surfaceAreaEstimated))
        return pArea
        
    def writeWetland(self, huc12: str, huc14: str, SWATId: int, area: float) -> None:
        """Write wetland data to WaterBodiesDb.  Convert area to ha."""
        sql = 'INSERT INTO wetlands VALUES(?,?,?,?)'
        self.waterConn.execute(sql, (huc12, huc14, SWATId, area / 1E4))
        
    def writePlaya(self, huc12: str, huc14: str, SWATId: int, area: float) -> None:
        """Write playa data to WaterBodiesDb.  Convert area to ha."""
        sql = 'INSERT INTO playas VALUES(?,?,?,?)'
        self.waterConn.execute(sql, (huc12, huc14, SWATId, area / 1E4))
        
    def fixStreams(self, huc12: str, currentDs: Dict[int, List[int]], terminals: Dict[int, List[Tuple[int, int]]], cursor: Any, problems: Any) -> int:
        """Set LINKNOs and DSLINKNOs according to position at end of merging, and DSNODEID for watershed exits and inlets.
        Also write subbasins table or file.
        Also write drainage areas for zero=length inlet channels.
        Also add streams to combined streams shapefile."""
        
        def isDownstream(link: int) -> bool:
            """Return True if link is downstream of some link."""
            for links in currentDs.values():
                if link in links:
                    return True
            return False
        
        def findNearestSource(linkNo: int, point: QgsPointXY, provider) -> int:
            """Find comid other than linkNo with last point closest to point."""
            # not very reliable as lastPoint may not be source point because applied after merging
            # and can produce result in different subbasin
            # so only to be used wjen result is not critical
            measure = float('inf')
            result = -1
            for link in provider.getFeatures():
                nextLinkNo = int(link[indexes['LINKNO']])  # type: ignore
                if linkNo == nextLinkNo:
                    continue
                nextPoint = Common.lastPoint(link.geometry())
                dx = nextPoint.x() - point.x()
                dy = nextPoint.y() - point.y()
                nextMeasure = dx * dx + dy * dy
                if nextMeasure < measure:
                    result = nextLinkNo
            return result
        
        noCatchments = self.noCatchments.get(huc12, set())
        SWATIds = self.SWATIds[huc12]
        mergeTargets = self.mergeTargets[huc12]
        #print('Fixing channels for {0}'.format(huc12))
        # replacement for startPoints map, using SWATIds
        SWATStartPoints: Dict[int, QgsPointXY] = dict()
        startPoints = self.startPoints[huc12]
        assert self.streamsLayer is not None
        indexes = self.streamsIndexes
        provider = self.streamsLayer.dataProvider()
        streamsToDelete: List[int] = []
        if self.isPlus: # TODO: change this to a table in project databnase
            subbasinsFile = posixpath.join(self.projectDirs[huc12], 'submapping.csv')
            subFile = open(subbasinsFile, 'w')
            subFile.write('SUBBASIN,HUC_ID,IsEnding1,IsEnding2\n')
        else:
            subbasinsFile = posixpath.join(self.projectDirs[huc12], 'submapping.csv')
            subFile = open(subbasinsFile, 'w')
            subFile.write('SUBBASIN,HUC_ID,IsEnding\n')
        # first deal with any non-merged noCatchment streams.  They don't have subbasins, so we need to remove,
        # but need to check if they are involved with inlets or outlets
        # need to make list because of search of links within the main for loop
        # We also look for tiny catchments, which can still be found between an inlet and a junction
        links = list(provider.getFeatures(QgsFeatureRequest().setFlags(QgsFeatureRequest.NoGeometry)))
        #print('Links: {0}'.format([int(link[indexes['LINKNO']]) for link in links]))
        # may have a link which has been removed but is still in outlets
        for link in links:
            linkNo = int(link[indexes['LINKNO']])  # type: ignore
            if linkNo < -1: # closed catchment
                continue
            if linkNo in noCatchments:
                streamsToDelete.append(link.id())
                #print('Removing 1 link {0}'.format(linkNo))
                dsLinkNos = currentDs.get(linkNo, [0])
                # note it is possible to have an inlet and an outlet on the same no catchment comid, eg 176271
                # check for inlet on a noCatchment link; move to downstream channel
                removeInlets: Set[int] = set()
                for inlet in self.inlets.get(huc12, set()):
                    if HUC14Catchments.finalTarget(inlet.inletComid, self.mergeTargets[huc12]) == linkNo:
                        if dsLinkNos == [0] or dsLinkNos == [-1]:
                            # inlet to no catchment channel with nothing downstream
                            # remove inlet
                            removeInlets.add(inlet.inletId)
                            #print('Removing {0} inlet {1}'.format(huc12, inlet.asString()))
                            # also need to remove zero-length channel leading to inlet
                            for link0 in links:
                                if int(link0[indexes['LINKNO']]) == inlet.inletId:
                                    streamsToDelete.append(link0.id())
                                    #print('Removing 2 link {0}'.format(linkNo))
                        else:
                            newLinkNo = dsLinkNos[0]
                            target = HUC14Catchments.finalTarget(newLinkNo, mergeTargets)
                            if target < 0:
                                # we have a no catchment linkNo involving three HUCs.
                                # HUC A, outlet point OutA -> inlet point InB -> linkNo in B -> outlet point OutB -> inlet point InC -> target
                                # where B is 'huc12', and inlet to InB is 'inlet'
                                # we will replace this with
                                # (HUC A, outlet point OutA, outlet point OutB) -> inlet point InC -> target
                                # ie the outlet point of A is routed directly to the outlet point of B, instead of through linkNo
                                # To do this, apart from merging LinkNo
                                # 1.  the inlet InB is removed (or at least its upper to A)
                                # 2.  the uppers inlet of C are changed to lose linkNo and gain the upstream(s) of point OutA
                                # 3.  for each upstream of point OutA:
                                #     3.1 if A has a terminal to LinkNo, changing it to target
                                #     3.2 otherwise a terminal is added to A's terminals linking the upstream to target
                                inlet.inletComid = 0 - target
                                sql3 = 'SELECT huc12 FROM {0} WHERE comid = ?'.format(self.selectcatchTable)
                                row3 = cursor.execute(sql3, (inlet.inletComid,)).fetchone()
                                if row3 is None:
                                    Common.error('Cannot find HUC for comid {0}'.format(inlet.inletComid),
                                                 problems)
                                else:
                                    newHUC = row3[0]
                                    #print('Original inlets for new HUC {0}:'.format(newHUC))
                                    #for inlet1 in self.inlets.get(newHUC, set()):
                                    #    print(inlet1.asString())
                                    # find the existing inlet for this target from linkNo, and remove the upper
                                    newInlets = list(self.inlets.get(newHUC, set()))
                                    for j in range(len(newInlets)):
                                        inlet1 = newInlets[j]
                                        if inlet1.inletComid == inlet.inletComid:
                                            for i in range(len(inlet1.uppers)):
                                                (_, upperComid) = inlet1.uppers[i]
                                                if upperComid == linkNo:
                                                    del inlet1.uppers[i]
                                                    break
                                            inlet1.uppers.extend(inlet.uppers)
                                            # for each upper in inlet add or change a terminal for its comid, so it can be matched with an outlet later
                                            for (HUC, comid) in inlet.uppers:
                                                terminals = self.terminals.setdefault(HUC, dict())
                                                terminal = terminals.setdefault(comid, [])
                                                found = False
                                                for i in range(len(terminal)):
                                                    if abs(terminal[i][1]) == linkNo:
                                                        terminal[i] = (terminal[i][0], target)
                                                        #print('Moved {0} terminal {1} from {2} to {3}'.format(HUC, terminal[i][0], linkNo, inlet.inletComid))
                                                        found = True
                                                        break
                                                if not found:
                                                    terminal.append((inlet.inletId, target))
                                                    #print('Added {0} terminal for {1}: ({2}, {3})'.format(HUC, comid, inlet.inletId, inlet.inletComid))
                                    self.inlets[newHUC] = set(newInlets)
                                    # remove from huc12
                                    removeInlets.add(inlet.inletId)
                                    #print('Moved {1} inlet {0} from {2} to {3} in {4}'.format(inlet.asString(), huc12, linkNo, inlet.inletComid, newHUC))
                                    #print('New inlets for new HUC {0}:'.format(newHUC))
                                    #for inlet1 in self.inlets.get(newHUC, set()):
                                    #    print(inlet1.asString())
                            else:
                                inlet.inletComid = target
                                #print('Moved {1} inlet {0} from {2} to {3}'.format(inlet.inletId, huc12, linkNo, inlet.inletComid))
                            # add to mergeTargets so can find new inlet comid when aligning inlets and outlets
                            # note linkNo cannot already be in mergeTargets since that would mean it was merged and hence not found here
                            self.mergeTargets[huc12][linkNo] = inlet.inletComid
                # do inlet removal
                self.inlets[huc12] = {inlet for inlet in self.inlets.get(huc12, set()) if inlet.inletId not in removeInlets}
                # do not continue since may be outlet as well as inlet
                # an outlet on a no catchment link: move upstream
                outlets = self.outlets.get(huc12, set())
                removeOutlets: Set[Tuple[int, int, QgsPointXY]] = set()
                newOutlets: Set[Tuple[int, int, QgsPointXY]] = set()
                terminal = terminals.get(linkNo, [])
                for outlet in outlets:
                    (comid, pointId, point) = outlet
                    if comid == linkNo:
                        moved = False
                        removeOutlets.add(outlet)
                        for upId in currentDs:
                            if linkNo in currentDs[upId]:
                                newOutlets.add((upId, pointId, point))
                                moved = True
                                #print('Moved outlet {0} from HUC {1} from {2} to {3}'.format(pointId, huc12, linkNo, upId))
                                terminals[upId] = terminal
                                currentDs[upId] = dsLinkNos
                                mergeTargets[linkNo] = upId
                        if not moved:
                            # outlet is removed, and needs to be removed from corresponding inlet's uppers
                            for HUC, (upperHUCs, _) in self.upperHUCs.items():
                                if huc12 in upperHUCs:
                                    inlets = list(self.inlets.get(HUC, set()))
                                    for j in range(len(inlets)):
                                        inlet = inlets[j]
                                        if inlet.inletComid in [abs(x) for x in dsLinkNos]:
                                            uppers = inlet.uppers
                                            for i in range(len(uppers)):
                                                (HUC1, comid1) = uppers[i]
                                                if HUC1 == huc12 and comid1 == comid:
                                                    #print('Removing upper {0} for removed outlet on comid {1}'.format(uppers[i], comid))
                                                    del uppers[i]
                                                    break
                                        if len(inlet.uppers) == 0:
                                            del inlets[j]
                                            break
                                    self.inlets[HUC] = set(inlets)
                outlets.difference_update(removeOutlets)
                outlets.update(newOutlets)
                if linkNo in terminals:
                    del terminals[linkNo]
                #rest was unreachable, and seems to be unnecessary anyway
#                 continue  # to avoid catchall section below
#                 # nocatchment link that is not an inlet or an outlet - stray channel segment with no subbasin: check isolated and remove
#                 if (0 in dsLinkNos or -1 in dsLinkNos) and not isDownstream(linkNo):
#                     #print('Deleting link {0} from HUC {1}'.format(linkNo, huc12))
#                     pass
#                 else:
#                     Common.error('Non-isolated no catchment comid {0}'.format(linkNo))
        #self.printTerminals(1)
        provider.deleteFeatures(streamsToDelete)
        mmap : Dict[int, Dict[int, Any]] = dict()
        for link in provider.getFeatures(QgsFeatureRequest().setFlags(QgsFeatureRequest.NoGeometry)):
            linkNo = int(link[indexes['LINKNO']])  # type: ignore
            dsLinkNos = currentDs.get(linkNo, [])
            isEnding = {0: 0, 1: 0}
            dsSWATIds = {0: -1, 1: -1}
            dsNodes = {0: -2, 1: -2} if linkNo < 0 else {0: -1, 1: -1}  # negative linkNo means closed, so dummy channel
            toInlet = False
            SWATId = self.getSWATNum(linkNo, huc12, SWATIds)
            huc14 = huc12 + '{0:0>2d}'.format(SWATId)
            pt = startPoints.get(linkNo, None)
            if pt is not None:
                SWATStartPoints[SWATId] = pt
            if len(dsLinkNos) > 0:
                if len(dsLinkNos) > 2:
                    problems.write('Downstream from {0} in huc12 {1} there are more than two channels: {2}\n'.format(linkNo, huc12, dsLinkNos))
                for i in range(len(dsLinkNos)):
                    dsLinkNo = dsLinkNos[i]
                    if dsLinkNo > 0:
                        dsSWATIds[i] = self.getSWATNum(dsLinkNo, huc12, SWATIds)
                terminal = terminals.get(linkNo, [])
                if len(terminal) > 0:
                    for i in range(len(dsLinkNos)):
                        for j in range(len(terminal)):
                            target = terminal[j][1]
                            if target > 0:
                                target = HUC14Catchments.finalTarget(target, self.mergeTargets[huc12])
                            if dsLinkNos[i] == target:
                                dsNodes[i] = terminal[j][0]
                                dsSWATIds[i] = -1
                                isEnding[i] = 1
                    #print('Terminal for {0} with downstream {1}: {2}.  DsNodes {3}'.format(linkNo, dsLinkNos, terminal, dsNodes))
                if self.isPlus:
                    #if len(dsLinkNos) == 2:
                    #    print('Channel {0} in huc12 {1} splits into {2} and {3}'.format(linkNo, huc12, dsLinkNos[0], dsLinkNos[1]))
                    mmap[link.id()] = {indexes['LINKNO']: SWATId, 
                                       indexes['DSLINKNO1']: dsSWATIds[0],
                                       indexes['PERCENT1']: HUC14Catchments.mainBifurcatePercent if len(dsLinkNos) == 2 else 100,  
                                       indexes['DSLINKNO2']: dsSWATIds[1], 
                                       indexes['DSNODEID1']: dsNodes[0],
                                       indexes['DSNODEID2']: dsNodes[1],
                                       indexes['WSNO']: SWATId,
                                       indexes[self.common.HUC12_10_8_6String]: huc12,
                                       indexes[self.common.HUC14_12_10_8String]: huc14}
                else:
                    mmap[link.id()] = {indexes['LINKNO']: SWATId, 
                                       indexes['DSLINKNO']: dsSWATIds[0], 
                                       indexes['DSNODEID']: dsNodes[0],
                                       indexes['WSNO']: SWATId,
                                       indexes[self.common.HUC12_10_8_6String]: huc12,
                                       indexes[self.common.HUC14_12_10_8String]: huc14}
            else:
                # deal with zero length streams draining to inlet points
                #print('Merge targets for {0}: {1}'.format(huc12, self.mergeTargets[huc12]))
                #print('Inlets for {0}'.format(huc12))
                for inlet in self.inlets.get(huc12, set()):
                    drainageArea = 0.0
                    for (_, outletComid) in inlet.uppers:
                        drainageArea += self.drainageAreas.get(outletComid, 0)
                        if outletComid == 0:
                            print('Zero outlet comid for inlet {0} to {1}'.format(inlet.asString(), huc12))
                    #print(inlet.asString())
                    if inlet.inletId == linkNo:
                        inletComid = inlet.inletComid
                        #print('Inlet {0}'.format(linkNo))
                        dsSWATId = self.getSWATNum(inletComid, huc12, SWATIds)
                        #print('{0} mapped to {1}'.format(inlet.inletComid, dsLinkNo))
                        if self.isPlus:
                            mmap[link.id()] = {indexes['DSLINKNO1']:  dsSWATId,
                                               indexes['TotDASqKm']: drainageArea}
                        else:
                            mmap[link.id()] = {indexes['DSLINKNO']:  dsSWATId,
                                               indexes['TotDASqKm']: drainageArea}
                        toInlet = True
                        break
            if not toInlet:  # leave out zero-length streams added for inlets, as they have no subbasins
                if self.isPlus:
                    subFile.write('{0},{1},{2},{3}\n'.format(SWATId, huc12 + '{0:0>2d}'.format(SWATId), isEnding[0], isEnding[1]))
                else:
                    subFile.write('{0},{1},{2}\n'.format(SWATId, huc12 + '{0:0>2d}'.format(SWATId), isEnding[0]))
        subFile.close()
        provider.changeAttributeValues(mmap)
        # replace startPoints map
        self.startPoints[huc12] = SWATStartPoints
        #print(command)
        # copy features to combined, omitting inlet streams
        assert self.combinedStreamsLayer is not None
        inletIds = [inlet.inletId for inlet in self.inlets.get(huc12, set())]
        linkIndex = indexes['LINKNO']
        combinedFeatures: List[QgsFeature] = []
        for feature in provider.getFeatures():
            if feature[linkIndex] not in inletIds:
                combinedFeatures.append(feature)
        self.combinedStreamsLayer.dataProvider().addFeatures(combinedFeatures)
        return len(combinedFeatures)  # only count non-inlet streams
    
    def fixChannels(self, parent: str, problems: Any) -> None:
        """From channels based on flowlines, set WSNO field to SWAT subbasin number, collect maximaL channel lengths and end points,
        merge channels within same subbasin and write channels shapefile with extended fields.  
        Also write non-inlet channels to combined channels shapefile."""
        
        def getDsSWATId(link: int, SWATId: int, ds0: Dict[int, List[int]], SWATPolygons: Set[int], SWATIds: Dict[int, int]) -> int:
            """Follow downstream from link until a link is found with a different SWAT id that is in SWATPolygons."""
            while True:
                dsLink = ds0.get(link, [-1])[0]
                if dsLink == -1:
                    return -1
                nextSWATId = SWATIds.get(dsLink, -1)
                if nextSWATId == SWATId:
                    link = dsLink
                    continue
                if nextSWATId in SWATPolygons:
                    return nextSWATId
                else:
                    link = dsLink
                
            
        fields, indexes = self.common.createChannelsFields(extended=True)
        #combinedChannelsWriter = QgsVectorFileWriter.create(combinedChannels, fields, QgsWkbTypes.LineString, self.crsProject, 
        #                                                         QgsCoordinateTransformContext(), self.common.options)
        combinedChannelsLayer = QgsVectorLayer('LineString?crs=epsg:5072', 'combinedChannels', 'memory')
        combinedChannelsProvider = combinedChannelsLayer.dataProvider()
        combinedChannelsProvider.addAttributes(fields.toList())
        combinedChannelsLayer.updateFields()
        for huc12 in self.catchmentAreas:
            Common.information('Fixing channels for HUC12 {0}'.format(huc12))
            projDir = self.projectDirs.get(huc12, '')
            if not os.path.isdir(projDir):
                #Common.information('No project directory {0} for huc12 {1}'.format(projDir, huc12))  # presumably empty project
                continue
            if self.isPlus:
                channelsFile0 = projDir + '/Watershed/Shapes/channels0.shp'
                channelsFile = projDir + '/Watershed/Shapes/channels.shp'
            else:
                channelsFile0 = projDir + '/Watershed/Shapes/channels0.shp' 
                channelsFile = projDir + '/Watershed/Shapes/channels.shp' 
            channelsLayer = QgsVectorLayer('LineString?crs=epsg:5072', 'channels_layer', 'memory')
            channelsProvider1 = channelsLayer.dataProvider()
            channelsProvider1.addAttributes(fields.toList())
            channelsLayer.updateFields()
            channelsLayer0 = QgsVectorLayer(channelsFile0, 'channels', 'ogr')
            channelsProvider0 = channelsLayer0.dataProvider()
            ds0 = self.dsCatchments0[huc12]
            outlets = self.outlets.get(huc12, set())
            SWATPolygons = self.SWATPolygons[huc12]
            #print('Downstream map for HUC12 {0}: {1}'.format(huc12, ds0))
            SWATIds = self.SWATIds[huc12]
            #print('SWATIds for HUC12 {0}: {1}'.format(huc12, SWATIds))
            # map SWATId -> wsno (= linkno) -> channel
            subChannels: Dict[int,  Dict[int, QgsFeature]] = dict()
            # map SWATId -> channel
            outletChannels: Dict[int, QgsFeature] = dict()
            inletFeatures: List[QgsFeature] = []
            features: List[QgsFeature] = []
            closed: Set[int] = set()
            for channel in channelsProvider0.getFeatures():
                linkNo = channel[indexes['LINKNO']]
                dsLinkNo = channel[indexes['DSLINKNO']]
                dsNodeId = channel[indexes['DSNODEID']]
                if linkNo == dsNodeId:  # inlet
                    dsSWATId = SWATIds.get(dsLinkNo, -1)
                    if dsSWATId not in SWATPolygons:
                        dsSWATId = getDsSWATId(dsLinkNo, dsSWATId, ds0, SWATPolygons, SWATIds)
                    if dsSWATId < 0:
                        continue
                    inletFeature = QgsFeature(fields)
                    geom = channel.geometry()
                    inletFeature.setGeometry(geom)
                    if geom.isMultipart():
                        pt = geom.asMultiPolyline()[0][0]
                    else:
                        pt = geom.asPolyline()[0]
                    attributes = channel.attributes() + [0, pt.x(), pt.y(), pt.x(), pt.y()]  # type: ignore
                    attributes[indexes['DSLINKNO']] = dsSWATId 
                    # huc14 of channel inlet feeds into
                    huc14 = huc12 + '{0:0>2d}'.format(dsSWATId)
                    attributes[indexes[self.common.HUC14_12_10_8String]] = huc14
                    inletFeature.setAttributes(attributes)
                    inletFeatures.append(inletFeature)
                else:
                    wsno = channel[indexes['WSNO']]
                    SWATId = SWATIds.get(wsno, 0)
                    if wsno < -1:
                        closed.add(SWATId)
                    if SWATId not in SWATPolygons:
                        continue
                    if SWATId > 0:
                        subChannels.setdefault(SWATId, dict())[wsno] = channel
                    dsSWATId = SWATIds.get(dsLinkNo, -1)
                    if dsSWATId != SWATId:
                        outletChannels[SWATId] = channel
            for SWATId in subChannels:
                channels = subChannels[SWATId]
                #print('Channels for subbasin {0} in HUC12 {1}: {2}'.format(SWATId, huc12, channels.keys()))
                if SWATId in closed:
                    for channel in channels.values():  # there should only be one
                        outletChannel = channel
                        geom = channel.geometry()
                        sourcePoint = Common.firstPoint(geom)
                        outletPoint = Common.lastPoint(geom)
                        area = float(channel[indexes['TotDASqKm']])
                        lnarea = math.log(area)
                        lnlength = 1.54 + 0.445 * lnarea  # TODO: formula for 03
                        length = math.exp(lnlength)
                        dsLinkNo = -1
                        dsNodeId = -2
                        geometry = channel.geometry()
                else:
                    # find furthest channel source point from outlet point for each subbasin
                    candidates = {wsno: Common.firstPoint(channel.geometry()) for wsno, channel in channels.items()}
                    outletChannel = outletChannels[SWATId]
                    outletPoint = Common.lastPoint(outletChannel.geometry())
                    comid, sourcePoint = Common.furthest(candidates, outletPoint)  # type: ignore
                    path: List[int] = [comid]
                    while True:
                        #print('Path so far for subbasin {0} in HUC12 {1}: {2}'.format(SWATId, huc12, path))
                        dsId = ds0.get(path[-1], [-1])[0]
                        if dsId < 0 or dsId not in channels:
                            break
                        else:
                            path.append(dsId)
                    length = 0.0
                    for comid in path:
                        length += channels[comid].geometry().length()
                    # dissolve channels in each subbasin and write channels shapefile
                    geometry = QgsGeometry.unaryUnion([channel.geometry() for channel in channels.values()])
                    outletFound = False
                    for (comid, outletId, _) in outlets:
                        if comid in channels.keys():
                            outletFound = True
                            dsNodeId = outletId
                            dsLinkNo = -1
                            break
                    if not outletFound:
                        #print('Outlet channel for subbasin {0} in HUC12 {1} is link {2}'.format(SWATId, huc12, outletChannel[indexes['LINKNO']]))
                        dsLinkNo = SWATIds.get(outletChannel[indexes['DSLINKNO']], -1)
                        if dsLinkNo == SWATId:
                            Common.error('Failed to find downstream subbasin from {0} in {1}'.format(SWATId, huc12),
                                         problems)
                        dsNodeId = outletChannel[indexes['DSNODEID']] if dsLinkNo == -1 else -1
                huc14 = huc12 + '{0:0>2d}'.format(SWATId)
                channel1 = QgsFeature(fields)
                assert sourcePoint is not None
                attributes = [SWATId, dsLinkNo, dsNodeId, SWATId, outletChannel[indexes['streamorde']], 
                               outletChannel[indexes['TotDASqKm']], outletChannel[indexes['NAME']], 
                               outletChannel[indexes[self.common.HUC12_10_8_6String]], huc14,
                               length, sourcePoint.x(), sourcePoint.y(), outletPoint.x(), outletPoint.y()]
                channel1.setAttributes(attributes)
                channel1.setGeometry(geometry)
                features.append(channel1)
            channelsProvider1.addFeatures(features)
            channelsProvider1.addFeatures(inletFeatures)
            QgsVectorFileWriter.writeAsVectorFormatV2(channelsLayer, channelsFile, QgsCoordinateTransformContext(), self.common.options)
            # copy features to combined
            combinedChannelsProvider.addFeatures(features)
            combinedChannelsProvider.addFeatures(inletFeatures)
        combinedChannels = parent + '/combinedChannels.shp'
        QgsVectorFileWriter.writeAsVectorFormatV2(combinedChannelsLayer, combinedChannels, QgsCoordinateTransformContext(), self.common.options)
            
    def fixCombinedChannels0(self):
        """Add HUC14 attributes to CombinedChannels0 shapefile."""
        combinedChannels0Layer = QgsVectorLayer('LineString?crs=epsg:5072', 'combinedChannels0_layer', 'memory')
        combinedChannels0Provider = combinedChannels0Layer.dataProvider()
        combinedChannels0Provider.addAttributes(self.channelFields0.toList())
        combinedChannels0Layer.updateFields()
        linkIndex = self.channelIndexes['LINKNO']
        huc12Index = self.channelIndexes['HUC12']
        huc14Index = self.channelIndexes['HUC14']
        for feature in self.combinedChannels0Features:
            comid = int(feature[linkIndex])
            huc12 = feature[huc12Index]
            finalComid = HUC14Catchments.finalTarget(comid, self.mergeTargets[huc12])
            # don't use getSWATId as it may create new ones
            SWATId = self.SWATIds.get(huc12, dict()).get(finalComid, 0)
            huc14 = huc12 + '{0:0>2d}'.format(SWATId)
            feature.setAttribute(huc14Index, huc14)
        combinedChannels0Provider.addFeatures(self.combinedChannels0Features)
        error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(combinedChannels0Layer, self.combinedChannels0, QgsCoordinateTransformContext(), self.common.options)
        if error != 0:  # QgsVectorFileWriter.NoError
            Common.error('Failed to write combined channels0  file {0}: {1}'.format(self.combinedChannels0, msg)) 
            return
        # make spatialite version
        db = os.path.splitext(self.combinedChannels0)[0] + '.sqlite'
        shutil.copy(Files.channels0Db, db)
        processing.run("qgis:importintospatialite", 
                       {'INPUT':self.combinedChannels0,
                        'DATABASE':db,
                        'TABLENAME':'combinedchannels0',
                        'PRIMARY_KEY':None,
                        'GEOMETRY_COLUMN':'GEOMETRY',
                        'ENCODING':'UTF-8',
                        'OVERWRITE':True,
                        'CREATEINDEX':False,
                        'LOWERCASE_NAMES':True,
                        'DROP_STRING_LENGTH':False,
                        'FORCE_SINGLEPART':False}) 
        conn = sqlite3.connect(db)  # @UndefinedVariable
        sql = 'CREATE INDEX huc10 ON combinedchannels0(HUC10)'
        conn.execute(sql)
        sql = 'CREATE INDEX huc12 ON combinedchannels0(HUC12)'
        conn.execute(sql)
        sql = 'CREATE INDEX huc14 ON combinedchannels0(HUC14)'
        conn.execute(sql)
        sql = 'CREATE INDEX order ON combinedchannels0(streamorde)'
        conn.commit()
                    
#     def setLayers(self) -> None:
#         """Set layers for HUC12s, catchments and flowlines."""
#         self.huc12Layer = QgsVectorLayer(Files.GDB + '|layername=HUC12', 'HUC12', 'ogsr')      
#         #count = self.huc12Layer.featureCount()
#         #print('Number of features in huc12 layer: {0}'.format(count))
#         self.catchmentsLayer = QgsVectorLayer(Files.GDB + '|layername=Catchment', 'Catchments', 'ogr')
#         self.streamsLayer = QgsVectorLayer(Files.GDB + '|layername=NHDFlowline_Network', 'Flowlines', 'ogr')
#         print('Number of streams in huc12 layer: {0}'.format(self.streamsLayer.featureCount()))
        
    def createWatershedShapefile(self, huc12: str, problems: Any) -> None:
        """
        Create watershed shapefile subbasinsFile from catchments."""
#         # flag set to true if catchments found with less than 5% overlap
#         # so affected HUC12s can be counted
#         hasExtraCatchments = False
        self.subbasinsLayer = None
        # create shapefile
        fields, indexes = self.common.createWatershedFields()
        subbasinsLayer = QgsVectorLayer('Polygon?crs=epsg:5072', 'subbasins_layer', 'memory')
        subbasinsProvider = subbasinsLayer.dataProvider()
        subbasinsProvider.addAttributes(fields.toList())
        subbasinsLayer.updateFields()
        comids = ','.join([str(comid) for comid in self.catchmentAreas[huc12].keys()])
        #print('Searching for: ' + '"{0}" IN ({1})'.format('FEATUREID', comids))
#         sql0 = "SELECT areahuc12, AsText(GEOMETRY) FROM huc12 WHERE huc_12 LIKE '{0}'".format(huc12)
#         # there can be several pieces
#         geomHUC12s: Set[QgsGeometry] = set()
#         for row0 in self.huc12Conn.execute(sql0):
#             #areaHUC12 = float(row0[0])
#             geomHUC12 = QgsGeometry.fromWkt(row0[1]).makeValid()
#             geomHUC12.transform(self.transformToProjected)
#             geomHUC12s.add(geomHUC12)
        sql = 'SELECT featureid, areasqkm, AsText(GEOMETRY) FROM catchment WHERE featureid IN ({0})'.format(comids)
        features: List[QgsFeature] = []
        for row in self.catchmentConn.execute(sql).fetchall():
            comid = row[0]
            #print('Found {0}'.format(comid))
            aream2 = float(row[1]) * 1E6  # area in m^2
            feature = QgsFeature(fields)
            feature.setAttributes([comid, aream2, comid, huc12, ''])
            geom = QgsGeometry.fromWkt(row[2]).makeValid()
            geom.transform(self.transformToProjected)
#             try:
#                 percent = 0.0
#                 for geomHUC12 in geomHUC12s:
#                     intersection = geom.intersection(geomHUC12)
#                     percent += (intersection.area() / aream2) * 100
#                 if percent < 5:
#                     Common.error('Only {0}% of catchment {1} lies within {2}'.format(percent, comid, huc12), problems)
#                     hasExtraCatchments = True
#                 #else:
#                 #    print('{0}% of catchment {1} lies within {2}'.format(percent, comid, huc12))
#             except Exception as e:
#                 Common.error('Failed to intersect catchment {0} with {1}: {2}'.format(comid, huc12, str(e)), problems)
            comid = int(comid)
            if comid < -1:
                self.centroids[comid] = geom.centroid().asPoint()
            feature.setGeometry(geom)
            features.append(feature)
        if len(features) == 0:  # empty project
            problems.write('HUC12 {0} is empty: it has no catchments\n'.format(huc12))
            self.emptyHUC12s.add(huc12)
            self.subbasinsLayer = None
            return
        subbasinsProvider.addFeatures(features)
        if self.makeOriginals:
            projDir = self.projectDirs[huc12]
            if self.isPlus:
                subbasinsFile0 = projDir + '/Watershed/Shapes/subbasins0.shp'
            else:
                subbasinsFile0 = projDir + '/Watershed/Shapes/demwshed0.shp' 
            QgsVectorFileWriter.writeAsVectorFormatV2(subbasinsLayer, subbasinsFile0, QgsCoordinateTransformContext(), self.common.options)
            assert self.combinedWshed0Layer is not None
            self.combinedWshed0Layer.dataProvider().addFeatures(features)
#         if hasExtraCatchments:
#             self.extraCatchmentsCount += 1
        self.subbasinsLayer = subbasinsLayer
        self.subbasinsIndexes = indexes
        
    def createStreamsShapefile(self, huc12: str, cursor: Any, problems: Any) -> None:
        """
        Create stream shapefile from flowlines."""
        
        def foundInTerminal(comid: int, terminal: List[Tuple[int, int]]) -> bool:
            """Return true if there is a term in terminal with comid its third component."""
            for (_, comid1) in terminal:
                if comid == comid1:
                    return True
            return False
            
        # create shapefile
        streamsLayer = QgsVectorLayer('LineString?crs=epsg:5072', 'streams_layer', 'memory')
        streamsProvider = streamsLayer.dataProvider()
        streamsProvider.addAttributes(self.channelFields.toList())
        streamsLayer.updateFields()
        ds = self.dsCatchments[huc12]
        terminals = self.terminals[huc12]
        upperHUCs = self.upperHUCs.get(huc12, (set(), True))[0]
        self.startPoints[huc12] = dict()
        comids = ','.join([str(comid) for comid in ds.keys()])
        # map comid -> drain area in sq km
        drainAreas: Dict[int, float] = dict()
        #print('Searching for: ' + '"{0}" IN ({1})'.format('COMID', comids))
        sql = 'SELECT comid, gnis_name, streamorde, totdasqkm, AsText(GEOMETRY) FROM flowline WHERE comid IN ({0})'.format(comids)
        features: List[QgsFeature] = []
        features0: List[QgsFeature] = []
        huc10 = huc12[:-2]
        for row in self.flowlineConn.execute(sql).fetchall():
            link = int(row[0])
            #print('Found {0}'.format(link))
            dsLinks = ds.get(link, [-1])
            #print('dsLinks: {0}'.format(dsLinks))
            if self.isPlus:
                dsLink = dsLinks[0]
                dsLink2 = -1 if len(dsLinks) == 1 else dsLinks[1]
                if dsLink <= 0:
                    dsLink = -1
                if dsLink2 <= 0:
                    dsLink2 = -1
            else:
                dsLink = dsLinks[0]
                if dsLink <= 0:
                    dsLink = -1
            feature = QgsFeature(self.channelFields)
            feature0 = QgsFeature(self.channelFields0)
            # totdasqkm is drainage area at outlet of stream; we only use this seriously for streams that feed inlets,
            # and include it as an attribute of the zero-length stream in the lower HUC
            # We also use drain areas to compare streams
            totDASqKm = float(row[3])
            drainAreas[link] = totDASqKm
            geom = QgsGeometry.fromWkt(row[4])
            geom.transform(self.transformToProjected)
            self.startPoints[huc12][link] = Common.firstPoint(geom)
            outletId0 = -1
            outletId1 = -1
            terminal = terminals.get(link, [])
            #print('Terminals for {1}: {0}'.format(terminals, huc12))
            #print('terminal 0 for link {0}: {1}'.format(link, terminal))
            huc0 = self.comidToHUC.get(abs(dsLinks[0]), None)
            if dsLinks[0] == -1 or (huc0 is not None and huc0 != huc12 and (self.isPlus or abs(dsLinks[0]) not in self.minors[huc0])):
                # check for same point on a different stream already a terminal to same target
                found = False
                for comid, terminal1 in terminals.items():
                    if comid != link:
                        for ptId, dsId in terminal1:
                            # no point adding another -1 terminal 
                            if dsId != -1 and dsId == dsLinks[0] and \
                                (ptId, dsId) not in terminal:            # or in adding an identical one
                                terminal.append((ptId, dsId))
                                #print('terminal 1 for link {0}: {1}'.format(link, terminal))
                                found = True
                                break
                        if found:
                            break
                # point may already have been created because link has terminal flag
                if not (found or foundInTerminal(dsLinks[0], terminal)):
                    #print('Link {0}, dsLink {1}, terminal {2}'.format(link, dsLinks[0], terminal))
                    _ = self.pointIds.setdefault(huc12, Common.basePointId)
                    self.pointIds[huc12] += 1
                    outletId0 = self.pointIds[huc12]
                    terminal.append((outletId0, dsLinks[0]))
                    #print('terminal 2 for link {0}: {1}'.format(link, terminal))
            if self.isPlus and len(dsLinks) > 1:
                huc1 = self.comidToHUC.get(abs(dsLinks[1]), None)
                if dsLinks[1] == 0 or huc1 != huc12:
                    if not foundInTerminal(dsLinks[1], terminal):
                        _ = self.pointIds.setdefault(huc12, Common.basePointId)
                        self.pointIds[huc12] += 1
                        outletId1 = self.pointIds[huc12]
                        terminal.append((outletId1, dsLinks[1]))
                        #print('terminal 3 for link {0}: {1}'.format(link, terminal))
            if terminal != []:
                terminals[link]  = terminal 
                #print('Terminals for {1} after all checked for link {2}: {0}'.format(self.terminals[huc12], huc12, link))
                outlets = self.outlets.setdefault(huc12, set())
                for (outletId, _) in terminal:
                    outlet = Common.lastPoint(geom)
                    outlets.add((link, outletId, outlet))
                #print('Outlets for {0}: {1}'.format(huc12, self.outlets[huc12]))
            # inlets will be negative comids in the dsCatchments values for an upstream huc12
            for upperHUC in upperHUCs:
                upperDs = self.dsCatchments.get(upperHUC, dict())
                for comid, dsIds in upperDs.items():
                    if 0 - link in dsIds:
                        inletPoint = Common.firstPoint(geom)
                        inlets = self.inlets.get(huc12, set())
                        for inlet in inlets:
                            # can have more than one upper HUC draining to same inlet link
                            if inlet.inletComid == link:
                                inlet.inletPoint = inletPoint
            if self.isPlus:
                if dsLink2 > 0:
                    feature.setAttributes([str(link), str(dsLink), HUC14Catchments.mainBifurcatePercent, str(dsLink2), 
                                           outletId0, outletId1, str(link), row[2], totDASqKm, row[1], huc12, ''])
                    feature0.setAttributes([str(link), str(dsLink), HUC14Catchments.mainBifurcatePercent, str(dsLink2), 
                                           outletId0, outletId1, str(link), row[2], totDASqKm, row[1], huc12, '', huc10])
                else:
                    feature.setAttributes([str(link), str(dsLink), 100, str(dsLink2), 
                                           outletId0, outletId1, str(link), row[2], totDASqKm, row[1], huc12, ''])
                    feature0.setAttributes([str(link), str(dsLink), 100, str(dsLink2), 
                                           outletId0, outletId1, str(link), row[2], totDASqKm, row[1], huc12, '', huc10])
            else:
                feature.setAttributes([str(link), str(dsLink), outletId0, str(link), row[2], totDASqKm, row[1], huc12, ''])
                feature0.setAttributes([str(link), str(dsLink), outletId0, str(link), row[2], totDASqKm, row[1], huc12, '', huc10])
            #print('Geometry: {0}'.format(geom))
            feature.setGeometry(geom)
            feature0.setGeometry(geom)
            features.append(feature)
            features0.append(feature0)
        # add features from closed basins.  No flowlines, so make channel starting and finishing at catchment centroid
        for comid in {comid for comid in ds.keys() if comid < -1}:
            feature = QgsFeature(self.channelFields)
            feature0 = QgsFeature(self.channelFields0)
            totDASqKm = self.catchmentAreas[huc12][comid]
            drainAreas[comid] = totDASqKm
            pt0 = self.centroids.get(comid, None)
            if pt0 is None:
                Common.error('Comid {0} has no catchment'.format(comid))
                continue
            pt1 = QgsPointXY(pt0.x() + 60, pt0.y() + 60)
            geom = QgsGeometry.fromPolylineXY([pt0, pt1])
            feature.setGeometry(geom)
            feature0.setGeometry(geom)
            # use -2 as dsnodeid so that these channels can be identified later after link and wsno replaced with SWAT numbers
            if self.isPlus:
                feature.setAttributes([comid, -1, 100, -1, -2, -2, comid, 1, totDASqKm, '', huc12, ''])
                feature0.setAttributes([comid, -1, 100, -1, -2, -2, comid, 1, totDASqKm, '', huc12, '', huc10])
            else:
                feature.setAttributes([comid, -1, -2, comid, 1, totDASqKm, '', huc12, ''])
                feature0.setAttributes([comid, -1, -2, comid, 1, totDASqKm, '', huc12, '', huc10])
            features.append(feature)
            features0.append(feature0)
        self.combinedChannels0Features.extend(features0)
        # add zero length streams for this HUC's inlets
        inlets = self.inlets.get(huc12, set())
        #print('Inlets for {0}'.format(huc12))
        for inlet in inlets:
            #print(inlet.asString())
            if inlet.inletPoint is not None:
                zfeature = QgsFeature(self.channelFields)
                zfeature.setGeometry(QgsGeometry.fromPolylineXY([inlet.inletPoint, inlet.inletPoint]))
                if self.isPlus:
                    zfeature.setAttributes([inlet.inletId, inlet.inletComid, 100, -1, inlet.inletId, -1, inlet.inletId, 
                                            1, drainAreas[inlet.inletComid], '', huc12, ''])
                else:
                    zfeature.setAttributes([inlet.inletId, inlet.inletComid, inlet.inletId, inlet.inletId, 
                                            1, drainAreas[inlet.inletComid], '', huc12, ''])
                features.append(zfeature)
            else:
                Common.error('Inlet {0} to {1} in HUC {2} has no inlet point'.
                             format(inlet.inletId, inlet.inletComid, huc12),
                             problems)
        streamsProvider.addFeatures(features)
        if self.makeOriginals or not self.isPlus:
            projDir = self.projectDirs[huc12]
            if self.isPlus:
                channelsFile0 = projDir + '/Watershed/Shapes/channels0.shp'
            else:
                channelsFile0 = projDir + '/Watershed/Shapes/channels0.shp' 
            QgsVectorFileWriter.writeAsVectorFormatV2(streamsLayer, channelsFile0, QgsCoordinateTransformContext(), self.common.options)
        self.streamsLayer = streamsLayer
        self.streamsIndexes = self.channelIndexes
        # make copy of current dsCatchments (after reduction but before merging)
        for huc12, ds in self.dsCatchments.items():
            ds0 = self.dsCatchments0.setdefault(huc12, dict())
            for comid, dsIds in ds.items():
                ds0[comid] = dsIds
                
        
    def createPointsShapefile(self, huc12: str, pointsFile: str, problems: Any) -> None:
        """Create shapefile containing the huc12 inlets, outlets, reservoirs and ponds."""
        # create shapefile
        fields, _ = self.common.createPointsFields()
        pointsLayer = QgsVectorLayer('Point?crs=epsg:5072', 'points_layer', 'memory')
        pointsProvider = pointsLayer.dataProvider()
        pointsProvider.addAttributes(fields.toList())
        pointsLayer.updateFields()
        features: List[QgsFeature] = []
        # don't define same outlet twice, since channels can share them
        done: Set[int] = set()
        # generate new ids for reservoirs
        maxPointId = 0
        for _, pointId, pt in self.outlets.get(huc12, set()):
            if pointId in done:
                # point already added
                continue
            feature = QgsFeature(fields)
            feature.setAttributes([pointId, 0, 0, 0, ''])
            feature.setGeometry(QgsGeometry.fromPointXY(pt))
            features.append(feature)
            done.add(pointId)
            maxPointId = max(maxPointId, pointId)
        for inletData in self.inlets.get(huc12, set()):
            if inletData.inletPoint is None:
                thisSWATId = self.getSWATNum(inletData.inletComid, huc12, self.SWATIds[huc12])
                for (upperHUC, outletComid)  in inletData.uppers:
                    Common.error('No inlet point {0} for catchment {1} ({5}) in huc {2} for outlet in catchment {3} in huc {4}'.
                                 format(inletData.inletId, inletData.inletComid, huc12, 
                                        outletComid, upperHUC, thisSWATId),
                                 problems)
                    break  # first example of upper is enough for error message
                #print('Inlets for huc12 {0}'.format(huc12))
                for inletData1 in self.inlets.get(huc12, set()):
                    thisSWATId = self.getSWATNum(inletData1.inletComid, huc12, self.SWATIds[huc12])
                    #print(inletData1.asString())
                    #print('from upper id {0} to lower id {1}'.format(upperSWATId, thisSWATId))
            else:
                # ignore inlet when only source is an empty HUC12
                upperHUCs = {upperHUC for (upperHUC, _) in inletData.uppers if upperHUC not in self.emptyHUC12s}
                if len(upperHUCs) > 0:
                    feature = QgsFeature(fields)
                    feature.setAttributes([inletData.inletId, 1, 0, 0, ''])
                    feature.setGeometry(QgsGeometry.fromPointXY(inletData.inletPoint))
                    features.append(feature)
                    maxPointId = max(maxPointId, inletData.inletId)
        # add reservoirs
        sql = "SELECT RES_NAME, RES_LAT, RES_LONG FROM reservoirs WHERE HUC12_10_8_6 LIKE '{0}'".format(huc12)
        for row in self.waterConn.execute(sql):
            maxPointId += 1
            geom = QgsGeometry.fromPointXY(QgsPointXY(float(row[2]), float(row[1])))
            geom.transform(self.transformToProjected)
            feature = QgsFeature(fields)
            feature.setAttributes([maxPointId, 0, 1, 0, '' if row[0] is None else row[0]])
            feature.setGeometry(geom)
            features.append(feature)
        # add ponds
        sql = "SELECT PND_NAME, PND_LAT, PND_LONG FROM ponds WHERE HUC12_10_8_6 LIKE '{0}'".format(huc12)
        for row in self.waterConn.execute(sql):
            maxPointId += 1
            geom = QgsGeometry.fromPointXY(QgsPointXY(float(row[2]), float(row[1])))
            geom.transform(self.transformToProjected)
            feature = QgsFeature(fields)
            feature.setAttributes([maxPointId, 0, 2, 0, '' if row[0] is None else row[0]])
            feature.setGeometry(geom)
            features.append(feature)
        pointsProvider.addFeatures(features)
        QgsVectorFileWriter.writeAsVectorFormatV2(pointsLayer, pointsFile, QgsCoordinateTransformContext(), self.common.options)
      
    def alignInletsOutlets(self, parent: str, problems: Any) -> None:
        """Create inletsOutlets csv file matching inlet ids to outlet ids."""
        outletInletFile = posixpath.join(parent, 'oi.csv')
        fromToFile = posixpath.join(parent, 'fromto.csv')
        with open(outletInletFile, 'w') as oiFile, open(fromToFile, 'w') as fromTo:
            oiFile.write('OutletHUC,OutletId,InletHUC,InletId\n')
            fromTo.write('OutletHUC,InletHUC\n')
            hucPairs: Set[Tuple[str, str]] = set()
            for huc12, inlets in self.inlets.items():
                if huc12 in self.emptyHUC12s:
                    continue
                #print('Inlets for HUC {0}'.format(huc12))
                for inlet in inlets:
                    #print('  {0}'.format(inlet.asString()))
                    targetInletComid = HUC14Catchments.finalTarget(inlet.inletComid, self.mergeTargets[huc12])
                    #print('Final inlet target is {0}'.format(targetInletComid))
                    for (upperHUC, outletComid) in inlet.uppers:
                        if upperHUC in self.emptyHUC12s:
                            continue
                        terminals = self.terminals.get(upperHUC, dict())
                        #print('  Terminals for {0}: {1}'.format(upperHUC, terminals))
                        # original outletComid may have been merged since stored
                        targetOutletComid = HUC14Catchments.finalTarget(outletComid, self.mergeTargets[upperHUC])
                        #print('  Target outlet for {0} is {1}'.format(outletComid, targetOutletComid))
                        terminal = terminals.get(targetOutletComid, [])
                        #print('  Terminal for {0}: {1}'.format(targetOutletComid, terminal))
                        found = False
                        for outletId, comid in terminal:
                            terminalTarget = HUC14Catchments.finalTarget(abs(comid), self.mergeTargets[huc12])
                            #print('  Outlet id is {0} with terminal target {1}'.format(outletId, terminalTarget))
                            if terminalTarget == targetInletComid:
                                oiFile.write('{0},{1},{2},{3}\n'.format(upperHUC, outletId, huc12, inlet.inletId))
                                hucPairs.add((upperHUC, huc12))
                                found = True
                                break
                        if not found:
                            Common.error('Cannot find outlet for {0} inlet {1} to comid {2}'.
                                                  format(huc12, inlet.inletId, inlet.inletComid),
                                                  problems)
            for upperHUC, huc in hucPairs:
                fromTo.write('{0},{1}\n'.format(upperHUC, huc))
                
    def markPonds(self) -> None:
        """Mark ponds in HUC12Watershed.sqlite as included or not."""
        conn = sqlite3.connect(Files.HUC12Data)  # @UndefinedVariable
        sqlInclude = 'UPDATE ponds SET included = 1 WHERE nidid = ?'
        sqlExclude = 'UPDATE ponds SET included = 0 WHERE nidid = ?'
        for nidid in self.includedPonds:
            conn.execute(sqlInclude, (nidid,))
        for nidid in self.excludedPonds:
            conn.execute(sqlExclude, (nidid,))
        conn.commit()
                        
    def getSWATNum(self, comid: int, huc12: str, SWATIds: Dict[int, int]) -> int:
        """Return SWATId number for comid and huc12, making a new one if necessary."""
        SWATNum = SWATIds.get(comid, -1)
        if SWATNum < 0: # comid is new
            self.lastSWATNum[huc12] += 1
            SWATNum = self.lastSWATNum[huc12]
            self.SWATIds[huc12][comid] = SWATNum
        return SWATNum
        
    def getUppers(self, huc12: str, cursor: Any) -> Set[str]:
        """Get points immediately upper from huc12, using the self.upperHUCs map or updating that map if necessary."""
        uppers, done = self.upperHUCs.setdefault(huc12, (set(), False))
        if not done:
            sql = 'SELECT huc12 FROM huc12ds where ds = ?'
            for row1 in cursor.execute(sql, (huc12,)).fetchall():
                uppers.add(row1[0])
            self.upperHUCs[huc12] = uppers, True
        return uppers
    
    def canAdd(self, a: str, b: str, cursor: Any) -> bool:
        """Return True if a can be added as an upper of b to self.upperHUCs, i.e. False if b is reachable up from a."""
        
        # todo is list of HUCs whose immediate uppers will be searched for b, and can be marked as done after the seartch
        # if b is found then the result is false
        # else if not in done these upper points are added to todo and we continue looking
        # circularities do not prevent termination, because we record visited points in done, and prevent them getting back into todo
        #print('Trying can add with {0}, {1}'.format(a, b))
        todo = [a]
        done: Set[str] = set()
        while len(todo) > 0:
            nextHUC = todo.pop(0)
            nextUppers = self.getUppers(nextHUC, cursor)
            if b in nextUppers:
                return False
            else:
                done.add(nextHUC)
                for c in nextUppers:
                    if c not in done:
                        todo.append(c)
        return True
    
    def deleteProjects(self, delete: List[str], selection: str) -> None:
        extDir = '/Fields_CDL' if self.isCDL else '/Fields'
        SWATDir = '/SWATPlus' + extDir if self.isPlus else '/SWAT' + extDir
        parent = Files.ModelsDir + SWATDir + Files.HUC14ProjectParent + '/' + selection
        fromToFile = posixpath.join(parent, 'fromto.csv')
        fromToFile2 = posixpath.join(parent, 'fromto2.csv')
        with open(fromToFile, 'r', newline='') as f, open(fromToFile2, 'w', newline='') as f2:
            reader = csv.reader(f)
            writer = csv.writer(f2)
            for row in reader:
                if row[0] not in delete and row[1] not in delete:
                    writer.writerow(row)
        os.remove(fromToFile)
        os.rename(fromToFile2, fromToFile)
        for huc12 in delete:
            Common.information('Deleting project huc{0}'.format(huc12))
            projDir = posixpath.join(parent, 'huc' + huc12)
            if os.path.isdir(projDir):
                try:
                    shutil.rmtree(projDir)
                    Common.information('Deleted directory {0}'.format(projDir))
                except:
                    Common.information('Please delete directory {0}'.format(projDir))
            if not self.isPlus:
                qgsFile = projDir + '.qgs'
                if os.path.isfile(qgsFile):
                    os.remove(qgsFile)
                backup = qgsFile + '~'
                if os.path.isfile(backup):
                    os.remove(backup)
                
    def addExternalInlet(self, huc12: str) -> None:
        """Add inlet point to channel with furthest start point."""
        outlets = self.outlets.get(huc12, None)
        if outlets is None:
            Common.error('Failed to find outlet point for project huc{0}'.format(huc12))
            return
        # arbitrarily select an outlet: almost certainly only one
        for _, _, outletPoint in outlets:
            break
        channel, inletPoint = Common.furthest(self.startPoints.get(huc12, dict()), outletPoint)
        if inletPoint is None:
            Common.error('Failed to find inlet point for project huc{0}'.format(huc12))
            return
        inletId = Common.basePointId # 100000: points already added start from id 100001
        # add inlet to points shapefile
        projDir = self.projectDirs[huc12]
        pointsFile = projDir + '/Watershed/Shapes/points.shp'
        pointsLayer = QgsVectorLayer(pointsFile, 'points', 'ogr')
        pointsProvider = pointsLayer.dataProvider()
        point = QgsFeature(pointsProvider.fields())
        point.setAttributes([inletId, 1, 0, 0, ''])
        point.setGeometry(QgsGeometry.fromPointXY(inletPoint))
        pointsProvider.addFeatures([point])
        # add zero length channel
        channelsFile = projDir + '/Watershed/Shapes/channels.shp'
        channelsLayer = QgsVectorLayer(channelsFile, 'channels', 'ogr')
        channelsProvider = channelsLayer.dataProvider()
        zfeature = QgsFeature(channelsProvider.fields())
        zfeature.setGeometry(QgsGeometry.fromPolylineXY([inletPoint, inletPoint]))
        if self.isPlus:
            zfeature.setAttributes([inletId, channel, 100, -1, inletId, -1, 0, 0, 0, '', huc12, 0,
                                    0, inletPoint.x(), inletPoint.y(), inletPoint.x(), inletPoint.y()])
        else:
            zfeature.setAttributes([inletId, channel, inletId, 0, 0, 0, '', huc12, 0,
                                    0, inletPoint.x(), inletPoint.y(), inletPoint.x(), inletPoint.y()])
        channelsProvider.addFeatures([zfeature])
        
    def printTerminals(self, num: int) -> None:
        """Debugging aid."""
        print('Terminals at {0}: '.format(num))
        for huc12, terminals in self.terminals.items():
            print('  {0}: {1}'.format(huc12, terminals))
        
    @staticmethod
    def finalTarget(comid: int, mergeTargets: Dict[int, int], huc12: str=None) -> int:
        """Return final target of mergers for comid.
        
        If huc12 is not None, include loop detection.  If loop detected raise value error."""
        if huc12 is not None:
            done = [comid]
        currentId = comid
        while True:
            nextId = mergeTargets.get(currentId, None)
            if nextId is None:
                return currentId
            else:
                if huc12 is not None:
                    if nextId in done:
                        Common.error('Loop {0} in mergeTargets {1} for {2}'.format(done + [nextId], mergeTargets, huc12))
                        raise ValueError
                    done.append(nextId)
                currentId = nextId
        
    @staticmethod
    def copyShapefile(inFile: str, outFile: str) -> None:
        """Copy files with same basename as infile to outFile."""
        if not os.path.isfile(inFile):
            return
        if os.path.isfile(outFile) and os.path.samefile(inFile, outFile):
            # avoid copying to same file
            return
        pattern: str = os.path.splitext(inFile)[0] + '.*'
        outBase = os.path.splitext(outFile)[0]
        for f in glob.iglob(pattern):
            suffix = os.path.splitext(f)[1]
            nextOutFile = outBase + suffix
            shutil.copy(f, nextOutFile)
    
class Common():
    """Common functions."""
    
    # added inlet and outlet points start from 10001
    basePointId = 100000  # must be same as _HUCPointId in QSWAT3 QSWATTopology.py
    
    def __init__(self, is10, is12, is14, isPlus, batFile: str, minHRUha: int):
        """Set crs data."""
        ## command to rerun project
        self.batFile = batFile
        ## minimum HRU size in ha
        self.minHRUha = minHRUha
        ## HUC12_10_6_8 list of projects to rerun
        self.reruns: List[str] = []
        crs4269 = QgsCoordinateReferenceSystem.fromEpsgId(4269)
        ## crs for project EPSG 5072 
        self.crsProject = QgsCoordinateReferenceSystem.fromEpsgId(5072)
        ## transform for projection from 4269 to 5072
        self.transformToProjected = QgsCoordinateTransform(crs4269, self.crsProject, QgsProject.instance())
        ## options for creating shapefiles
        self.options = QgsVectorFileWriter.SaveVectorOptions()
        self.options.ActionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
        self.options.driverName = "ESRI Shapefile"
        self.options.fileEncoding = "UTF-8"
        ## Field name for subbasins and channels
        self.HUC12_10_8_6String = 'HUC12' if is14 else 'HUC10' if is12 else 'HUC8' if is10 else 'HUC6'
        self.HUC14_12_10_8String = 'HUC14' if is14 else 'HUC12' if is12 else 'HUC10' if is10 else 'HUC8'
        self.upperString = 'HUC14' if is12 else 'HUC12' if is10 else 'HUC10'
        self.isPlus = isPlus

    def createWatershedFields(self) -> Tuple[QgsFields, Dict[str, int]]:
        """Return watershed shapefile fields plus indexes dictionary."""
        fields = QgsFields()
        # note if the order of fields is changed the calls of setAttributes will need changing
        fields.append(QgsField('PolygonId', QVariant.Int))
        fields.append(QgsField('Area', QVariant.Double, len=20, prec=2))
        fields.append(QgsField('Subbasin', QVariant.Int))
        fields.append(QgsField(self.HUC12_10_8_6String, QVariant.String, len=20))
        fields.append(QgsField(self.HUC14_12_10_8String, QVariant.String, len=20))
        return fields, {'PolygonId': 0, 'Area': 1, 'Subbasin': 2, 
                        self.HUC12_10_8_6String: 3, self.HUC14_12_10_8String: 4}
        
    def createChannelsFields(self, extended=False, is14_0=False) -> Tuple[QgsFields, Dict[str, int]]:
        """Return channels shapefile fields plus indexes dictionary.
        Extended adds fields to be used by QSWAT3.  is14_0 is used for combinedChannels0, which has HUC14, 12 and 10 fields."""
        fields = QgsFields()
        # note if the number or order of fields is changed the calls of setAttributes will need changing
        fields.append(QgsField('LINKNO', QVariant.Int))
        if self.isPlus:
            fields.append(QgsField('DSLINKNO1', QVariant.Int))
            fields.append(QgsField('PERCENT1', QVariant.Double))
            fields.append(QgsField('DSLINKNO2', QVariant.Int))
            fields.append(QgsField('DSNODEID1', QVariant.Int)) 
            fields.append(QgsField('DSNODEID2', QVariant.Int))
        else:
            fields.append(QgsField('DSLINKNO', QVariant.Int))
            fields.append(QgsField('DSNODEID', QVariant.Int))
        fields.append(QgsField('WSNO', QVariant.Int))
        fields.append(QgsField('streamorde', QVariant.Int))
        fields.append(QgsField('TotDASqKm', QVariant.Double, len=20, prec=2))
        fields.append(QgsField('NAME', QVariant.String, len=254))
        fields.append(QgsField(self.HUC12_10_8_6String,  QVariant.String, len=20))
        fields.append(QgsField(self.HUC14_12_10_8String, QVariant.String, len=20))
        if is14_0:
            fields.append(QgsField('HUC10', QVariant.String, len=20))
        elif extended:
            fields.append(QgsField('Length', QVariant.Double, len=20, prec=2))
            fields.append(QgsField('SourceX', QVariant.Double, len=20, prec=2))
            fields.append(QgsField('SourceY', QVariant.Double, len=20, prec=2))
            fields.append(QgsField('OutletX', QVariant.Double, len=20, prec=2))
            fields.append(QgsField('OutletY', QVariant.Double, len=20, prec=2))
        if self.isPlus:
            return fields, {'LINKNO': 0, 'DSLINKNO1': 1, 'PERCENT1': 2, 'DSLINKNO2': 3, 'DSNODEID1': 4, 'DSNODEID2': 5,
                            'WSNO': 6, 'streamorde': 7, 'TotDASqKm': 8, 'NAME': 9, self.HUC12_10_8_6String: 10,
                            self.HUC14_12_10_8String: 11, 'Length': 12, 'SourceX': 13, 'SourceY': 14, 'OutletX': 15, 'OutletY': 16}
        else:
            return fields, {'LINKNO': 0, 'DSLINKNO': 1, 'DSNODEID': 2,
                            'WSNO': 3, 'streamorde': 4, 'TotDASqKm': 5, 'NAME': 6, self.HUC12_10_8_6String: 7,
                            self.HUC14_12_10_8String: 8, 'Length': 9, 'SourceX': 10, 'SourceY': 11, 'OutletX': 12, 'OutletY': 13}
    
    def createPointsFields(self) -> Tuple[QgsFields, Dict[str, int]]:
        """Return fields for points shapefile plus indexes dictionary."""
        fields = QgsFields()
        fields.append(QgsField('ID', QVariant.Int))
        fields.append(QgsField('INLET', QVariant.Int))
        fields.append(QgsField('RES', QVariant.Int))
        fields.append(QgsField('PTSOURCE', QVariant.Int))
        fields.append(QgsField('Name', QVariant.String, len=100))
        return fields, {'ID': 0, 'INLET': 1, 'RES': 2, 'PTSOURCE': 3, 'Name': 4}
    
    # replaced by fixBasindata
    #===========================================================================
    # def fixHRUsTable(self, projDb: str, conn: Any, SWATId: int, hrusArea: float, problems: Any, batchRerun: str) -> None:
    #     """Correct hrus table in project database connected by conn, 
    #     using hrusArea in hectares as subbasin area for subbasin SWATId."""
    #     sql = 'SELECT ARLU, ARSO, ARSLP, HRU_ID FROM hrus WHERE SUBBASIN = ?'
    #     # map from HRU number to (landuse area, soil area, slope range area in ha)
    #     hruData: Dict[int, Tuple[float, float, float]] = dict()
    #     totalHRUsArea = 0.0
    #     try:
    #         for row in conn.execute(sql, (SWATId,)):
    #             hruArea = float(row[2])
    #             totalHRUsArea += hruArea
    #             hruNum = int(row[3])
    #             hruData[hruNum] = (float(row[0]), float(row[1]), hruArea)
    #     except:
    #         Common.error('Failed to read hrus table in {0}: {1}'.format(projDb, traceback.format_exc()), problems)
    #         return
    #     if totalHRUsArea == 0:
    #         if hrusArea > 0:
    #             Common.error('No HRUs in subbasin {0} of hrus table of {1}.  You will need to run rerun.bat'.format(SWATId, projDb), problems)
    #             projDir, dbFile = os.path.split(projDb)
    #             projName = os.path.splitext(dbFile)[0]
    #             huc12_10_8_6 = projName[3:]
    #             if huc12_10_8_6 not in self.reruns:
    #                 self.reruns.append(huc12_10_8_6)
    #                 mode = 'a' if os.path.isfile(batchRerun) else 'w'
    #                 with open(batchRerun, mode) as rerun:
    #                     projectFile = projDir + '.qgs'
    #                     rerun.write('call "{0}" "{1}" "{2}" "{3}" "{4}" 0\n'.format(self.batFile, projectFile, Files.DataDir, '14', str(self.minHRUha)))
    #         return
    #     factor = hrusArea / totalHRUsArea
    #     sql2 = 'UPDATE hrus SET ARSUB = {0}, ARLU = {1}, ARSO = {2}, ARSLP = {3} WHERE HRU_ID = ?'
    #     for hruId, (arlu, arso, arslp) in hruData.items():
    #         sql3 = sql2.format(hrusArea, arlu * factor, arso * factor, arslp * factor)
    #         try:
    #             conn.execute(sql3, (hruId,))
    #         except:
    #             Common.error('Failed to update hrus table in {0}: {1}'.format(projDb, traceback.format_exc()), problems)
    #             return
    # 
    # def fixWater(self, conn: Any, waterConn: Any, huc12_10_8_6: str, SWATId: int, aream2: float) -> None:
    #     """Correct pnd and res tables according to data in WaterBodies.sqlite."""
    #     sqlRes = 'SELECT IYRES, RES_ESA, RES_EVOL, RES_PSA, RES_PVOL, RES_VOL FROM reservoirs WHERE HUC12_10_8_6=? AND SUBBASIN=?'
    #     sqlPnd = 'SELECT PND_ESA, PND_EVOL, PND_PSA, PND_PVOL, PND_VOL, PND_DRAIN FROM ponds WHERE HUC12_10_8_6=? AND SUBBASIN=?'
    #     sqlRes2 = 'INSERT INTO res (OID, SUBBASIN, IYRES, RES_ESA, RES_EVOL, RES_PSA, RES_PVOL, RES_VOL) VALUES (?,?,?,?,?,?,?,?)'
    #     sqlPnd2 = 'INSERT INTO pnd (OID, SUBBASIN, PND_FR, PND_ESA, PND_EVOL, PND_PSA, PND_PVOL, PND_VOL) VALUES (?,?,?,?,?,?,?,?)'
    #     oid = 0
    #     for row in waterConn.execute(sqlRes, (huc12_10_8_6, SWATId)):
    #         # wipe old data
    #         conn.execute('DELETE FROM res')
    #         oid += 1
    #         conn.execute(sqlRes2, (oid, SWATId) + tuple(row))
    #     oid = 0
    #     for row in waterConn.execute(sqlPnd, (huc12_10_8_6, SWATId)):
    #         conn.execute('DELETE FROM pnd')
    #         oid += 1
    #         conn.execute(sqlPnd2, (oid, SWATId, min(0.95, row[5] * 1E6 / aream2)) + tuple(row)[:5])
    #===========================================================================
    
    @staticmethod
    def error(msg: str, problems: Any=None) -> None:
        """Report msg as an error."""
        print(msg)
        if problems:
            problems.write(msg + '\n')
        
    @staticmethod
    def information(msg: str) -> None:
        """Report msg."""
        print(msg)
        
    @staticmethod    
    def firstPoint(geom: QgsGeometry) -> QgsPointXY:
        """Return first point of polyline or multipolyine.
        
        Really assumes line is not multipart, as seach for first point should be more thorough,
        by seeking for joins between parts.
        So intended to be used on initial flowlines, not after merges."""
        if geom.isMultipart():
            mpl = geom.asMultiPolyline()
            return mpl[0][0]
        else:
            pl = geom.asPolyline()
            return pl[0]
        
    @staticmethod    
    def lastPoint(geom: QgsGeometry) -> QgsPointXY:
        """Return last point of polyline or multipolyine.
        
        Really assumes line is not multipart, as seach for last point should be more thorough,
        by seeking for joins between parts.
        So intended to be used on initial flowlines, not after merges."""
        if geom.isMultipart():
            mpl = geom.asMultiPolyline()
            return mpl[-1][-1]
        else:
            pl = geom.asPolyline()
            return pl[-1]
        
    @staticmethod
    def samePoint(pt1: QgsPointXY, pt2: QgsPointXY):
        """Regard two points as the same if their separation is less than a third of a DEM pixel (assumed to be 30mx30m)
        
        Does not guarantee they are in different DEM pixels, but makes it very likely."""
        return abs(pt1.x() - pt2.x()) < 10 and abs(pt1.y() - pt2.y()) < 10
        
    @staticmethod
    def furthest(candidates: Dict[int, QgsPointXY], start: QgsPointXY) -> Tuple[int, Optional[QgsPointXY]]:
        """Find candidate furthest from start."""
        measure = 0.0
        startX = start.x()
        startY = start.y()
        result: Tuple[int, Optional[QgsPointXY]] = (0, None)
        for channel, candidate in candidates.items():
            dx = candidate.x() - startX
            dy = candidate.y() - startY
            nextMeasure = dx * dx + dy * dy
            if nextMeasure > measure:
                result = channel, candidate
                measure = nextMeasure
        return result
              
    @staticmethod          
    def addRaster(region: str, base: str, isPlus: bool, isCDL: bool, projDir: str):
        """Clip raster map and add to project.  Base is DEM, soil or landuse."""
        if base == 'DEM':
            raster = posixpath.join(Files.demDir, 'DEM_30m_{0}.tif'.format(region))
            if isPlus:
                outRaster = projDir + '/Watershed/Rasters/DEM/dem.tif'
            else:
                outRaster = projDir + '/Source/dem.tif'
        elif base == 'soil':
            raster = posixpath.join(Files.soilDir, 'Soils_30m_{0}.tif'.format(region))
            if isPlus:
                outRaster = projDir + '/Watershed/Rasters/Soil/soil.tif'
            else:
                outRaster = projDir + '/Source/soil/soil.tif'
        else:
            if isCDL:
                fileName = 'NLCD_NWI_Fields_CDL_30m_{0}.tif'.format(region)
            else:
                fileName = 'NLCD_NWI_Fields_30m_{0}.tif'.format(region)
            reg = region[1:] if region.startswith('0') else region
            raster = posixpath.join(posixpath.join(Files.landuseDir, 'Region{0}'.format(reg)), fileName)
            if isPlus:
                outRaster = projDir + '/Watershed/Rasters/Landuse/landuse.tif'
            else:
                outRaster = projDir + '/Source/crop/landuse.tif'
        if isPlus:
            subbasinsFile = projDir + '/Watershed/Shapes/subbasins.shp'
        else:
            subbasinsFile = projDir + '/Watershed/Shapes/' + 'demwshed.shp'
        if not os.path.isfile(subbasinsFile):
            Common.error('Cannot find subbasins file {0}, so cannot create clipped raster {1}'.format(subbasinsFile, outRaster))
            return
        subbasinsLayer = QgsVectorLayer(subbasinsFile, 'subbasins', 'ogr')
        extent = subbasinsLayer.extent()
        #print('Extent: ({0}, {1}), ({2}, {3})'.format(extent.xMinimum(), extent.yMinimum(), extent.xMaximum(), extent.yMaximum()))
        # allow 3 dem pixels (~90m) all round
        yMax = extent.yMaximum() + 100
        yMin = extent.yMinimum() - 100
        xMax = extent.xMaximum() + 100
        xMin = extent.xMinimum() - 100
        command = 'gdal_translate -q -of GTiff -projwin {0} {1} {2} {3} "{4}" "{5}"' \
                        .format(xMin, yMax, xMax, yMin, raster, outRaster)
        os.system(command)
        if not os.path.isfile(outRaster):
            Common.error('Failed to create {0}'.format(outRaster))
        
    @staticmethod
    def polyCombine(geom1: QgsGeometry, geom2: QgsGeometry) -> QgsGeometry:
        """Combines two polygon or multipolygon geometries by simply appending one list to the other.
        
        Not ideal, as polygons may abut and this leaves a line between them, 
        but usful when QgsGeometry.combine fails.
        Assumes both geomtries are polygon or multipolygon type"""
        if geom1.wkbType() == QgsWkbTypes.Polygon:
            list1 = [geom1.asPolygon()]
        else:
            list1 = geom1.asMultiPolygon() 
        if geom2.wkbType() == QgsWkbTypes.Polygon:
            list2 = [geom2.asPolygon()]
        else:
            list2 = geom2.asMultiPolygon()
        return QgsGeometry.fromMultiPolygonXY(list1 + list2)
        
    @staticmethod
    def linkCombine(geom1: QgsGeometry, geom2: QgsGeometry) -> QgsGeometry:
        """Combines two line or multiline geometries by simply appending one list to the other.
        
        Not ideal, as polygons may abut and this leaves a line between them, 
        but usful when QgsGeometry.combine fails.
        Assumes both geomtries are polygon or multipolygon type"""
        if geom1.wkbType() == QgsWkbTypes.Polyline:
            list1 = [geom1.asPolyline()]
        else:
            list1 = geom1.asMultiPolyline() 
        if geom2.wkbType() == QgsWkbTypes.Polyline:
            list2 = [geom2.asPolyline()]
        else:
            list2 = geom2.asMultiPolyline()
        return QgsGeometry.fromMultiPolylineXY(list1 + list2)
                    
    @staticmethod                
    def minMaxMean(areas: Dict[int, float]) -> Tuple[float, float, float]:
        """Return min, max and mean for range values of areas map."""
        mina = float('inf')
        maxa = 0.0
        suma = 0.0
        counta = 0
        for a in areas.values():
            mina = min(mina, a)
            maxa = max(maxa, a)
            suma += a
            counta += 1
        meana = 0 if counta == 0 else suma / counta
        return (mina, maxa, meana)                                   
        
#     @staticmethod
#     def testMerge():   
#         areas = {1597: 8, 1711: 1, 1051: 0.03, 1041: 8, 1057: 7, 1591: 7, 
#                  1037: 10, 1047: 4, 1581: 15, 1707: 0.003, 1601: 10, 1067: 10, 
#                  1709: 0.9, 1087: 4, 1089: 10, 1109: 9, 1105: 4, 1123: 2,
#                  1101: 2.11, 1729: 0.2, 1129: 10, 1141: 10, 1127: 2, 1133: 10}
#         ds =    {1597: 1711, 1711: 1051, 1051: 1047, 1041: 1591, 1057: 1591, 1591: 1047, 
#                  1037: 1707, 1047: 1067, 1581: 1067, 1707: 1709, 1601: 1089, 1067: 1089, 
#                  1709: 1087, 1087: 1109, 1089: 1109, 1109: 1123, 1105: 1123, 1123: 1141,
#                  1101: 1729, 1729: 1127, 1129: 1141, 1141: 1133, 1127: 1133, 1133: 0}
#         HUC14Catchments.mergeBasin(areas, ds)

# def testspatial():
#     f = 'E:/temp.sqlite'
#     if os.path.isfile(f):
#         os.remove(f)
#     with sqlite3.connect(f) as conn:
#         conn.enable_load_extension(True)
#         conn.execute("SELECT load_extension('mod_spatialite')")
#         conn.execute("SELECT InitSpatialMetaData(1)")
#         sql = 'CREATE TABLE points (id INTEGER)'
#         conn.execute(sql)
#         sql = "SELECT AddGeometryColumn('points', 'geometry', 5072, 'POINT', 'XY')"
#         conn.execute(sql)
#         pt = QgsPointXY(1,1)
#         geom = QgsGeometry.fromPointXY(pt).asWkb()
#         sql = 'INSERT INTO points VALUES(?,GeomFromText(?))'
#         conn.execute(sql, (1, geom))

if __name__ == '__main__': 
    # pattern for choosing region (2 to 12 digits)
    selection = ''
    if len(sys.argv) > 1:
        selection = sys.argv[1]
    if selection == '':
        selection = '14'
    print('Selection is {0}'.format(selection))
    # set true to run QSWAT3_64
    is64bit = False
    if len(sys.argv) > 2:
        bits = int(sys.argv[2])
        is64bit = bits == 64
    # minimum HRU size in hectares
    minHRUha = 1
    #selection = '090203050204'
#     time1 = time.process_time()
#     HUC14Catchments.importCatchmentTable()
#     time2 = time.process_time()
#     print('Importing catchments took {0} seconds'.format(int(time2 - time1 + 0.5)))
#     HUC14Catchments.importFromToTable()
#     time3 = time.process_time()
#     print('Importing fromto took {0} seconds'.format(int(time3 - time2 + 0.5)))
#     HUC14Catchments.importAreasTable()
#     time4 = time.process_time()
#     print('Importing areas took {0} seconds'.format(int(time4 - time3 + 0.5)))
#     HUC14Catchments.importDsTable()
#     time5 = time.process_time()
#     HUC14Catchments.createFromToMajorTable()
#     time6 = time.process_time()
#     print('Creating fromtomajor table took {0} seconds'.format(int(time6 - time5 + 0.5)))

    # run this if catchments_patch.csv is changed
    # HUC14Catchments.patchCatchmentsTable()
    # run this if fromto_patch.csv is changed
    # HUC14Catchments.patchFromToTable()

#     testspatial()
    # set True to make shapefiles and project directories.  withProject will fail if this is False and you have not previously run with it True
    withShapes = True
    # set true to make project files, project databases, DEMs, landuse and soil files
    withProject = False
    # set true to run projects.  Needs withProject to be true as well, or run previously
    runProject = False
    # if True use Fields_CDL landuse maps and lookup table, else Fields
    isCDL = True
    # set true for SWAT+, false for SWAT
    isPlus = False
    # set true for HUC8 projects.
    is8 = False
    # set true for HUC10 projects.
    is10 = False
    # set true for HUC12 projects
    is12 = False
    # set true for HUC14 projects
    is14 = True
    home = str(Path.home())
    # QSWAT directory: 
    pluginName = 'QSWAT3_64' if is64bit else 'QSWAT3'
    batName = 'runHUC64.bat' if is64bit else 'runHUC.bat'
    QSWATDirLocal = home + '/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/' + pluginName
    pf = 'C:/Program Files/' if is64bit else 'C:/Program Files (x86)/'
    QSWATDirAll = pf + 'QGIS 3.16/apps/qgis-ltr/python/plugins/' + pluginName
    if not os.path.isdir(QSWATDirLocal):
        if not os.path.isdir(QSWATDirAll):
            print('Cannot find {0} plugin, so cannot run projects'.format(pluginName))
            runProject = False
        else:
            batFile = QSWATDirAll + '/' + batName
    else:
        batFile = QSWATDirLocal + '/' + batName
    for num in range(4):
        run14 = is14 and num == 0
        run12 = is12 and num == 1
        run10 = is10 and num == 2
        run8 = is8 and num == 3
        if run14:
            # set true to make copy of catchment map with comid numbering and before merging
            makeOriginals = False
            huc14Model = HUC14Catchments(isPlus, isCDL, withProject, runProject, makeOriginals, batFile, minHRUha)
            problemsFile = huc14Model.mergeBasins(selection, withShapes)
            #print('Problems file: {0}'.format(problemsFile))
            if runProject: 
                time1 = time.monotonic()
                deletedProjects: List[str] = []
                newInletProjects: List[str] = []
                batchRerun = problemsFile.replace('problems.txt', 'rerun.bat')
                needsRerun = False
                with open(problemsFile, 'a') as problems, open(batchRerun, 'w') as rerun:
                    extDir = '/Fields_CDL' if isCDL else '/Fields'
                    SWATDir = '/SWATPlus' + extDir if isPlus else '/SWAT' + extDir
                    parent = Files.ModelsDir + SWATDir + Files.HUC14ProjectParent + '/' + selection
                    waterStatsFile = os.path.join(parent, 'waterStats{0}_HUC14.csv'.format(selection[:2]))
                    if os.path.isfile(waterStatsFile):
                        os.remove(waterStatsFile)
                    args = [batFile, parent, Files.DataDir, '14', str(minHRUha), '0']
                    #print(args)
                    with subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,  # type:ignore
                                            bufsize=1, text=True) as proc: 
                        exception = False
                        while True:
                            line = proc.stdout.readline()
                            if not line:
                                break
                            record = False
                            if line.startswith('Running project'):
                                path = line.rstrip()[19:]
                                currentHUC12 = os.path.split(path)[1][3:]
                                exception = False
                                record = True
                            elif line.startswith('UNDER95 WARNING'):
                                # need to add inlet point
                                newInletProjects.append(currentHUC12)
                                record = True
                            if record or line.startswith('Completed project') or line.startswith('ERROR') or line.startswith('WARNING'):
                                if line.startswith('ERROR: exception'):
                                    needsRerun = True
                                    projectFile = parent + '/huc{0}.qgs'.format(currentHUC12)
                                    rerun.write('call "{0}" "{1}" "{2}" "{3}" "{4}" 0\n'.format(batFile, projectFile, Files.DataDir, '14', str(minHRUha)))
                                    exception = True
                                problems.write(line)
                                if not (line.startswith('ERROR: SSURGO') or line.startswith('WARNING')):  # leave these messages in problems only
                                    Common.information(line.rstrip())
                            elif line.startswith('EMPTY PROJECT'):
                                deletedProjects.append(currentHUC12)
                                msg = 'Deleting empty project huc{0}'.format(currentHUC12)
                                problems.write(msg + '\n')
                                Common.information(msg)
                            elif exception:
                                problems.write(line)
                                Common.information(line.rstrip())
                            # uncomment next line to see all QSWAT output
                            #print(line.rstrip())
                    for huc12 in newInletProjects:
                        Common.information('Adding inlet to project huc{0}'.format(huc12))
                        huc14Model.addExternalInlet(huc12)
                        extDir = '/Fields_CDL' if isCDL else '/Fields'
                        SWATDir = '/SWATPlus' + extDir if isPlus else '/SWAT' + extDir
                        parent = Files.ModelsDir + SWATDir + Files.HUC14ProjectParent + '/' + selection
                        args = [batFile, 
                                parent + '/huc' + huc12,
                                Files.DataDir,
                                '0'
                                '0',
                                str(Common.basePointId)]
                        with subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,   # type:ignore
                                                bufsize=1, text=True) as proc:
                            while True:
                                line = proc.stdout.readline()
                                if not line:
                                    break
                                if line.startswith('ERROR') or line.startswith('WARNING'):
                                    problems.write(line)
                                    Common.information(line.rstrip())
                                # uncomment next line to see all QSWAT output
                                #print(line.rstrip())
                if needsRerun:
                    print('One or more projects raised exceptions.')
                    print('Please investigate and then run {0} to rerun them.'.format(batchRerun))
                else:
                    os.remove(batchRerun)
                if len(deletedProjects) > 0:
                    huc14Model.deleteProjects(deletedProjects, selection)
                time2 = time.monotonic()
                runTime = time2 - time1
                print('Running HUC14 projects took {0} seconds'.format(int(runTime + 0.5)))
        elif run12 or run10 or run8:
            huc12_10Model = HUC12_10_8Catchments(run10, run12, isPlus, isCDL, withProject, runProject, batFile, minHRUha)
            problemsFile = huc12_10Model.form(selection, withShapes)  # type: ignore
            if runProject and problemsFile is not None:
                deletedProjects = []
                newInletProjects = []
                batchRerun = problemsFile.replace('problems.txt', 'rerun.bat')
                needsRerun = False
                with open(problemsFile, 'a') as problems, open(batchRerun, 'w') as rerun:
                    extDir = '/Fields_CDL' if isCDL else '/Fields'
                    SWATDir = '/SWATPlus' + extDir if isPlus else '/SWAT' + extDir
                    projectParent = Files.HUC12ProjectParent if run12 else Files.HUC10ProjectParent if run10 else Files.HUC8ProjectParent
                    parent = Files.ModelsDir + SWATDir + projectParent + '/' + selection
                    scale = '12' if run12 else '10' if run10 else '8'
                    waterStatsFile = os.path.join(parent, 'waterStats{0}_HUC{1}.csv'.format(selection[:2], scale))
                    if os.path.isfile(waterStatsFile):
                        os.remove(waterStatsFile)
                    args = [batFile, parent, Files.DataDir, scale, str(minHRUha), '0']
                    #print(args)
                    with subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,   # type:ignore
                                            bufsize=1, text=True) as proc:
                        exception = False
                        while True:
                            line = proc.stdout.readline()
                            if not line:
                                break
                            record = False
                            if line.startswith('Running project'):
                                path = line.rstrip()[19:]
                                currentHUC10 = os.path.split(path)[1][3:]
                                exception = False
                                record = True
                            elif line.startswith('UNDER95 WARNING'):
                                # need to add inlet point
                                newInletProjects.append(currentHUC10)
                                record = True
                            if record or line.startswith('Completed project') or line.startswith('ERROR') or line.startswith('WARNING') or line.startswith('Traceback'):
                                if line.startswith('ERROR: exception') or line.startswith('Traceback'):
                                    needsRerun = True
                                    projectFile = parent + '/huc{0}.qgs'.format(currentHUC10)
                                    rerun.write('call "{0}" "{1}" "{2}" "{3}" "{4}" 0\n'.format(batFile, projectFile, Files.DataDir, scale, str(minHRUha)))
                                    exception = True
                                problems.write(line)
                                if not (line.startswith('ERROR: SSURGO') or line.startswith('WARNING')):  # leave these messages in problems only
                                    Common.information(line.rstrip())
                            elif line.startswith('EMPTY PROJECT'):
                                deletedProjects.append(currentHUC10)
                                msg = 'Deleting empty project huc{0}'.format(currentHUC10)
                                problems.write(msg + '\n')
                                Common.information(msg)
                            elif exception:
                                problems.write(line)
                                Common.information(line.rstrip())
                            # uncomment next line to see all QSWAT output
                            #print(line.rstrip())
                    for huc10_8_6 in newInletProjects:
                        Common.information('Adding inlet to project huc{0}'.format(huc10_8_6))
                        huc12_10Model.addExternalInlet(huc10_8_6)
                        args = [batFile, 
                                parent + '/huc' + huc10_8_6,
                                Files.DataDir,
                                '0'
                                '0',
                                str(Common.basePointId)]
                        with subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,   # type:ignore
                                                bufsize=1, text=True) as proc:
                            while True:
                                line = proc.stdout.readline()
                                if not line:
                                    break
                                if line.startswith('ERROR') or line.startswith('WARNING'):
                                    problems.write(line)
                                    Common.information(line.rstrip())
                                # uncomment next line to see all QSWAT output
                                #print(line.rstrip())
                if needsRerun:
                    print('One or more projects raised exceptions.')
                    print('Please investigate and then run {0} to rerun them.'.format(batchRerun))
                else:
                    os.remove(batchRerun)
                if len(deletedProjects) > 0:
                    huc12_10Model.deleteProjects(deletedProjects, selection)
        
    Common.information('Done')
        
