'''
Created on Oct 26, 2021

@author: Chris
'''

"""
Create NHDlakeCentroids.sqlite from MHDLakes.sqlite, both spatialite databases.
Add centroids as lat, lon to lakes table.
"""

from osgeo import ogr, osr
import sqlite3
from qgis.core import QgsGeometry
import os

ogr.UseExceptions()
dataDir = 'I:/Data/5072/'
huc12Db = 'I:/Data/HUC12Watershed.sqlite'
centroidsFile = dataDir + 'NHDLakeCentroids.sqlite'
lakesFile = dataDir + 'NHDLakes.sqlite'
driver = ogr.GetDriverByName('SQLite')
if os.path.isfile(centroidsFile):
    os.remove(centroidsFile)
data_source = driver.CreateDataSource(centroidsFile, options=['SPATIALITE=YES'])
srs = osr.SpatialReference()
srs.ImportFromEPSG(5072)
wgs84 = osr.SpatialReference()
wgs84.ImportFromEPSG(4269)
tx = osr.CoordinateTransformation (srs, wgs84)
layer = data_source.CreateLayer("centroids", srs, geom_type=ogr.wkbPoint)
idField = ogr.FieldDefn('objectid', ogr.OFTInteger)
areaField = ogr.FieldDefn('areaha', ogr.OFTReal)
nameField = ogr.FieldDefn('gnis_name', ogr.OFTString)
layer.CreateField(idField)
layer.CreateField(areaField)
layer.CreateField(nameField)
sql = 'SELECT objectid, AsText(geometry), gnis_name FROM nhdlakes'
sql2 = 'UPDATE lakes SET latitude=?, longitude=? WHERE objectid=?'
with sqlite3.connect(lakesFile) as conn, sqlite3.connect(huc12Db) as conn2:
    conn.enable_load_extension(True)
    conn.execute("SELECT load_extension('mod_spatialite')")
    for row in conn.execute(sql):
        objectid = int(row[0])
        geom = QgsGeometry.fromWkt(row[1])
        point = geom.centroid().asPoint()
        areaha = geom.area() / 1E4
        x = point.x()
        y = point.y()
        lat, lon, _ = tx.TransformPoint(x, y)  # strange inversion on left seems necessary
        conn2.execute(sql2, (lat, lon, objectid))
        wkt = 'POINT({0} {1})'.format(x, y)
        feature = ogr.Feature(layer.GetLayerDefn())
        feature.SetField('objectid', objectid)
        feature.SetField('areaha', areaha)
        feature.SetField('gnis_name', row[2])
        feature.SetGeometry(ogr.CreateGeometryFromWkt(wkt))
        layer.CreateFeature(feature)
    conn2.commit()
data_source = None