'''
Created on Nov 7, 2021

@author: Chris
'''

"""
Report on lakes and reservoirs in region with less than 50% inclusion in HUC12 registered for them.
"""

import os 
import sys
import sqlite3
from qgis.core import QgsExpression, QgsFeatureRequest, QgsVectorLayer, QgsGeometry, QgsApplication

app = QgsApplication([], False)

region = '01'
modelsDir = 'I:/Models/'
dataDir = 'I:/Data/'
lakesFile = dataDir + "NHDLake5072Fixed.shp"

def checkLakes(region):
    """Report on lakes and reservoirs in region with less than 50% inclusion in HUC12 registered for them."""
    wshedFile = modelsDir + 'SWAT/Fields_CDL/HUC12/{0}/combinedWshed.shp'.format(region)
    waterbodiesFile = modelsDir + 'SWAT/Fields_CDL/HUC14/{0}/WaterBodies.sqlite'.format(region)
    if not os.path.isfile(wshedFile):
        print('Cannot find {0}'.format(wshedFile))
        return 
    if not os.path.isfile(waterbodiesFile):
        print('Cannot find {0}'.format(waterbodiesFile))
        return
    wshedLayer = QgsVectorLayer(wshedFile, 'wshed', 'ogr')
    huc12Index = wshedLayer.fields().indexOf('HUC12')
    lakesLayer = QgsVectorLayer(lakesFile, 'lakes', 'ogr')
    for table in ['lakes', 'reservoirs']:
        print('Checking {0}'.format(table)) 
        sql = 'SELECT HUC12_10_8_6, OBJECTID from {0}'.format(table)
        sql2 = 'UPDATE {0} SET  HUC12_10_8_6 = ? WHERE OBJECTID = ?'.format(table)
        changes = dict()
        with sqlite3.connect(waterbodiesFile) as conn:
            for row in conn.execute(sql):
                huc12 = row[0]
                objectid = int(row[1])
                exp1 = QgsExpression('"HUC12" = \'{0}\''.format(huc12))
                wshed = next((feature for feature in wshedLayer.getFeatures(QgsFeatureRequest(exp1))), None)
                if wshed is None:
                    print('Cannot find watershed {0}'.format(huc12))
                    break
                wshedGeom = wshed.geometry()
                exp2 = QgsExpression('"OBJECTID" = {0}'.format(objectid))
                lake = next((lake for lake in lakesLayer.getFeatures(QgsFeatureRequest(exp2))), None)
                if lake is None:
                    print('Cannot find lake {0}'.format(objectid))
                    break
                lakeGeom = lake.geometry()
                threshold = 0.5 * lakeGeom.area()
                intersect = wshedGeom.intersection(lakeGeom)
                if QgsGeometry.isEmpty(intersect):
                    overlap = 0.0
                else:
                    overlap = intersect.area()
                if overlap < threshold:
                    best = (wshed[huc12Index], overlap)
                    print('Lake {0} has poor intersection with {1}'.format(objectid, huc12))
                    suggested = False
                    for wshed2 in wshedLayer.getFeatures():
                        wshed2Geom = wshed2.geometry()
                        intersect2 = wshed2Geom.intersection(lakeGeom)
                        if QgsGeometry.isEmpty(intersect2):
                            overlap2 = 0.0
                        else:
                            overlap2 = intersect2.area()
                        if overlap2 >= threshold:
                            huc12_2 = wshed2[huc12Index]
                            print('suggest {0} at {1:.1F}%'.format(huc12_2, (overlap2 / threshold) * 50))
                            if huc12_2 != huc12:
                                changes[objectid] = huc12_2
                            suggested = True
                            break
                        elif overlap2 > best[1]:
                            best = (huc12_2, overlap2)
                    if suggested:
                        continue
                    print('best is {0} at {1:.1F}%'.format(best[0], (best[1] / threshold) * 50 ))
                    if best[1] != huc12:
                        changes[objectid] = best[0]
            for (objectid, huc12_2) in changes.items():
                conn.execute(sql2, (huc12_2, objectid))
    print('Done checking')     
    
if __name__ == '__main__':
    if len(sys.argv) > 1:
        checkLakes(sys.argv[1])
    else:
        print('No region parameter')
        
    
    
