'''
Created on Oct 31, 2021

@author: Chris
'''
"""
Add lakes to WaterBodies.sqlite files, using lakes table.  
Should be donw by HUC12Models.py but we don't want to rerun it in case subbasins are renumbered.
So use COMIDSRR.sqlite to get HUC14/12/10/8 numbers from catchment comids.
"""

import sqlite3
import os
import sys
import glob 
import shutil

from qgis.core import QgsVectorLayer, QgsExpression, QgsFeatureRequest, QgsApplication, QgsFields, QgsField, QgsWkbTypes, QgsFeature, QgsGeometry  # @UnresolvedImport
from PyQt5.QtCore import QVariant  # @UnresolvedImport

app = QgsApplication([], False)
QgsApplication.setPrefixPath('C:/Program Files/QGIS 3.16/apps/qgis-ltr', True)
QgsApplication.initQgis()

is14 = True 
is12 = True 
is10 = True 
is8 = True 


modelsDir = 'I:/Models/'
#modelsDir = 'D:/oldmodels/'
dataDir = 'I:/Data/'
huc12Db = dataDir + 'HUC12Watershed.sqlite'
lakes5072 = dataDir + 'NHDLake5072Fixed.shp'

def makeSubMap(regionDir, scale):
    """Make map HUC12_10_8 -> local subbasin in cases where subbasin is not same as last two digits of HUC12_10_8."""
    subMap = dict()
    pattern = regionDir + 'huc*'
    for f in glob.iglob(pattern):
        wshedFile = f + '/Watershed/Shapes/demwshed.shp'
        layer = QgsVectorLayer(wshedFile, 'wshed', 'ogr')
        provider = layer.dataProvider()
        fields = provider.fields()
        HUCIndex = fields.indexOf('HUC{0}'.format(scale))
        subIndex = fields.indexOf('Subbasin')
        for feature in provider.getFeatures(QgsFeatureRequest().setFlags(QgsFeatureRequest.NoGeometry)):
            huc = feature[HUCIndex]
            subbasin = int(feature[subIndex])
            if subbasin != int(huc[-2:]):
                subMap[huc] = subbasin
    #print('Submap for {0}: {1}'.format(scale, subMap))
    return subMap
     
def hasObjectid(table, conn):
    """Check if table has OBJECTID column: intended to check for new/old versions of reservoirs."""
    sql = 'PRAGMA table_info({0})'.format(table)
    for row in conn.execute(sql):
        if row[1] == 'OBJECTID':
            return True 
    return False

def hasData(conn, table):
    """Return True if table exists with data."""
    sql = 'SELECT * FROM ' + table
    try:
        row = conn.execute(sql).fetchone()
        return (not row is None)
    except Exception:
        return False
                
def hasTable(conn, table) -> bool:
    """Uses existing connection conn to return true if table exists."""
    try:
        sql = 'SELECT name FROM sqlite_master WHERE type="table" AND name=?'
        row = conn.execute(sql, (table,)).fetchone()
        return (not row is None)
    except Exception:
        return False
    
def samePath(p1: str, p2: str)-> bool:
    """Return true if paths both represent the same file or directory."""
    # guard against source or target path not yet existing
    if not (os.path.exists(p1) and os.path.exists(p2)):
        return False
    return os.path.samefile(p1, p2)
    
def copyShapefile(inFile: str, outBase: str, outDir: str) -> None:
    """Copy files with same basename as infile to outdir, setting basename to outbase."""
    inDir, inName = os.path.split(inFile)
    if samePath(inDir, outDir) and os.path.splitext(inName)[0] == outBase:
        # avoid copying to same file
        return
    pattern: str = os.path.splitext(inFile)[0] + '.*'
    for f in glob.iglob(pattern):
        suffix: str = os.path.splitext(f)[1]
        outfile: str = os.path.join(outDir, outBase + suffix)
        shutil.copy(f, outfile)
    
def polyCombine(geom1: QgsGeometry, geom2: QgsGeometry) -> QgsGeometry:
    """Combines two polygon or multipolygon geometries by simply appending one list to the other.
    
    Not ideal, as polygons may abut and this leaves a line between them, 
    but useful when QgsGeometry.combine fails.
    Assumes both geomtries are polygon or multipolygon type"""
    if geom1.isEmpty():
        return geom2
    if geom2.isEmpty():
        return geom1
    if geom1.wkbType() == QgsWkbTypes.Polygon:
        list1 = [geom1.asPolygon()]
    else:
        list1 = geom1.asMultiPolygon() 
    if geom2.wkbType() == QgsWkbTypes.Polygon:
        list2 = [geom2.asPolygon()]
    else:
        list2 = geom2.asMultiPolygon()
    return QgsGeometry.fromMultiPolygonXY(list1 + list2)
    
    
def error(msg, problems):
    """Write error to problems and stdout."""
    print(msg)
    problems.write(msg + '\n')
    
def fixWaterbodies(waterConn, regionDir, problems):
    """Update WaterBodies.sqlite for lakes and reservoirs with less than 50% inclusion in HUC12 originally 
    listed for them, finding the best alternative.  Only used with scale 14."""
    
    def mergeWatershed(huc12layer):
        """Merge combinedWshedLayer HUC14 polygons into HUC12 polygons"""
        combinedWshedFile = regionDir + 'combinedWshed.shp'
        combinedWshedLayer = QgsVectorLayer(combinedWshedFile, 'wshed', 'ogr')
        HUC14Provider = combinedWshedLayer.dataProvider()
        HUC14Fields = HUC14Provider.fields()
        huc12Index = HUC14Fields.indexFromName('HUC12')
        huc12Provider = huc12Layer.dataProvider()
        fields = QgsFields()
        fields.append(QgsField('HUC12', QVariant.String, len=20))
        fields.append(QgsField('Area', QVariant.Double))
        huc12Provider.addAttributes(fields.toList())
        huc12layer.updateFields()
        polys = dict()
        for feature in HUC14Provider.getFeatures():
            huc12 = feature[huc12Index]
            if huc12 in polys:
                polys[huc12].append(feature)
            else:
                polys[huc12] = [feature]
        # merge polygons and write to huc12 layer
        areaIndex = HUC14Fields.indexFromName('Area')
        for huc12, features in polys.items():
            poly = QgsGeometry.unaryUnion([f.geometry().makeValid() for f in features])
            if poly.isEmpty():
                # just collect the polygons into a multipolygon
                f0 = features.pop()
                poly = f0.geometry()
                area = f0[areaIndex]
                for feature in features:
                    nexArea = feature[areaIndex]
                    nextGeom = feature.geometry().makeValid()
                    merge = polyCombine(poly, nextGeom)
                    poly = merge
                    area += nexArea
            else:
                area = poly.area()
            huc12Feature = QgsFeature(fields)
            huc12Feature.setAttributes([huc12, area])
            huc12Feature.setGeometry(poly)
            huc12Provider.addFeatures([huc12Feature])
    
    
    huc12Layer = QgsVectorLayer('Polygon?crs=epsg:5072', 'huc12_layer', 'memory')
    mergeWatershed(huc12Layer)
    huc12Index = huc12Layer.fields().indexOf('HUC12')
    lakesFile = lakes5072
    lakesLayer = QgsVectorLayer(lakesFile, 'lakes', 'ogr')
    for table in ['lakes', 'reservoirs']:
        print('Checking {0}'.format(table)) 
        sql = 'SELECT HUC12_10_8_6, OBJECTID from {0}'.format(table)
        sql2 = 'UPDATE {0} SET  HUC12_10_8_6 = ? WHERE OBJECTID = ?'.format(table)
        changes = dict()
        for row in waterConn.execute(sql):
            huc12 = row[0]
            ids = row[1].split(',')  # lake or reservoir can be composed of more than one
            objectid = int(ids[0])  # first is largest
            exp1 = QgsExpression('"HUC12" = \'{0}\''.format(huc12))
            wshed = next((feature for feature in huc12Layer.getFeatures(QgsFeatureRequest(exp1))), None)
            if wshed is None:
                error('Cannot find huc12 {0}'.format(huc12), problems)
                break
            wshedGeom = wshed.geometry()
            exp2 = QgsExpression('"OBJECTID" = {0}'.format(objectid))
            lake = next((lake for lake in lakesLayer.getFeatures(QgsFeatureRequest(exp2))), None)
            if lake is None:
                error('Cannot find lake {0}'.format(objectid), problems)
                break
            lakeGeom = lake.geometry()
            threshold = 0.5 * lakeGeom.area()
            intersect = wshedGeom.intersection(lakeGeom)
            if QgsGeometry.isEmpty(intersect):
                overlap = 0.0
            else:
                overlap = intersect.area()
            if overlap < threshold:
                best = (wshed[huc12Index], overlap)
                waterbody = 'Lake' if table == 'lakes' else 'Reservoir'
                problems.write('{0} {1} has poor intersection with {2}: '.format(waterbody, objectid, huc12))
                suggested = False
                for wshed2 in huc12Layer.getFeatures():
                    wshed2Geom = wshed2.geometry()
                    intersect2 = wshed2Geom.intersection(lakeGeom)
                    if QgsGeometry.isEmpty(intersect2):
                        overlap2 = 0.0
                    else:
                        overlap2 = intersect2.area()
                    huc12_2 = wshed2[huc12Index]
                    if overlap2 >= threshold:
                        problems.write('suggest {0} at {1:.1F}%\n'.format(huc12_2, (overlap2 / threshold) * 50))
                        if huc12_2 != huc12:
                            changes[objectid] = huc12_2
                        suggested = True
                        break
                    elif overlap2 > best[1]:
                        best = (huc12_2, overlap2)
                if suggested:
                    continue
                problems.write('best is {0} at {1:.1F}%\n'.format(best[0], (best[1] / threshold) * 50 ))
                if best[0] != huc12:
                    changes[row[1]] = best[0]
        for (objectid, huc12_2) in changes.items():
            waterConn.execute(sql2, (huc12_2, objectid))
        waterConn.commit()

def mergeWaterBodies(subMap, waterConn, upperWaterDb) -> None:
    """Update water bodies sqlite file.  Used for scales 12, 10, 8 as assumes upper scale file exists."""
    
    def addReservoir(row1, row2):
        """Add row2 reservoir to row1 reservoir."""
        larger = row2 if row2[7] > row1[7] else row1
        smaller = row2 if larger == row1 else row1
        if larger[3] is None:
            name = smaller[3]
        elif smaller[3] is None:
            name = larger[3]
        else:
            name = '{0},{1}'.format(larger[3], smaller[3])
        if row2[4] == 0:
            year = row1[4]
        else:
            if row1[4] == 0:
                year = row2[4]
            else:
                year = min(row1[4], row2[4])
        return (row1[0], row1[1], row1[2], name, year, row1[5] + row2[5], row1[6] + row2[6], row1[7] + row2[7], 
                row1[8] + row2[8], row1[9] + row2[9], max(row1[10], row2[10]),
                larger[11], larger[12], ','.join([larger[13], smaller[13]]))
    
    def addPond(row1, row2):
        """Add row2 pond to row1 pond."""
        larger = row2 if row2[7] > row1[7] else row1
        smaller = row2 if larger == row1 else row1
        if larger[3] is None:
            nid = smaller[3]
        elif smaller[3] is None:
            nid = larger[3]
        else:
            nid = '{0},{1}'.format(larger[3], smaller[3])
        if larger[4] is None:
            name = smaller[4]
        elif smaller[4] is None:
            name = larger[4]
        else:
            name = '{0},{1}'.format(larger[4], smaller[4])
        return (row1[0], row1[1], row1[2], nid, name, row1[5] + row2[5], row1[6] + row2[6], row1[7] + row2[7], 
                row1[8] + row2[8], row1[9] + row2[9], max(row1[10], row2[10]),
                larger[11], larger[12], row1[13] or row2[13], row1[14] or row2[14])
    
    def addLake(row1, row2):
        """Add row2 lake to row1 lake."""
        larger = row2 if row2[4] > row1[4] else row1
        smaller = row2 if larger == row1 else row1
        if larger[3] is None:
            name = smaller[3]
        elif smaller[3] is None:
            name = larger[3]
        else:
            name = '{0},{1}'.format(larger[3], smaller[3])
        area = row1[4] + row2[4]
        return (row1[0], row1[1], row1[2], name, area, larger[5], larger[6], ','.join([larger[7], smaller[7]]))
    
    def addPlaya(row1, row2):
        """Add row2 playa to row1 playa."""
        area = row1[3] + row2[3]
        return (row1[0], row1[1], row1[2], area)
    
    def addWetland(row1, row2):
        """Add row2 wetland to row1 wetland."""
        area = row1[3] + row2[3]
        return (row1[0], row1[1], row1[2], area)
    
    # completely replace all tables as some old tables have too few columns, and we can merge entities from upper level
    waterConn.execute('DROP TABLE IF EXISTS reservoirs')
    waterConn.execute(reservoirsCreate)
    with sqlite3.connect(upperWaterDb) as upperConn:
        reservoirs = dict()
        sqlReservoir = 'SELECT * FROM reservoirs'
        for reservoir in upperConn.execute(sqlReservoir):
            reservoirs.setdefault(reservoir[0], []).append(reservoir)
        sqlReservoir1 = 'INSERT INTO reservoirs VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        for huc12_10_8, reservoirList in reservoirs.items():
            huc10_8_6 = huc12_10_8[:-2]
            subbasin = subMap.get(huc12_10_8, int(huc12_10_8[-2:]))
            reservoir0 = reservoirList.pop(0)
            for reservoir in reservoirList:
                reservoir0 = addReservoir(reservoir0, reservoir)
            waterConn.execute(sqlReservoir1, (huc10_8_6, huc12_10_8, subbasin, reservoir0[3], reservoir0[4], reservoir0[5], 
                                     reservoir0[6], reservoir0[7], reservoir0[8], reservoir0[9], reservoir0[10], 
                                     reservoir0[11], reservoir0[12], reservoir0[13]))
        waterConn.execute('DROP TABLE IF EXISTS ponds')
        waterConn.execute(pondsCreate)
        ponds = dict()
        sqlPond = 'SELECT * FROM ponds'
        for pond in upperConn.execute(sqlPond):
            ponds.setdefault(pond[0], []).append(pond)
        sqlPond1 = 'INSERT INTO ponds VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        for huc12_10_8, pondList in ponds.items():
            huc10_8_6 = huc12_10_8[:-2]
            subbasin = subMap.get(huc12_10_8, int(huc12_10_8[-2:]))
            pond0 = pondList.pop(0)
            for pond in pondList:
                pond0 = addPond(pond0, pond)
            waterConn.execute(sqlPond1, (huc10_8_6, huc12_10_8, subbasin, pond0[3], pond0[4], pond0[5], pond0[6], pond0[7],
                                         pond0[8], pond0[9], pond0[10], pond0[11], pond0[12], pond0[13], pond0[14]))
        waterConn.execute('DROP TABLE IF EXISTS lakes')
        waterConn.execute(lakesCreate)
        lakes = dict()
        sqlLake = 'SELECT * FROM lakes'
        for lake in upperConn.execute(sqlLake):
            lakes.setdefault(lake[0], []).append(lake)
        sqlLake1 = 'INSERT INTO lakes VALUES(?,?,?,?,?,?,?,?)'
        for huc12_10_8, lakeList in lakes.items():
            huc10_8_6 = huc12_10_8[:-2]
            subbasin = subMap.get(huc12_10_8, int(huc12_10_8[-2:]))
            lake0 = lakeList.pop(0)
            for lake in lakeList:
                lake0 = addLake(lake0, lake)
            waterConn.execute(sqlLake1, (huc10_8_6, huc12_10_8, subbasin, lake0[3], lake0[4], lake0[5], lake0[6], lake0[7]))
        waterConn.execute('DROP TABLE IF EXISTS playas')
        waterConn.execute(playasCreate)
        playas = dict()
        sqlPlaya = 'SELECT * FROM playas'
        for playa in upperConn.execute(sqlPlaya):
            playas.setdefault(playa[0], []).append(playa)
        sqlPlaya1 = 'INSERT INTO playas VALUES(?,?,?,?)'
        for huc12_10_8, playaList in playas.items():
            huc10_8_6 = huc12_10_8[:-2]
            subbasin = subMap.get(huc12_10_8, int(huc12_10_8[-2:]))
            playa0 = playaList.pop(0)
            for playa in playaList:
                playa0 = addPlaya(playa0, playa)
            waterConn.execute(sqlPlaya1, (huc10_8_6, huc12_10_8, subbasin, playa0[3]))
        waterConn.execute('DROP TABLE IF EXISTS wetlands')
        waterConn.execute(wetlandsCreate)
        wetlands = dict()
        sqlWetland = 'SELECT * FROM wetlands'
        for wetland in upperConn.execute(sqlWetland):
            wetlands.setdefault(wetland[0], []).append(wetland)
        sqlWetland1 = 'INSERT INTO wetlands VALUES(?,?,?,?)'
        for huc12_10_8, wetlandList in wetlands.items():
            huc10_8_6 = huc12_10_8[:-2]
            subbasin = subMap.get(huc12_10_8, int(huc12_10_8[-2:]))
            wetland0 = wetlandList.pop(0)
            for wetland in wetlandList:
                wetland0 = addWetland(wetland0, wetland)
            waterConn.execute(sqlWetland1, (huc10_8_6, huc12_10_8, subbasin, wetland0[3]))
        waterConn.commit()
        
reservoirsCreate = """CREATE TABLE reservoirs (
    HUC12_10_8_6  TEXT,
    HUC14_12_10_8 TEXT,
    SUBBASIN      INTEGER,
    RES_NAME      TEXT,
    IYRES         INTEGER,
    RES_ESA       REAL,
    RES_EVOL      REAL,
    RES_PSA       REAL,
    RES_PVOL      REAL,
    RES_VOL       REAL,
    RES_DRAIN     REAL,
    RES_LAT       REAL,
    RES_LONG      REAL,
    OBJECTID      TEXT
);"""

pondsCreate = """CREATE TABLE ponds (
    HUC12_10_8_6      TEXT,
    HUC14_12_10_8     TEXT,
    SUBBASIN          INTEGER,
    NIDID             TEXT (512),
    PND_NAME          TEXT,
    PND_ESA           REAL,
    PND_EVOL          REAL,
    PND_PSA           REAL,
    PND_PVOL          REAL,
    PND_VOL           REAL,
    PND_DRAIN         REAL,
    PND_LAT           REAL,
    PND_LONG          REAL,
    PND_VOL_ESTIMATED BOOLEAN,
    PND_SA_ESTIMATED  BOOLEAN
);"""

lakesCreate = """CREATE TABLE IF NOT EXISTS lakes (
    HUC12_10_8_6  TEXT,
    HUC14_12_10_8 TEXT,
    SUBBASIN      INTEGER,
    LAKE_NAME     TEXT,
    LAKE_AREA     REAL,
    LAKE_LAT      REAL,
    LAKE_LONG     REAL,
    OBJECTID      TEXT
);"""

playasCreate = """CREATE TABLE IF NOT EXISTS playas (
    HUC12_10_8_6  TEXT,
    HUC14_12_10_8 TEXT,
    SUBBASIN      INTEGER,
    PLA_AREA      REAL
);"""

wetlandsCreate = """CREATE TABLE IF NOT EXISTS wetlands (
    HUC12_10_8_6  TEXT,
    HUC14_12_10_8 TEXT,
    SUBBASIN      INTEGER,
    WET_AREA      REAL
);"""


sql0 = 'SELECT objectid, areaha, comid, huc12, gnis_name, latitude, longitude FROM lakes WHERE huc12 LIKE ?'
sql1 = 'SELECT huc14, huc12 FROM comids WHERE comid=?'
sql2 = 'INSERT INTO lakes VALUES(?,?,?,?,?,?,?,?)'
def addLakes(region):
    """Add lakes in region.  Also add OBJECTID to reservoirs if necessary.  
    Check lakes and reservoirs for overlap with proposed HUC12."""
    regionStr = f'{region:02}'
    likeStr = regionStr + '%'
    scales = []
    if is14:
        scales.append(14)
    if is12:
        scales.append(12)
    if is10:
        scales.append(10)
    if is8:
        scales.append(8)
    for scale in scales:
        regionDir = modelsDir + 'SWAT/Fields_CDL/HUC{1}/{0}/'.format(regionStr, scale)
        #regionDir = modelsDir + 'Models{0}/SWAT/Fields_CDL/HUC{1}/{0}/'.format(regionStr, scale)
        if not os.path.isdir(regionDir):
            continue
        print('Adding lakes in {0} scale {1}'.format(regionStr, scale))
        waterBodiesDb = regionDir + 'WaterBodies.sqlite'
        comidsDb = regionDir + 'COMIDS{0}.sqlite'.format(regionStr)
        problemsFile = regionDir + 'waterProblems.txt'
        if not os.path.isfile(waterBodiesDb):
            print('Cannot find {0}: skipping region {1}'.format(waterBodiesDb, regionStr))
            return
        #copy to old file if not there already (for safety during testing)
        waterBodiesOld = regionDir + 'WaterBodiesOld.sqlite'
        if not os.path.isfile(waterBodiesOld):
            copyShapefile(waterBodiesDb, 'waterBodiesOld', regionDir)
        with sqlite3.connect(waterBodiesDb) as waterConn, sqlite3.connect(huc12Db) as huc12Conn, open(problemsFile, 'w') as problems:
            if hasTable(waterConn, 'lakes'):
                print('{0} already has lakes table: skipping region {1} scale {2}'.format(waterBodiesDb, regionStr, scale))
                continue
            subMap = makeSubMap(regionDir, scale)
            waterConn.execute(lakesCreate)
            # old projects may lack these
            waterConn.execute(playasCreate)
            waterConn.execute(wetlandsCreate)
            if scale == 14:
                if not os.path.isfile(comidsDb):
                    print('Cannot find {0}: skipping region {1} scale {2}'.format(comidsDb, regionStr, scale))
                    continue
                with sqlite3.connect(comidsDb) as comidsConn:
                    for lake in huc12Conn.execute(sql0, (likeStr,)):
                        objectid = int(lake[0])
                        huc12 = lake[3]
                        comid = int(lake[2])
                        areaha = float(lake[1])
                        name = lake[4]
                        latitude = float(lake[5])
                        longitude = float(lake[6])
                        row1 = comidsConn.execute(sql1, (comid,)).fetchone()
                        if row1 is None:
                            error('Cannot find (probably minor) comid {2} in {3} for lake {1} in {0}: default to subbasin 1'.format(comidsDb, objectid, comid, huc12), problems)
                            huc14 = huc12 + '01'
                            subbasin = 1
                        else: 
                            huc14 = row1[0] 
                            if huc12 != row1[1]:
                                error('Comid {0} is in {1} in lakes and {2} in {3}'.format(comid, huc12, row1[1], comidsDb), problems)
                            # find subbasin
                            subbasin = subMap.get(huc14, int(huc14[-2:]))
                    waterConn.execute(sql2, (huc12, huc14, subbasin, name, areaha, latitude, longitude, str(objectid)))
                if not hasObjectid('reservoirs', waterConn):
                    sql3 = 'ALTER TABLE reservoirs ADD COLUMN OBJECTID TEXT'
                    waterConn.execute(sql3)
                    sql4 = 'SELECT HUC12_10_8_6, RES_NAME FROM reservoirs'
                    sql5 = 'SELECT OBJECTID FROM reservoirs WHERE huc12=? AND res_dam_name LIKE "%{0}%"'
                    sql6 = 'UPDATE reservoirs SET OBJECTID=? WHERE HUC12_10_8_6=? AND RES_NAME=?'
                    for reservoir in waterConn.execute(sql4):
                        res = huc12Conn.execute(sql5.format(reservoir[1]), (reservoir[0],)).fetchone()
                        if res is None:
                            error('Cannot find reservoitr {0} in {1}'.format(reservoir[1], reservoir[0]), problems)
                            objectid = '0'
                        else:
                            objectid = res[0]
                        waterConn.execute(sql6, (objectid, reservoir[0], reservoir[1]))
                fixWaterbodies(waterConn, regionDir, problems)
            else: 
                upperDir = regionDir.replace('HUC{0}'.format(scale), 'HUC{0}'.format(int(scale+2)))
                #print('UpperDir: {0}'.format(upperDir))
                upperWaterDb = upperDir + 'waterBodies.sqlite'
                if not os.path.isfile(upperWaterDb):
                    print('Cannot find {0}: skipping region {1} scale {2}'.format(upperWaterDb, regionStr, scale))
                    return
                mergeWaterBodies(subMap, waterConn, upperWaterDb)
                
if __name__ == '__main__':
    if len(sys.argv) > 1:
        addLakes(int(sys.argv[1]))
    else:
        print('No region parameter')
            