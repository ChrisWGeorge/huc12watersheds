@echo off
SET OSGEO4W_ROOT=C:\Program Files\QGIS 3.16

call "%OSGEO4W_ROOT%\bin\o4w_env.bat"
call "%OSGEO4W_ROOT%\bin\py3_env.bat"

"%OSGEO4W_ROOT%/bin/python3.exe" "%~dp0fixBasinData.py" "%1"
