# -*- coding: utf-8 -*-
'''
Created on February 5, 2021

@author: Chris George
copyright: (C) 2021 by Chris George

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
'''

"""
Remove huc_reservoirs from nid_pond.shp, copy of nid5072.shp
huc_reservoirs is copy of nid_lake_join.
"""

from qgis.core import QgsVectorLayer
from PyQt5.QtCore import QVariant

dir5072 = 'H:/Data/5072'
resShapes = dir5072 + '/huc_reservoirs.shp'
pondShapes = dir5072 + '/nid_ponds.shp'


resLayer = QgsVectorLayer(resShapes, 'reservoirs', 'ogr')
resProvider = resLayer.dataProvider()
fields = resProvider.fields()
nididIndex = fields.indexOf('NIDID')
longitudeIndex = fields.indexOf('Longitude')
latitudeIndex = fields.indexOf('Latitude')
nameIndex = fields.indexOf('Dam_name')
storageIndex = fields.indexOf('Normal_sto')
nidids = []
data = dict()
for feature in resProvider.getFeatures():
    nidid = feature[nididIndex]
    nidids.append(nidid)
    latitude = feature[latitudeIndex]
    longitude = feature[longitudeIndex]
    name = feature[nameIndex]
    if name is None or name == '' or (isinstance(name, QVariant) and name.isNull()):
        lname = ''
    else:
        leng = int(len(name) * 0.8)
        lname = name[:leng]
    storage = feature[storageIndex]
    latInt = int(latitude * 10)
    longInt = int(longitude * 10)
    lakeData = (nidid, latitude, longitude, storage, lname)
    latData = data.setdefault(latInt, dict())
    longData = latData.setdefault(longInt, [])
    longData.append(lakeData)

pondLayer = QgsVectorLayer(pondShapes, 'ponds', 'ogr')
pondProvider = pondLayer.dataProvider()
print('{0} features'.format(pondProvider.featureCount()))
toDelete = []
with open('H:/Data/5072/nearmisses.txt', 'w') as misses:
    for feature in pondProvider.getFeatures():
        nidid = feature[nididIndex]
        if nidid in nidids:
            toDelete.append(feature.id())
        else:
            latitude = feature[latitudeIndex]
            longitude = feature[longitudeIndex]
            name = feature[nameIndex]
            if name is None or (isinstance(name, QVariant) and name.isNull()):
                name = ''
            storage = feature[storageIndex]
            latInt = int(latitude * 10)
            longInt = int(longitude * 10)
            latData = data.get(latInt, dict())
            longData = latData.get(longInt, [])
            for (lnidid, llat, llong, lstorage, lname) in longData:
                near = False
                if latitude == llat and longitude == llong:
                    toDelete.append(feature.id())
                elif abs(llat - latitude) <= 0.02 and abs(llong - longitude) <= 0.02:
                    if name.startswith(lname) or storage == lstorage:
                        toDelete.append(feature.id())
                    else:
                        near = True 
                if not near and abs(llat - latitude) <= 0.04 and abs(llong - longitude) <= 0.04:
                    near = True
                if near:
                    misses.write('({0}, {1:.2F}, {2:.2F}, {3}, {4}) close to ({5}, {6:.2F}, {7:.2F}, {8}, {9})\n'
                                 .format(nidid, latitude, longitude, storage, name, lnidid, llat, llong, lstorage, lname))            
pondProvider.deleteFeatures(toDelete)
    

